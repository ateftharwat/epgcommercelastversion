﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.Payments.EPGPayment
{
    public class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Plugin.Payments.EPGPayment.PayTransaction",
               "Plugins/EPGPayment/PayTransaction/{orderId}/{sourceGUID}",
               new { controller = "EPGPayment", action = "PayTransaction", orderId = UrlParameter.Optional},
               new[] { "Nop.Plugin.Payments.EPGPayment.Controllers" }
          );
            routes.MapRoute("Plugin.Payments.EPGPayment.EPGReturnHandler",
               "Plugins/EPGPayment/EPGReturnHandler/",
               new { controller = "EPGPayment", action = "EPGReturnHandler" },
               new[] { "Nop.Plugin.Payments.EPGPayment.Controllers" }
          );
        }

        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
