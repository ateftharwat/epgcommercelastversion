using Nop.Core.Configuration;

namespace Nop.Plugin.Payments.EPGPayment
{
    public class EPGPaymentSettings : ISettings
    {
        public TransactMode TransactMode { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether to "additional fee" is specified as percentage. true - percentage, false - fixed value.
        /// </summary>
        public bool AdditionalFeePercentage { get; set; }
        /// <summary>
        /// Additional fee
        /// </summary>
        public decimal AdditionalFee { get; set; }
        /// <summary>
        /// Service Post URL
        /// </summary>
        public string ServiceURL { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string APPName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SERVICENAME { get; set; }

        public string AuthtokenURL { get; set; }

        public string sourceID { get; set; }

        public string AuthPass { get; set; }

        public string InquiryURL { get; set; }
    }
}
