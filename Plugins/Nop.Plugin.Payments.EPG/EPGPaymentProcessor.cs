using System;
using System.Collections.Generic;
using System.Web.Routing;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Plugins;
using Nop.Plugin.Payments.EPGPayment.Controllers;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Payments;
using System.Text;
using System.IO;
using System.Net;
using System.Web;
using Nop.Services.Logging;
using Nop.EPGIntegrationService;
using Nop.Services.Security;
using Nop.Core;
using Newtonsoft.Json.Linq;
using Nop.Plugin.Payments.EPGPayment.Models;
using System.Web.Mvc;
using System.Security.Policy;

namespace Nop.Plugin.Payments.EPGPayment
{
    /// <summary>
    /// EPG payment processor
    /// </summary>
    public class EPGPaymentProcessor : BasePlugin, IPaymentMethod
    {
        #region Fields

        private readonly EPGPaymentSettings _epgPaymentSettings;
        private readonly ISettingService _settingService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IOrderService _orderService;
        private readonly IWebHelper _webHelper;
        private readonly IEncryptionService _encryptionService;
        private readonly ILocalizationService _localizationService;
        private readonly HttpContextBase _httpContext;
        private readonly ILogger _logger;

        #endregion

        #region Ctor

        public EPGPaymentProcessor(EPGPaymentSettings epgPaymentSettings,
            ISettingService settingService,
            IOrderTotalCalculationService orderTotalCalculationService,
            HttpContextBase httpContext,
            IOrderService orderService,
            IWebHelper webHelper,
            IEncryptionService encryptionService, 
            ILocalizationService localizationService,
            ILogger logger)
        {
            this._epgPaymentSettings = epgPaymentSettings;
            this._settingService = settingService;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._orderService = orderService;
            this._webHelper = webHelper;
            this._encryptionService = encryptionService;
            this._localizationService = localizationService;
            this._httpContext = httpContext;
            this._logger = logger;
        }

        #endregion

        #region Methods
        
        /// <summary>
        /// Process a payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();
            result.NewPaymentStatus = PaymentStatus.Pending;
            result.AllowStoringCreditCardNumber = false;
            return result;
            
        }

        /// <summary>
        /// Post process payment (used by payment gateways that require redirecting to a third-party URL)
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            postProcessPaymentRequest.Order.PaymentStatus = PaymentStatus.Pending;
            string sourceGUID = AuthnticateToken(_epgPaymentSettings.sourceID, _epgPaymentSettings.AuthPass);
            dynamic data = JObject.Parse(sourceGUID);
            var processPaymentRequest = _httpContext.Session["OrderPaymentInfo"] as ProcessPaymentRequest;
            EPGPaymentGatewayModel epgPaymentGatewayModel = new EPGPaymentGatewayModel();
            _httpContext.Response.Redirect(_webHelper.GetStoreLocation(false) + "Plugins/EPGPayment/PayTransaction/" + postProcessPaymentRequest.Order.Id + "/" + data.sourceGUID);
        }

        public string AuthnticateToken (string sourceID,string authPass)
        {
            Dictionary<string, string> PostData = new Dictionary<string, string>();
            Dictionary<string, string> HeaderData = new Dictionary<string, string>();
            HeaderData["sourceID"] = sourceID;
            HeaderData["authPass"] = authPass;
            string responseContent = IntegrationService.SendRequest(_epgPaymentSettings.AuthtokenURL, "POST", PostData, HeaderData);
            return responseContent;
        }
        /// <summary>
        /// Gets additional handling fee
        /// </summary>
        /// <returns>Additional handling fee</returns>
        public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart)
        {
            var result = this.CalculateAdditionalFee(_orderTotalCalculationService,  cart,
                _epgPaymentSettings.AdditionalFee, _epgPaymentSettings.AdditionalFeePercentage);
            return result;
        }

        /// <summary>
        /// Captures payment
        /// </summary>
        /// <param name="capturePaymentRequest">Capture payment request</param>
        /// <returns>Capture payment result</returns>
        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            var result = new CapturePaymentResult();
            result.AddError("Capture method not supported");
            return result;
        }

        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            var result = new RefundPaymentResult();
            result.AddError("Refund method not supported");
            return result;
        }

        /// <summary>
        /// Voids a payment
        /// </summary>
        /// <param name="voidPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            var result = new VoidPaymentResult();
            result.AddError("Void method not supported");
            return result;
        }

        /// <summary>
        /// Process recurring payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();

            result.AllowStoringCreditCardNumber = false;
            switch (_epgPaymentSettings.TransactMode)
            {
                case TransactMode.Pending:
                    result.NewPaymentStatus = PaymentStatus.Pending;
                    break;
                case TransactMode.AuthorizeAndCapture:
                    result.NewPaymentStatus = PaymentStatus.Paid;
                    break;
                default:
                    {
                        result.AddError("Not supported transaction type");
                        return result;
                    }
            }
            
            return result;
        }

        /// <summary>
        /// Cancels a recurring payment
        /// </summary>
        /// <param name="cancelPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            //always success
            return new CancelRecurringPaymentResult();
        }

        /// <summary>
        /// Gets a value indicating whether customers can complete a payment after order is placed but not completed (for redirection payment methods)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Result</returns>
        public bool CanRePostProcessPayment(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            //it's not a redirection payment method. So we always return false
            return false;
        }

        /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "PaymentEPG";
            routeValues = new RouteValueDictionary() { { "Namespaces", "Nop.Plugin.Payments.EPGPayment.Controllers" }, { "area", null } };
        }

        /// <summary>
        /// Gets a route for payment info
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetPaymentInfoRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "PaymentInfo";
            controllerName = "PaymentEPG";
            routeValues = new RouteValueDictionary() { { "Namespaces", "Nop.Plugin.Payments.EPGPayment.Controllers" }, { "area", null } };
        }

        public Type GetControllerType()
        {
            return typeof(EPGPaymentController);
        }

        public override void Install()
        {
            //settings
            var settings = new EPGPaymentSettings()
            {
                TransactMode = TransactMode.Pending
            };
            _settingService.SaveSetting(settings);

            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.EPGPayment.Fields.AdditionalFee", "Additional fee");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.EPGPayment.Fields.AdditionalFee.Hint", "Enter additional fee to charge your customers.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.EPGPayment.Fields.AdditionalFeePercentage", "Additional fee. Use percentage");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.EPGPayment.Fields.AdditionalFeePercentage.Hint", "Determines whether to apply a percentage additional fee to the order total. If not enabled, a fixed value is used.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.EPGPayment.Fields.TransactMode", "After checkout mark payment as");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.EPGPayment.Fields.TransactMode.Hint", "Specify transaction mode.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.EPGPayment.Fields.ServiceURL", "Service URL");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.EPGPayment.Fields.ServiceURL.Hint", "URL that plugin uses for payment processing.");
            
            

            base.Install();
        }

        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<EPGPaymentSettings>();

            //locales
            this.DeletePluginLocaleResource("Plugins.Payments.EPGPayment.Fields.AdditionalFee");
            this.DeletePluginLocaleResource("Plugins.Payments.EPGPayment.Fields.AdditionalFee.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.EPGPayment.Fields.AdditionalFeePercentage");
            this.DeletePluginLocaleResource("Plugins.Payments.EPGPayment.Fields.AdditionalFeePercentage.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.EPGPayment.Fields.TransactMode");
            this.DeletePluginLocaleResource("Plugins.Payments.EPGPayment.Fields.TransactMode.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.EPGPayment.Fields.ServiceURL");
            this.DeletePluginLocaleResource("Plugins.Payments.EPGPayment.Fields.ServiceURL.Hint");
            
            base.Uninstall();
        }

        public bool GetEPGPaymentDetails(string transactionGuid, out Dictionary<string, string> values, out string response)
        {
            string sourceGUID = AuthnticateToken(_epgPaymentSettings.sourceID, _epgPaymentSettings.AuthPass);
            dynamic data = JObject.Parse(sourceGUID);
            Dictionary<string, string> headerData = new Dictionary<string, string>();
            headerData.Add("sourceID", _epgPaymentSettings.sourceID);
            headerData.Add("sourceGUID", data.sourceGUID.ToString());
            Dictionary<string, string> PostData = new Dictionary<string, string>();
            PostData["txn_ref"] = transactionGuid;
            string responseContent = IntegrationService.SendRequest(_epgPaymentSettings.InquiryURL, "POST", PostData, headerData);
            bool success = false;
            if (responseContent.ToLower().Contains("error"))
            {
                success = false;
            }
            else
            {
                success = true;
            }
            values = new Dictionary<string, string>();
            response = responseContent;
            return success;
        }
        #endregion

        #region Properies

        /// <summary>
        /// Gets a value indicating whether capture is supported
        /// </summary>
        public bool SupportCapture
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether partial refund is supported
        /// </summary>
        public bool SupportPartiallyRefund
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether refund is supported
        /// </summary>
        public bool SupportRefund
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether void is supported
        /// </summary>
        public bool SupportVoid
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a recurring payment type of payment method
        /// </summary>
        public RecurringPaymentType RecurringPaymentType
        {
            get
            {
                return RecurringPaymentType.Manual;
            }
        }

        /// <summary>
        /// Gets a payment method type
        /// </summary>
        public PaymentMethodType PaymentMethodType
        {
            get
            {
                return PaymentMethodType.Redirection;
            }
        }
        
        /// <summary>
        /// Gets a value indicating whether we should display a payment information page for this plugin
        /// </summary>
        public bool SkipPaymentInfo
        {
            get
            {
                return true;
            }
        }

        #endregion
        
    }
}
