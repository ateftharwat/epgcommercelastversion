﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Payments.EPGPayment.Models
{
    public class EPGPaymentGatewayModel : BaseNopModel
    {
        public string sourceID { get; set; }
        public string sourceGUID { get; set; }
        public string APP_NAME { get; set; }
        public string SERVICE_NAME { get; set; }
        public string BILL_TO_FNAME { get; set; }
        public string BILL_TO_LNAME { get; set; }
        public string BILL_TO_EMAIL { get; set; }
        public string BILL_TO_MOBILE { get; set; }
        public string BILL_TO_STREET1 { get; set; }
        public string BILL_TO_STREET2 { get; set; }
        public string BILL_TO_STATE { get; set; }
        public string BILL_TO_COUNTRY { get; set; }
        public string BILL_TO_CITY { get; set; }
        public string BILL_TO_POSTCODE { get; set; }
        public string CURRENCY { get; set; }
        public string TOTAL_AMOUNT { get; set; }
        public string TXN_REF { get; set; }
        public string CHANNEL { get; set; }
        public string RETURN_URL { get; set; }
        public Dictionary<string, string> ItemsValues { get; set; }
        public Dictionary<string, string> ItemsNames { get; set; }
    }
}
