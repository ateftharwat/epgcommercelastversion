﻿using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Payments.EPGPayment.Models
{
    public class ConfigurationModel : BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugins.Payments.EPG.Fields.AdditionalFeePercentage")]
        public bool AdditionalFeePercentage { get; set; }
        public bool AdditionalFeePercentage_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.EPG.Fields.AdditionalFee")]
        public decimal AdditionalFee { get; set; }
        public bool AdditionalFee_OverrideForStore { get; set; }

        public int TransactModeId { get; set; }
        [NopResourceDisplayName("Plugins.Payments.EPG.Fields.TransactMode")]
        public SelectList TransactModeValues { get; set; }
        public bool TransactModeId_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.EPG.Fields.ServiceURL")]
        public string ServiceURL { get; set; }
        /// </summary>
        public string APPName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SERVICENAME { get; set; }

        public string AuthtokenURL { get; set; }

        public string sourceID { get; set; }

        public string AuthPass { get; set; }

        public string InquiryURL { get; set; }
        public bool ServiceURL_OverrideForStore { get; set; }
    }
}