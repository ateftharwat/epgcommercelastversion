﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nop.Core;
using Nop.Plugin.Payments.EPGPayment.Models;
using Nop.Plugin.Payments.EPGPayment.Validators;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Payments;
using Nop.Services.Stores;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Services.Orders;
using Nop.Services.Logging;
using Nop.Core.Domain.Orders;
using System.Globalization;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Nop.Plugin.Payments.EPGPayment.Controllers
{
    public class EPGPaymentController : BasePaymentController
    {
        private readonly IWorkContext _workContext;
        private readonly IStoreService _storeService;
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;
        private readonly IPaymentService _paymentService;
        private readonly IOrderService _orderService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly ILogger _logger;
        private readonly IWebHelper _webHelper;
        private readonly EPGPaymentSettings _epgPaymentSettings;
        public EPGPaymentController (IWorkContext workContext, 
            IPaymentService paymentService,
            IOrderService orderService,
            IOrderProcessingService orderProcessingService,
            ILogger logger, IWebHelper webHelper,
            EPGPaymentSettings epgPaymentSettings,
            IStoreService storeService, 
            ISettingService settingService, 
            ILocalizationService localizationService)
        {
            this._paymentService = paymentService;
            this._orderService = orderService;
            this._orderProcessingService = orderProcessingService;
            this._logger = logger;
            this._webHelper = webHelper;
            this._workContext = workContext;
            this._storeService = storeService;
            this._settingService = settingService;
            this._localizationService = localizationService;
            this._epgPaymentSettings = epgPaymentSettings;
        }

        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var epgPaymentSettings = _settingService.LoadSetting<EPGPaymentSettings>(storeScope);

            var model = new ConfigurationModel();
            model.TransactModeId = Convert.ToInt32(epgPaymentSettings.TransactMode);
            model.AdditionalFee = epgPaymentSettings.AdditionalFee;
            model.AdditionalFeePercentage = epgPaymentSettings.AdditionalFeePercentage;
            model.TransactModeValues = epgPaymentSettings.TransactMode.ToSelectList();
            model.ServiceURL = epgPaymentSettings.ServiceURL;
            model.APPName = epgPaymentSettings.APPName;
            model.SERVICENAME = epgPaymentSettings.SERVICENAME;
            model.AuthtokenURL = epgPaymentSettings.AuthtokenURL;
            model.sourceID = epgPaymentSettings.sourceID;
            model.AuthPass = epgPaymentSettings.AuthPass;
            model.InquiryURL = epgPaymentSettings.InquiryURL;

            model.ActiveStoreScopeConfiguration = storeScope;
            if (storeScope > 0)
            {
                model.TransactModeId_OverrideForStore = _settingService.SettingExists(epgPaymentSettings, x => x.TransactMode, storeScope);
                model.AdditionalFee_OverrideForStore = _settingService.SettingExists(epgPaymentSettings, x => x.AdditionalFee, storeScope);
                model.AdditionalFeePercentage_OverrideForStore = _settingService.SettingExists(epgPaymentSettings, x => x.AdditionalFeePercentage, storeScope);
                model.ServiceURL_OverrideForStore = _settingService.SettingExists(epgPaymentSettings, x => x.ServiceURL, storeScope);


            }

            return View("Nop.Plugin.Payments.EPGPayment.Views.PaymentEPG.Configure", model);
        }

        [HttpPost]
        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure(ConfigurationModel model)
        {
            if (!ModelState.IsValid)
                return Configure();

            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var epgPaymentSettings = _settingService.LoadSetting<EPGPaymentSettings>(storeScope);

            //save settings
            epgPaymentSettings.TransactMode = (TransactMode)model.TransactModeId;
            epgPaymentSettings.AdditionalFee = model.AdditionalFee;
            epgPaymentSettings.AdditionalFeePercentage = model.AdditionalFeePercentage;
            epgPaymentSettings.ServiceURL = model.ServiceURL;
            epgPaymentSettings.APPName = model.APPName;
            epgPaymentSettings.SERVICENAME = model.SERVICENAME;
            epgPaymentSettings.AuthtokenURL = model.AuthtokenURL;
            epgPaymentSettings.sourceID = model.sourceID;
            epgPaymentSettings.AuthPass = model.AuthPass;
            epgPaymentSettings.InquiryURL = model.InquiryURL;
            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            _settingService.SaveSetting(epgPaymentSettings);
            if (model.TransactModeId_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(epgPaymentSettings, x => x.TransactMode, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(epgPaymentSettings, x => x.TransactMode, storeScope);

            if (model.AdditionalFee_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(epgPaymentSettings, x => x.AdditionalFee, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(epgPaymentSettings, x => x.AdditionalFee, storeScope);

            if (model.AdditionalFeePercentage_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(epgPaymentSettings, x => x.AdditionalFeePercentage, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(epgPaymentSettings, x => x.AdditionalFeePercentage, storeScope);

            if (model.ServiceURL_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(epgPaymentSettings, x => x.ServiceURL, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(epgPaymentSettings, x => x.ServiceURL, storeScope);

            //now clear settings cache
            _settingService.ClearCache();

            return Configure();
        }

        [ChildActionOnly]
        public ActionResult PaymentInfo()
        {
            var model = new PaymentInfoModel();
            
            //CC types
            model.CreditCardTypes.Add(new SelectListItem()
            {
                Text = "Visa",
                Value = "Visa",
            });
            model.CreditCardTypes.Add(new SelectListItem()
            {
                Text = "Master card",
                Value = "MasterCard",
            });
            model.CreditCardTypes.Add(new SelectListItem()
            {
                Text = "Discover",
                Value = "Discover",
            });
            model.CreditCardTypes.Add(new SelectListItem()
            {
                Text = "Amex",
                Value = "Amex",
            });
            
            //years
            for (int i = 0; i < 15; i++)
            {
                string year = Convert.ToString(DateTime.Now.Year + i);
                model.ExpireYears.Add(new SelectListItem()
                {
                    Text = year,
                    Value = year,
                });
            }

            //months
            for (int i = 1; i <= 12; i++)
            {
                string text = (i < 10) ? "0" + i.ToString() : i.ToString();
                model.ExpireMonths.Add(new SelectListItem()
                {
                    Text = text,
                    Value = i.ToString(),
                });
            }

            //set postback values
            var form = this.Request.Form;
            model.CardholderName = form["CardholderName"];
            model.CardNumber = form["CardNumber"];
            var selectedCcType = model.CreditCardTypes.FirstOrDefault(x => x.Value.Equals(form["CreditCardType"], StringComparison.InvariantCultureIgnoreCase));
            if (selectedCcType != null)
                selectedCcType.Selected = true;
            var selectedMonth = model.ExpireMonths.FirstOrDefault(x => x.Value.Equals(form["ExpireMonth"], StringComparison.InvariantCultureIgnoreCase));
            if (selectedMonth != null)
                selectedMonth.Selected = true;
            var selectedYear = model.ExpireYears.FirstOrDefault(x => x.Value.Equals(form["ExpireYear"], StringComparison.InvariantCultureIgnoreCase));
            if (selectedYear != null)
                selectedYear.Selected = true;

            return View("Nop.Plugin.Payments.EPG.Views.PaymentEPG.PaymentInfo", model);
        }

        [NonAction]
        public override IList<string> ValidatePaymentForm(FormCollection form)
        {
            var warnings = new List<string>();

            //validate
            var validator = new PaymentInfoValidator(_localizationService);
            var model = new PaymentInfoModel()
            {
                CardholderName = form["CardholderName"],
                CardNumber = form["CardNumber"],
                ExpireMonth = form["ExpireMonth"],
                ExpireYear = form["ExpireYear"]
            };
            var validationResult = validator.Validate(model);
            if (!validationResult.IsValid)
                foreach (var error in validationResult.Errors)
                    warnings.Add(error.ErrorMessage);
            return warnings;
        }

        [NonAction]
        public override ProcessPaymentRequest GetPaymentInfo(FormCollection form)
        {
            var paymentInfo = new ProcessPaymentRequest();
            paymentInfo.CreditCardType = form["CreditCardType"];
            paymentInfo.CreditCardName = form["CardholderName"];
            paymentInfo.CreditCardNumber = form["CardNumber"];
            paymentInfo.CreditCardExpireMonth = int.Parse(form["ExpireMonth"]);
            paymentInfo.CreditCardExpireYear = int.Parse(form["ExpireYear"]);
            paymentInfo.CreditCardCvv2 = form["CardCode"];
            return paymentInfo;
        }

        [ValidateInput(false)]
        public ActionResult EPGReturnHandler(FormCollection form)
        {
            string TXN_GUID = _webHelper.QueryString<string>("TXN_GUID");
            string TXN_REF = _webHelper.QueryString<string>("TXN_REF");
            string STATUS = _webHelper.QueryString<string>("STATUS");
            string AUTH_CODE = _webHelper.QueryString<string>("AUTH_CODE");
            string PG_REF = _webHelper.QueryString<string>("PG_REF");
            Dictionary<string, string> values = new Dictionary<string,string>();
            string response=string.Empty;
            var processor = _paymentService.LoadPaymentMethodBySystemName("Payments.EPGPayment") as EPGPaymentProcessor;
            if (processor.GetEPGPaymentDetails(TXN_REF, out values, out response))
            {
                string odrderId = TXN_REF;
                if (TXN_REF.Contains("-")) 
                {
                    string[] txn_ref_array = TXN_REF.Split("-".ToCharArray());
                    if (txn_ref_array.Length > 0) 
                    {
                        odrderId = txn_ref_array[0];
                    }

                }
                Order order = _orderService.GetOrderById(int.Parse(odrderId));
                if (order != null)
                {
                    //decimal total = decimal.Zero;
                    //try
                    //{
                    //    total = decimal.Parse(values["Amount"], new CultureInfo("en-US"));
                    //}
                    //catch (Exception exc)
                    //{
                    //    _logger.Error("SSO Payment PDT. Error getting Amount", exc);
                    //}

                    //string ReceiptNo = string.Empty;
                    //values.TryGetValue("ReceiptNo", out ReceiptNo);

                    //string UserTransactionInfo = string.Empty;
                    //values.TryGetValue("UserTransactionInfo", out UserTransactionInfo);

                    //string TransactionID = string.Empty;
                    //values.TryGetValue("TransactionID", out TransactionID);

                    //string Status = string.Empty;
                    //values.TryGetValue("Status", out Status);

                    //string error = string.Empty;
                    //values.TryGetValue("Error", out error);

                    var sb = new StringBuilder();
                    sb.AppendLine("Paypal PDT:");
                    sb.AppendLine("total: " + order.OrderTotal);
                    sb.AppendLine("Receipt No: " + PG_REF);
                    sb.AppendLine("Order ID: " + TXN_REF);
                    sb.AppendLine("Payment Tansaction ID: " + AUTH_CODE);
                    sb.AppendLine("Status: " + STATUS);
                    sb.AppendLine("Error: " + TXN_REF);


                    //order note
                    order.OrderNotes.Add(new OrderNote()
                    {
                        Note = sb.ToString(),
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow
                    });
                    _orderService.UpdateOrder(order);

                    //validate order total
                    //if (!Math.Round(total, 2).Equals(Math.Round(order.OrderTotal, 2)))
                    //{
                    //    string errorStr = string.Format("SSO payment Handler. Returned order total {0} doesn't equal order total {1}", total, order.OrderTotal);
                    //    _logger.Error(errorStr);

                    //    return RedirectToAction("Index", "Home", new { area = "" });
                    //}
                    //mark order as paid
                    if (_orderProcessingService.CanMarkOrderAsPaid(order))
                    {
                        order.AuthorizationTransactionId = PG_REF;
                        _orderService.UpdateOrder(order);
                        _orderProcessingService.MarkOrderAsPaid(order);
                        EPGIntegrationService.IntegrationService IntegrationService = new EPGIntegrationService.IntegrationService();
                        try
                        {
                            foreach (var item in order.OrderItems)
                            {
                                if (!string.IsNullOrEmpty(item.Product.Sku))
                                    IntegrationService.AddTransactioninInventory(item.Product.Sku, item.Quantity, order.Id, item.Product.Price.ToString());
                            }
                            IntegrationService.TransactionAudit(order.Id.ToString(), "Purchase", order.OrderTotal.ToString(), JsonConvert.SerializeObject(order));
                        }
                        catch(Exception ex)
                        {
                            LogException(ex);
                        }
                       
                    }
                }

                return RedirectToRoute("CheckoutCompleted", new { orderId = order.Id });
            }
            else
            {
                string orderNumber = string.Empty;
                values.TryGetValue("UserTransactionInfo", out orderNumber);
                Guid orderNumberGuid = Guid.Empty;
                try
                {
                    orderNumberGuid = new Guid(orderNumber);
                }
                catch { }
                Order order = _orderService.GetOrderByGuid(orderNumberGuid);
                if (order != null)
                {
                    //order note
                    order.OrderNotes.Add(new OrderNote()
                    {
                        Note = "SSO Payment Handler failed. " + response,
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow
                    });
                    _orderService.UpdateOrder(order);
                }
                return RedirectToAction("ErrorPayment", "Home", new { area = "", msg = response });
            }
        }

        public ActionResult PayTransaction(string orderId, string sourceGUID)
        {
            var order = _orderService.GetOrderById(int.Parse(orderId));
            var orderShippingExclTax = order.OrderShippingExclTax;
            var paymentMethodAdditionalFeeExclTax = order.PaymentMethodAdditionalFeeExclTax;
            var orderTax = order.OrderTax;
            var ccChargeAmount = orderShippingExclTax + paymentMethodAdditionalFeeExclTax + orderTax;
            var totalAmount = order.OrderSubtotalExclTax + ccChargeAmount;

            EPGPaymentGatewayModel model = new EPGPaymentGatewayModel();
            model.sourceID = _epgPaymentSettings.sourceID;
            model.sourceGUID = sourceGUID;
            model.APP_NAME = _epgPaymentSettings.APPName;
            model.SERVICE_NAME = _epgPaymentSettings.SERVICENAME;
            model.BILL_TO_FNAME = order.BillingAddress.FirstName.ToString();
            model.BILL_TO_LNAME = order.BillingAddress.LastName.ToString();
            model.BILL_TO_EMAIL = order.BillingAddress.Email;
            model.BILL_TO_MOBILE = order.BillingAddress.PhoneNumber;
            model.BILL_TO_STREET1 = order.BillingAddress.Address1;
            model.BILL_TO_STREET2 = order.BillingAddress.Address2;
            model.BILL_TO_STATE = order.BillingAddress.City;
            model.BILL_TO_CITY = order.BillingAddress.City;
            model.BILL_TO_COUNTRY = "AE";//order.BillingAddress.CountryName;
            model.BILL_TO_POSTCODE = order.BillingAddress.ZipPostalCode;
            model.CURRENCY = order.CustomerCurrencyCode;
            model.TOTAL_AMOUNT = totalAmount.ToString("#.##");
            model.TXN_REF = order.Id.ToString() + "-" + DateTime.Now.ToString("yyyyMMddHHmmss");
            model.CHANNEL = "Web";
            int i = 1;
            Dictionary<string, string> itemsProductsValues = new Dictionary<string, string>();
            Dictionary<string, string> itemsProductsNames = new Dictionary<string, string>();
            foreach (var item in order.OrderItems)
            {
                itemsProductsValues.Add("ITEM_VALUE_" + i, item.PriceInclTax.ToString("#.##"));
                itemsProductsNames.Add("ITEM_CATEGORY_"+i,item.Product.Name.ToString());
            }
            model.ItemsValues = itemsProductsValues;
            model.ItemsNames = itemsProductsNames;
            model.RETURN_URL = _webHelper.GetStoreLocation(false) + "Plugins/EPGPayment/EPGReturnHandler/";
            return View("Nop.Plugin.Payments.EPGPayment.Views.PaymentEPG.EPGPaymentGateway", model);
        }
    }
}