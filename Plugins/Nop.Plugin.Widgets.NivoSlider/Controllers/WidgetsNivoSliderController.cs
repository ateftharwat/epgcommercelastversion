﻿using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Plugin.Widgets.NivoSlider.Infrastructure.Cache;
using Nop.Plugin.Widgets.NivoSlider.Models;
using Nop.Services.Configuration;
using Nop.Services.Media;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Widgets.NivoSlider.Controllers
{
    public class WidgetsNivoSliderController : BasePluginController
    {
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IStoreService _storeService;
        private readonly IPictureService _pictureService;
        private readonly ISettingService _settingService;
        private readonly ICacheManager _cacheManager;

        public WidgetsNivoSliderController(IWorkContext workContext,
            IStoreContext storeContext,
            IStoreService storeService, 
            IPictureService pictureService,
            ISettingService settingService,
            ICacheManager cacheManager)
        {
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._storeService = storeService;
            this._pictureService = pictureService;
            this._settingService = settingService;
            this._cacheManager = cacheManager;
        }

        protected string GetPictureUrl(int pictureId)
        {
            string cacheKey = string.Format(ModelCacheEventConsumer.PICTURE_URL_MODEL_KEY, pictureId);
            return _cacheManager.Get(cacheKey, () =>
            {
                var url = _pictureService.GetPictureUrl(pictureId, showDefaultPicture: false);
                //little hack here. nulls aren't cacheable so set it to ""
                if (url == null)
                    url = "";

                return url;
            });
        }

        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var nivoSliderSettings = _settingService.LoadSetting<NivoSliderSettings>(storeScope);
            var model = new ConfigurationModel();
            model.Picture1Id = nivoSliderSettings.Picture1Id;
            model.Text1 = nivoSliderSettings.Text1;
            model.ArabicText1 = nivoSliderSettings.ArabicText1;
            model.Link1 = nivoSliderSettings.Link1;
            model.Picture2Id = nivoSliderSettings.Picture2Id;
            model.Text2 = nivoSliderSettings.Text2;
            model.ArabicText2 = nivoSliderSettings.ArabicText2;
            model.Link2 = nivoSliderSettings.Link2;
            model.Picture3Id = nivoSliderSettings.Picture3Id;
            model.Text3 = nivoSliderSettings.Text3;
            model.ArabicText3 = nivoSliderSettings.ArabicText3;
            model.Link3 = nivoSliderSettings.Link3;
            model.Picture4Id = nivoSliderSettings.Picture4Id;
            model.Text4 = nivoSliderSettings.Text4;
            model.ArabicText4 = nivoSliderSettings.ArabicText4;
            model.Link4 = nivoSliderSettings.Link4;
            model.Picture5Id = nivoSliderSettings.Picture5Id;
            model.Text5 = nivoSliderSettings.Text5;
            model.ArabicText5 = nivoSliderSettings.ArabicText5;
            model.Link5 = nivoSliderSettings.Link5;

            //Left Ad
            model.LeftAdPictureId = nivoSliderSettings.LeftAdPictureId;
            model.LeftAdText = nivoSliderSettings.LeftAdText;
            model.LeftAdArabicText = nivoSliderSettings.LeftAdArabicText;
            model.LeftAdLink = nivoSliderSettings.LeftAdLink;

            //Three Blocks Ads
            model.Block1PictureId = nivoSliderSettings.Block1PictureId;
            model.Block1Text = nivoSliderSettings.Block1Text;
            model.Block1ArabicText = nivoSliderSettings.Block1ArabicText;
            model.Block1Link = nivoSliderSettings.Block1Link;
            model.Block1LinkName = nivoSliderSettings.Block1LinkName;
            model.Block1ArabicLinkName = nivoSliderSettings.Block1ArabicLinkName;
            model.Block2PictureId = nivoSliderSettings.Block2PictureId;
            model.Block2Text = nivoSliderSettings.Block2Text;
            model.Block2ArabicText = nivoSliderSettings.Block2ArabicText;
            model.Block2Link = nivoSliderSettings.Block2Link;
            model.Block2LinkName = nivoSliderSettings.Block2LinkName;
            model.Block2ArabicLinkName = nivoSliderSettings.Block2ArabicLinkName;
            model.Block3PictureId = nivoSliderSettings.Block3PictureId;
            model.Block3Text = nivoSliderSettings.Block3Text;
            model.Block3ArabicText = nivoSliderSettings.Block3ArabicText;
            model.Block3Link = nivoSliderSettings.Block3Link;
            model.Block3LinkName = nivoSliderSettings.Block3LinkName;
            model.Block3ArabicLinkName = nivoSliderSettings.Block3ArabicLinkName;

            model.ActiveStoreScopeConfiguration = storeScope;
            if (storeScope > 0)
            {
                model.Picture1Id_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Picture1Id, storeScope);
                model.Text1_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Text1, storeScope);
                model.ArabicText1_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.ArabicText1, storeScope);
                model.Link1_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Link1, storeScope);
                model.Picture2Id_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Picture2Id, storeScope);
                model.Text2_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Text2, storeScope);
                model.ArabicText2_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.ArabicText2, storeScope);
                model.Link2_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Link2, storeScope);
                model.Picture3Id_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Picture3Id, storeScope);
                model.Text3_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Text3, storeScope);
                model.ArabicText3_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.ArabicText3, storeScope);
                model.Link3_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Link3, storeScope);
                model.Picture4Id_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Picture4Id, storeScope);
                model.Text4_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Text4, storeScope);
                model.ArabicText4_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.ArabicText4, storeScope);
                model.Link4_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Link4, storeScope);
                model.Picture5Id_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Picture5Id, storeScope);
                model.Text5_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Text5, storeScope);
                model.ArabicText5_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.ArabicText5, storeScope);
                model.Link5_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Link5, storeScope);

                //Left Ad
                model.LeftAdPictureId_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.LeftAdPictureId, storeScope);
                model.LeftAdText_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.LeftAdText, storeScope);
                model.LeftAdArabicText_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.LeftAdArabicText, storeScope);
                model.LeftAdLink_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.LeftAdLink, storeScope);

                //Three Blocks Ads
                model.Block1PictureId_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Block1PictureId, storeScope);
                model.Block1Text_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Block1Text, storeScope);
                model.Block1ArabicText_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Block1ArabicText, storeScope);
                model.Block1Link_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Block1Link, storeScope);
                model.Block1LinkName_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Block1LinkName, storeScope);
                model.Block1ArabicLinkName_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Block1ArabicLinkName, storeScope);
                model.Block2PictureId_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Block2PictureId, storeScope);
                model.Block2Text_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Block2Text, storeScope);
                model.Block2ArabicText_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Block2ArabicText, storeScope);
                model.Block2Link_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Block2Link, storeScope);
                model.Block2LinkName_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Block2LinkName, storeScope);
                model.Block2ArabicLinkName_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Block2ArabicLinkName, storeScope);
                model.Block3PictureId_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Block3PictureId, storeScope);
                model.Block3Text_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Block3Text, storeScope);
                model.Block3ArabicText_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Block3ArabicText, storeScope);
                model.Block3Link_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Block3Link, storeScope);
                model.Block3LinkName_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Block3LinkName, storeScope);
                model.Block3ArabicLinkName_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Block3ArabicLinkName, storeScope);
            }

            return View("Nop.Plugin.Widgets.NivoSlider.Views.WidgetsNivoSlider.Configure", model);
        }

        [HttpPost]
        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure(ConfigurationModel model)
        {
            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var nivoSliderSettings = _settingService.LoadSetting<NivoSliderSettings>(storeScope);
            nivoSliderSettings.Picture1Id = model.Picture1Id;
            nivoSliderSettings.Text1 = model.Text1;
            nivoSliderSettings.ArabicText1 = model.ArabicText1;
            nivoSliderSettings.Link1 = model.Link1;
            nivoSliderSettings.Picture2Id = model.Picture2Id;
            nivoSliderSettings.Text2 = model.Text2;
            nivoSliderSettings.ArabicText2 = model.ArabicText2;
            nivoSliderSettings.Link2 = model.Link2;
            nivoSliderSettings.Picture3Id = model.Picture3Id;
            nivoSliderSettings.Text3 = model.Text3;
            nivoSliderSettings.ArabicText3 = model.ArabicText3;
            nivoSliderSettings.Link3 = model.Link3;
            nivoSliderSettings.Picture4Id = model.Picture4Id;
            nivoSliderSettings.Text4 = model.Text4;
            nivoSliderSettings.ArabicText4 = model.ArabicText4;
            nivoSliderSettings.Link4 = model.Link4;
            nivoSliderSettings.Picture5Id = model.Picture5Id;
            nivoSliderSettings.Text5 = model.Text5;
            nivoSliderSettings.ArabicText5 = model.ArabicText5;
            nivoSliderSettings.Link5 = model.Link5;

            //Left Ad
            nivoSliderSettings.LeftAdPictureId = model.LeftAdPictureId;
            nivoSliderSettings.LeftAdText = model.LeftAdText;
            nivoSliderSettings.LeftAdArabicText = model.LeftAdArabicText;
            nivoSliderSettings.LeftAdLink = model.LeftAdLink;

            //Three Blocks Ads
            nivoSliderSettings.Block1PictureId = model.Block1PictureId;
            nivoSliderSettings.Block1Text = model.Block1Text;
            nivoSliderSettings.Block1ArabicText = model.Block1ArabicText;
            nivoSliderSettings.Block1Link = model.Block1Link;
            nivoSliderSettings.Block1LinkName = model.Block1LinkName;
            nivoSliderSettings.Block1ArabicLinkName = model.Block1ArabicLinkName;
            nivoSliderSettings.Block2PictureId = model.Block2PictureId;
            nivoSliderSettings.Block2Text = model.Block2Text;
            nivoSliderSettings.Block2ArabicText = model.Block2ArabicText;
            nivoSliderSettings.Block2Link = model.Block2Link;
            nivoSliderSettings.Block2LinkName = model.Block2LinkName;
            nivoSliderSettings.Block2ArabicLinkName = model.Block2ArabicLinkName;
            nivoSliderSettings.Block3PictureId = model.Block3PictureId;
            nivoSliderSettings.Block3Text = model.Block3Text;
            nivoSliderSettings.Block3ArabicText = model.Block3ArabicText;
            nivoSliderSettings.Block3Link = model.Block3Link;
            nivoSliderSettings.Block3LinkName = model.Block3LinkName;
            nivoSliderSettings.Block3ArabicLinkName = model.Block3ArabicLinkName;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            if (model.Picture1Id_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Picture1Id, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Picture1Id, storeScope);
            
            if (model.Text1_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Text1, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Text1, storeScope);

            if (model.ArabicText1_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.ArabicText1, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.ArabicText1, storeScope);
            
            if (model.Link1_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Link1, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Link1, storeScope);
            
            if (model.Picture2Id_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Picture2Id, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Picture2Id, storeScope);
            
            if (model.Text2_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Text2, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Text2, storeScope);

            if (model.ArabicText2_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.ArabicText2, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.ArabicText2, storeScope);
            
            if (model.Link2_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Link2, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Link2, storeScope);
            
            if (model.Picture3Id_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Picture3Id, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Picture3Id, storeScope);
            
            if (model.Text3_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Text3, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Text3, storeScope);

            if (model.ArabicText3_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.ArabicText3, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.ArabicText3, storeScope);
            
            if (model.Link3_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Link3, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Link3, storeScope);
            
            if (model.Picture4Id_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Picture4Id, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Picture4Id, storeScope);
            
            if (model.Text4_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Text4, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Text4, storeScope);

            if (model.ArabicText4_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.ArabicText4, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.ArabicText4, storeScope);

            if (model.Link4_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Link4, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Link4, storeScope);

            if (model.Picture5Id_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Picture5Id, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Picture5Id, storeScope);

            if (model.Text5_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Text5, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Text5, storeScope);

            if (model.ArabicText5_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.ArabicText5, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.ArabicText5, storeScope);

            if (model.Link5_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Link5, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Link5, storeScope);


            //Left Ad
            if (model.LeftAdPictureId_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.LeftAdPictureId, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.LeftAdPictureId, storeScope);

            if (model.LeftAdText_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.LeftAdText, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.LeftAdText, storeScope);

            if (model.LeftAdArabicText_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.LeftAdArabicText, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.LeftAdArabicText, storeScope);

            if (model.LeftAdLink_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.LeftAdLink, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.LeftAdLink, storeScope);

            //Three Blocks Ads
            if (model.Block1PictureId_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Block1PictureId, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Block1PictureId, storeScope);

            if (model.Block1Text_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Block1Text, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Block1Text, storeScope);

            if (model.Block1ArabicText_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Block1ArabicText, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Block1ArabicText, storeScope);

            if (model.Block1Link_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Block1Link, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Block1Link, storeScope);

            if (model.Block1LinkName_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Block1LinkName, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Block1LinkName, storeScope);

            if (model.Block1ArabicLinkName_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Block1ArabicLinkName, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Block1ArabicLinkName, storeScope);

            if (model.Block2PictureId_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Block2PictureId, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Block2PictureId, storeScope);

            if (model.Block2Text_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Block2Text, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Block2Text, storeScope);

            if (model.Block2ArabicText_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Block2ArabicText, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Block2ArabicText, storeScope);

            if (model.Block2Link_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Block2Link, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Block2Link, storeScope);

            if (model.Block2LinkName_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Block2LinkName, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Block2LinkName, storeScope);

            if (model.Block2ArabicLinkName_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Block2ArabicLinkName, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Block2ArabicLinkName, storeScope);

            if (model.Block3PictureId_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Block3PictureId, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Block3PictureId, storeScope);

            if (model.Block3Text_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Block3Text, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Block3Text, storeScope);

            if (model.Block3ArabicText_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Block3ArabicText, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Block3ArabicText, storeScope);

            if (model.Block3Link_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Block3Link, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Block3Link, storeScope);

            if (model.Block3LinkName_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Block3LinkName, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Block3LinkName, storeScope);

            if (model.Block3ArabicLinkName_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(nivoSliderSettings, x => x.Block3ArabicLinkName, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(nivoSliderSettings, x => x.Block3ArabicLinkName, storeScope);

            
            //now clear settings cache
            _settingService.ClearCache();
            
            return Configure();
        }

        [ChildActionOnly]
        public ActionResult PublicInfo(string widgetZone)
        {
            var nivoSliderSettings = _settingService.LoadSetting<NivoSliderSettings>(_storeContext.CurrentStore.Id);
            //use arabic text in view in case of arabic culture is used
            bool isArabicCultureUsed = (_workContext.WorkingLanguage.UniqueSeoCode == "ar");

            var model = new PublicInfoModel();
            model.Picture1Url = GetPictureUrl(nivoSliderSettings.Picture1Id);
            if (isArabicCultureUsed)
                model.Text1 = nivoSliderSettings.ArabicText1;
            else
                model.Text1 = nivoSliderSettings.Text1;
            model.Link1 = nivoSliderSettings.Link1;

            model.Picture2Url = GetPictureUrl(nivoSliderSettings.Picture2Id);
            if (isArabicCultureUsed)
                model.Text2 = nivoSliderSettings.ArabicText2;
            else
                model.Text2 = nivoSliderSettings.Text2;
            model.Link2 = nivoSliderSettings.Link2;

            model.Picture3Url = GetPictureUrl(nivoSliderSettings.Picture3Id);
            if (isArabicCultureUsed)
                model.Text3 = nivoSliderSettings.ArabicText3;
            else
                model.Text3 = nivoSliderSettings.Text3;
            model.Link3 = nivoSliderSettings.Link3;

            model.Picture4Url = GetPictureUrl(nivoSliderSettings.Picture4Id);
            if (isArabicCultureUsed)
                model.Text4 = nivoSliderSettings.ArabicText4;
            else
                model.Text4 = nivoSliderSettings.Text4;
            model.Link4 = nivoSliderSettings.Link4;

            model.Picture5Url = GetPictureUrl(nivoSliderSettings.Picture5Id);
            if (isArabicCultureUsed)
                model.Text5 = nivoSliderSettings.ArabicText5;
            else
                model.Text5 = nivoSliderSettings.Text5;
            model.Link5 = nivoSliderSettings.Link5;

            if (string.IsNullOrEmpty(model.Picture1Url) && string.IsNullOrEmpty(model.Picture2Url) &&
                string.IsNullOrEmpty(model.Picture3Url) && string.IsNullOrEmpty(model.Picture4Url) &&
                string.IsNullOrEmpty(model.Picture5Url))
                //no pictures uploaded
                return Content("");
            

            return View("Nop.Plugin.Widgets.NivoSlider.Views.WidgetsNivoSlider.PublicInfo", model);
        }
    }
}