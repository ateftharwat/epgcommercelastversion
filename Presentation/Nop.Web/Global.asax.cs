﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.WebPages;
using FluentValidation.Mvc;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain;
using Nop.Core.Domain.Common;
using Nop.Core.Infrastructure;
using Nop.Services.Logging;
using Nop.Services.Tasks;
using Nop.Web.Framework;
using Nop.Web.Framework.EmbeddedViews;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Routes;
using Nop.Web.Framework.Themes;
using StackExchange.Profiling;
using StackExchange.Profiling.MVCHelpers;
using System.ServiceModel.Activation;

namespace Nop.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //do not register HandleErrorAttribute. use classic error handling mode
            //filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("favicon.ico");
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("Services/{resource}.svc/{*pathInfo}");

            //register custom routes (plugins, etc)
            var routePublisher = EngineContext.Current.Resolve<IRoutePublisher>();
            routePublisher.RegisterRoutes(routes);

            routes.MapRoute(
                "categoryService", // Route name
                "Catalog/ShowAllCategories/{language}", // URL with parameters
                new { controller = "Catalog", action = "ShowAllCategories", language = "en" },
                new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
                "productService", // Route name
                "Catalog/GetProductByCategoryId/{language}/{categoryId}/{pageIndex}/{count}", // URL with parameters
                new { controller = "Catalog", action = "GetProductByCategoryId", language = "en", categoryId = 1, pageIndex = UrlParameter.Optional, count = UrlParameter.Optional },
                new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
                "productDetails", // Route name
                "Catalog/GetProductDetails/{language}/{productId}/{currencyCode}", // URL with parameters
                new { controller = "Catalog", action = "GetProductDetails", language = "en", productId = 1, currencyCode = "" },
                new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
                "countryService", // Route name
                "Catalog/GetAllCountries/{language}", // URL with parameters
                new { controller = "Catalog", action = "GetAllCountries", language = "" },
                new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
                "statesService", // Route name
                "Catalog/GetAllStates/{language}", // URL with parameters
                new { controller = "Catalog", action = "GetAllStates", language = "" },
                new[] { "Nop.Web.Controllers" });
            
            routes.MapRoute(
                "currencyService", // Route name
                "Catalog/GetAllCurrencies/{language}", // URL with parameters
                new { controller = "Catalog", action = "GetAllCurrencies", language = "en" },
                new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
              "AddProductsFromInventory", // Route name
              "Catalog/AddProductsFromInventory/", // URL with parameters
              new { controller = "Catalog", action = "AddProductsFromInventory"},
              new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
              "UpdateProductsQauntityFromInventory", // Route name
              "Catalog/UpdateProductsQauntityFromInventory/", // URL with parameters
              new { controller = "Catalog", action = "UpdateProductsQauntityFromInventory" },
              new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
               "getAllAddresses", // Route name
               "Customer/GetCustomerAddresses/{customerId}", // URL with parameters
               new { controller = "Customer", action = "GetCustomerAddresses", customerId = 0 },
               new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
                "loginService", // Route name
                "Customer/LogInService/{email}/{password}", // URL with parameters
                new { controller = "Customer", action = "LogInService", email = "", password = "" },
                new[] { "Nop.Web.Controllers" });
           

            routes.MapRoute(
                "forgetPasswordService", // Route name
                "Customer/ForgetPassword/{email}", // URL with parameters
                new { controller = "Customer", action = "ForgetPassword", email = "" },
                new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
                "registerationService", // Route name
                "Customer/CreateAccount", // URL with parameters
                new { controller = "Customer", action = "CreateAccount" },
                new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
                "uploadPictureService", // Route name
                "Customer/UploadPicture", // URL with parameters
                new { controller = "Customer", action = "UploadPicture" },
                new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
                "uploadBinaryPictureService", // Route name
                "Customer/UploadBinaryPicture", // URL with parameters
                new { controller = "Customer", action = "UploadBinaryPicture" },
                new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
                "addItemToCartTEST", // Route name
                "ShoppingCart/AddItemToCart_TEST", // URL with parameters
                new { controller = "ShoppingCart", action = "AddItemToCart_TEST" },
                new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
                "addItemToCart", // Route name
                "ShoppingCart/AddItemToCart/{cartTypeId}/{language}", // URL with parameters
                new { controller = "ShoppingCart", action = "AddItemToCart", cartTypeId = 3, language = "" },
                new[] { "Nop.Web.Controllers" });
            
            routes.MapRoute(
                "deleteCartItem", // Route name
                "ShoppingCart/DeleteCartItem/{customerId}/{cartTypeId}/{shoppingCartItemId}/{language}", // URL with parameters
                new { controller = "ShoppingCart", action = "DeleteCartItem", customerId = 0, cartTypeId = 3, language = "" },
                new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
                "GetItemsFromShoppingCartService", // Route name
                "ShoppingCart/GetItemsFromShoppingCartByCustomerId/{customerId}/{cartTypeId}/{currencyCode}", // URL with parameters
                new { controller = "ShoppingCart", action = "GetItemsFromShoppingCartByCustomerId", customerId = 0, cartTypeId = 3, currencyCode = "" },
                new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
                "GetOneItemFromShoppingCartService", // Route name
                "ShoppingCart/GetItemFromShoppingCartByItemId/{customerId}/{ItemId}/{currencyCode}", // URL with parameters
                new { controller = "ShoppingCart", action = "GetItemFromShoppingCartByItemId", customerId = 0, currencyCode = "" },
                new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
                "parseCSVFileService", // Route name
                "ShoppingCart/ParseCSVFile", // URL with parameters
                new { controller = "ShoppingCart", action = "ParseCSVFile" },
                new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
                "GetCustomerPictures", // Route name
                "Customer/GetCustomerPictures/{customerId}", // URL with parameters
                new { controller = "Customer", action = "GetCustomerPictures", customerId = 0 },
                new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
                "GetPictureURLBySize", // Route name
                "Customer/GetPictureURLBySize/{pictureId}/{pictureSize}", // URL with parameters
                new { controller = "Customer", action = "GetPictureURLBySize", pictureId = 0, pictureSize = 0 },
                new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
                "SaveOrderItem", // Route name
                "Order/SaveOrderItem", // URL with parameters
                new { controller = "Order", action = "SaveOrderItem" },
                new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
                "GetOrderItemService", // Route name
                "Order/GetOrderItemByItemId/{ItemId}", // URL with parameters
                new { controller = "Order", action = "GetOrderItemByItemId" },
                new[] { "Nop.Web.Controllers" });

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                new[] { "Nop.Web.Controllers" });
        }

        protected void Application_Start()
        {
            //we use our own mobile devices support (".Mobile" is reserved). that's why we disable it.
            var mobileDisplayMode = DisplayModeProvider.Instance.Modes
                .FirstOrDefault(x => x.DisplayModeId == DisplayModeProvider.MobileDisplayModeId);
            if (mobileDisplayMode != null)
                DisplayModeProvider.Instance.Modes.Remove(mobileDisplayMode);

            //initialize engine context
            EngineContext.Initialize(false);

            bool databaseInstalled = DataSettingsHelper.DatabaseIsInstalled();

            //set dependency resolver
            var dependencyResolver = new NopDependencyResolver();
            DependencyResolver.SetResolver(dependencyResolver);

            //model binders
            ModelBinders.Binders.Add(typeof(BaseNopModel), new NopModelBinder());

            if (databaseInstalled)
            {
                //remove all view engines
                ViewEngines.Engines.Clear();
                //except the themeable razor view engine we use
                ViewEngines.Engines.Add(new ThemeableRazorViewEngine());
            }

            //Add some functionality on top of the default ModelMetadataProvider
            ModelMetadataProviders.Current = new NopMetadataProvider();

            //Registering some regular mvc stuff
            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            //StackExchange profiler
            if (CanPerformProfilingAction())
            {
                GlobalFilters.Filters.Add(new ProfilingActionFilter());
            }
            
            //fluent validation
            DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;
            ModelValidatorProviders.Providers.Add(new FluentValidationModelValidatorProvider(new NopValidatorFactory()));

            //register virtual path provider for embedded views
            var embeddedViewResolver = EngineContext.Current.Resolve<IEmbeddedViewResolver>();
            var embeddedProvider = new EmbeddedViewVirtualPathProvider(embeddedViewResolver.GetEmbeddedViews());
            HostingEnvironment.RegisterVirtualPathProvider(embeddedProvider);

            //start scheduled tasks
            if (databaseInstalled)
            {
                TaskManager.Instance.Initialize();
                TaskManager.Instance.Start();
            }

            //remove "X-AspNetMvc-Version" from http header
            MvcHandler.DisableMvcResponseHeader = true;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            //remove the "Server" from http header
            var application = sender as HttpApplication;
            if (this.Response != null)
            {
                this.Response.Headers.Remove("Server");
            }

            //ignore static resources
            var webHelper = EngineContext.Current.Resolve<IWebHelper>();
            if (webHelper.IsStaticResource(this.Request))
                return;

            //keep alive page requested (we ignore it to prevent creating a guest customer records)
            string keepAliveUrl = string.Format("{0}keepalive/index", webHelper.GetStoreLocation());
            if (webHelper.GetThisPageUrl(false).StartsWith(keepAliveUrl, StringComparison.InvariantCultureIgnoreCase))
                return;

            EnsureDatabaseIsInstalled();

            if (CanPerformProfilingAction())
            {
                MiniProfiler.Start();
            }
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {                
            //ignore static resources
            var webHelper = EngineContext.Current.Resolve<IWebHelper>();
            if (webHelper.IsStaticResource(this.Request))
                return;

            if (Response != null)
            {
                if (Response.StatusCode == 403 || Response.StatusCode == 404)
                {
                    if (!webHelper.IsStaticResource(this.Request))
                    {
                        Response.Clear();
                        Server.ClearError();
                        Response.TrySkipIisCustomErrors = true;

                        // Call target Controller and pass the routeData.
                        IController errorController = EngineContext.Current.Resolve<Nop.Web.Controllers.CommonController>();

                        var routeData = new RouteData();
                        routeData.Values.Add("controller", "Common");
                        routeData.Values.Add("action", "PageNotFound");

                        errorController.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
                    }
                }
            }

            if (CanPerformProfilingAction())
            {
                //stop as early as you can, even earlier with MvcMiniProfiler.MiniProfiler.Stop(discardResults: true);
                MiniProfiler.Stop();
            }

            //dispose registered resources
            //we do not register AutofacRequestLifetimeHttpModule as IHttpModule 
            //because it disposes resources before this Application_EndRequest method is called
            //and in this case the code above will throw an exception
            //UPDATE: we cannot do it. For more info see the following forum topic - http://www.nopcommerce.com/boards/t/22456/30-changeset-3db3868edcf2-loaderlock-was-detected.aspx
            //AutofacRequestLifetimeHttpModule.ContextEndRequest(sender, e);
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        { 
            //we don't do it in Application_BeginRequest because a user is not authenticated yet
            SetWorkingCulture();
        }

        protected void Application_Error(Object sender, EventArgs e)
        {
            var exception = Server.GetLastError();

            //log error
            LogException(exception);

            //process 404 HTTP errors
            var httpException = exception as HttpException;
            if (httpException != null && (httpException.GetHttpCode() == 404 || httpException.GetHttpCode() == 403))
            {
                var webHelper = EngineContext.Current.Resolve<IWebHelper>();
                if (!webHelper.IsStaticResource(this.Request))
                {
                    Response.Clear();
                    Server.ClearError();
                    Response.TrySkipIisCustomErrors = true;

                    // Call target Controller and pass the routeData.
                    IController errorController = EngineContext.Current.Resolve<Nop.Web.Controllers.CommonController>();

                    var routeData = new RouteData();
                    routeData.Values.Add("controller", "Common");
                    routeData.Values.Add("action", "PageNotFound");

                    errorController.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
                }
            }
        }
        
        protected void EnsureDatabaseIsInstalled()
        {
            var webHelper = EngineContext.Current.Resolve<IWebHelper>();
            string installUrl = string.Format("{0}install", webHelper.GetStoreLocation());
            if (!webHelper.IsStaticResource(this.Request) &&
                !DataSettingsHelper.DatabaseIsInstalled() &&
                !webHelper.GetThisPageUrl(false).StartsWith(installUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                this.Response.Redirect(installUrl);
            }
        }

        protected void SetWorkingCulture()
        {
            if (!DataSettingsHelper.DatabaseIsInstalled())
                return;

            //ignore static resources
            var webHelper = EngineContext.Current.Resolve<IWebHelper>();
            if (webHelper.IsStaticResource(this.Request))
                return;

            //keep alive page requested (we ignore it to prevent creating a guest customer records)
            string keepAliveUrl = string.Format("{0}keepalive/index", webHelper.GetStoreLocation());
            if (webHelper.GetThisPageUrl(false).StartsWith(keepAliveUrl, StringComparison.InvariantCultureIgnoreCase))
                return;


            if (webHelper.GetThisPageUrl(false).StartsWith(string.Format("{0}admin", webHelper.GetStoreLocation()),
                StringComparison.InvariantCultureIgnoreCase))
            {
                //admin area


                //always set culture to 'en-US'
                //we set culture of admin area to 'en-US' because current implementation of Telerik grid 
                //doesn't work well in other cultures
                //e.g., editing decimal value in russian culture
                CommonHelper.SetTelerikCulture();
            }
            else
            {
                //public store
                var workContext = EngineContext.Current.Resolve<IWorkContext>();
                if (workContext.CurrentCustomer != null && workContext.WorkingLanguage != null)
                {
                    var culture = new CultureInfo(workContext.WorkingLanguage.LanguageCulture);
                    Thread.CurrentThread.CurrentCulture = culture;
                    Thread.CurrentThread.CurrentUICulture = culture;
                }
            }
        }

        protected void LogException(Exception exc)
        {
            if (exc == null)
                return;
            
            if (!DataSettingsHelper.DatabaseIsInstalled())
                return;

            //ignore 404 HTTP errors
            var httpException = exc as HttpException;
            if (httpException != null && httpException.GetHttpCode() == 404 &&
                !EngineContext.Current.Resolve<CommonSettings>().Log404Errors)
                return;

            try
            {
                //log
                var logger = EngineContext.Current.Resolve<ILogger>();
                var workContext = EngineContext.Current.Resolve<IWorkContext>();
                logger.Error(exc.Message, exc, workContext.CurrentCustomer);
            }
            catch (Exception)
            {
                //don't throw new exception if occurs
            }
        }

        protected bool CanPerformProfilingAction()
        {
            //will not run in medium trust
            if (CommonHelper.GetTrustLevel() < AspNetHostingPermissionLevel.High)
                return false;

            if (!DataSettingsHelper.DatabaseIsInstalled())
                return false;

            return EngineContext.Current.Resolve<StoreInformationSettings>().DisplayMiniProfilerInPublicStore;
        }
    }
}