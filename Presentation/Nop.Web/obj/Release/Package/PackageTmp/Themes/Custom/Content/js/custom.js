﻿// responsive menu
function toggleMenu() {
        $(".meganizr").slideToggle('fast');
}

$(window).resize(function () {
    if (window.innerWidth > 940) {
        $(".meganizr").addClass('meganizrShow');
    }
    else { $(".meganizr").removeClass('meganizrShow'); }
});

$(document).ready(function () {
     //collapse sitemap
    $('.siteMapCollapse .mzr-content .plusIcon').toggle(function () {
        $(this).parent('a').parent('.one-col').children('ul').slideDown();
        $(this).addClass('plusIconClose');
    },
    function () {
        $(this).parent('a').parent('.one-col').children('ul').slideUp();
        $(this).removeClass('plusIconClose');
    });

/* add separator for Featured products for each 4 items */
$('.home-page-product-grid > div > .featureProduct').each(function (i, e) {
    if (((i + 1) % 4) == 0)
        $(this).after('<div class="clearFix"></div><div class="featuredProSeparator"></div>');
});
//$('#nivo-slider').nivoSlider({
//    effect: 'fade'
    //});

// mobile menu at the bottom
$('.slider').click(function () {
    $('.navHolder').slideToggle(300);
    $(this).toggleClass('sliderDown');
    var img = $(this).find('img');
    if ($(img).attr('id') == 'bot') {
        $(img).attr('src', 'images/arrow_top.png');
        $(img).attr('id', 'top');
    } else {
        $(img).attr('src', 'images/arrow_bottom.png');
        $(img).attr('id', 'bot');
    }
});

$('.showMore').click(function () {
    //var cur = $(this).prev();
    //$('#nav li ul').each(function () {
    //    if ($(this)[0] != $(cur)[0])
    //        $(this).slideUp(300);
    //});
    $(this).toggleClass('showMoreHover');
    $('.moreLinks').slideToggle(300);
});
//horizontalTabs
$('#horizontalTab').easyResponsiveTabs({
    type: 'default', //Types: default, vertical, accordion           
    width: 'auto', //auto or any width like 600px
    fit: true,   // 100% fit in a container
    closed: 'accordion', // Start closed if in accordion view
    activate: function (event) { // Callback function if tab is switched
        var $tab = $(this);
        var $info = $('#tabInfo');
        var $name = $('span', $info);

        $name.text($tab.text());

        $info.show();
    }
});
$('#verticalTab').easyResponsiveTabs({
    type: 'vertical',
    width: 'auto',
    fit: true
});
/* clone account-navigation from left to center side*/
$(".block-account-navigation").clone(true).attr("id", "menuClone").insertBefore(".account-page");
// end of document ready
});
/* ali checkout datepicker */
function OnChangedrpMonths(ctrl) {
    var selectedMonth = ctrl.value - 1;
    var selectedYear = $('#yearList').val();
    var selectedDay = $('#daysList').val();
    var numberOfDaysPerMonth = daysInMonth(selectedMonth, selectedYear);
    $('#daysList option[value!="0"]').remove();
    for (var i = 1; i <= numberOfDaysPerMonth; i++) {
        $('#daysList').append($("<option></option>").attr("value", i).text(i));
    }
    if (selectedDay <= numberOfDaysPerMonth) {
        $('#daysList').val(selectedDay);
    }
}

function OnChangedrpYear(ctrl) {
    var selectedYear = ctrl.value;
    var selectedMonth = $('#monthList').val() - 1;
    var selectedDay = $('#daysList').val();
    var numberOfDaysPerMonth = daysInMonth(selectedMonth, selectedYear);
    $('#daysList option[value!="0"]').remove();
    for (var i = 1; i <= numberOfDaysPerMonth; i++) {
        $('#daysList').append($("<option></option>").attr("value", i).text(i));
    }
    if (selectedDay <= numberOfDaysPerMonth) {
        $('#daysList').val(selectedDay);
    }
}

function daysInMonth(m, y) { // m is 0 indexed: 0-11
    switch (m) {
        case 1:
            return (y % 4 == 0 && y % 100) || y % 400 == 0 ? 29 : 28;
        case 8: case 3: case 5: case 10:
            return 30;
        default:
            return 31
    }
}
