﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FluentValidation.Attributes;

namespace Nop.Web.Models.FeaturesLists
{
    public partial class FeaturesListItemModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("FeaturesList.Fields.PictureId")]
        [AllowHtml]
        public int PictureId { get; set; }
        public string PictureURL { get; set; }

        [NopResourceDisplayName("FeaturesList.Fields.Comment")]
        [AllowHtml]
        public string Comment { get; set; }

        [NopResourceDisplayName("FeaturesList.Fields.Text")]
        [AllowHtml]
        public string Text { get; set; }

        [NopResourceDisplayName("FeaturesList.Fields.URL")]
        [AllowHtml]
        public string URL { get; set; }

        [NopResourceDisplayName("FeaturesList.Fields.DisplayOrder")]
        [AllowHtml]
        public int DisplayOrder { get; set; }
    }
}