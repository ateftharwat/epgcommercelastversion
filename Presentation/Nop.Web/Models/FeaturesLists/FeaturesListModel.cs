﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FluentValidation.Attributes;

namespace Nop.Web.Models.FeaturesLists
{
    public partial class FeaturesListModel : BaseNopEntityModel
    {
        public FeaturesListModel()
        {
            FeaturesListItems = new List<FeaturesListItemModel>();
        }

        [NopResourceDisplayName("FeaturesList.Fields.Name")]
        [AllowHtml]
        public string Name { get; set; }

        public List<FeaturesListItemModel> FeaturesListItems { get; set; }
    }
}