﻿using Nop.Core.Domain.Common;
using Nop.Web.Models.Common;
using Nop.Web.Models.ShoppingCart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Customer
{
    public class SimpleCustomerModel
    {
        public SimpleCustomerModel()
        {
            addresses = new List<SimpleAddressModel>();
            cartItems = new List<ShoppingCartItemSimpleInfoModel>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public List<SimpleAddressModel> addresses { get; set; }

        public List<ShoppingCartItemSimpleInfoModel> cartItems { get; set; }
    }
}