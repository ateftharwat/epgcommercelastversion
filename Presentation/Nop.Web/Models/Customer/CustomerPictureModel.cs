﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Customer
{
    public partial class CustomerPictureModel
    {
        public int PictureId { get; set; }

        public string PictureURL { get; set; }
    }
}