﻿using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Customer
{
    public partial class CustomerPicturesListModel : BaseNopModel
    {
        public CustomerPicturesListModel()
        {
            Pictures = new List<PictureModel>();
            PicturesIds = new List<int>();
            PagingModel = new CustomerPagingListModel();
        }

        public IList<int> PicturesIds { get; set; }
        public IList<PictureModel> Pictures { get; set; }
        public CustomerNavigationModel NavigationModel { get; set; }
        public CustomerPagingListModel PagingModel { get; set; }
    }
}