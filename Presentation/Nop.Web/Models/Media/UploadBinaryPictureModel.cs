﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Media
{
    public partial class UploadBinaryPictureModel
    {
        public int CustomerId { get; set; }

        public byte[] PictureData { get; set; }

        public string FileName { get; set; }

        public string ImageUrl { get; set; }

        public int PictureId { get; set; }
    }
}