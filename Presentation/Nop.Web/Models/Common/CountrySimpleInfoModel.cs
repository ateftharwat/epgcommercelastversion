﻿using Nop.Web.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Web.Models.Common
{
    public class CountrySimpleInfoModel
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }

    public class StateSimpleInfoModel
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}