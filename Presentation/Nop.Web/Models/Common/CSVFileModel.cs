﻿using Nop.Web.Models.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Common
{
    public partial class CSVFileModel
    {
        public int ProductId { get; set; }

        public string CSVFile { get; set; }
    }

    public partial class ParsedCSVFileModel
    {
        public ParsedCSVFileModel()
        {
            Addresses = new List<SimpleAddressModel>();
        }

        public int ProductId { get; set; }

        public int Quantity { get; set; }

        public decimal TotalPrice { get; set; }

        public IList<SimpleAddressModel> Addresses { get; set; }
    }
}