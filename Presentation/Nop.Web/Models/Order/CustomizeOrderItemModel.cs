﻿using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Order
{
    public partial class CustomizeOrderItemModel
    {
        public int CustomerId { get; set; }

        public int ProductId { get; set; }

        public Product Product { get; set; }

        public int OrderItemId { get; set; }

        public bool IsEditing { get; set; }

        public bool ErrorExist { get; set; }

        public string ErrorMessege { get; set; }
    }
}