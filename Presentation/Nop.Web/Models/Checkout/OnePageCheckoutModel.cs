﻿using Nop.Core.Domain.Orders;
using Nop.Web.Framework.Mvc;

namespace Nop.Web.Models.Checkout
{
    public partial class OnePageCheckoutModel : BaseNopModel
    {
        public bool ShippingRequired { get; set; }
        public bool DisableBillingAddressCheckoutStep { get; set; }
        public int CartTypeId { get; set; }
        public int CustomerId { get; set; }
    }
}