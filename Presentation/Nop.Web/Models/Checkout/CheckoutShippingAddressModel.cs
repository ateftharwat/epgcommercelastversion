﻿using System.Collections.Generic;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Common;
using Nop.Core.Domain.Common;

namespace Nop.Web.Models.Checkout
{
    public partial class CheckoutShippingAddressModel : BaseNopModel
    {
        public CheckoutShippingAddressModel()
        {
            ExistingAddresses = new List<AddressModel>();
            NewAddress = new AddressModel();

            ExistingAddressesPerProduct = new List<List<AddressModel>>();            
            NewAddressPerProduct = new List<AddressModel>();
            NewAddressesPreselected = new List<int>();
            ProhibitedCountryNamesPerProduct = new List<string>();
        }

        //in case of single address
        public IList<AddressModel> ExistingAddresses { get; set; }        

        public AddressModel NewAddress { get; set; }

        public bool NewAddressPreselected { get; set; }

        public string AllProhibitedCountryNames { get; set; }

        //in cas of multi address
        public IList<List<AddressModel>> ExistingAddressesPerProduct { get; set; }

        public IList<AddressModel> NewAddressPerProduct { get; set; }

        public IList<int> NewAddressesPreselected { get; set; }

        public IList<string> ProhibitedCountryNamesPerProduct { get; set; }


        public AddressSettings AddressSettings { get; set; }

        public bool SendToManyAddressSelected { get; set; }

        /// <summary>
        /// value indicating that there is only customized products in the shopping cart
        /// </summary>
        public bool OnlyCustomizedProducts { get; set; }

        /// <summary>
        /// value indicating that there is a customized products in the shopping cart
        /// </summary>
        public bool HasCustomizableProduct { get; set; }

        /// <summary>
        /// value indicating that there is only one product in the shopping cart
        /// </summary>
        public bool IsSingleProduct { get; set; }

        public int CartTypeId { get; set; }

        public int CustomerId { get; set; }
    }
}