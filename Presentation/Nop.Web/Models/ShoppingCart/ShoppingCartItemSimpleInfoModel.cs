﻿using Nop.Core.Domain.Common;
using Nop.Web.Models.Common;
using Nop.Web.Models.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.ShoppingCart
{
    public partial class ShoppingCartItemSimpleInfoModel
    {
        public ShoppingCartItemSimpleInfoModel()
        {
            Addresses = new List<SimpleAddressModel>();
            Pages = new List<PageModel>();
            TemplateImages = new List<string>();
            ProductPictures = new List<string>();
        }
        public int Id { get; set; }

        public int CustomerId { get; set; }

        public int ProductId { get; set; }

        public bool IsEditing { get; set; }

        public List<SimpleAddressModel> Addresses { get; set; }

        public int Quantity { get; set; }

        public bool? SendToManyAddressSelected { get; set; }

        public string ProductName { get; set; }

        public List<string> TemplateImages { get; set; }

        public List<string> ProductPictures { get; set; }

        public int NumberOfPages { get; set; }

        public decimal Width { get; set; }

        public decimal Height { get; set; }

        public bool IsCustomizable { get; set; }

        public decimal TotalPrice { get; set; }

        public List<PageModel> Pages { get; set; }

    }

    public partial class PageModel
    {
        public PageModel()
        {
            PlaceHolders = new List<PlaceHolderModel>();
        }
        public int Id { get; set; }

        public List<PlaceHolderModel> PlaceHolders { get; set; }
    }

    public partial class PlaceHolderModel
    {
        public int Id { get; set; }

        public string Type { get; set; }

        //text attributes
        public string Text { get; set; }

        public string TextFamily { get; set; }

        public string TextStyle { get; set; }

        public string FontColor { get; set; }

        public int TextSize { get; set; }

        //picture transformation attributes
        public int BrightnessValue { get; set; }

        public int ContrastValue { get; set; }

        public int RotationValue { get; set; }

        public int ZoomValue { get; set; }

        public int ScaleX { get; set; }

        public int ScaleY { get; set; }

        public int ImageScaleX { get; set; }

        public int ImageScaleY { get; set; }

        public int ScaleWidth { get; set; }

        public int ScaleHeight { get; set; }

        //picture atributes
        public string ImageURL { get; set; }

        public string FileName { get; set; }

        public int PictureId { get; set; }

        public string PictureData { get; set; }
    }
}