﻿using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.ShoppingCart
{
    public partial class CustomizeProductModel
    {
        public int CustomerId { get; set; }

        public int ProductId { get; set; }

        public Product Product { get; set; }

        public int ShoppingCartItemId { get; set; }

        public bool IsEditing { get; set; }

        public bool IsMobileDevice { get; set; }
    }
}