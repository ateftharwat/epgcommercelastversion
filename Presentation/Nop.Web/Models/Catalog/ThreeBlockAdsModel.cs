﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Catalog
{
    public class ThreeBlockAdsModel
    {
        //Ad Block 1
        public string Block1PictureURL { get; set; }
        public string Block1Text { get; set; }
        public string Block1Link { get; set; }
        public string Block1LinkName { get; set; }

        //Ad Block 2
        public string Block2PictureURL { get; set; }
        public string Block2Text { get; set; }
        public string Block2Link { get; set; }
        public string Block2LinkName { get; set; }

        //Ad Block 3
        public string Block3PictureURL { get; set; }
        public string Block3Text { get; set; }
        public string Block3Link { get; set; }
        public string Block3LinkName { get; set; }
    }
}