﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Catalog
{
    public class TemplateImage
    {
        public TemplateImage()
        { 

        }
        public string TemplateImageURL { get; set; }
        public float DPI { get; set; }        
    }
}