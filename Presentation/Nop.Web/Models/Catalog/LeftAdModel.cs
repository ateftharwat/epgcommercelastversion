﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Catalog
{
    public class LeftAdModel
    {
        public string LeftAdPictureURL { get; set; }

        public string LeftAdText { get; set; }

        public string LeftAdLink { get; set; }
    }
}