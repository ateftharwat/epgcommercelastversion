﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Catalog
{
    public class ProductSimpleModel
    {
        public ProductSimpleModel()
        {
            ImageURL = new List<string>();
        }
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsCustomizable { get; set; }

        public List<string> ImageURL { get; set; }
    }

    public class ProductSimpleDetailsModel
    {
        public ProductSimpleDetailsModel()
        {
            TemplateImages = new List<string>();
            ProductPictures = new List<string>();
            Pages = new List<ProductSimpleDetailsModel.PageModel>();
        }

        public decimal PriceWithoutDiscount { get; set; }

        public decimal PriceWithDiscount { get; set; }

        public decimal InternationalPriceWithoutDiscount { get; set; }

        public decimal InternationalPriceWithDiscount { get; set; }

        public decimal Width { get; set; }

        public decimal Height { get; set; }

        public decimal DPI { get; set; }

        public bool IsCustomizable { get; set; }

        public List<string> TemplateImages { get; set; }        

        public List<string> ProductPictures { get; set; }

        public List<ProductSimpleDetailsModel.PageModel> Pages { get; set; }

        #region NestedClasses

        public class PageModel
        {
            public PageModel()
            {
                PageHolders = new List<PagePlaceHolderModel>();
            }
            public int PageId { get; set; }
            public int PageIndex { get; set; }
            public string PageName { get; set; }
            public List<PagePlaceHolderModel> PageHolders { get; set; }
        }

        public class PagePlaceHolderModel
        {
            public int PlaceHolderId { get; set; }
            public string Type { get; set; }
            public int TextMaximumCharacters { get; set; }
            public decimal XPosition { get; set; }
            public decimal YPosition { get; set; }
            public decimal DPI { get; set; }
            public decimal Width { get; set; }
            public decimal Height { get; set; }
        }
        #endregion
    }
}