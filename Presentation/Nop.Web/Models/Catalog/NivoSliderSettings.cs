﻿using Nop.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Catalog
{
    public class NivoSliderSettings : ISettings
    {
        public int Picture1Id { get; set; }
        public string Text1 { get; set; }
        public string ArabicText1 { get; set; }
        public string Link1 { get; set; }

        public int Picture2Id { get; set; }
        public string Text2 { get; set; }
        public string ArabicText2 { get; set; }
        public string Link2 { get; set; }

        public int Picture3Id { get; set; }
        public string Text3 { get; set; }
        public string ArabicText3 { get; set; }
        public string Link3 { get; set; }

        public int Picture4Id { get; set; }
        public string Text4 { get; set; }
        public string ArabicText4 { get; set; }
        public string Link4 { get; set; }

        public int Picture5Id { get; set; }
        public string Text5 { get; set; }
        public string ArabicText5 { get; set; }
        public string Link5 { get; set; }


        //Left Advertise
        public int LeftAdPictureId { get; set; }
        public string LeftAdText { get; set; }
        public string LeftAdArabicText { get; set; }
        public string LeftAdLink { get; set; }


        //Three Blocks Ads
        public int Block1PictureId { get; set; }
        public string Block1Text { get; set; }
        public string Block1ArabicText { get; set; }
        public string Block1Link { get; set; }
        public string Block1LinkName { get; set; }
        public string Block1ArabicLinkName { get; set; }

        public int Block2PictureId { get; set; }
        public string Block2Text { get; set; }
        public string Block2ArabicText { get; set; }
        public string Block2Link { get; set; }
        public string Block2LinkName { get; set; }
        public string Block2ArabicLinkName { get; set; }

        public int Block3PictureId { get; set; }
        public string Block3Text { get; set; }
        public string Block3ArabicText { get; set; }
        public string Block3Link { get; set; }
        public string Block3LinkName { get; set; }
        public string Block3ArabicLinkName { get; set; }

    }
}