﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Catalog
{
    public class CategoryServiceModel
    {
        public CategoryServiceModel()
        {
            SubCategories = new List<CategoryServiceModel>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string IconURL { get; set; }

        public string ImageURL { get; set; }

        public List<CategoryServiceModel> SubCategories { get; set; }
    }
}