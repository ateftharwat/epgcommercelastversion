﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Nop.Web.Controllers
{
    public class LogoutController : Controller
    {
        // GET: Logout
        public ActionResult Index()
        {
            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://localhost/Nop.Web/Logout");
            ////HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://localhost/Nop.Web/Logout/logout");
            //request.Method = "GET";            
            //var response = (HttpWebResponse)request.GetResponse();

            Session["IsLoggedIn"] = false;

            return View("logout");
        }

        public ActionResult Logout()
        {
            object isloggedin = Session["IsLoggedIn"];
            if (isloggedin != null) 
            {
                if ((bool)isloggedin == false) 
                {
                    DoLogout();
                }
            }
            
            return View("logout");

        }

        public void DoLogout()
        {
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();            
            FormsAuthentication.SignOut();          
        }

        public ActionResult Login()
        {
            Session["IsLoggedIn"] = true;
            return View("logout");

        }
    }
}