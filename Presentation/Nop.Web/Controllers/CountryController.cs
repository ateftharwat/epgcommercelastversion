﻿using System;
using System.Linq;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Web.Infrastructure.Cache;
using System.Collections.Generic;
using Nop.Services.Common;
using Nop.EPGIntegrationService;
using Nop.EPGIntegrationService.Models;

namespace Nop.Web.Controllers
{
    public partial class CountryController : BasePublicController
	{
		#region Fields

        private readonly ICountryService _countryService;
        private readonly IAddressService _addressService; 
        private readonly IStateProvinceService _stateProvinceService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly ICacheManager _cacheManager;

	    #endregion

		#region Constructors

        public CountryController(ICountryService countryService,
            IAddressService addressService,
            IStateProvinceService stateProvinceService, 
            ILocalizationService localizationService, 
            IWorkContext workContext,
            ICacheManager cacheManager)
		{
            this._countryService = countryService;
            this._addressService = addressService;
            this._stateProvinceService = stateProvinceService;
            this._localizationService = localizationService;
            this._workContext = workContext;
            this._cacheManager = cacheManager;
		}

        #endregion

        #region States / provinces

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetStatesByCountryId(string countryId, bool addEmptyStateIfRequired)
        {
            //this action method gets called via an ajax request
            if (String.IsNullOrEmpty(countryId))
                throw new ArgumentNullException("countryId");
            
            string cacheKey = string.Format(ModelCacheEventConsumer.STATEPROVINCES_BY_COUNTRY_MODEL_KEY, countryId, addEmptyStateIfRequired, _workContext.WorkingLanguage.Id);
            var cacheModel = _cacheManager.Get(cacheKey, () =>
            {
                var country = _countryService.GetCountryById(Convert.ToInt32(countryId));
                var states = _stateProvinceService.GetStateProvincesByCountryId(country != null ? country.Id : 0).ToList();
                var result = (from s in states
                              select new { id = s.Id, name = s.GetLocalized(x => x.Name) })
                              .ToList();

                if (addEmptyStateIfRequired && result.Count == 0)
                    result.Insert(0, new { id = 0, name = _localizationService.GetResource("Address.OtherNonUS") });
                return result;

            });
            
            return Json(cacheModel, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetStatesByCountryName(string countryName, bool addEmptyStateIfRequired)
        {
            //this action method gets called via an ajax request
            if (String.IsNullOrEmpty(countryName))
                throw new ArgumentNullException("countryName");

            IntegrationService EPGServices = new IntegrationService();
            var language = _workContext.WorkingLanguage.UniqueSeoCode;

            string cacheKey = string.Format(ModelCacheEventConsumer.STATEPROVINCES_BY_COUNTRY_MODEL_KEY, countryName, addEmptyStateIfRequired, _workContext.WorkingLanguage.Id);
            var cacheModel = _cacheManager.Get(cacheKey, () =>
            {
                var result = new List<StateSelect>();
                if (_addressService.CheckDomesticCountry(countryName))
                {
                    var states = EPGServices.GetDestinations("Local").Cast<OriginModel>().ToList();
                    //sort the states alphabetically on languange
                    if (language.ToLower() == "ar")
                        states = states.OrderBy(x => x.emirate_name_ar).ToList();
                    else
                        states = states.OrderBy(x => x.emirate_name_en).ToList();

                    result = (from s in states
                              select new StateSelect { id = s.emirate_id, name = (language == "ar") ? s.emirate_name_ar : s.emirate_name_en })
                                  .ToList();
                }
                else if (addEmptyStateIfRequired)
                {
                    result.Add(new StateSelect { id = "0", name = _localizationService.GetResource("Address.OtherNonUAE") });
                }
                return result;

            });

            return Json(cacheModel, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }

    public class StateSelect
    {
        public string id { get; set; }
        public string name { get; set; }
    }
}
