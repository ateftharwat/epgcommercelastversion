﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Seo;
using Nop.Services.Shipping;
using Nop.Web.Extensions;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Order;
using Nop.Web.Models.Common;
using Nop.EPGIntegrationService;
using Nop.EPGIntegrationService.Models;
using Nop.Web.Models.ShoppingCart;
using System.Web.Script.Serialization;
using Nop.Services.Media;
using Nop.Core.Domain.Customers;
using Nop.Services.Customers;

namespace Nop.Web.Controllers
{
    public partial class OrderController : BasePublicController
    {
		#region Fields

        private readonly IOrderService _orderService;
        private readonly IShipmentService _shipmentService;
        private readonly IWorkContext _workContext;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPaymentService _paymentService;
        private readonly ILocalizationService _localizationService;
        private readonly IPdfService _pdfService;
        private readonly IShippingService _shippingService;
        private readonly ICountryService _countryService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IWebHelper _webHelper;
        private readonly IPictureService _pictureService;
        private readonly ICustomerService _customerService;
        private readonly IProductService _productService;

        private readonly OrderSettings _orderSettings;
        private readonly TaxSettings _taxSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly PdfSettings _pdfSettings;
        private readonly ShippingSettings _shippingSettings;
        private readonly AddressSettings _addressSettings;

        private readonly IntegrationService EPGServices;

        #endregion

		#region Constructors

        public OrderController(IOrderService orderService, 
            IShipmentService shipmentService, IWorkContext workContext,
            ICurrencyService currencyService, IPriceFormatter priceFormatter,
            IOrderProcessingService orderProcessingService, IDateTimeHelper dateTimeHelper,
            IPaymentService paymentService, ILocalizationService localizationService,
            IPdfService pdfService, IShippingService shippingService,
            ICountryService countryService, IProductAttributeParser productAttributeParser,
            IProductAttributeFormatter productAttributeFormatter, IWebHelper webHelper,
            IPictureService pictureService, ICustomerService customerService, IProductService productService,
            CatalogSettings catalogSettings, OrderSettings orderSettings, 
            TaxSettings taxSettings, PdfSettings pdfSettings,
            ShippingSettings shippingSettings, AddressSettings addressSettings)
        {
            this._orderService = orderService;
            this._shipmentService = shipmentService;
            this._workContext = workContext;
            this._currencyService = currencyService;
            this._priceFormatter = priceFormatter;
            this._orderProcessingService = orderProcessingService;
            this._dateTimeHelper = dateTimeHelper;
            this._paymentService = paymentService;
            this._localizationService = localizationService;
            this._pdfService = pdfService;
            this._shippingService = shippingService;
            this._countryService = countryService;
            this._productAttributeParser = productAttributeParser;
            this._productAttributeFormatter = productAttributeFormatter;
            this._webHelper = webHelper;
            this._pictureService = pictureService;
            this._customerService = customerService;
            this._productService = productService;

            this._catalogSettings = catalogSettings;
            this._orderSettings = orderSettings;
            this._taxSettings = taxSettings;
            this._pdfSettings = pdfSettings;
            this._shippingSettings = shippingSettings;
            this._addressSettings = addressSettings;

            this.EPGServices = new IntegrationService();
        }

        #endregion

        #region Utilities

        [NonAction]
        protected OrderDetailsModel PrepareOrderDetailsModel(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");
            var model = new OrderDetailsModel();

            //get list of countries and states
            var countries = EPGServices.GetDestinations("International Outgoing").Cast<DestinationModel>().ToList();
            var states = EPGServices.GetDestinations("Local").Cast<OriginModel>().ToList();

            model.Id = order.Id;
            model.CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc);
            model.OrderStatus = order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext);
            model.IsReOrderAllowed = _orderSettings.IsReOrderAllowed;
            model.IsReturnRequestAllowed = _orderProcessingService.IsReturnRequestAllowed(order);

            //shipping info
            model.ShippingStatus = order.ShippingStatus.GetLocalizedEnum(_localizationService, _workContext);
            if (order.ShippingStatus != ShippingStatus.ShippingNotRequired)
            {
                model.IsShippable = true;
                if(order.SendToManyAddressSelected == false)
                {                    
                    Address shippingAddress = null;
                    foreach (var item in order.OrderItems)
                    {
                        if (!item.Product.IsCustomizable)
                            shippingAddress = item.OrderItemShippingAddresses.First().Address;
                    }
                    if (shippingAddress != null)
                        //model.ShippingAddress.PrepareModel(shippingAddress, false, _addressSettings);
                        model.ShippingAddress.PrepareNewModel(shippingAddress, false, _addressSettings, countries, states, language: _workContext.WorkingLanguage.UniqueSeoCode, loadCountry: false);
                }
                //model.ShippingMethod = order.ShippingMethod;
   

                //shipments (only already shipped)
                //var shipments = order.Shipments.Where(x => x.ShippedDateUtc.HasValue).OrderBy(x => x.CreatedOnUtc).ToList();
                var shipments = order.Shipments.OrderBy(x => x.CreatedOnUtc).ToList();
                foreach (var shipment in shipments)
                {
                    var shipmentModel = new OrderDetailsModel.ShipmentBriefModel()
                    {
                        Id = shipment.Id,
                        TrackingNumber = shipment.TrackingNumber,
                        AddressId = shipment.AddressId,
                        ShippingMethod = shipment.ShippingMethod,
                        ShippingMethodID = shipment.ShippingMethodID
                    };

                    foreach (var item in shipment.ShipmentItems)
                    {
                        var shippmentItemModel = new OrderDetailsModel.ShipmentItemBriefModel()
                        {
                            Id = item.Id,
                            OrderItemId = item.OrderItemId,
                            ShippmentId = item.ShipmentId,
                        };
                        shipmentModel.ShippmentItems.Add(shippmentItemModel);
                    }

                    if (shipment.ShippedDateUtc.HasValue)
                        shipmentModel.ShippedDate = _dateTimeHelper.ConvertToUserTime(shipment.ShippedDateUtc.Value, DateTimeKind.Utc);
                    if (shipment.DeliveryDateUtc.HasValue)
                        shipmentModel.DeliveryDate = _dateTimeHelper.ConvertToUserTime(shipment.DeliveryDateUtc.Value, DateTimeKind.Utc);
                    model.Shipments.Add(shipmentModel);
                }
            }


            //billing info
            //model.BillingAddress.PrepareModel(order.BillingAddress, false, _addressSettings);
            model.BillingAddress.PrepareNewModel(order.BillingAddress, false, _addressSettings, countries, states, language: _workContext.WorkingLanguage.UniqueSeoCode, loadCountry: false);

            //VAT number
            model.VatNumber = order.VatNumber;

            //payment method
            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
            model.PaymentMethod = paymentMethod != null ? paymentMethod.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id) : order.PaymentMethodSystemName;
            model.CanRePostProcessPayment = _paymentService.CanRePostProcessPayment(order);

            //purchase order number (we have to find a better to inject this information because it's related to a certain plugin)
            if (paymentMethod != null && paymentMethod.PluginDescriptor.SystemName.Equals("Payments.PurchaseOrder", StringComparison.InvariantCultureIgnoreCase))
            {
                model.DisplayPurchaseOrderNumber = true;
                model.PurchaseOrderNumber = order.PurchaseOrderNumber;
            }


            //order subtotal
            if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal)
            {
                //including tax

                //order subtotal
                var orderSubtotalInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalInclTax, order.CurrencyRate);
                model.OrderSubtotal = _priceFormatter.FormatPrice(orderSubtotalInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                //discount (applied to order subtotal)
                var orderSubTotalDiscountInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountInclTax, order.CurrencyRate);
                if (orderSubTotalDiscountInclTaxInCustomerCurrency > decimal.Zero)
                    model.OrderSubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
            }
            else
            {
                //excluding tax

                //order subtotal
                var orderSubtotalExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalExclTax, order.CurrencyRate);
                model.OrderSubtotal = _priceFormatter.FormatPrice(orderSubtotalExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                //discount (applied to order subtotal)
                var orderSubTotalDiscountExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountExclTax, order.CurrencyRate);
                if (orderSubTotalDiscountExclTaxInCustomerCurrency > decimal.Zero)
                    model.OrderSubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
            }

            if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
            {
                //including tax

                //order shipping
                var orderShippingInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingInclTax, order.CurrencyRate);
                model.OrderShipping = _priceFormatter.FormatShippingPrice(orderShippingInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                //payment method additional fee
                var paymentMethodAdditionalFeeInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeInclTax, order.CurrencyRate);
                if (paymentMethodAdditionalFeeInclTaxInCustomerCurrency > decimal.Zero)
                    model.PaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
            }
            else
            {
                //excluding tax

                //order shipping
                var orderShippingExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingExclTax, order.CurrencyRate);
                model.OrderShipping = _priceFormatter.FormatShippingPrice(orderShippingExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                //payment method additional fee
                var paymentMethodAdditionalFeeExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeExclTax, order.CurrencyRate);
                if (paymentMethodAdditionalFeeExclTaxInCustomerCurrency > decimal.Zero)
                    model.PaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
            }

            //tax
            bool displayTax = true;
            bool displayTaxRates = true;
            if (_taxSettings.HideTaxInOrderSummary && order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
            {
                displayTax = false;
                displayTaxRates = false;
            }
            else
            {
                if (order.OrderTax == 0 && _taxSettings.HideZeroTax)
                {
                    displayTax = false;
                    displayTaxRates = false;
                }
                else
                {
                    displayTaxRates = _taxSettings.DisplayTaxRates && order.TaxRatesDictionary.Count > 0;
                    displayTax = !displayTaxRates;

                    var orderTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTax, order.CurrencyRate);
                    //TODO pass languageId to _priceFormatter.FormatPrice
                    model.Tax = _priceFormatter.FormatPrice(orderTaxInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);

                    foreach (var tr in order.TaxRatesDictionary)
                    {
                        model.TaxRates.Add(new OrderDetailsModel.TaxRate()
                        {
                            Rate = _priceFormatter.FormatTaxRate(tr.Key),
                            //TODO pass languageId to _priceFormatter.FormatPrice
                            Value = _priceFormatter.FormatPrice(_currencyService.ConvertCurrency(tr.Value, order.CurrencyRate), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage),
                        });
                    }
                }
            }
            model.DisplayTaxRates = displayTaxRates;
            model.DisplayTax = displayTax;


            //discount (applied to order total)
            var orderDiscountInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderDiscount, order.CurrencyRate);
            if (orderDiscountInCustomerCurrency > decimal.Zero)
                model.OrderTotalDiscount = _priceFormatter.FormatPrice(-orderDiscountInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);


            //gift cards
            foreach (var gcuh in order.GiftCardUsageHistory)
            {
                model.GiftCards.Add(new OrderDetailsModel.GiftCard()
                {
                    CouponCode = gcuh.GiftCard.GiftCardCouponCode,
                    Amount = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(gcuh.UsedValue, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage),
                });
            }

            //reward points           
            if (order.RedeemedRewardPointsEntry != null)
            {
                model.RedeemedRewardPoints = -order.RedeemedRewardPointsEntry.Points;
                model.RedeemedRewardPointsAmount = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(order.RedeemedRewardPointsEntry.UsedAmount, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);
            }

            //total
            var orderTotalInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTotal, order.CurrencyRate);
            model.OrderTotal = _priceFormatter.FormatPrice(orderTotalInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);

            //checkout attributes
            model.CheckoutAttributeInfo = order.CheckoutAttributeDescription;

            //order notes
            foreach (var orderNote in order.OrderNotes
                .Where(on => on.DisplayToCustomer)
                .OrderByDescending(on => on.CreatedOnUtc)
                .ToList())
            {
                model.OrderNotes.Add(new OrderDetailsModel.OrderNote()
                {
                    Id = orderNote.Id,
                    HasDownload = orderNote.DownloadId > 0,
                    Note = orderNote.FormatOrderNoteText(),
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(orderNote.CreatedOnUtc, DateTimeKind.Utc)
                });
            }


            //purchased products
            model.ShowSku = _catalogSettings.ShowProductSku;
            var orderItems = _orderService.GetAllOrderItems(order.Id, null, null, null, null, null, null);
            foreach (var orderItem in orderItems)
            {
                var orderItemModel = new OrderDetailsModel.OrderItemModel()
                {
                    Id = orderItem.Id,
                    Sku = orderItem.Product.FormatSku(orderItem.AttributesXml, _productAttributeParser),
                    ProductId = orderItem.Product.Id,
                    ProductName = orderItem.Product.GetLocalized(x => x.Name),
                    ProductSeName = orderItem.Product.GetSeName(),
                    Quantity = orderItem.Quantity,
                    AttributeInfo = orderItem.AttributeDescription,
                    ProductIsCustomizable = orderItem.Product.IsCustomizable,
                };

                foreach (var item in orderItem.OrderItemShippingAddresses)
                {
                    for (int i = 0; i < item.Quantity; i++)
                    {
                        if (item != null)
                        {
                            var newModel = new AddressModel();
                            //newModel.PrepareModel(item.Address, false, _addressSettings);
                            newModel.PrepareNewModel(item.Address, false, _addressSettings, countries, states, language: _workContext.WorkingLanguage.UniqueSeoCode, loadCountry: false);
                            orderItemModel.ShippingAddresses.Add(newModel);
                            orderItemModel.ShippingMethods.Add(item.ShippingMethod);
                        }
                    }
                    
                }


                model.Items.Add(orderItemModel);

                //unit price, subtotal
                if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
                {
                    //including tax
                    var unitPriceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceInclTax, order.CurrencyRate);
                    orderItemModel.UnitPrice = _priceFormatter.FormatPrice(unitPriceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);

                    var internationalPriceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.InternationalPriceInclTax, order.CurrencyRate);
                    orderItemModel.InternationalPrice = _priceFormatter.FormatPrice(internationalPriceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);

                    var priceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceInclTax, order.CurrencyRate);
                    orderItemModel.SubTotal = _priceFormatter.FormatPrice(priceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                }
                else
                {
                    //excluding tax
                    var unitPriceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceExclTax, order.CurrencyRate);
                    orderItemModel.UnitPrice = _priceFormatter.FormatPrice(unitPriceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);

                    var internationalPriceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.InternationalPriceExclTax, order.CurrencyRate);
                    orderItemModel.InternationalPrice = _priceFormatter.FormatPrice(internationalPriceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);

                    var priceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceExclTax, order.CurrencyRate);
                    orderItemModel.SubTotal = _priceFormatter.FormatPrice(priceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                }
            }

            return model;
        }

        [NonAction]
        protected ShipmentDetailsModel PrepareShipmentDetailsModel(Shipment shipment)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            var order = shipment.Order;
            if (order == null)
                throw new Exception("order cannot be loaded");
            var model = new ShipmentDetailsModel();

            //get list of countries and states
            var countries = EPGServices.GetDestinations("International Outgoing").Cast<DestinationModel>().ToList();
            var states = EPGServices.GetDestinations("Local").Cast<OriginModel>().ToList();
            
            model.Id = shipment.Id;
            if (shipment.ShippedDateUtc.HasValue)
                model.ShippedDate = _dateTimeHelper.ConvertToUserTime(shipment.ShippedDateUtc.Value, DateTimeKind.Utc);
            if (shipment.DeliveryDateUtc.HasValue)
                model.DeliveryDate = _dateTimeHelper.ConvertToUserTime(shipment.DeliveryDateUtc.Value, DateTimeKind.Utc);
            
            //tracking number and shipment information
            model.TrackingNumber = shipment.TrackingNumber;
            //model.ShippingAddress.PrepareModel(shipment.Address, false, _addressSettings);
            model.ShippingAddress.PrepareNewModel(shipment.Address, false, _addressSettings, countries, states, language: _workContext.WorkingLanguage.UniqueSeoCode, loadCountry: false);
            model.ShippingMethod = shipment.ShippingMethod;
            model.ShippingMethodID = shipment.ShippingMethodID;
            var srcm = _shippingService.LoadShippingRateComputationMethodBySystemName(order.ShippingRateComputationMethodSystemName);
            if (srcm != null &&
                srcm.PluginDescriptor.Installed &&
                srcm.IsShippingRateComputationMethodActive(_shippingSettings))
            {
                var shipmentTracker = srcm.ShipmentTracker;
                if (shipmentTracker != null)
                {
                    model.TrackingNumberUrl = shipmentTracker.GetUrl(shipment.TrackingNumber);
                    if (_shippingSettings.DisplayShipmentEventsToCustomers)
                    {
                        var shipmentEvents = shipmentTracker.GetShipmentEvents(shipment.TrackingNumber);
                        if (shipmentEvents != null)
                            foreach (var shipmentEvent in shipmentEvents)
                            {
                                var shipmentStatusEventModel = new ShipmentDetailsModel.ShipmentStatusEventModel();
                                var shipmentEventCountry = _countryService.GetCountryByTwoLetterIsoCode(shipmentEvent.CountryCode);
                                shipmentStatusEventModel.Country = shipmentEventCountry != null
                                                                       ? shipmentEventCountry.GetLocalized(x => x.Name)
                                                                       : shipmentEvent.CountryCode;
                                shipmentStatusEventModel.Date = shipmentEvent.Date;
                                shipmentStatusEventModel.EventName = shipmentEvent.EventName;
                                shipmentStatusEventModel.Location = shipmentEvent.Location;
                                model.ShipmentStatusEvents.Add(shipmentStatusEventModel);
                            }
                    }
                }
            }
            
            //products in this shipment
            model.ShowSku = _catalogSettings.ShowProductSku;
            foreach (var shipmentItem in shipment.ShipmentItems)
            {
                var orderItem = _orderService.GetOrderItemById(shipmentItem.OrderItemId);
                if (orderItem == null)
                    continue;

                var shipmentItemModel = new ShipmentDetailsModel.ShipmentItemModel()
                {
                    Id = shipmentItem.Id,
                    Sku = orderItem.Product.FormatSku(orderItem.AttributesXml, _productAttributeParser),
                    ProductId = orderItem.Product.Id,
                    ProductName = orderItem.Product.GetLocalized(x => x.Name),
                    ProductIsCustomizable = orderItem.Product.IsCustomizable,
                    ProductSeName = orderItem.Product.GetSeName(),
                    AttributeInfo = orderItem.AttributeDescription,
                    QuantityOrdered = orderItem.Quantity,
                    QuantityShipped = shipmentItem.Quantity,
                };
                model.Items.Add(shipmentItemModel);
            }

            //order details model
            model.Order = PrepareOrderDetailsModel(order);
            
            return model;
        }

        #endregion

        #region Order details

        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult Details(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            var model = PrepareOrderDetailsModel(order);

            if(order.SendToManyAddressSelected ?? true)
                return View("DetailsMultiAddress", model);

            return View(model);
        }

        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult PrintOrderDetails(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            var model = PrepareOrderDetailsModel(order);
            model.PrintMode = true;

            
            if (order.SendToManyAddressSelected ?? true)
                return View("DetailsMultiAddress", model);

            return View("Details", model);
        }

        public ActionResult GetPdfInvoice(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            var orders = new List<Order>();
            orders.Add(order);
            byte[] bytes = null;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintOrdersToPdf(stream, orders, _workContext.WorkingLanguage.Id);
                bytes = stream.ToArray();
            }
            return File(bytes, "application/pdf", string.Format("order_{0}.pdf", order.Id));
        }

        //TODO: needing new flag to differentiate cart edit than order edit
        public ActionResult EditOrderItemCustomization(int orderItemId)
        {
            var orderItem = _orderService.GetOrderItemById(orderItemId);

            var model = new CustomizeOrderItemModel();
            model.ProductId = 0;

            model.Product = orderItem.Product;

            model.CustomerId = _workContext.CurrentCustomer.Id;
            model.OrderItemId = orderItemId;
            model.IsEditing = true;            

            if (Request.Browser.IsMobileDevice)
            { 
                model.ErrorExist = true;
                model.ErrorMessege = _localizationService.GetResource("shoppingcart.CardCustomization.MobileBrowser");
            }
            else if (!orderItem.CustomerCanEdit)
            {
                model.ErrorExist = true;
                model.ErrorMessege = _localizationService.GetResource("OrderItem.CardCustomization.EditNotAllowed");
            }
            else
            {
                model.ErrorExist = false;
                model.ErrorMessege = "";
            }

            return View(model);
        }

        public ActionResult ReOrder(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            _orderProcessingService.ReOrder(order);
            return RedirectToRoute("ShoppingCart");
        }

        [HttpPost, ActionName("Details")]
        [FormValueRequired("repost-payment")]
        public ActionResult RePostPayment(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            if (!_paymentService.CanRePostProcessPayment(order))
                return RedirectToRoute("OrderDetails", new { orderId = orderId });

            var postProcessPaymentRequest = new PostProcessPaymentRequest()
            {
                Order = order
            };
            _paymentService.PostProcessPayment(postProcessPaymentRequest);

            if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
            {
                //redirection or POST has been done in PostProcessPayment
                return Content("Redirected");
            }
            else
            {
                //if no redirection has been done (to a third-party payment page)
                //theoretically it's not possible
                return RedirectToRoute("OrderDetails", new { orderId = orderId });
            }
        }

        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult ShipmentDetails(int shipmentId)
        {
            var shipment = _shipmentService.GetShipmentById(shipmentId);
            if (shipment == null)
                return new HttpUnauthorizedResult();

            var order = shipment.Order;
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            var model = PrepareShipmentDetailsModel(shipment);

            return View(model);
        }
        #endregion

        #region Order Item Services

        public ActionResult UpdateOrdersShippingStatus()
        {
            var orders = _orderService.GetOrdersToUpdateShippingStatus();
            IntegrationService EPGServices = new IntegrationService();

            foreach (var order in orders)
            {
                var shipments = _shipmentService.GetShipmentsByOrderId(order.Id);

                foreach (var shipment in shipments)
                {
                    //string trackingno = "1070003270094";
                    //get the shipping status from Service using tracking number
                    var status = EPGServices.GetShippingStatus(shipment.TrackingNumber);

                    //get the shipping status value and update the shipment
                    if (status != null && status.consignment_shipmentlog != null)
                    {
                        double? cons_status_id = status.consignment_shipmentlog.FirstOrDefault().cons_status_id;
                        if (cons_status_id.HasValue && shipment.EPGShippingStatusId != (int)cons_status_id.Value)
                        {
                            shipment.EPGShippingStatusId = (int)cons_status_id.Value;
                            _shipmentService.UpdateShipment(shipment);
                        }
                    }
                    //if the shipment took a status other than the intial status "NotShippedYet"
                    if (shipment.EPGShippingStatusId != (int)EPGShippingStatus.NotShippedYet)
                    {
                        //if shipment not shipped yet set this shipment as shipped
                        if (!shipment.ShippedDateUtc.HasValue)
                            _orderProcessingService.Ship(shipment, true);

                        //if the shipment is shipped and not delivered and has status "delivered" set this shipment as delivered
                        if (shipment.ShippedDateUtc.HasValue && !shipment.DeliveryDateUtc.HasValue 
                            && shipment.EPGShippingStatusId != (int)EPGShippingStatus.Delivered)
                        {
                            //if shipment has a delivery date set it with that date
                            var deliverDate = status.consignment.FirstOrDefault().delivered_date;
                            if (deliverDate.HasValue)
                                _orderProcessingService.Deliver(shipment, deliverDate.Value, true);
                            else
                                _orderProcessingService.Deliver(shipment, true);
                        }
                    }
                }
            }

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOrderItemByItemId(int ItemId)
        {
            var customer = _workContext.CurrentCustomer;
            var orderItem = _orderService.GetOrderItemById(ItemId);

            var orderItemSimpleInfo = new ShoppingCartItemSimpleInfoModel();

            orderItemSimpleInfo.Id = orderItem.Id;
            orderItemSimpleInfo.CustomerId = customer.Id;
            orderItemSimpleInfo.ProductId = orderItem.Product.Id;
            orderItemSimpleInfo.ProductName = orderItem.Product.Name;
            var pictures = orderItem.Product.ProductPictures.OrderBy(x => x.DisplayOrder);
            foreach (var pic in pictures)
            {
                if (pic.IsTemplatePicture)
                {
                    var URL = _pictureService.GetPictureUrl(pic.PictureId);
                    orderItemSimpleInfo.TemplateImages.Add(URL);
                }
                else
                {
                    var URL = _pictureService.GetPictureUrl(pic.PictureId);
                    orderItemSimpleInfo.ProductPictures.Add(URL);
                }
            }
            orderItemSimpleInfo.SendToManyAddressSelected = orderItem.SendToManyAddressSelected;
            orderItemSimpleInfo.Quantity = orderItem.Quantity;
            orderItemSimpleInfo.Width = orderItem.Product.Width;
            orderItemSimpleInfo.Height = orderItem.Product.Height;
            orderItemSimpleInfo.IsCustomizable = orderItem.Product.IsCustomizable;

            orderItemSimpleInfo.NumberOfPages = orderItem.Product.CardPages.Select(cardPage => cardPage.PageIndex).Distinct().Count();

            /* not needed for editing order item
            //addresses
            var Addresses = new List<Address>();
            foreach (var item in orderItem.OrderItemShippingAddresses)
            {
                for (int i = 0; i < item.Quantity; i++)
                {
                    Addresses.Add(item.Address);
                }
            }

            orderItemSimpleInfo.Addresses = new List<SimpleAddressModel>();
            orderItemSimpleInfo.Addresses.prepareAddressSimpleModel(Addresses);
            */

            //parsing the values of pages
            IList<string> pagesValues = new List<string>();
            var productAttributes = _productAttributeParser.ParseProductVariantAttributes(orderItem.AttributesXml);
            foreach (var attribute in productAttributes)
            {
                if (attribute.ProductAttribute.Name.ToLower() == "card customization")
                {
                    pagesValues = _productAttributeParser.ParseValues(orderItem.AttributesXml, attribute.Id);
                }
            }
            if (pagesValues.Count > 0)
                orderItemSimpleInfo.Pages = (List<PageModel>)new JavaScriptSerializer().Deserialize(pagesValues.First(), typeof(List<PageModel>));

            return Json(orderItemSimpleInfo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveOrderItem(ShoppingCartItemSimpleInfoModel model)
        {
            string attributes = "";

            var customer = _customerService.GetCustomerById(model.CustomerId);
            if (customer == null)
                //no customer found
                return Json(_localizationService.GetResource("ShoppingCart.CustomerNotFound"), JsonRequestBehavior.AllowGet);

            var product = _productService.GetProductById(model.ProductId);
            if (product == null)
                //no product found
                return Json(_localizationService.GetResource("ShoppingCart.ProductNotFound"), JsonRequestBehavior.AllowGet);

            if (model.Pages != null && model.Pages.Count > 0)
            {
                //save the customization images in DB and attach it with customer
                foreach (var page in model.Pages)
                {
                    foreach (var holder in page.PlaceHolders)
                    {
                        if (holder.PictureData != null)
                        {
                            var PictureBinary = Convert.FromBase64String(holder.PictureData);
                            var picture = _pictureService.InsertUploadedPicture(PictureBinary, holder.FileName);
                            holder.PictureId = picture.Id;
                            holder.ImageURL = _pictureService.GetPictureUrl(picture);
                            holder.PictureData = null;

                            var customerPicture = new CustomerPicture()
                            {
                                CustomerId = customer.Id,
                                Customer = customer,
                                PictureId = picture.Id,
                                Picture = picture
                            };

                            customer.CustomerPictures.Add(customerPicture);
                        }
                    }
                }
                _customerService.UpdateCustomer(customer);

                //save the pages values as a product attribute
                string PagesJson = new JavaScriptSerializer().Serialize(model.Pages);
                foreach (var attribute in product.ProductVariantAttributes)
                {
                    if (attribute.ProductAttribute.Name.ToLower() == "card customization")
                    {
                        var ctrlAttributes = PagesJson;
                        if (!String.IsNullOrEmpty(ctrlAttributes))
                        {
                            string enteredText = ctrlAttributes.Trim();
                            attributes = _productAttributeParser.AddProductAttribute(attributes,
                                attribute, enteredText);
                        }
                    }
                }
            }

            var orderItem = _orderService.GetOrderItemById(model.Id);
            var order = _orderService.GetOrderById(orderItem.OrderId);

            //updating the card customization in the order item
            orderItem.AttributesXml = attributes;
            orderItem.AttributeDescription = _productAttributeFormatter.FormatAttributes(product, attributes, customer);
            //disabling future edits without admin permission
            orderItem.CustomerCanEdit = false;
            _orderService.UpdateOrder(order);

            return Json(_localizationService.GetResource("Order.OrderItem.CustomizationSaved"), JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
