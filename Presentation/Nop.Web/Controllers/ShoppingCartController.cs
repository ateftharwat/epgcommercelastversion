﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Shipping;
using Nop.Services.Tax;
using Nop.Web.Extensions;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Security;
using Nop.Web.Framework.UI.Captcha;
using Nop.Web.Infrastructure.Cache;
using Nop.Web.Models.Media;
using Nop.Web.Models.ShoppingCart;
using Nop.Web.Models.Common;
using Nop.Web.Models.Customer;
using System.Web.Script.Serialization;
using System.Text;
using Nop.Web.Models.Checkout;
using Nop.EPGIntegrationService;
using Nop.EPGIntegrationService.Models;
using System.Drawing;
using System.Configuration;
using System.Drawing.Imaging;

namespace Nop.Web.Controllers
{
    public partial class ShoppingCartController : BasePublicController
    {
        #region Fields

        private readonly IProductService _productService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IAddressService _addressService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IPictureService _pictureService;
        private readonly ILocalizationService _localizationService;
        private readonly ILanguageService _languageService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly ITaxService _taxService;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly ICheckoutAttributeParser _checkoutAttributeParser;
        private readonly ICheckoutAttributeFormatter _checkoutAttributeFormatter;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IDiscountService _discountService;
        private readonly ICustomerService _customerService;
        private readonly IGiftCardService _giftCardService;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IShippingService _shippingService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly ICheckoutAttributeService _checkoutAttributeService;
        private readonly IPaymentService _paymentService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IPermissionService _permissionService;
        private readonly IDownloadService _downloadService;
        private readonly ICacheManager _cacheManager;
        private readonly IWebHelper _webHelper;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IGenericAttributeService _genericAttributeService;

        private readonly MediaSettings _mediaSettings;
        private readonly ShoppingCartSettings _shoppingCartSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly OrderSettings _orderSettings;
        private readonly ShippingSettings _shippingSettings;
        private readonly TaxSettings _taxSettings;
        private readonly CaptchaSettings _captchaSettings;
        private readonly AddressSettings _addressSettings;

        private readonly IntegrationService EPGServices;

        #endregion

        #region Constructors

        public ShoppingCartController(IProductService productService,
            IStoreContext storeContext,
            IWorkContext workContext,
            IAddressService addressService,
            IShoppingCartService shoppingCartService,
            IPictureService pictureService,
            ILocalizationService localizationService,
            ILanguageService languageService,
            IProductAttributeService productAttributeService,
            IProductAttributeFormatter productAttributeFormatter,
            IProductAttributeParser productAttributeParser,
            ITaxService taxService, ICurrencyService currencyService,
            IPriceCalculationService priceCalculationService,
            IPriceFormatter priceFormatter,
            ICheckoutAttributeParser checkoutAttributeParser,
            ICheckoutAttributeFormatter checkoutAttributeFormatter,
            IOrderProcessingService orderProcessingService,
            IDiscountService discountService,
            ICustomerService customerService,
            IGiftCardService giftCardService,
            ICountryService countryService,
            IStateProvinceService stateProvinceService,
            IShippingService shippingService,
            IOrderTotalCalculationService orderTotalCalculationService,
            ICheckoutAttributeService checkoutAttributeService,
            IPaymentService paymentService,
            IWorkflowMessageService workflowMessageService,
            IPermissionService permissionService,
            IDownloadService downloadService,
            ICacheManager cacheManager,
            IWebHelper webHelper,
            ICustomerActivityService customerActivityService,
            IGenericAttributeService genericAttributeService,
            MediaSettings mediaSettings,
            ShoppingCartSettings shoppingCartSettings,
            CatalogSettings catalogSettings,
            OrderSettings orderSettings,
            ShippingSettings shippingSettings,
            TaxSettings taxSettings,
            CaptchaSettings captchaSettings,
            AddressSettings addressSettings)
        {
            this._productService = productService;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._addressService = addressService;
            this._shoppingCartService = shoppingCartService;
            this._pictureService = pictureService;
            this._localizationService = localizationService;
            this._languageService = languageService;
            this._productAttributeService = productAttributeService;
            this._productAttributeFormatter = productAttributeFormatter;
            this._productAttributeParser = productAttributeParser;
            this._taxService = taxService;
            this._currencyService = currencyService;
            this._priceCalculationService = priceCalculationService;
            this._priceFormatter = priceFormatter;
            this._checkoutAttributeParser = checkoutAttributeParser;
            this._checkoutAttributeFormatter = checkoutAttributeFormatter;
            this._orderProcessingService = orderProcessingService;
            this._discountService = discountService;
            this._customerService = customerService;
            this._giftCardService = giftCardService;
            this._countryService = countryService;
            this._stateProvinceService = stateProvinceService;
            this._shippingService = shippingService;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._checkoutAttributeService = checkoutAttributeService;
            this._paymentService = paymentService;
            this._workflowMessageService = workflowMessageService;
            this._permissionService = permissionService;
            this._downloadService = downloadService;
            this._cacheManager = cacheManager;
            this._webHelper = webHelper;
            this._customerActivityService = customerActivityService;
            this._genericAttributeService = genericAttributeService;

            this._mediaSettings = mediaSettings;
            this._shoppingCartSettings = shoppingCartSettings;
            this._catalogSettings = catalogSettings;
            this._orderSettings = orderSettings;
            this._shippingSettings = shippingSettings;
            this._taxSettings = taxSettings;
            this._captchaSettings = captchaSettings;
            this._addressSettings = addressSettings;

            this.EPGServices = new IntegrationService();
        }

        #endregion

        #region Utilities

        [NonAction]
        protected PictureModel PrepareCartItemPictureModel(ShoppingCartItem sci,
            int pictureSize, bool showDefaultPicture, string productName)
        {
            var pictureCacheKey = string.Format(ModelCacheEventConsumer.CART_PICTURE_MODEL_KEY, sci.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
            var model = _cacheManager.Get(pictureCacheKey,
                //as we cache per user (shopping cart item identifier)
                //let's cache just for 3 minutes
                3, () =>
            {
                //shopping cart item picture
                Picture sciPicture = null;

                //first, let's see whether a shopping cart item has some attribute values with custom pictures
                var pvaValues = _productAttributeParser.ParseProductVariantAttributeValues(sci.AttributesXml);
                foreach (var pvaValue in pvaValues)
                {
                    var pvavPicture = _pictureService.GetPictureById(pvaValue.PictureId);
                    if (pvavPicture != null)
                    {
                        sciPicture = pvavPicture;
                        break;
                    }
                }

                //now let's load the default product picture
                var product = sci.Product;
                if (sciPicture == null)
                {
                    sciPicture = _pictureService.GetPicturesByProductId(product.Id, 1).FirstOrDefault();
                }

                //let's check whether this product has some parent "grouped" product
                if (sciPicture == null && !product.VisibleIndividually && product.ParentGroupedProductId > 0)
                {
                    sciPicture = _pictureService.GetPicturesByProductId(product.ParentGroupedProductId, 1).FirstOrDefault();
                }
                return new PictureModel()
                {
                    ImageUrl = _pictureService.GetPictureUrl(sciPicture, pictureSize, showDefaultPicture),
                    Title = string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"), productName),
                    AlternateText = string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"), productName),
                };
            });
            return model;
        }

        /// <summary>
        /// Prepare shopping cart model
        /// </summary>
        /// <param name="model">Model instance</param>
        /// <param name="cart">Shopping cart</param>
        /// <param name="isEditable">A value indicating whether cart is editable</param>
        /// <param name="validateCheckoutAttributes">A value indicating whether we should validate checkout attributes when preparing the model</param>
        /// <param name="prepareEstimateShippingIfEnabled">A value indicating whether we should prepare "Estimate shipping" model</param>
        /// <param name="setEstimateShippingDefaultAddress">A value indicating whether we should prefill "Estimate shipping" model with the default customer address</param>
        /// <param name="prepareAndDisplayOrderReviewData">A value indicating whether we should prepare review data (such as billing/shipping address, payment or shipping data entered during checkout)</param>
        /// <returns>Model</returns>
        [NonAction]
        protected void PrepareShoppingCartModel(ShoppingCartModel model,
            IList<ShoppingCartItem> cart, bool isEditable = true,
            bool validateCheckoutAttributes = false,
            bool prepareEstimateShippingIfEnabled = true, bool setEstimateShippingDefaultAddress = true,
            bool prepareAndDisplayOrderReviewData = false)
        {
            if (cart == null)
                throw new ArgumentNullException("cart");

            if (model == null)
                throw new ArgumentNullException("model");

            if (cart.Count == 0)
                return;

            var customer = cart.GetCustomer();
            model.CustomerId = customer.Id;

            #region Simple properties

            model.IsEditable = isEditable;
            model.ShowProductImages = _shoppingCartSettings.ShowProductImagesOnShoppingCart;
            model.ShowSku = _catalogSettings.ShowProductSku;
            var checkoutAttributesXml = customer.GetAttribute<string>(SystemCustomerAttributeNames.CheckoutAttributes, _genericAttributeService, _storeContext.CurrentStore.Id);
            model.CheckoutAttributeInfo = _checkoutAttributeFormatter.FormatAttributes(checkoutAttributesXml, customer);
            bool minOrderSubtotalAmountOk = _orderProcessingService.ValidateMinOrderSubtotalAmount(cart);
            if (!minOrderSubtotalAmountOk)
            {
                decimal minOrderSubtotalAmount = _currencyService.ConvertFromPrimaryStoreCurrency(_orderSettings.MinOrderSubtotalAmount, _workContext.WorkingCurrency);
                model.MinOrderSubtotalWarning = string.Format(_localizationService.GetResource("Checkout.MinOrderSubtotalAmount"), _priceFormatter.FormatPrice(minOrderSubtotalAmount, true, false));
            }
            model.TermsOfServiceOnShoppingCartPage = _orderSettings.TermsOfServiceOnShoppingCartPage;
            model.TermsOfServiceOnOrderConfirmPage = _orderSettings.TermsOfServiceOnOrderConfirmPage;
            model.OnePageCheckoutEnabled = _orderSettings.OnePageCheckoutEnabled;

            //gift card and gift card boxes
            model.DiscountBox.Display = _shoppingCartSettings.ShowDiscountBox;
            var discountCouponCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.DiscountCouponCode);
            var discount = _discountService.GetDiscountByCouponCode(discountCouponCode);
            if (discount != null &&
                discount.RequiresCouponCode &&
                _discountService.IsDiscountValid(discount, customer))
                model.DiscountBox.CurrentCode = discount.CouponCode;
            model.GiftCardBox.Display = _shoppingCartSettings.ShowGiftCardBox;

            //cart warnings
            var cartWarnings = _shoppingCartService.GetShoppingCartWarnings(cart, checkoutAttributesXml, validateCheckoutAttributes);
            foreach (var warning in cartWarnings)
                model.Warnings.Add(warning);

            #endregion

            #region Checkout attributes

            var checkoutAttributes = _checkoutAttributeService.GetAllCheckoutAttributes(_storeContext.CurrentStore.Id);
            if (!cart.RequiresShipping())
            {
                //remove attributes which require shippable products
                checkoutAttributes = checkoutAttributes.RemoveShippableAttributes();
            }
            foreach (var attribute in checkoutAttributes)
            {
                var caModel = new ShoppingCartModel.CheckoutAttributeModel()
                {
                    Id = attribute.Id,
                    Name = attribute.GetLocalized(x => x.Name),
                    TextPrompt = attribute.GetLocalized(x => x.TextPrompt),
                    IsRequired = attribute.IsRequired,
                    AttributeControlType = attribute.AttributeControlType
                };
                if (!String.IsNullOrEmpty(attribute.ValidationFileAllowedExtensions))
                {
                    caModel.AllowedFileExtensions = attribute.ValidationFileAllowedExtensions
                        .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                        .ToList();
                }

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var caValues = _checkoutAttributeService.GetCheckoutAttributeValues(attribute.Id);
                    foreach (var caValue in caValues)
                    {
                        var pvaValueModel = new ShoppingCartModel.CheckoutAttributeValueModel()
                        {
                            Id = caValue.Id,
                            Name = caValue.GetLocalized(x => x.Name),
                            ColorSquaresRgb = caValue.ColorSquaresRgb,
                            IsPreSelected = caValue.IsPreSelected
                        };
                        caModel.Values.Add(pvaValueModel);

                        //display price if allowed
                        if (_permissionService.Authorize(StandardPermissionProvider.DisplayPrices))
                        {
                            decimal priceAdjustmentBase = _taxService.GetCheckoutAttributePrice(caValue);
                            decimal priceAdjustment = _currencyService.ConvertFromPrimaryStoreCurrency(priceAdjustmentBase, _workContext.WorkingCurrency);
                            if (priceAdjustmentBase > decimal.Zero)
                                pvaValueModel.PriceAdjustment = "+" + _priceFormatter.FormatPrice(priceAdjustment);
                            else if (priceAdjustmentBase < decimal.Zero)
                                pvaValueModel.PriceAdjustment = "-" + _priceFormatter.FormatPrice(-priceAdjustment);
                        }
                    }
                }



                //set already selected attributes
                string selectedCheckoutAttributes = customer.GetAttribute<string>(SystemCustomerAttributeNames.CheckoutAttributes, _genericAttributeService, _storeContext.CurrentStore.Id);
                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                    case AttributeControlType.RadioList:
                    case AttributeControlType.Checkboxes:
                    case AttributeControlType.ColorSquares:
                        {
                            if (!String.IsNullOrEmpty(selectedCheckoutAttributes))
                            {
                                //clear default selection
                                foreach (var item in caModel.Values)
                                    item.IsPreSelected = false;

                                //select new values
                                var selectedCaValues = _checkoutAttributeParser.ParseCheckoutAttributeValues(selectedCheckoutAttributes);
                                foreach (var caValue in selectedCaValues)
                                    foreach (var item in caModel.Values)
                                        if (caValue.Id == item.Id)
                                            item.IsPreSelected = true;
                            }
                        }
                        break;
                    case AttributeControlType.TextBox:
                    case AttributeControlType.MultilineTextbox:
                        {
                            if (!String.IsNullOrEmpty(selectedCheckoutAttributes))
                            {
                                var enteredText = _checkoutAttributeParser.ParseValues(selectedCheckoutAttributes, attribute.Id);
                                if (enteredText.Count > 0)
                                    caModel.DefaultValue = enteredText[0];
                            }
                        }
                        break;
                    case AttributeControlType.Datepicker:
                        {
                            //keep in mind my that the code below works only in the current culture
                            var selectedDateStr = _checkoutAttributeParser.ParseValues(selectedCheckoutAttributes, attribute.Id);
                            if (selectedDateStr.Count > 0)
                            {
                                DateTime selectedDate;
                                if (DateTime.TryParseExact(selectedDateStr[0], "D", CultureInfo.CurrentCulture,
                                                       DateTimeStyles.None, out selectedDate))
                                {
                                    //successfully parsed
                                    caModel.SelectedDay = selectedDate.Day;
                                    caModel.SelectedMonth = selectedDate.Month;
                                    caModel.SelectedYear = selectedDate.Year;
                                }
                            }

                        }
                        break;
                    default:
                        break;
                }

                model.CheckoutAttributes.Add(caModel);
            }

            #endregion

            #region Estimate shipping

            if (prepareEstimateShippingIfEnabled)
            {
                model.EstimateShipping.Enabled = cart.Count > 0 && cart.RequiresShipping() && _shippingSettings.EstimateShippingEnabled;
                if (model.EstimateShipping.Enabled)
                {
                    //countries
                    int? defaultEstimateCountryId = (setEstimateShippingDefaultAddress && customer.ShippingAddress != null) ? customer.ShippingAddress.CountryId : model.EstimateShipping.CountryId;
                    model.EstimateShipping.AvailableCountries.Add(new SelectListItem() { Text = _localizationService.GetResource("Address.SelectCountry"), Value = "0" });
                    foreach (var c in _countryService.GetAllCountriesForShipping())
                        model.EstimateShipping.AvailableCountries.Add(new SelectListItem()
                        {
                            Text = c.GetLocalized(x => x.Name),
                            Value = c.Id.ToString(),
                            Selected = c.Id == defaultEstimateCountryId
                        });
                    //states
                    int? defaultEstimateStateId = (setEstimateShippingDefaultAddress && customer.ShippingAddress != null) ? customer.ShippingAddress.StateProvinceId : model.EstimateShipping.StateProvinceId;
                    var states = defaultEstimateCountryId.HasValue ? _stateProvinceService.GetStateProvincesByCountryId(defaultEstimateCountryId.Value).ToList() : new List<StateProvince>();
                    if (states.Count > 0)
                        foreach (var s in states)
                            model.EstimateShipping.AvailableStates.Add(new SelectListItem()
                            {
                                Text = s.GetLocalized(x => x.Name),
                                Value = s.Id.ToString(),
                                Selected = s.Id == defaultEstimateStateId
                            });
                    else
                        model.EstimateShipping.AvailableStates.Add(new SelectListItem() { Text = _localizationService.GetResource("Address.OtherNonUS"), Value = "0" });

                    if (setEstimateShippingDefaultAddress && customer.ShippingAddress != null)
                        model.EstimateShipping.ZipPostalCode = customer.ShippingAddress.ZipPostalCode;
                }
            }

            #endregion

            #region Cart items
            string ShippingOrigin = ConfigurationManager.AppSettings["ShippingOrigin"];
            CheckoutMultiShippingMethodModel ShippingMethodModel = null;
            if (customer.ShippingMethodsAttributes != null)
                ShippingMethodModel = (CheckoutMultiShippingMethodModel)new JavaScriptSerializer().Deserialize(customer.ShippingMethodsAttributes, typeof(CheckoutMultiShippingMethodModel));

            //get list of countries and states
            var countries = EPGServices.GetDestinations("International Outgoing").Cast<DestinationModel>().ToList();
            var stateList = EPGServices.GetDestinations("Local").Cast<OriginModel>().ToList();

            foreach (var sci in cart)
            {
                var cartItemModel = new ShoppingCartModel.ShoppingCartItemModel()
                {
                    Id = sci.Id,
                    Sku = sci.Product.FormatSku(sci.AttributesXml, _productAttributeParser),
                    ProductId = sci.Product.Id,
                    ProductName = sci.Product.GetLocalized(x => x.Name),
                    ProductIsCustomizable = sci.Product.IsCustomizable,
                    ProductSeName = sci.Product.GetSeName(),
                    Quantity = sci.Quantity,
                    AttributeInfo = _productAttributeFormatter.FormatAttributes(sci.Product, sci.AttributesXml),
                };

                //multishipping addresses
                foreach (var ShoppingCartItemAddress in sci.ShoppingCartItemAddresses)
                {
                    var ShippingAddress = ShoppingCartItemAddress.Address;
                    for (int i = 0; i < ShoppingCartItemAddress.Quantity; i++)
                    {
                        if (ShippingAddress != null)
                        {
                            var newModel = new AddressModel();
                            //newModel.PrepareModel(ShippingAddress, false, _addressSettings);
                            newModel.PrepareNewModel(ShippingAddress, false, _addressSettings, countries, stateList, language: _workContext.WorkingLanguage.UniqueSeoCode, loadCountry: false);
                            cartItemModel.ShippingAddresses.Add(newModel);
                        }

                        //in case of non customizable product get the selected shipping method
                        if (ShippingMethodModel != null && !sci.Product.IsCustomizable)
                        {
                            var ItemId = (sci.Product.Height.ToString() + sci.Product.Width.ToString() + sci.Product.Length.ToString() + sci.Product.Weight.ToString() + ShippingOrigin.ToLower() + (ShippingAddress.StateName ?? ShippingAddress.CountryName)).GetHashCode();
                            //find selected options
                            foreach (var method in ShippingMethodModel.ItemsShippingMethods)
                            {
                                if (method.ItemId == ItemId)
                                {
                                    foreach (var option in method.ShippingOptions)
                                    {
                                        if (option.Selected)
                                            cartItemModel.ShippingMethodNames.Add(option.Name + "<br>(" + option.Fee + ") " + option.ExpectedDeliveryTime);
                                    }
                                }
                            }
                        }
                    }
                }

                //allow editing?
                //1. setting enabled?
                //2. simple product?
                //3. has attribute or gift card?
                //4. visible individually?
                cartItemModel.AllowItemEditing = _shoppingCartSettings.AllowCartItemEditing &&
                    sci.Product.ProductType == ProductType.SimpleProduct &&
                    (!String.IsNullOrEmpty(cartItemModel.AttributeInfo) || sci.Product.IsGiftCard) &&
                    sci.Product.VisibleIndividually;

                //allowed quantities
                var allowedQuantities = sci.Product.ParseAllowedQuatities();
                foreach (var qty in allowedQuantities)
                {
                    cartItemModel.AllowedQuantities.Add(new SelectListItem()
                    {
                        Text = qty.ToString(),
                        Value = qty.ToString(),
                        Selected = sci.Quantity == qty
                    });
                }

                //recurring info
                if (sci.Product.IsRecurring)
                    cartItemModel.RecurringInfo = string.Format(_localizationService.GetResource("ShoppingCart.RecurringPeriod"), sci.Product.RecurringCycleLength, sci.Product.RecurringCyclePeriod.GetLocalizedEnum(_localizationService, _workContext));

                //unit prices
                if (sci.Product.CallForPrice)
                {
                    cartItemModel.UnitPrice = _localizationService.GetResource("Products.CallForPrice");
                }
                else
                {
                    decimal taxRate = decimal.Zero;
                    decimal shoppingCartUnitPriceWithDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetUnitPrice(sci, true), out taxRate);
                    decimal shoppingCartUnitPriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartUnitPriceWithDiscountBase, _workContext.WorkingCurrency);
                    cartItemModel.UnitPrice = _priceFormatter.FormatPrice(shoppingCartUnitPriceWithDiscount);
                }
                //international prices
                if (sci.Product.CallForPrice)
                {
                    cartItemModel.InternationalPrice = _localizationService.GetResource("Products.CallForPrice");
                }
                else
                {
                    decimal taxRate = decimal.Zero;
                    decimal shoppingCartInternationalPriceWithDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetInternationalPrice(sci.Product, true), out taxRate);
                    decimal shoppingCartInternationalPriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartInternationalPriceWithDiscountBase, _workContext.WorkingCurrency);
                    cartItemModel.InternationalPrice = _priceFormatter.FormatPrice(shoppingCartInternationalPriceWithDiscount);
                }
                //subtotal, discount
                if (sci.Product.CallForPrice)
                {
                    cartItemModel.SubTotal = _localizationService.GetResource("Products.CallForPrice");
                }
                else
                {
                    //sub total
                    decimal taxRate = decimal.Zero;
                    decimal shoppingCartItemSubTotalWithDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetSubTotal(sci, true, sci.SendToManyAddressSelected ?? false), out taxRate);
                    decimal shoppingCartItemSubTotalWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartItemSubTotalWithDiscountBase, _workContext.WorkingCurrency);
                    cartItemModel.SubTotal = _priceFormatter.FormatPrice(shoppingCartItemSubTotalWithDiscount);

                    //display an applied discount amount
                    decimal shoppingCartItemSubTotalWithoutDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetSubTotal(sci, false, sci.SendToManyAddressSelected ?? false), out taxRate);
                    decimal shoppingCartItemDiscountBase = shoppingCartItemSubTotalWithoutDiscountBase - shoppingCartItemSubTotalWithDiscountBase;
                    if (shoppingCartItemDiscountBase > decimal.Zero)
                    {
                        decimal shoppingCartItemDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartItemDiscountBase, _workContext.WorkingCurrency);
                        cartItemModel.Discount = _priceFormatter.FormatPrice(shoppingCartItemDiscount);
                    }
                }

                //picture
                if (_shoppingCartSettings.ShowProductImagesOnShoppingCart)
                {
                    cartItemModel.Picture = PrepareCartItemPictureModel(sci,
                        _mediaSettings.CartThumbPictureSize, true, cartItemModel.ProductName);
                }

                //item warnings
                var itemWarnings = _shoppingCartService.GetShoppingCartItemWarnings(
                    customer,
                    sci.ShoppingCartType,
                    sci.Product,
                    sci.StoreId,
                    sci.AttributesXml,
                    sci.CustomerEnteredPrice,
                    sci.Quantity,
                    false);
                foreach (var warning in itemWarnings)
                    cartItemModel.Warnings.Add(warning);

                model.Items.Add(cartItemModel);
            }

            #endregion

            #region Button payment methods

            var boundPaymentMethods = _paymentService
                .LoadActivePaymentMethods(customer.Id, _storeContext.CurrentStore.Id)
                .Where(pm => pm.PaymentMethodType == PaymentMethodType.Button)
                .ToList();
            foreach (var pm in boundPaymentMethods)
            {
                if (cart.IsRecurring() && pm.RecurringPaymentType == RecurringPaymentType.NotSupported)
                    continue;

                string actionName;
                string controllerName;
                RouteValueDictionary routeValues;
                pm.GetPaymentInfoRoute(out actionName, out controllerName, out routeValues);

                model.ButtonPaymentMethodActionNames.Add(actionName);
                model.ButtonPaymentMethodControllerNames.Add(controllerName);
                model.ButtonPaymentMethodRouteValues.Add(routeValues);
            }

            #endregion

            #region Order review data

            if (prepareAndDisplayOrderReviewData)
            {
                model.OrderReviewData.Display = true;

                //billing info
                var billingAddress = customer.BillingAddress;
                if (billingAddress != null)
                    //model.OrderReviewData.BillingAddress.PrepareModel(billingAddress, false, _addressSettings);
                    model.OrderReviewData.BillingAddress.PrepareNewModel(billingAddress, false, _addressSettings, countries, stateList, language: _workContext.WorkingLanguage.UniqueSeoCode, loadCountry: false);

                //shipping info
                if (cart.RequiresShipping())
                {
                    model.OrderReviewData.IsShippable = true;

                    Address shippingAddress = null;
                    //single shipping address
                    foreach (var item in cart)
                    {
                        if (!item.Product.IsCustomizable && item.ShoppingCartItemAddresses.Count > 0)
                            shippingAddress = item.ShoppingCartItemAddresses.FirstOrDefault().Address;
                    }

                    if (shippingAddress != null)
                        //model.OrderReviewData.ShippingAddress.PrepareModel(shippingAddress, false, _addressSettings);
                        model.OrderReviewData.ShippingAddress.PrepareNewModel(shippingAddress, false, _addressSettings, countries, stateList, language: _workContext.WorkingLanguage.UniqueSeoCode, loadCountry: false);

                    //selected shipping method
                    //var shippingOption = customer.GetAttribute<ShippingOption>(SystemCustomerAttributeNames.SelectedShippingOption, _storeContext.CurrentStore.Id);
                    //if (shippingOption != null)
                    //    model.OrderReviewData.ShippingMethod = shippingOption.Name;
                }
                //payment info
                var selectedPaymentMethodSystemName = customer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod, _storeContext.CurrentStore.Id);
                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(selectedPaymentMethodSystemName);
                model.OrderReviewData.PaymentMethod = paymentMethod != null ? paymentMethod.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id) : "";
            }
            #endregion
        }

        [NonAction]
        protected void PrepareWishlistModel(WishlistModel model,
            IList<ShoppingCartItem> cart, bool isEditable = true)
        {
            if (cart == null)
                throw new ArgumentNullException("cart");

            if (model == null)
                throw new ArgumentNullException("model");

            model.EmailWishlistEnabled = _shoppingCartSettings.EmailWishlistEnabled;
            model.IsEditable = isEditable;
            model.DisplayAddToCart = _permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart);

            if (cart.Count == 0)
                return;

            #region Simple properties

            var customer = cart.FirstOrDefault().Customer;
            model.CustomerGuid = customer.CustomerGuid;
            model.CustomerFullname = customer.GetFullName();
            model.ShowProductImages = _shoppingCartSettings.ShowProductImagesOnShoppingCart;
            model.ShowSku = _catalogSettings.ShowProductSku;

            //cart warnings
            var cartWarnings = _shoppingCartService.GetShoppingCartWarnings(cart, "", false);
            foreach (var warning in cartWarnings)
                model.Warnings.Add(warning);

            #endregion

            #region Cart items

            foreach (var sci in cart)
            {
                var cartItemModel = new WishlistModel.ShoppingCartItemModel()
                {
                    Id = sci.Id,
                    Sku = sci.Product.FormatSku(sci.AttributesXml, _productAttributeParser),
                    ProductId = sci.Product.Id,
                    ProductName = sci.Product.GetLocalized(x => x.Name),
                    ProductSeName = sci.Product.GetSeName(),
                    ProductIsCustomizable = sci.Product.IsCustomizable,
                    Quantity = sci.Quantity,
                    AttributeInfo = _productAttributeFormatter.FormatAttributes(sci.Product, sci.AttributesXml),
                };

                //allowed quantities
                var allowedQuantities = sci.Product.ParseAllowedQuatities();
                foreach (var qty in allowedQuantities)
                {
                    cartItemModel.AllowedQuantities.Add(new SelectListItem()
                    {
                        Text = qty.ToString(),
                        Value = qty.ToString(),
                        Selected = sci.Quantity == qty
                    });
                }


                //recurring info
                if (sci.Product.IsRecurring)
                    cartItemModel.RecurringInfo = string.Format(_localizationService.GetResource("ShoppingCart.RecurringPeriod"), sci.Product.RecurringCycleLength, sci.Product.RecurringCyclePeriod.GetLocalizedEnum(_localizationService, _workContext));

                //unit prices
                if (sci.Product.CallForPrice)
                {
                    cartItemModel.UnitPrice = _localizationService.GetResource("Products.CallForPrice");
                }
                else
                {
                    decimal taxRate = decimal.Zero;
                    decimal shoppingCartUnitPriceWithDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetUnitPrice(sci, true), out taxRate);
                    decimal shoppingCartUnitPriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartUnitPriceWithDiscountBase, _workContext.WorkingCurrency);
                    cartItemModel.UnitPrice = _priceFormatter.FormatPrice(shoppingCartUnitPriceWithDiscount);
                }
                //subtotal, discount
                if (sci.Product.CallForPrice)
                {
                    cartItemModel.SubTotal = _localizationService.GetResource("Products.CallForPrice");
                }
                else
                {
                    //sub total
                    decimal taxRate = decimal.Zero;
                    decimal shoppingCartItemSubTotalWithDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetSubTotal(sci, true, sci.SendToManyAddressSelected ?? false), out taxRate);
                    decimal shoppingCartItemSubTotalWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartItemSubTotalWithDiscountBase, _workContext.WorkingCurrency);
                    cartItemModel.SubTotal = _priceFormatter.FormatPrice(shoppingCartItemSubTotalWithDiscount);

                    //display an applied discount amount
                    decimal shoppingCartItemSubTotalWithoutDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetSubTotal(sci, false, sci.SendToManyAddressSelected ?? false), out taxRate);
                    decimal shoppingCartItemDiscountBase = shoppingCartItemSubTotalWithoutDiscountBase - shoppingCartItemSubTotalWithDiscountBase;
                    if (shoppingCartItemDiscountBase > decimal.Zero)
                    {
                        decimal shoppingCartItemDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartItemDiscountBase, _workContext.WorkingCurrency);
                        cartItemModel.Discount = _priceFormatter.FormatPrice(shoppingCartItemDiscount);
                    }
                }

                //picture
                if (_shoppingCartSettings.ShowProductImagesOnShoppingCart)
                {
                    cartItemModel.Picture = PrepareCartItemPictureModel(sci,
                        _mediaSettings.CartThumbPictureSize, true, cartItemModel.ProductName);
                }

                //item warnings
                var itemWarnings = _shoppingCartService.GetShoppingCartItemWarnings(_workContext.CurrentCustomer,
                            sci.ShoppingCartType,
                            sci.Product,
                            sci.StoreId,
                            sci.AttributesXml,
                            sci.CustomerEnteredPrice,
                            sci.Quantity, false);
                foreach (var warning in itemWarnings)
                    cartItemModel.Warnings.Add(warning);

                model.Items.Add(cartItemModel);
            }

            #endregion
        }

        protected void PrepareSaveforlaterModel(SaveforlaterModel model,
            IList<ShoppingCartItem> cart, bool isEditable = true)
        {
            if (cart == null)
                throw new ArgumentNullException("cart");

            if (model == null)
                throw new ArgumentNullException("model");

            model.IsEditable = isEditable;
            model.DisplayAddToCart = _permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart);

            if (cart.Count == 0)
                return;

            #region Simple properties

            var customer = cart.FirstOrDefault().Customer;
            model.CustomerGuid = customer.CustomerGuid;
            model.CustomerFullname = customer.GetFullName();
            model.ShowProductImages = _shoppingCartSettings.ShowProductImagesOnShoppingCart;
            model.ShowSku = _catalogSettings.ShowProductSku;

            //cart warnings
            var cartWarnings = _shoppingCartService.GetShoppingCartWarnings(cart, "", false);
            foreach (var warning in cartWarnings)
                model.Warnings.Add(warning);

            #endregion

            #region Cart items

            foreach (var sci in cart)
            {
                var cartItemModel = new SaveforlaterModel.ShoppingCartItemModel()
                {
                    Id = sci.Id,
                    Sku = sci.Product.FormatSku(sci.AttributesXml, _productAttributeParser),
                    ProductId = sci.Product.Id,
                    ProductName = sci.Product.GetLocalized(x => x.Name),
                    ProductSeName = sci.Product.GetSeName(),
                    ProductIsCustomizable = sci.Product.IsCustomizable,
                    Quantity = sci.Quantity,
                    AttributeInfo = _productAttributeFormatter.FormatAttributes(sci.Product, sci.AttributesXml),
                };

                //allowed quantities
                var allowedQuantities = sci.Product.ParseAllowedQuatities();
                foreach (var qty in allowedQuantities)
                {
                    cartItemModel.AllowedQuantities.Add(new SelectListItem()
                    {
                        Text = qty.ToString(),
                        Value = qty.ToString(),
                        Selected = sci.Quantity == qty
                    });
                }


                //recurring info
                if (sci.Product.IsRecurring)
                    cartItemModel.RecurringInfo = string.Format(_localizationService.GetResource("ShoppingCart.RecurringPeriod"), sci.Product.RecurringCycleLength, sci.Product.RecurringCyclePeriod.GetLocalizedEnum(_localizationService, _workContext));

                //unit prices
                if (sci.Product.CallForPrice)
                {
                    cartItemModel.UnitPrice = _localizationService.GetResource("Products.CallForPrice");
                }
                else
                {
                    decimal taxRate = decimal.Zero;
                    decimal shoppingCartUnitPriceWithDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetUnitPrice(sci, true), out taxRate);
                    decimal shoppingCartUnitPriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartUnitPriceWithDiscountBase, _workContext.WorkingCurrency);
                    cartItemModel.UnitPrice = _priceFormatter.FormatPrice(shoppingCartUnitPriceWithDiscount);
                }
                //subtotal, discount
                if (sci.Product.CallForPrice)
                {
                    cartItemModel.SubTotal = _localizationService.GetResource("Products.CallForPrice");
                }
                else
                {
                    //sub total
                    decimal taxRate = decimal.Zero;
                    decimal shoppingCartItemSubTotalWithDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetSubTotal(sci, true, sci.SendToManyAddressSelected ?? false), out taxRate);
                    decimal shoppingCartItemSubTotalWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartItemSubTotalWithDiscountBase, _workContext.WorkingCurrency);
                    cartItemModel.SubTotal = _priceFormatter.FormatPrice(shoppingCartItemSubTotalWithDiscount);

                    //display an applied discount amount
                    decimal shoppingCartItemSubTotalWithoutDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetSubTotal(sci, false, sci.SendToManyAddressSelected ?? false), out taxRate);
                    decimal shoppingCartItemDiscountBase = shoppingCartItemSubTotalWithoutDiscountBase - shoppingCartItemSubTotalWithDiscountBase;
                    if (shoppingCartItemDiscountBase > decimal.Zero)
                    {
                        decimal shoppingCartItemDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartItemDiscountBase, _workContext.WorkingCurrency);
                        cartItemModel.Discount = _priceFormatter.FormatPrice(shoppingCartItemDiscount);
                    }
                }

                //picture
                if (_shoppingCartSettings.ShowProductImagesOnShoppingCart)
                {
                    cartItemModel.Picture = PrepareCartItemPictureModel(sci,
                        _mediaSettings.CartThumbPictureSize, true, cartItemModel.ProductName);
                }

                //item warnings
                var itemWarnings = _shoppingCartService.GetShoppingCartItemWarnings(_workContext.CurrentCustomer,
                            sci.ShoppingCartType,
                            sci.Product,
                            sci.StoreId,
                            sci.AttributesXml,
                            sci.CustomerEnteredPrice,
                            sci.Quantity, false);
                foreach (var warning in itemWarnings)
                    cartItemModel.Warnings.Add(warning);

                model.Items.Add(cartItemModel);
            }

            #endregion
        }

        [NonAction]
        protected MiniShoppingCartModel PrepareMiniShoppingCartModel()
        {
            var model = new MiniShoppingCartModel()
            {
                ShowProductImages = _shoppingCartSettings.ShowProductImagesInMiniShoppingCart,
                //let's always display it
                DisplayShoppingCartButton = true,
                CurrentCustomerIsGuest = _workContext.CurrentCustomer.IsGuest(),
                AnonymousCheckoutAllowed = _orderSettings.AnonymousCheckoutAllowed,
            };

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            model.TotalProducts = cart.GetTotalProducts();
            if (cart.Count > 0)
            {
                //subtotal
                decimal subtotalBase = decimal.Zero;
                decimal orderSubTotalDiscountAmountBase = decimal.Zero;
                Discount orderSubTotalAppliedDiscount = null;
                decimal subTotalWithoutDiscountBase = decimal.Zero;
                decimal subTotalWithDiscountBase = decimal.Zero;
                var subTotalIncludingTax = _workContext.TaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal;
                _orderTotalCalculationService.GetShoppingCartSubTotal(cart, subTotalIncludingTax,
                    out orderSubTotalDiscountAmountBase, out orderSubTotalAppliedDiscount,
                    out subTotalWithoutDiscountBase, out subTotalWithDiscountBase);
                subtotalBase = subTotalWithoutDiscountBase;
                decimal subtotal = _currencyService.ConvertFromPrimaryStoreCurrency(subtotalBase, _workContext.WorkingCurrency);
                model.SubTotal = _priceFormatter.FormatPrice(subtotal, false, _workContext.WorkingCurrency, _workContext.WorkingLanguage, subTotalIncludingTax);

                var requiresShipping = cart.RequiresShipping();
                //a customer should visit the shopping cart page (hide checkout button) before going to checkout if:
                //1. "terms of service" are enabled
                //2. min order sub-total is OK
                //3. we have at least one checkout attribute
                var checkoutAttributesExistCacheKey = string.Format(ModelCacheEventConsumer.CHECKOUTATTRIBUTES_EXIST_KEY,
                    _storeContext.CurrentStore.Id, requiresShipping);
                var checkoutAttributesExist = _cacheManager.Get(checkoutAttributesExistCacheKey,
                    () =>
                    {
                        var checkoutAttributes = _checkoutAttributeService.GetAllCheckoutAttributes(_storeContext.CurrentStore.Id);
                        if (!requiresShipping)
                        {
                            //remove attributes which require shippable products
                            checkoutAttributes = checkoutAttributes.RemoveShippableAttributes();
                        }
                        return checkoutAttributes.Count > 0;
                    });

                bool minOrderSubtotalAmountOk = _orderProcessingService.ValidateMinOrderSubtotalAmount(cart);
                model.DisplayCheckoutButton = !_orderSettings.TermsOfServiceOnShoppingCartPage &&
                    minOrderSubtotalAmountOk &&
                    !checkoutAttributesExist;

                //products. sort descending (recently added products)
                foreach (var sci in cart
                    .OrderByDescending(x => x.Id)
                    .Take(_shoppingCartSettings.MiniShoppingCartProductNumber)
                    .ToList())
                {
                    var cartItemModel = new MiniShoppingCartModel.ShoppingCartItemModel()
                    {
                        Id = sci.Id,
                        ProductId = sci.Product.Id,
                        ProductName = sci.Product.GetLocalized(x => x.Name),
                        ProductSeName = sci.Product.GetSeName(),
                        ProductIsCustomizable = sci.Product.IsCustomizable,
                        Quantity = sci.Quantity,
                        AttributeInfo = _productAttributeFormatter.FormatAttributes(sci.Product, sci.AttributesXml)
                    };

                    //unit prices
                    if (sci.Product.CallForPrice)
                    {
                        cartItemModel.UnitPrice = _localizationService.GetResource("Products.CallForPrice");
                    }
                    else
                    {
                        decimal taxRate = decimal.Zero;
                        decimal shoppingCartUnitPriceWithDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetUnitPrice(sci, true), out taxRate);
                        decimal shoppingCartUnitPriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartUnitPriceWithDiscountBase, _workContext.WorkingCurrency);
                        cartItemModel.UnitPrice = _priceFormatter.FormatPrice(shoppingCartUnitPriceWithDiscount);
                    }

                    //picture
                    if (_shoppingCartSettings.ShowProductImagesInMiniShoppingCart)
                    {
                        cartItemModel.Picture = PrepareCartItemPictureModel(sci,
                            _mediaSettings.MiniCartThumbPictureSize, true, cartItemModel.ProductName);
                    }

                    model.Items.Add(cartItemModel);
                }
            }

            return model;
        }

        [NonAction]
        protected void ParseAndSaveCheckoutAttributes(List<ShoppingCartItem> cart, FormCollection form)
        {
            if (cart == null)
                throw new ArgumentNullException("cart");

            if (form == null)
                throw new ArgumentNullException("form");

            string selectedAttributes = "";
            var checkoutAttributes = _checkoutAttributeService.GetAllCheckoutAttributes(_storeContext.CurrentStore.Id);
            if (!cart.RequiresShipping())
            {
                //remove attributes which require shippable products
                checkoutAttributes = checkoutAttributes.RemoveShippableAttributes();
            }
            foreach (var attribute in checkoutAttributes)
            {
                string controlId = string.Format("checkout_attribute_{0}", attribute.Id);
                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                    case AttributeControlType.RadioList:
                    case AttributeControlType.ColorSquares:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(ctrlAttributes))
                            {
                                int selectedAttributeId = int.Parse(ctrlAttributes);
                                if (selectedAttributeId > 0)
                                    selectedAttributes = _checkoutAttributeParser.AddCheckoutAttribute(selectedAttributes,
                                        attribute, selectedAttributeId.ToString());
                            }
                        }
                        break;
                    case AttributeControlType.Checkboxes:
                        {
                            var cblAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(cblAttributes))
                            {
                                foreach (var item in cblAttributes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                                {
                                    int selectedAttributeId = int.Parse(item);
                                    if (selectedAttributeId > 0)
                                        selectedAttributes = _checkoutAttributeParser.AddCheckoutAttribute(selectedAttributes,
                                            attribute, selectedAttributeId.ToString());
                                }
                            }
                        }
                        break;
                    case AttributeControlType.TextBox:
                    case AttributeControlType.MultilineTextbox:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(ctrlAttributes))
                            {
                                string enteredText = ctrlAttributes.Trim();
                                selectedAttributes = _checkoutAttributeParser.AddCheckoutAttribute(selectedAttributes,
                                    attribute, enteredText);
                            }
                        }
                        break;
                    case AttributeControlType.Datepicker:
                        {
                            var date = form[controlId + "_Day"];
                            var month = form[controlId + "_Month"];
                            var year = form[controlId + "_Year"];
                            DateTime? selectedDate = null;
                            try
                            {
                                selectedDate = new DateTime(Int32.Parse(year), Int32.Parse(month), Int32.Parse(date));
                            }
                            catch { }
                            if (selectedDate.HasValue)
                            {
                                selectedAttributes = _checkoutAttributeParser.AddCheckoutAttribute(selectedAttributes,
                                    attribute, selectedDate.Value.ToString("D"));
                            }
                        }
                        break;
                    case AttributeControlType.FileUpload:
                        {
                            Guid downloadGuid;
                            Guid.TryParse(form[controlId], out downloadGuid);
                            var download = _downloadService.GetDownloadByGuid(downloadGuid);
                            if (download != null)
                            {
                                selectedAttributes = _checkoutAttributeParser.AddCheckoutAttribute(selectedAttributes,
                                           attribute, download.DownloadGuid.ToString());
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            //save checkout attributes
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.CheckoutAttributes, selectedAttributes, _storeContext.CurrentStore.Id);
        }

        /// <summary>
        /// Parse product attributes on the product details page
        /// </summary>
        /// <param name="product">Product</param>
        /// <param name="form">Form</param>
        /// <returns>Parsed attributes</returns>
        [NonAction]
        protected string ParseProductAttributes(Product product, FormCollection form)
        {
            string attributes = "";

            #region Product attributes
            string selectedAttributes = string.Empty;
            var productVariantAttributes = _productAttributeService.GetProductVariantAttributesByProductId(product.Id);
            foreach (var attribute in productVariantAttributes)
            {
                string controlId = string.Format("product_attribute_{0}_{1}_{2}", attribute.ProductId, attribute.ProductAttributeId, attribute.Id);
                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                    case AttributeControlType.RadioList:
                    case AttributeControlType.ColorSquares:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(ctrlAttributes))
                            {
                                int selectedAttributeId = int.Parse(ctrlAttributes);
                                if (selectedAttributeId > 0)
                                    selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
                                        attribute, selectedAttributeId.ToString());
                            }
                        }
                        break;
                    case AttributeControlType.Checkboxes:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(ctrlAttributes))
                            {
                                foreach (var item in ctrlAttributes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                                {
                                    int selectedAttributeId = int.Parse(item);
                                    if (selectedAttributeId > 0)
                                        selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
                                            attribute, selectedAttributeId.ToString());
                                }
                            }
                        }
                        break;
                    case AttributeControlType.TextBox:
                    case AttributeControlType.MultilineTextbox:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(ctrlAttributes))
                            {
                                string enteredText = ctrlAttributes.Trim();
                                selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
                                    attribute, enteredText);
                            }
                        }
                        break;
                    case AttributeControlType.Datepicker:
                        {
                            var day = form[controlId + "_day"];
                            var month = form[controlId + "_month"];
                            var year = form[controlId + "_year"];
                            DateTime? selectedDate = null;
                            try
                            {
                                selectedDate = new DateTime(Int32.Parse(year), Int32.Parse(month), Int32.Parse(day));
                            }
                            catch { }
                            if (selectedDate.HasValue)
                            {
                                selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
                                    attribute, selectedDate.Value.ToString("D"));
                            }
                        }
                        break;
                    case AttributeControlType.FileUpload:
                        {
                            Guid downloadGuid;
                            Guid.TryParse(form[controlId], out downloadGuid);
                            var download = _downloadService.GetDownloadByGuid(downloadGuid);
                            if (download != null)
                            {
                                selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
                                        attribute, download.DownloadGuid.ToString());
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            attributes = selectedAttributes;

            #endregion

            #region Gift cards

            if (product.IsGiftCard)
            {
                string recipientName = "";
                string recipientEmail = "";
                string senderName = "";
                string senderEmail = "";
                string giftCardMessage = "";
                foreach (string formKey in form.AllKeys)
                {
                    if (formKey.Equals(string.Format("giftcard_{0}.RecipientName", product.Id), StringComparison.InvariantCultureIgnoreCase))
                    {
                        recipientName = form[formKey];
                        continue;
                    }
                    if (formKey.Equals(string.Format("giftcard_{0}.RecipientEmail", product.Id), StringComparison.InvariantCultureIgnoreCase))
                    {
                        recipientEmail = form[formKey];
                        continue;
                    }
                    if (formKey.Equals(string.Format("giftcard_{0}.SenderName", product.Id), StringComparison.InvariantCultureIgnoreCase))
                    {
                        senderName = form[formKey];
                        continue;
                    }
                    if (formKey.Equals(string.Format("giftcard_{0}.SenderEmail", product.Id), StringComparison.InvariantCultureIgnoreCase))
                    {
                        senderEmail = form[formKey];
                        continue;
                    }
                    if (formKey.Equals(string.Format("giftcard_{0}.Message", product.Id), StringComparison.InvariantCultureIgnoreCase))
                    {
                        giftCardMessage = form[formKey];
                        continue;
                    }
                }

                attributes = _productAttributeParser.AddGiftCardAttribute(attributes,
                    recipientName, recipientEmail, senderName, senderEmail, giftCardMessage);
            }

            #endregion

            return attributes;
        }

        #endregion

        #region Shopping cart

        //add product to cart using AJAX
        //currently we use this method on catalog pages (category/manufacturer/etc)
        [HttpPost]
        public ActionResult AddProductToCart_Catalog(int productId, int shoppingCartTypeId,
            int quantity, bool forceredirection = false)
        {
            var cartType = (ShoppingCartType)shoppingCartTypeId;

            var product = _productService.GetProductById(productId);
            if (product == null)
                //no product found
                return Json(new
                {
                    success = false,
                    message = "No product found with the specified ID"
                });

            //we can add only simple products
            if (product.ProductType != ProductType.SimpleProduct)
            {
                return Json(new
                {
                    redirect = Url.RouteUrl("Product", new { SeName = product.GetSeName() }),
                });
            }

            if (product.CustomerEntersPrice)
            {
                //cannot be added to the cart (requires a customer to enter price)
                return Json(new
                {
                    redirect = Url.RouteUrl("Product", new { SeName = product.GetSeName() }),
                });
            }

            var allowedQuantities = product.ParseAllowedQuatities();
            if (allowedQuantities.Length > 0)
            {
                //cannot be added to the cart (requires a customer to select a quantity from dropdownlist)
                return Json(new
                {
                    redirect = Url.RouteUrl("Product", new { SeName = product.GetSeName() }),
                });
            }

            //get standard warnings without attribute validations
            //first, try to find existing shopping cart item
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == cartType)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var shoppingCartItem = _shoppingCartService.FindShoppingCartItemInTheCart(cart, cartType, product);
            //if we already have the same product in the cart, then use the total quantity to validate
            var quantityToValidate = shoppingCartItem != null ? shoppingCartItem.Quantity + quantity : quantity;
            var addToCartWarnings = _shoppingCartService
                .GetShoppingCartItemWarnings(_workContext.CurrentCustomer, cartType,
                product, _storeContext.CurrentStore.Id, string.Empty,
                decimal.Zero, quantityToValidate, false, true, false, false, false);
            if (addToCartWarnings.Count > 0)
            {
                //cannot be added to the cart
                //let's display standard warnings
                return Json(new
                {
                    success = false,
                    message = addToCartWarnings.ToArray()
                });
            }

            //now let's try adding product to the cart (now including product attribute validation, etc)
            addToCartWarnings = _shoppingCartService.AddToCart(_workContext.CurrentCustomer,
                product, cartType, _storeContext.CurrentStore.Id,
                string.Empty, decimal.Zero, quantity, true);
            if (addToCartWarnings.Count > 0)
            {
                //cannot be added to the cart
                //but we do not display attribute and gift card warnings here. let's do it on the product details page
                return Json(new
                {
                    redirect = Url.RouteUrl("Product", new { SeName = product.GetSeName() }),
                });
            }

            //added to the cart/wishlist
            switch (cartType)
            {
                case ShoppingCartType.Wishlist:
                    {
                        //activity log
                        _customerActivityService.InsertActivity("PublicStore.AddToWishlist", _localizationService.GetResource("ActivityLog.PublicStore.AddToWishlist"), product.Name);

                        if (_shoppingCartSettings.DisplayWishlistAfterAddingProduct || forceredirection)
                        {
                            //redirect to the wishlist page
                            return Json(new
                            {
                                redirect = Url.RouteUrl("Wishlist"),
                            });
                        }
                        else
                        {
                            //display notification message and update appropriate blocks
                            var updatetopwishlistsectionhtml = string.Format(_localizationService.GetResource("Wishlist.HeaderQuantity"),
                                 _workContext.CurrentCustomer.ShoppingCartItems
                                 .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
                                 .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                                 .ToList()
                                 .GetTotalProducts());
                            return Json(new
                            {
                                success = true,
                                message = string.Format(_localizationService.GetResource("Products.ProductHasBeenAddedToTheWishlist.Link"), Url.RouteUrl("Wishlist")),
                                updatetopwishlistsectionhtml = updatetopwishlistsectionhtml,
                            });
                        }
                    }
                case ShoppingCartType.ShoppingCart:
                default:
                    {
                        //activity log
                        _customerActivityService.InsertActivity("PublicStore.AddToShoppingCart", _localizationService.GetResource("ActivityLog.PublicStore.AddToShoppingCart"), product.Name);

                        if (_shoppingCartSettings.DisplayCartAfterAddingProduct || forceredirection)
                        {
                            //redirect to the shopping cart page
                            return Json(new
                            {
                                redirect = Url.RouteUrl("ShoppingCart"),
                            });
                        }
                        else
                        {

                            //display notification message and update appropriate blocks
                            var updatetopcartsectionhtml = string.Format(_localizationService.GetResource("ShoppingCart.HeaderQuantity"),
                                 _workContext.CurrentCustomer.ShoppingCartItems
                                 .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                                 .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                                 .ToList()
                                 .GetTotalProducts());
                            var updateflyoutcartsectionhtml = _shoppingCartSettings.MiniShoppingCartEnabled
                                ? this.RenderPartialViewToString("FlyoutShoppingCart", PrepareMiniShoppingCartModel())
                                : "";

                            return Json(new
                            {
                                success = true,
                                message = string.Format(_localizationService.GetResource("Products.ProductHasBeenAddedToTheCart.Link"), Url.RouteUrl("ShoppingCart")),
                                updatetopcartsectionhtml = updatetopcartsectionhtml,
                                updateflyoutcartsectionhtml = updateflyoutcartsectionhtml
                            });
                        }
                    }
            }
        }

        //add product to cart using AJAX
        //currently we use this method on the product details pages
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddProductToCart_Details(int productId, int shoppingCartTypeId, FormCollection form)
        {
            var product = _productService.GetProductById(productId);
            if (product == null)
            {
                return Json(new
                {
                    redirect = Url.RouteUrl("HomePage"),
                });
            }

            //we can add only simple products
            if (product.ProductType != ProductType.SimpleProduct)
            {
                return Json(new
                {
                    success = false,
                    message = "Only simple products could be added to the cart"
                });
            }

            #region Update existing shopping cart item?
            int updatecartitemid = 0;
            foreach (string formKey in form.AllKeys)
                if (formKey.Equals(string.Format("addtocart_{0}.UpdatedShoppingCartItemId", productId), StringComparison.InvariantCultureIgnoreCase))
                {
                    int.TryParse(form[formKey], out updatecartitemid);
                    break;
                }
            ShoppingCartItem updatecartitem = null;
            if (_shoppingCartSettings.AllowCartItemEditing && updatecartitemid > 0)
            {
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(x => x.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();
                updatecartitem = cart.FirstOrDefault(x => x.Id == updatecartitemid);
                //not found?
                if (updatecartitem == null)
                {
                    return Json(new
                    {
                        success = false,
                        message = "No shopping cart item found to update"
                    });
                }
                //is it this product?
                if (product.Id != updatecartitem.ProductId)
                {
                    return Json(new
                    {
                        success = false,
                        message = "This product does not match a passed shopping cart item identifier"
                    });
                }
            }
            #endregion

            #region Customer entered price
            decimal customerEnteredPriceConverted = decimal.Zero;
            if (product.CustomerEntersPrice)
            {
                foreach (string formKey in form.AllKeys)
                {
                    if (formKey.Equals(string.Format("addtocart_{0}.CustomerEnteredPrice", productId), StringComparison.InvariantCultureIgnoreCase))
                    {
                        decimal customerEnteredPrice = decimal.Zero;
                        if (decimal.TryParse(form[formKey], out customerEnteredPrice))
                            customerEnteredPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(customerEnteredPrice, _workContext.WorkingCurrency);
                        break;
                    }
                }
            }
            #endregion

            #region Quantity

            int quantity = 1;
            foreach (string formKey in form.AllKeys)
                if (formKey.Equals(string.Format("addtocart_{0}.EnteredQuantity", productId), StringComparison.InvariantCultureIgnoreCase))
                {
                    int.TryParse(form[formKey], out quantity);
                    break;
                }

            #endregion

            string attributes = ParseProductAttributes(product, form);

            //save item
            var addToCartWarnings = new List<string>();
            var cartType = (ShoppingCartType)shoppingCartTypeId;
            if (updatecartitem == null)
            {
                //add to the cart
                addToCartWarnings.AddRange(_shoppingCartService.AddToCart(_workContext.CurrentCustomer,
                    product, cartType, _storeContext.CurrentStore.Id,
                    attributes, customerEnteredPriceConverted, quantity, true));
            }
            else
            {
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(x => x.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();
                var otherCartItemWithSameParameters = _shoppingCartService.FindShoppingCartItemInTheCart(
                    cart, cartType, product, attributes, customerEnteredPriceConverted);
                if (otherCartItemWithSameParameters != null &&
                    otherCartItemWithSameParameters.Id == updatecartitem.Id)
                {
                    //ensure it's other shopping cart cart item
                    otherCartItemWithSameParameters = null;
                }
                //update existing item
                addToCartWarnings.AddRange(_shoppingCartService.UpdateShoppingCartItem(_workContext.CurrentCustomer,
                    updatecartitem.Id, attributes, customerEnteredPriceConverted, quantity, true));
                if (otherCartItemWithSameParameters != null && addToCartWarnings.Count == 0)
                {
                    //delete the same shopping cart item
                    _shoppingCartService.DeleteShoppingCartItem(otherCartItemWithSameParameters);
                }
            }

            #region Return result

            if (addToCartWarnings.Count > 0)
            {
                //cannot be added to the cart/wishlist
                //let's display warnings
                return Json(new
                {
                    success = false,
                    message = addToCartWarnings.ToArray()
                });
            }

            //added to the cart/wishlist
            switch (cartType)
            {
                case ShoppingCartType.Wishlist:
                    {
                        //activity log
                        _customerActivityService.InsertActivity("PublicStore.AddToWishlist", _localizationService.GetResource("ActivityLog.PublicStore.AddToWishlist"), product.Name);

                        if (_shoppingCartSettings.DisplayWishlistAfterAddingProduct)
                        {
                            //redirect to the wishlist page
                            return Json(new
                            {
                                redirect = Url.RouteUrl("Wishlist"),
                            });
                        }
                        else
                        {
                            //display notification message and update appropriate blocks
                            var updatetopwishlistsectionhtml = string.Format(_localizationService.GetResource("Wishlist.HeaderQuantity"),
                                 _workContext.CurrentCustomer.ShoppingCartItems
                                 .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
                                 .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                                 .ToList()
                                 .GetTotalProducts());
                            return Json(new
                            {
                                success = true,
                                message = string.Format(_localizationService.GetResource("Products.ProductHasBeenAddedToTheWishlist.Link"), Url.RouteUrl("Wishlist")),
                                updatetopwishlistsectionhtml = updatetopwishlistsectionhtml,
                            });
                        }
                    }
                case ShoppingCartType.ShoppingCart:
                default:
                    {
                        //activity log
                        _customerActivityService.InsertActivity("PublicStore.AddToShoppingCart", _localizationService.GetResource("ActivityLog.PublicStore.AddToShoppingCart"), product.Name);

                        if (_shoppingCartSettings.DisplayCartAfterAddingProduct)
                        {
                            //redirect to the shopping cart page
                            return Json(new
                            {
                                redirect = Url.RouteUrl("ShoppingCart"),
                            });
                        }
                        else
                        {

                            //display notification message and update appropriate blocks
                            var updatetopcartsectionhtml = string.Format(_localizationService.GetResource("ShoppingCart.HeaderQuantity"),
                                 _workContext.CurrentCustomer.ShoppingCartItems
                                 .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                                 .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                                 .ToList()
                                 .GetTotalProducts());
                            var updateflyoutcartsectionhtml = _shoppingCartSettings.MiniShoppingCartEnabled
                                ? this.RenderPartialViewToString("FlyoutShoppingCart", PrepareMiniShoppingCartModel())
                                : "";

                            return Json(new
                            {
                                success = true,
                                message = string.Format(_localizationService.GetResource("Products.ProductHasBeenAddedToTheCart.Link"), Url.RouteUrl("ShoppingCart")),
                                updatetopcartsectionhtml = updatetopcartsectionhtml,
                                updateflyoutcartsectionhtml = updateflyoutcartsectionhtml
                            });
                        }
                    }
            }


            #endregion
        }

        public ActionResult CustomizeProduct(int productId)
        {
            var model = new CustomizeProductModel();
            model.ProductId = productId;

            model.Product = _productService.GetProductById(productId);

            model.CustomerId = _workContext.CurrentCustomer.Id;
            model.ShoppingCartItemId = 0;
            model.IsEditing = false;

            model.IsMobileDevice = Request.Browser.IsMobileDevice;

            return View(model);
        }

        //TODO: needing new flag to differentiate cart edit than order edit
        public ActionResult EditProductCustomization(int cartItemId)
        {
            var model = new CustomizeProductModel();
            model.ProductId = 0;

            var cartItem = _workContext.CurrentCustomer.ShoppingCartItems.Where(x => x.Id == cartItemId).FirstOrDefault();
            model.Product = cartItem.Product;

            model.CustomerId = _workContext.CurrentCustomer.Id;
            model.ShoppingCartItemId = cartItemId;
            model.IsEditing = true;

            model.IsMobileDevice = Request.Browser.IsMobileDevice;

            return View("CustomizeProduct", model);
        }

        [HttpPost]
        public ActionResult GetTopCartHtml()
        {
            //display notification message and update appropriate blocks
            var updatetopcartsectionhtml = string.Format(_localizationService.GetResource("ShoppingCart.HeaderQuantity"),
                    _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList()
                    .GetTotalProducts());
            var updateflyoutcartsectionhtml = _shoppingCartSettings.MiniShoppingCartEnabled
                ? this.RenderPartialViewToString("FlyoutShoppingCart", PrepareMiniShoppingCartModel())
                : "";

            return Json(new
            {
                success = true,
                message = string.Format(_localizationService.GetResource("Products.ProductHasBeenAddedToTheCart.Link"), Url.RouteUrl("ShoppingCart")),
                updatetopcartsectionhtml = updatetopcartsectionhtml,
                updateflyoutcartsectionhtml = updateflyoutcartsectionhtml
            });
        }

        //handle product attribute selection event. this way we return new price, overriden gtin/sku/mpn
        //currently we use this method on the product details pages
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ProductDetails_AttributeChange(int productId, FormCollection form)
        {
            var product = _productService.GetProductById(productId);
            if (product == null)
                return new NullJsonResult();

            string attributeXml = ParseProductAttributes(product, form);

            string sku = product.FormatSku(attributeXml, _productAttributeParser);
            string mpn = product.FormatMpn(attributeXml, _productAttributeParser);
            string gtin = product.FormatGtin(attributeXml, _productAttributeParser);

            return Json(new
            {
                gtin = gtin,
                mpn = mpn,
                sku = sku
            });
        }

        [HttpPost]
        public ActionResult UploadFileProductAttribute(int attributeId)
        {
            var attribute = _productAttributeService.GetProductVariantAttributeById(attributeId);
            if (attribute == null || attribute.AttributeControlType != AttributeControlType.FileUpload)
            {
                return Json(new
                {
                    success = false,
                    downloadGuid = Guid.Empty,
                }, "text/plain");
            }

            //we process it distinct ways based on a browser
            //find more info here http://stackoverflow.com/questions/4884920/mvc3-valums-ajax-file-upload
            Stream stream = null;
            var fileName = "";
            var contentType = "";
            if (String.IsNullOrEmpty(Request["qqfile"]))
            {
                // IE
                HttpPostedFileBase httpPostedFile = Request.Files[0];
                if (httpPostedFile == null)
                    throw new ArgumentException("No file uploaded");
                stream = httpPostedFile.InputStream;
                fileName = Path.GetFileName(httpPostedFile.FileName);
                contentType = httpPostedFile.ContentType;
            }
            else
            {
                //Webkit, Mozilla
                stream = Request.InputStream;
                fileName = Request["qqfile"];
            }

            var fileBinary = new byte[stream.Length];
            stream.Read(fileBinary, 0, fileBinary.Length);

            var fileExtension = Path.GetExtension(fileName);
            if (!String.IsNullOrEmpty(fileExtension))
                fileExtension = fileExtension.ToLowerInvariant();

            if (attribute.ValidationFileMaximumSize.HasValue)
            {
                //compare in bytes
                var maxFileSizeBytes = attribute.ValidationFileMaximumSize.Value * 1024;
                if (fileBinary.Length > maxFileSizeBytes)
                {
                    //when returning JSON the mime-type must be set to text/plain
                    //otherwise some browsers will pop-up a "Save As" dialog.
                    return Json(new
                    {
                        success = false,
                        message = string.Format(_localizationService.GetResource("ShoppingCart.MaximumUploadedFileSize"), attribute.ValidationFileMaximumSize.Value),
                        downloadGuid = Guid.Empty,
                    }, "text/plain");
                }
            }

            var download = new Download()
            {
                DownloadGuid = Guid.NewGuid(),
                UseDownloadUrl = false,
                DownloadUrl = "",
                DownloadBinary = fileBinary,
                ContentType = contentType,
                //we store filename without extension for downloads
                Filename = Path.GetFileNameWithoutExtension(fileName),
                Extension = fileExtension,
                IsNew = true
            };
            _downloadService.InsertDownload(download);

            //when returning JSON the mime-type must be set to text/plain
            //otherwise some browsers will pop-up a "Save As" dialog.
            return Json(new
            {
                success = true,
                message = _localizationService.GetResource("ShoppingCart.FileUploaded"),
                downloadGuid = download.DownloadGuid,
            }, "text/plain");
        }

        [HttpPost]
        public ActionResult UploadFileCheckoutAttribute(int attributeId)
        {
            var attribute = _checkoutAttributeService.GetCheckoutAttributeById(attributeId);
            if (attribute == null || attribute.AttributeControlType != AttributeControlType.FileUpload)
            {
                return Json(new
                {
                    success = false,
                    downloadGuid = Guid.Empty,
                }, "text/plain");
            }

            //we process it distinct ways based on a browser
            //find more info here http://stackoverflow.com/questions/4884920/mvc3-valums-ajax-file-upload
            Stream stream = null;
            var fileName = "";
            var contentType = "";
            if (String.IsNullOrEmpty(Request["qqfile"]))
            {
                // IE
                HttpPostedFileBase httpPostedFile = Request.Files[0];
                if (httpPostedFile == null)
                    throw new ArgumentException("No file uploaded");
                stream = httpPostedFile.InputStream;
                fileName = Path.GetFileName(httpPostedFile.FileName);
                contentType = httpPostedFile.ContentType;
            }
            else
            {
                //Webkit, Mozilla
                stream = Request.InputStream;
                fileName = Request["qqfile"];
            }

            var fileBinary = new byte[stream.Length];
            stream.Read(fileBinary, 0, fileBinary.Length);

            var fileExtension = Path.GetExtension(fileName);
            if (!String.IsNullOrEmpty(fileExtension))
                fileExtension = fileExtension.ToLowerInvariant();

            if (attribute.ValidationFileMaximumSize.HasValue)
            {
                //compare in bytes
                var maxFileSizeBytes = attribute.ValidationFileMaximumSize.Value * 1024;
                if (fileBinary.Length > maxFileSizeBytes)
                {
                    //when returning JSON the mime-type must be set to text/plain
                    //otherwise some browsers will pop-up a "Save As" dialog.
                    return Json(new
                    {
                        success = false,
                        message = string.Format(_localizationService.GetResource("ShoppingCart.MaximumUploadedFileSize"), attribute.ValidationFileMaximumSize.Value),
                        downloadGuid = Guid.Empty,
                    }, "text/plain");
                }
            }

            var download = new Download()
            {
                DownloadGuid = Guid.NewGuid(),
                UseDownloadUrl = false,
                DownloadUrl = "",
                DownloadBinary = fileBinary,
                ContentType = contentType,
                //we store filename without extension for downloads
                Filename = Path.GetFileNameWithoutExtension(fileName),
                Extension = fileExtension,
                IsNew = true
            };
            _downloadService.InsertDownload(download);

            //when returning JSON the mime-type must be set to text/plain
            //otherwise some browsers will pop-up a "Save As" dialog.
            return Json(new
            {
                success = true,
                message = _localizationService.GetResource("ShoppingCart.FileUploaded"),
                downloadGuid = download.DownloadGuid,
            }, "text/plain");
        }


        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult Cart()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart))
                return RedirectToRoute("HomePage");

            //intializing selection of shipping options
            //_workContext.CurrentCustomer.ShippingMethodsAttributes = "";
            //_customerService.UpdateCustomer(_workContext.CurrentCustomer);

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var model = new ShoppingCartModel();
            PrepareShoppingCartModel(model, cart);
            return View(model);
        }

        [ChildActionOnly]
        public ActionResult OrderSummary(bool? prepareAndDisplayOrderReviewData)
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var model = new ShoppingCartModel();
            PrepareShoppingCartModel(model, cart,
                isEditable: false,
                prepareEstimateShippingIfEnabled: false,
                prepareAndDisplayOrderReviewData: prepareAndDisplayOrderReviewData.HasValue ? prepareAndDisplayOrderReviewData.Value : false);
            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult OpcSummary(int customerId, bool? prepareAndDisplayOrderReviewData, int cartTypeId = 1)
        {
            var customer = _customerService.GetCustomerById(customerId);

            var cartType = (ShoppingCartType)cartTypeId;
            var cart = customer.ShoppingCartItems//_workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == cartType)//ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var model = new ShoppingCartModel();
            PrepareShoppingCartModel(model, cart,
                isEditable: false,
                prepareEstimateShippingIfEnabled: false,
                prepareAndDisplayOrderReviewData: prepareAndDisplayOrderReviewData.HasValue ? prepareAndDisplayOrderReviewData.Value : false);
            model.CartTypeId = cartTypeId;
            bool? SendToManyAddressSelected = _workContext.CurrentCustomer.SendToManyAddressSelected;

            bool OnlyCustomizedProducts = true;
            foreach (var item in cart)
            {
                if (!item.Product.IsCustomizable)
                {
                    OnlyCustomizedProducts = false;
                    break;
                }
            }

            //single shipping address selected
            if (SendToManyAddressSelected == false && !OnlyCustomizedProducts)
                return PartialView("OpcSummarySingleShipping", model);

            return PartialView(model);
        }

        //update all shopping cart items on the page
        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired("updatecart")]
        public ActionResult UpdateCartAll(FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart))
                return RedirectToRoute("HomePage");

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();

            var allIdsToRemove = form["removefromcart"] != null ? form["removefromcart"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToList() : new List<int>();

            //current warnings <cart item identifier, warnings>
            var innerWarnings = new Dictionary<int, IList<string>>();
            foreach (var sci in cart)
            {
                bool remove = allIdsToRemove.Contains(sci.Id);
                if (remove)
                    _shoppingCartService.DeleteShoppingCartItem(sci, ensureOnlyActiveCheckoutAttributes: true);
                else
                {
                    foreach (string formKey in form.AllKeys)
                        if (formKey.Equals(string.Format("itemquantity{0}", sci.Id), StringComparison.InvariantCultureIgnoreCase))
                        {
                            int newQuantity = sci.Quantity;
                            if (int.TryParse(form[formKey], out newQuantity))
                            {
                                var currSciWarnings = _shoppingCartService.UpdateShoppingCartItem(_workContext.CurrentCustomer,
                                    sci.Id, sci.AttributesXml, sci.CustomerEnteredPrice,
                                    newQuantity, true);
                                innerWarnings.Add(sci.Id, currSciWarnings);
                            }
                            break;
                        }
                }
            }

            //updated cart
            cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var model = new ShoppingCartModel();
            PrepareShoppingCartModel(model, cart);
            //update current warnings
            foreach (var kvp in innerWarnings)
            {
                //kvp = <cart item identifier, warnings>
                var sciId = kvp.Key;
                var warnings = kvp.Value;
                //find model
                var sciModel = model.Items.FirstOrDefault(x => x.Id == sciId);
                if (sciModel != null)
                    foreach (var w in warnings)
                        if (!sciModel.Warnings.Contains(w))
                            sciModel.Warnings.Add(w);
            }
            return View(model);
        }

        //update a certain shopping cart item on the page
        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired(FormValueRequirement.StartsWith, "updatecartitem-")]
        public ActionResult UpdateCartItem(FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart))
                return RedirectToRoute("HomePage");

            //get shopping cart item identifier
            int sciId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("updatecartitem-", StringComparison.InvariantCultureIgnoreCase))
                    sciId = Convert.ToInt32(formValue.Substring("updatecartitem-".Length));
            //get shopping cart item
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(x => x.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var sci = cart.FirstOrDefault(x => x.Id == sciId);
            if (sci == null)
            {
                return RedirectToRoute("ShoppingCart");
            }

            //update the cart item
            var warnings = new List<string>();
            foreach (string formKey in form.AllKeys)
                if (formKey.Equals(string.Format("itemquantity{0}", sci.Id), StringComparison.InvariantCultureIgnoreCase))
                {
                    int newQuantity = sci.Quantity;
                    if (int.TryParse(form[formKey], out newQuantity))
                    {
                        warnings.AddRange(_shoppingCartService.UpdateShoppingCartItem(_workContext.CurrentCustomer,
                            sci.Id, sci.AttributesXml, sci.CustomerEnteredPrice, newQuantity, true));
                    }
                    break;
                }


            //updated cart
            cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(x => x.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var model = new ShoppingCartModel();
            PrepareShoppingCartModel(model, cart);
            //update current warnings
            //find model
            var sciModel = model.Items.FirstOrDefault(x => x.Id == sciId);
            if (sciModel != null)
                foreach (var w in warnings)
                    if (!sciModel.Warnings.Contains(w))
                        sciModel.Warnings.Add(w);
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired("addtosaveforlaterbutton")]
        public ActionResult AddItemstoSaveforlaterFromCart(Guid? customerGuid, FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart))
                return RedirectToRoute("HomePage");

            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist))
                return RedirectToRoute("HomePage");

            var pageCustomer = customerGuid.HasValue
                ? _customerService.GetCustomerByGuid(customerGuid.Value)
                : _workContext.CurrentCustomer;
            if (pageCustomer == null)
                return RedirectToRoute("HomePage");

            var pageCart = pageCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();

            var allWarnings = new List<string>();
            var numberOfAddedItems = 0;
            var allIdsToAdd = form["addtosaveforlater"] != null ? form["addtosaveforlater"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToList() : new List<int>();
            foreach (var sci in pageCart)
            {
                if (allIdsToAdd.Contains(sci.Id))
                {
                    var warnings = _shoppingCartService.AddToCart(_workContext.CurrentCustomer,
                        sci.Product, ShoppingCartType.Saveforlater,
                        _storeContext.CurrentStore.Id,
                        sci.AttributesXml, sci.CustomerEnteredPrice, sci.Quantity, true,
                        sci.ShoppingCartItemAddresses.ToList(), sci.SendToManyAddressSelected);

                    //skip the warning of out of stock to move this item to save for later if its out of stock
                    bool skipOutOfStockWarning = (warnings.Count == 1 && warnings.FirstOrDefault() == _localizationService.GetResource("ShoppingCart.OutOfStock"));

                    if (warnings.Count == 0 || skipOutOfStockWarning)
                        numberOfAddedItems++;
                    if (!customerGuid.HasValue && //own save for later
                        (warnings.Count == 0 || skipOutOfStockWarning)) //no warnings ( already in the cart)
                    {
                        //let's remove the item from shopping cart
                        _shoppingCartService.DeleteShoppingCartItem(sci);
                    }
                    allWarnings.AddRange(warnings);
                }
            }

            if (numberOfAddedItems > 0)
            {
                //redirect to the Saveforlater page
                return RedirectToRoute("Saveforlater");
            }
            else
            {
                //no items added. redisplay the shopping cart page
                var cart = pageCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();
                var model = new ShoppingCartModel();
                PrepareShoppingCartModel(model, cart, !customerGuid.HasValue);
                return View(model);
            }
        }

        //add a certain shopping cart item on the page to the Saveforlater
        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired(FormValueRequirement.StartsWith, "addtosaveforlater-")]
        public ActionResult AddOneItemtoSaveforlaterFromCart(Guid? customerGuid, FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart))
                return RedirectToRoute("HomePage");

            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist))
                return RedirectToRoute("HomePage");

            //get shopping cart item identifier
            int sciId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("addtosaveforlater-", StringComparison.InvariantCultureIgnoreCase))
                    sciId = Convert.ToInt32(formValue.Substring("addtosaveforlater-".Length));
            //get shopping cart item
            var pageCustomer = customerGuid.HasValue
                ? _customerService.GetCustomerByGuid(customerGuid.Value)
                : _workContext.CurrentCustomer;
            if (pageCustomer == null)
                return RedirectToRoute("HomePage");

            var pageCart = pageCustomer.ShoppingCartItems
                .Where(x => x.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                .ToList();

            var sci = pageCart.FirstOrDefault(x => x.Id == sciId);
            if (sci == null)
            {
                return RedirectToRoute("ShoppingCart");
            }
            var warnings = _shoppingCartService.AddToCart(_workContext.CurrentCustomer,
                                           sci.Product, ShoppingCartType.Saveforlater,
                                           _storeContext.CurrentStore.Id,
                                           sci.AttributesXml, sci.CustomerEnteredPrice, sci.Quantity, true,
                                           sci.ShoppingCartItemAddresses.ToList(), sci.SendToManyAddressSelected);

            //skip the warning of out of stock to move this item to save for later if its out of stock
            bool skipOutOfStockWarning = (warnings.Count == 1 && warnings.FirstOrDefault() == _localizationService.GetResource("ShoppingCart.OutOfStock"));

            if (!customerGuid.HasValue && //own save for later
                (warnings.Count == 0 || skipOutOfStockWarning)) //no warnings ( already in the cart)
            {
                //let's remove the item from Shopping Cart
                _shoppingCartService.DeleteShoppingCartItem(sci);
            }

            if (warnings.Count == 0 || skipOutOfStockWarning)
            {
                //redirect to the saveforlater page
                return RedirectToRoute("Saveforlater");
            }
            else
            {
                //no items added. redisplay the wishlist page
                var cart = pageCustomer.ShoppingCartItems
                    .Where(x => x.ShoppingCartType == ShoppingCartType.Saveforlater)
                    .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();
                var model = new ShoppingCartModel();
                PrepareShoppingCartModel(model, cart, !customerGuid.HasValue);
                return View(model);
            }
        }

        //remove a certain shopping cart item on the page
        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired(FormValueRequirement.StartsWith, "removefromcart-")]
        public ActionResult RemoveCartItem(FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart))
                return RedirectToRoute("HomePage");

            //get shopping cart item identifier
            int sciId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("removefromcart-", StringComparison.InvariantCultureIgnoreCase))
                    sciId = Convert.ToInt32(formValue.Substring("removefromcart-".Length));
            //get shopping cart item
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(x => x.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var sci = cart.FirstOrDefault(x => x.Id == sciId);
            if (sci == null)
            {
                return RedirectToRoute("ShoppingCart");
            }

            //remove the cart item
            _shoppingCartService.DeleteShoppingCartItem(sci, ensureOnlyActiveCheckoutAttributes: true);


            //updated cart
            cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(x => x.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var model = new ShoppingCartModel();
            PrepareShoppingCartModel(model, cart);
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired("continueshopping")]
        public ActionResult ContinueShopping()
        {
            string returnUrl = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.LastContinueShoppingPage, _storeContext.CurrentStore.Id);
            if (!String.IsNullOrEmpty(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToRoute("HomePage");
            }
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired("checkout")]
        public ActionResult StartCheckout(FormCollection form)
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();

            //parse and save checkout attributes
            ParseAndSaveCheckoutAttributes(cart, form);

            //validate attributes
            string checkoutAttributes = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.CheckoutAttributes, _genericAttributeService, _storeContext.CurrentStore.Id);
            var checkoutAttributeWarnings = _shoppingCartService.GetShoppingCartWarnings(cart, checkoutAttributes, true);
            if (checkoutAttributeWarnings.Count > 0)
            {
                //something wrong, redisplay the page with warnings
                var model = new ShoppingCartModel();
                PrepareShoppingCartModel(model, cart, validateCheckoutAttributes: true);
                return View(model);
            }

            //everything is OK
            if (_workContext.CurrentCustomer.IsGuest())
            {
                if (_orderSettings.AnonymousCheckoutAllowed)
                {
                    return RedirectToRoute("LoginCheckoutAsGuest", new { returnUrl = Url.RouteUrl("ShoppingCart") });
                }
                else
                {
                    return new HttpUnauthorizedResult();
                }
            }
            else
            {
                return RedirectToRoute("Checkout");
            }
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired("applydiscountcouponcode")]
        public ActionResult ApplyDiscountCoupon(string discountcouponcode, FormCollection form)
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();

            //parse and save checkout attributes
            ParseAndSaveCheckoutAttributes(cart, form);

            var model = new ShoppingCartModel();
            if (!String.IsNullOrWhiteSpace(discountcouponcode))
            {
                var discount = _discountService.GetDiscountByCouponCode(discountcouponcode);
                bool isDiscountValid = discount != null &&
                    discount.RequiresCouponCode &&
                    _discountService.IsDiscountValid(discount, _workContext.CurrentCustomer, discountcouponcode);
                if (isDiscountValid)
                {
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        SystemCustomerAttributeNames.DiscountCouponCode, discountcouponcode);
                    model.DiscountBox.Message = _localizationService.GetResource("ShoppingCart.DiscountCouponCode.Applied");
                }
                else
                {
                    model.DiscountBox.Message = _localizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount");
                }
            }
            else
                model.DiscountBox.Message = _localizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount");

            PrepareShoppingCartModel(model, cart);
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired("applygiftcardcouponcode")]
        public ActionResult ApplyGiftCard(string giftcardcouponcode, FormCollection form)
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();

            //parse and save checkout attributes
            ParseAndSaveCheckoutAttributes(cart, form);

            var model = new ShoppingCartModel();
            if (!cart.IsRecurring())
            {
                if (!String.IsNullOrWhiteSpace(giftcardcouponcode))
                {
                    var giftCard = _giftCardService.GetAllGiftCards(null, null,
                        null, null, giftcardcouponcode, 0, int.MaxValue).FirstOrDefault();
                    bool isGiftCardValid = giftCard != null && giftCard.IsGiftCardValid();
                    if (isGiftCardValid)
                    {
                        _workContext.CurrentCustomer.ApplyGiftCardCouponCode(giftcardcouponcode);
                        _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                        model.GiftCardBox.Message = _localizationService.GetResource("ShoppingCart.GiftCardCouponCode.Applied");
                    }
                    else
                        model.GiftCardBox.Message = _localizationService.GetResource("ShoppingCart.GiftCardCouponCode.WrongGiftCard");
                }
                else
                    model.GiftCardBox.Message = _localizationService.GetResource("ShoppingCart.GiftCardCouponCode.WrongGiftCard");
            }
            else
                model.GiftCardBox.Message = _localizationService.GetResource("ShoppingCart.GiftCardCouponCode.DontWorkWithAutoshipProducts");

            PrepareShoppingCartModel(model, cart);
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired("estimateshipping")]
        public ActionResult GetEstimateShipping(EstimateShippingModel shippingModel, FormCollection form)
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();

            //parse and save checkout attributes
            ParseAndSaveCheckoutAttributes(cart, form);

            var model = new ShoppingCartModel();
            model.EstimateShipping.CountryId = shippingModel.CountryId;
            model.EstimateShipping.StateProvinceId = shippingModel.StateProvinceId;
            model.EstimateShipping.ZipPostalCode = shippingModel.ZipPostalCode;
            PrepareShoppingCartModel(model, cart, setEstimateShippingDefaultAddress: false);

            if (cart.RequiresShipping())
            {
                var address = new Address()
                {
                    CountryId = shippingModel.CountryId,
                    Country = shippingModel.CountryId.HasValue ? _countryService.GetCountryById(shippingModel.CountryId.Value) : null,
                    StateProvinceId = shippingModel.StateProvinceId,
                    StateProvince = shippingModel.StateProvinceId.HasValue ? _stateProvinceService.GetStateProvinceById(shippingModel.StateProvinceId.Value) : null,
                    ZipPostalCode = shippingModel.ZipPostalCode,
                };
                GetShippingOptionResponse getShippingOptionResponse = _shippingService
                    .GetShippingOptions(cart, address, "", _storeContext.CurrentStore.Id);
                if (!getShippingOptionResponse.Success)
                {
                    foreach (var error in getShippingOptionResponse.Errors)
                        model.EstimateShipping.Warnings.Add(error);
                }
                else
                {
                    if (getShippingOptionResponse.ShippingOptions.Count > 0)
                    {
                        foreach (var shippingOption in getShippingOptionResponse.ShippingOptions)
                        {
                            var soModel = new EstimateShippingModel.ShippingOptionModel()
                            {
                                Name = shippingOption.Name,
                                ID = shippingOption.ID,
                                Description = shippingOption.Description,

                            };
                            //calculate discounted and taxed rate
                            Discount appliedDiscount = null;
                            decimal shippingTotal = _orderTotalCalculationService.AdjustShippingRate(shippingOption.Rate,
                                cart, out appliedDiscount);

                            decimal rateBase = _taxService.GetShippingPrice(shippingTotal, _workContext.CurrentCustomer);
                            decimal rate = _currencyService.ConvertFromPrimaryStoreCurrency(rateBase, _workContext.WorkingCurrency);
                            soModel.Price = _priceFormatter.FormatShippingPrice(rate, true);
                            model.EstimateShipping.ShippingOptions.Add(soModel);
                        }
                    }
                    else
                    {
                        model.EstimateShipping.Warnings.Add(_localizationService.GetResource("Checkout.ShippingIsNotAllowed"));
                    }
                }
            }

            return View(model);
        }

        [ChildActionOnly]
        public ActionResult OrderTotals(bool isEditable, int customerId, int cartTypeId = 1)
        {
            var cartType = (ShoppingCartType)cartTypeId;
            var customer = _customerService.GetCustomerById(customerId);
            var cart = customer.ShoppingCartItems//_workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == cartType)//ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var model = new OrderTotalsModel();
            model.IsEditable = isEditable;

            if (cart.Count > 0)
            {
                //subtotal
                decimal subtotalBase = decimal.Zero;
                decimal orderSubTotalDiscountAmountBase = decimal.Zero;
                Discount orderSubTotalAppliedDiscount = null;
                decimal subTotalWithoutDiscountBase = decimal.Zero;
                decimal subTotalWithDiscountBase = decimal.Zero;
                var subTotalIncludingTax = _workContext.TaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal;
                _orderTotalCalculationService.GetShoppingCartSubTotal(cart, subTotalIncludingTax,
                    out orderSubTotalDiscountAmountBase, out orderSubTotalAppliedDiscount,
                    out subTotalWithoutDiscountBase, out subTotalWithDiscountBase);
                subtotalBase = subTotalWithoutDiscountBase;
                decimal subtotal = _currencyService.ConvertFromPrimaryStoreCurrency(subtotalBase, _workContext.WorkingCurrency);
                model.SubTotal = _priceFormatter.FormatPrice(subtotal, true, _workContext.WorkingCurrency, _workContext.WorkingLanguage, subTotalIncludingTax);

                if (orderSubTotalDiscountAmountBase > decimal.Zero)
                {
                    decimal orderSubTotalDiscountAmount = _currencyService.ConvertFromPrimaryStoreCurrency(orderSubTotalDiscountAmountBase, _workContext.WorkingCurrency);
                    model.SubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountAmount, true, _workContext.WorkingCurrency, _workContext.WorkingLanguage, subTotalIncludingTax);
                    model.AllowRemovingSubTotalDiscount = orderSubTotalAppliedDiscount != null &&
                                                          orderSubTotalAppliedDiscount.RequiresCouponCode &&
                                                          !String.IsNullOrEmpty(orderSubTotalAppliedDiscount.CouponCode) &&
                                                          model.IsEditable;
                }


                //shipping info
                model.RequiresShipping = cart.RequiresShipping();
                if (model.RequiresShipping)
                {
                    //decimal? shoppingCartShippingBase = _orderTotalCalculationService.GetShoppingCartShippingTotal(cart);
                    decimal? shoppingCartShippingBase = _orderTotalCalculationService.GetShoppingCartMultiShippingTotal(cart);
                    if (shoppingCartShippingBase.HasValue)
                    {
                        var shippingFee = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartShippingBase.Value, _workContext.WorkingCurrency);
                        model.Shipping = _priceFormatter.FormatShippingPrice(shippingFee, true);


                        //calculating the original shipping charge to get the dicount value
                        MultiShippingMethodModel ShippingMethodModel = null;
                        if (customer.ShippingMethodsAttributes != null)
                            ShippingMethodModel = (MultiShippingMethodModel)new JavaScriptSerializer().Deserialize(customer.ShippingMethodsAttributes, typeof(MultiShippingMethodModel));

                        decimal? shoppingCartShippingWithoutDiscountBase;
                        bool isFreeShipping = _orderTotalCalculationService.IsFreeShipping(cart);
                        if (!isFreeShipping)
                        {
                            if (ShippingMethodModel != null)
                            {
                                decimal shippingTemp = 0;
                                foreach (var item in ShippingMethodModel.ItemsShippingMethods)
                                {
                                    foreach (var option in item.ShippingOptions)
                                    {
                                        if (option.Selected)
                                        {
                                            decimal rate = option.ShippingOption.Rate;
                                            shippingTemp += rate;
                                            break;
                                        }
                                    }
                                }
                                //adjust shipping rate
                                decimal additionalCharge = _orderTotalCalculationService.GetShoppingCartAdditionalShippingCharge(cart);
                                shoppingCartShippingWithoutDiscountBase = shippingTemp + additionalCharge;

                                var shippingWithoutDiscountFee = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartShippingWithoutDiscountBase.Value, _workContext.WorkingCurrency);
                                var discountAmount = shippingWithoutDiscountFee - shippingFee;
                                model.ShippingDiscount = discountAmount > 0 ? _priceFormatter.FormatShippingPrice(discountAmount, true) : null;
                            }
                        }
                        //selected shipping method
                        //var shippingOption = customer.GetAttribute<ShippingOption>(SystemCustomerAttributeNames.SelectedShippingOption, _storeContext.CurrentStore.Id);
                        //if (shippingOption != null)
                        //    model.SelectedShippingMethod = shippingOption.Name;
                    }
                }

                //payment method fee
                string paymentMethodSystemName = customer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod, _storeContext.CurrentStore.Id);
                decimal paymentMethodAdditionalFee = _paymentService.GetAdditionalHandlingFee(cart, paymentMethodSystemName);
                decimal paymentMethodAdditionalFeeWithTaxBase = _taxService.GetPaymentMethodAdditionalFee(paymentMethodAdditionalFee, customer);
                if (paymentMethodAdditionalFeeWithTaxBase > decimal.Zero)
                {
                    decimal paymentMethodAdditionalFeeWithTax = _currencyService.ConvertFromPrimaryStoreCurrency(paymentMethodAdditionalFeeWithTaxBase, _workContext.WorkingCurrency);
                    model.PaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeWithTax, true);
                }

                //tax
                bool displayTax = true;
                bool displayTaxRates = true;
                if (_taxSettings.HideTaxInOrderSummary && _workContext.TaxDisplayType == TaxDisplayType.IncludingTax)
                {
                    displayTax = false;
                    displayTaxRates = false;
                }
                else
                {
                    SortedDictionary<decimal, decimal> taxRates = null;
                    decimal shoppingCartTaxBase = _orderTotalCalculationService.GetTaxTotal(cart, out taxRates);
                    decimal shoppingCartTax = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartTaxBase, _workContext.WorkingCurrency);

                    if (shoppingCartTaxBase == 0 && _taxSettings.HideZeroTax)
                    {
                        displayTax = false;
                        displayTaxRates = false;
                    }
                    else
                    {
                        displayTaxRates = _taxSettings.DisplayTaxRates && taxRates.Count > 0;
                        displayTax = !displayTaxRates;

                        model.Tax = _priceFormatter.FormatPrice(shoppingCartTax, true, false);
                        foreach (var tr in taxRates)
                        {
                            model.TaxRates.Add(new OrderTotalsModel.TaxRate()
                                {
                                    Rate = _priceFormatter.FormatTaxRate(tr.Key),
                                    Value = _priceFormatter.FormatPrice(_currencyService.ConvertFromPrimaryStoreCurrency(tr.Value, _workContext.WorkingCurrency), true, false),
                                });
                        }
                    }
                }
                model.DisplayTaxRates = displayTaxRates;
                model.DisplayTax = displayTax;

                //total
                decimal orderTotalDiscountAmountBase = decimal.Zero;
                Discount orderTotalAppliedDiscount = null;
                List<AppliedGiftCard> appliedGiftCards = null;
                int redeemedRewardPoints = 0;
                decimal redeemedRewardPointsAmount = decimal.Zero;
                decimal? shoppingCartTotalBase = _orderTotalCalculationService.GetShoppingCartTotal(cart,
                    out orderTotalDiscountAmountBase, out orderTotalAppliedDiscount,
                    out appliedGiftCards, out redeemedRewardPoints, out redeemedRewardPointsAmount);
                if (shoppingCartTotalBase.HasValue)
                {
                    decimal shoppingCartTotal = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartTotalBase.Value, _workContext.WorkingCurrency);
                    model.OrderTotal = _priceFormatter.FormatPrice(shoppingCartTotal, true, false);
                }

                //discount
                if (orderTotalDiscountAmountBase > decimal.Zero)
                {
                    decimal orderTotalDiscountAmount = _currencyService.ConvertFromPrimaryStoreCurrency(orderTotalDiscountAmountBase, _workContext.WorkingCurrency);
                    model.OrderTotalDiscount = _priceFormatter.FormatPrice(-orderTotalDiscountAmount, true, false);
                    model.AllowRemovingOrderTotalDiscount = orderTotalAppliedDiscount != null &&
                        orderTotalAppliedDiscount.RequiresCouponCode &&
                        !String.IsNullOrEmpty(orderTotalAppliedDiscount.CouponCode) &&
                        model.IsEditable;
                }

                //gift cards
                if (appliedGiftCards != null && appliedGiftCards.Count > 0)
                {
                    foreach (var appliedGiftCard in appliedGiftCards)
                    {
                        var gcModel = new OrderTotalsModel.GiftCard()
                            {
                                Id = appliedGiftCard.GiftCard.Id,
                                CouponCode = appliedGiftCard.GiftCard.GiftCardCouponCode,
                            };
                        decimal amountCanBeUsed = _currencyService.ConvertFromPrimaryStoreCurrency(appliedGiftCard.AmountCanBeUsed, _workContext.WorkingCurrency);
                        gcModel.Amount = _priceFormatter.FormatPrice(-amountCanBeUsed, true, false);

                        decimal remainingAmountBase = appliedGiftCard.GiftCard.GetGiftCardRemainingAmount() - appliedGiftCard.AmountCanBeUsed;
                        decimal remainingAmount = _currencyService.ConvertFromPrimaryStoreCurrency(remainingAmountBase, _workContext.WorkingCurrency);
                        gcModel.Remaining = _priceFormatter.FormatPrice(remainingAmount, true, false);

                        model.GiftCards.Add(gcModel);
                    }
                }

                //reward points
                if (redeemedRewardPointsAmount > decimal.Zero)
                {
                    decimal redeemedRewardPointsAmountInCustomerCurrency = _currencyService.ConvertFromPrimaryStoreCurrency(redeemedRewardPointsAmount, _workContext.WorkingCurrency);
                    model.RedeemedRewardPoints = redeemedRewardPoints;
                    model.RedeemedRewardPointsAmount = _priceFormatter.FormatPrice(-redeemedRewardPointsAmountInCustomerCurrency, true, false);
                }
            }


            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult OrderTotalsCart(bool isEditable, int customerId, int cartTypeId = 1)
        {
            var cartType = (ShoppingCartType)cartTypeId;
            var customer = _customerService.GetCustomerById(customerId);
            var cart = customer.ShoppingCartItems//_workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == cartType)//ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var model = new OrderTotalsModel();
            model.IsEditable = isEditable;

            if (cart.Count > 0)
            {
                //subtotal
                decimal subtotalBase = decimal.Zero;
                decimal orderSubTotalDiscountAmountBase = decimal.Zero;
                Discount orderSubTotalAppliedDiscount = null;
                decimal subTotalWithoutDiscountBase = decimal.Zero;
                decimal subTotalWithDiscountBase = decimal.Zero;
                var subTotalIncludingTax = _workContext.TaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal;
                _orderTotalCalculationService.GetShoppingCartSubTotal(cart, subTotalIncludingTax,
                    out orderSubTotalDiscountAmountBase, out orderSubTotalAppliedDiscount,
                    out subTotalWithoutDiscountBase, out subTotalWithDiscountBase);
                subtotalBase = subTotalWithoutDiscountBase;
                decimal subtotal = _currencyService.ConvertFromPrimaryStoreCurrency(subtotalBase, _workContext.WorkingCurrency);
                model.SubTotal = _priceFormatter.FormatPrice(subtotal, true, _workContext.WorkingCurrency, _workContext.WorkingLanguage, subTotalIncludingTax);

                if (orderSubTotalDiscountAmountBase > decimal.Zero)
                {
                    decimal orderSubTotalDiscountAmount = _currencyService.ConvertFromPrimaryStoreCurrency(orderSubTotalDiscountAmountBase, _workContext.WorkingCurrency);
                    model.SubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountAmount, true, _workContext.WorkingCurrency, _workContext.WorkingLanguage, subTotalIncludingTax);
                    model.AllowRemovingSubTotalDiscount = orderSubTotalAppliedDiscount != null &&
                                                          orderSubTotalAppliedDiscount.RequiresCouponCode &&
                                                          !String.IsNullOrEmpty(orderSubTotalAppliedDiscount.CouponCode) &&
                                                          model.IsEditable;
                }


                //shipping info
                model.RequiresShipping = cart.RequiresShipping();
                if (model.RequiresShipping)
                {
                    //decimal? shoppingCartShippingBase = _orderTotalCalculationService.GetShoppingCartShippingTotal(cart);
                    decimal? shoppingCartShippingBase = _orderTotalCalculationService.GetShoppingCartMultiShippingTotal(cart);
                    if (shoppingCartShippingBase.HasValue)
                    {
                        var shippingFee = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartShippingBase.Value, _workContext.WorkingCurrency);
                        model.Shipping = _priceFormatter.FormatShippingPrice(shippingFee, true);

                        //selected shipping method
                        //var shippingOption = customer.GetAttribute<ShippingOption>(SystemCustomerAttributeNames.SelectedShippingOption, _storeContext.CurrentStore.Id);
                        //if (shippingOption != null)
                        //    model.SelectedShippingMethod = shippingOption.Name;
                    }
                }

                //payment method fee
                string paymentMethodSystemName = customer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod, _storeContext.CurrentStore.Id);
                decimal paymentMethodAdditionalFee = _paymentService.GetAdditionalHandlingFee(cart, paymentMethodSystemName);
                decimal paymentMethodAdditionalFeeWithTaxBase = _taxService.GetPaymentMethodAdditionalFee(paymentMethodAdditionalFee, customer);
                if (paymentMethodAdditionalFeeWithTaxBase > decimal.Zero)
                {
                    decimal paymentMethodAdditionalFeeWithTax = _currencyService.ConvertFromPrimaryStoreCurrency(paymentMethodAdditionalFeeWithTaxBase, _workContext.WorkingCurrency);
                    model.PaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeWithTax, true);
                }

                //tax
                bool displayTax = true;
                bool displayTaxRates = true;
                if (_taxSettings.HideTaxInOrderSummary && _workContext.TaxDisplayType == TaxDisplayType.IncludingTax)
                {
                    displayTax = false;
                    displayTaxRates = false;
                }
                else
                {
                    SortedDictionary<decimal, decimal> taxRates = null;
                    decimal shoppingCartTaxBase = _orderTotalCalculationService.GetTaxTotal(cart, out taxRates);
                    decimal shoppingCartTax = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartTaxBase, _workContext.WorkingCurrency);

                    if (shoppingCartTaxBase == 0 && _taxSettings.HideZeroTax)
                    {
                        displayTax = false;
                        displayTaxRates = false;
                    }
                    else
                    {
                        displayTaxRates = _taxSettings.DisplayTaxRates && taxRates.Count > 0;
                        displayTax = !displayTaxRates;

                        model.Tax = _priceFormatter.FormatPrice(shoppingCartTax, true, false);
                        foreach (var tr in taxRates)
                        {
                            model.TaxRates.Add(new OrderTotalsModel.TaxRate()
                                {
                                    Rate = _priceFormatter.FormatTaxRate(tr.Key),
                                    Value = _priceFormatter.FormatPrice(_currencyService.ConvertFromPrimaryStoreCurrency(tr.Value, _workContext.WorkingCurrency), true, false),
                                });
                        }
                    }
                }
                model.DisplayTaxRates = displayTaxRates;
                model.DisplayTax = displayTax;

                //total
                decimal orderTotalDiscountAmountBase = decimal.Zero;
                Discount orderTotalAppliedDiscount = null;
                List<AppliedGiftCard> appliedGiftCards = null;
                int redeemedRewardPoints = 0;
                decimal redeemedRewardPointsAmount = decimal.Zero;
                decimal? shoppingCartTotalBase = _orderTotalCalculationService.GetShoppingCartTotal(cart,
                    out orderTotalDiscountAmountBase, out orderTotalAppliedDiscount,
                    out appliedGiftCards, out redeemedRewardPoints, out redeemedRewardPointsAmount);
                if (shoppingCartTotalBase.HasValue)
                {
                    decimal shoppingCartTotal = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartTotalBase.Value, _workContext.WorkingCurrency);
                    model.OrderTotal = _priceFormatter.FormatPrice(shoppingCartTotal, true, false);
                }

                //discount
                if (orderTotalDiscountAmountBase > decimal.Zero)
                {
                    decimal orderTotalDiscountAmount = _currencyService.ConvertFromPrimaryStoreCurrency(orderTotalDiscountAmountBase, _workContext.WorkingCurrency);
                    model.OrderTotalDiscount = _priceFormatter.FormatPrice(-orderTotalDiscountAmount, true, false);
                    model.AllowRemovingOrderTotalDiscount = orderTotalAppliedDiscount != null &&
                        orderTotalAppliedDiscount.RequiresCouponCode &&
                        !String.IsNullOrEmpty(orderTotalAppliedDiscount.CouponCode) &&
                        model.IsEditable;
                }

                //gift cards
                if (appliedGiftCards != null && appliedGiftCards.Count > 0)
                {
                    foreach (var appliedGiftCard in appliedGiftCards)
                    {
                        var gcModel = new OrderTotalsModel.GiftCard()
                            {
                                Id = appliedGiftCard.GiftCard.Id,
                                CouponCode = appliedGiftCard.GiftCard.GiftCardCouponCode,
                            };
                        decimal amountCanBeUsed = _currencyService.ConvertFromPrimaryStoreCurrency(appliedGiftCard.AmountCanBeUsed, _workContext.WorkingCurrency);
                        gcModel.Amount = _priceFormatter.FormatPrice(-amountCanBeUsed, true, false);

                        decimal remainingAmountBase = appliedGiftCard.GiftCard.GetGiftCardRemainingAmount() - appliedGiftCard.AmountCanBeUsed;
                        decimal remainingAmount = _currencyService.ConvertFromPrimaryStoreCurrency(remainingAmountBase, _workContext.WorkingCurrency);
                        gcModel.Remaining = _priceFormatter.FormatPrice(remainingAmount, true, false);

                        model.GiftCards.Add(gcModel);
                    }
                }

                //reward points
                if (redeemedRewardPointsAmount > decimal.Zero)
                {
                    decimal redeemedRewardPointsAmountInCustomerCurrency = _currencyService.ConvertFromPrimaryStoreCurrency(redeemedRewardPointsAmount, _workContext.WorkingCurrency);
                    model.RedeemedRewardPoints = redeemedRewardPoints;
                    model.RedeemedRewardPointsAmount = _priceFormatter.FormatPrice(-redeemedRewardPointsAmountInCustomerCurrency, true, false);
                }
            }


            return PartialView(model);
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired("removesubtotaldiscount", "removeordertotaldiscount", "removediscountcouponcode")]
        public ActionResult RemoveDiscountCoupon()
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var model = new ShoppingCartModel();

            _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                SystemCustomerAttributeNames.DiscountCouponCode, null);

            PrepareShoppingCartModel(model, cart);
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("Cart")]
        [FormValueRequired(FormValueRequirement.StartsWith, "removegiftcard-")]
        public ActionResult RemoveGiftardCode(FormCollection form)
        {
            var model = new ShoppingCartModel();

            //get gift card identifier
            int giftCardId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("removegiftcard-", StringComparison.InvariantCultureIgnoreCase))
                    giftCardId = Convert.ToInt32(formValue.Substring("removegiftcard-".Length));
            var gc = _giftCardService.GetGiftCardById(giftCardId);
            if (gc != null)
            {
                _workContext.CurrentCustomer.RemoveGiftCardCouponCode(gc.GiftCardCouponCode);
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);
            }

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            PrepareShoppingCartModel(model, cart);
            return View(model);
        }

        [ChildActionOnly]
        public ActionResult FlyoutShoppingCart()
        {
            if (!_shoppingCartSettings.MiniShoppingCartEnabled)
                return Content("");

            if (!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart))
                return Content("");

            var model = PrepareMiniShoppingCartModel();
            return PartialView(model);
        }

        #endregion

        #region Wishlist

        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult Wishlist(Guid? customerGuid)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist))
                return RedirectToRoute("HomePage");

            Customer customer = customerGuid.HasValue ?
                _customerService.GetCustomerByGuid(customerGuid.Value)
                : _workContext.CurrentCustomer;
            if (customer == null)
                return RedirectToRoute("HomePage");
            var cart = customer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var model = new WishlistModel();
            PrepareWishlistModel(model, cart, !customerGuid.HasValue);
            return View(model);
        }

        //update all wishlist cart items on the page
        [ValidateInput(false)]
        [HttpPost, ActionName("Wishlist")]
        [FormValueRequired("updatecart")]
        public ActionResult UpdateWishlistAll(FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist))
                return RedirectToRoute("HomePage");

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();

            var allIdsToRemove = form["removefromcart"] != null ? form["removefromcart"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToList() : new List<int>();

            //current warnings <cart item identifier, warnings>
            var innerWarnings = new Dictionary<int, IList<string>>();
            foreach (var sci in cart)
            {
                bool remove = allIdsToRemove.Contains(sci.Id);
                if (remove)
                    _shoppingCartService.DeleteShoppingCartItem(sci);
                else
                {
                    foreach (string formKey in form.AllKeys)
                        if (formKey.Equals(string.Format("itemquantity{0}", sci.Id), StringComparison.InvariantCultureIgnoreCase))
                        {
                            int newQuantity = sci.Quantity;
                            if (int.TryParse(form[formKey], out newQuantity))
                            {
                                var currSciWarnings = _shoppingCartService.UpdateShoppingCartItem(_workContext.CurrentCustomer,
                                    sci.Id, sci.AttributesXml, sci.CustomerEnteredPrice, newQuantity, true);
                                innerWarnings.Add(sci.Id, currSciWarnings);
                            }
                            break;
                        }
                }
            }

            //updated wishlist
            cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var model = new WishlistModel();
            PrepareWishlistModel(model, cart);
            //update current warnings
            foreach (var kvp in innerWarnings)
            {
                //kvp = <cart item identifier, warnings>
                var sciId = kvp.Key;
                var warnings = kvp.Value;
                //find model
                var sciModel = model.Items.FirstOrDefault(x => x.Id == sciId);
                if (sciModel != null)
                    foreach (var w in warnings)
                        if (!sciModel.Warnings.Contains(w))
                            sciModel.Warnings.Add(w);
            }
            return View(model);
        }

        //update a certain wishlist cart item on the page
        [ValidateInput(false)]
        [HttpPost, ActionName("Wishlist")]
        [FormValueRequired(FormValueRequirement.StartsWith, "updatecartitem-")]
        public ActionResult UpdateWishlistItem(FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist))
                return RedirectToRoute("HomePage");

            //get wishlist cart item identifier
            int sciId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("updatecartitem-", StringComparison.InvariantCultureIgnoreCase))
                    sciId = Convert.ToInt32(formValue.Substring("updatecartitem-".Length));
            //get shopping cart item
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(x => x.ShoppingCartType == ShoppingCartType.Wishlist)
                .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var sci = cart.FirstOrDefault(x => x.Id == sciId);
            if (sci == null)
            {
                return RedirectToRoute("Wishlist");
            }

            //update the wishlist cart item
            var warnings = new List<string>();
            foreach (string formKey in form.AllKeys)
                if (formKey.Equals(string.Format("itemquantity{0}", sci.Id), StringComparison.InvariantCultureIgnoreCase))
                {
                    int newQuantity = sci.Quantity;
                    if (int.TryParse(form[formKey], out newQuantity))
                    {
                        warnings.AddRange(_shoppingCartService.UpdateShoppingCartItem(_workContext.CurrentCustomer,
                            sci.Id, sci.AttributesXml, sci.CustomerEnteredPrice, newQuantity, true));
                    }
                    break;
                }


            //updated wishlist
            cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(x => x.ShoppingCartType == ShoppingCartType.Wishlist)
                .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var model = new WishlistModel();
            PrepareWishlistModel(model, cart);
            //update current warnings
            //find model
            var sciModel = model.Items.FirstOrDefault(x => x.Id == sciId);
            if (sciModel != null)
                foreach (var w in warnings)
                    if (!sciModel.Warnings.Contains(w))
                        sciModel.Warnings.Add(w);
            return View(model);
        }

        //remove a certain wishlist cart item on the page
        [ValidateInput(false)]
        [HttpPost, ActionName("Wishlist")]
        [FormValueRequired(FormValueRequirement.StartsWith, "removefromcart-")]
        public ActionResult RemoveWishlistItem(FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist))
                return RedirectToRoute("HomePage");

            //get wishlist cart item identifier
            int sciId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("removefromcart-", StringComparison.InvariantCultureIgnoreCase))
                    sciId = Convert.ToInt32(formValue.Substring("removefromcart-".Length));
            //get wishlist cart item
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(x => x.ShoppingCartType == ShoppingCartType.Wishlist)
                .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var sci = cart.FirstOrDefault(x => x.Id == sciId);
            if (sci == null)
            {
                return RedirectToRoute("Wishlist");
            }

            //remove the wishlist cart item
            _shoppingCartService.DeleteShoppingCartItem(sci);


            //updated wishlist
            cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(x => x.ShoppingCartType == ShoppingCartType.Wishlist)
                .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var model = new WishlistModel();
            PrepareWishlistModel(model, cart);
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("Wishlist")]
        [FormValueRequired("addtocartbutton")]
        public ActionResult AddItemstoCartFromWishlist(Guid? customerGuid, FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart))
                return RedirectToRoute("HomePage");

            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist))
                return RedirectToRoute("HomePage");

            var pageCustomer = customerGuid.HasValue
                ? _customerService.GetCustomerByGuid(customerGuid.Value)
                : _workContext.CurrentCustomer;
            if (pageCustomer == null)
                return RedirectToRoute("HomePage");

            var pageCart = pageCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();

            var allWarnings = new List<string>();
            var numberOfAddedItems = 0;
            var allIdsToAdd = form["addtocart"] != null ? form["addtocart"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToList() : new List<int>();
            foreach (var sci in pageCart)
            {
                if (allIdsToAdd.Contains(sci.Id))
                {
                    var warnings = _shoppingCartService.AddToCart(_workContext.CurrentCustomer,
                        sci.Product, ShoppingCartType.ShoppingCart,
                        _storeContext.CurrentStore.Id,
                        sci.AttributesXml, sci.CustomerEnteredPrice, sci.Quantity, true);
                    if (warnings.Count == 0)
                        numberOfAddedItems++;
                    if (_shoppingCartSettings.MoveItemsFromWishlistToCart && //settings enabled
                        !customerGuid.HasValue && //own wishlist
                        warnings.Count == 0) //no warnings ( already in the cart)
                    {
                        //let's remove the item from wishlist
                        _shoppingCartService.DeleteShoppingCartItem(sci);
                    }
                    allWarnings.AddRange(warnings);
                }
            }

            if (numberOfAddedItems > 0)
            {
                //redirect to the shopping cart page
                return RedirectToRoute("ShoppingCart");
            }
            else
            {
                //no items added. redisplay the wishlist page
                var cart = pageCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();
                var model = new WishlistModel();
                PrepareWishlistModel(model, cart, !customerGuid.HasValue);
                return View(model);
            }
        }

        //add a certain wishlist cart item on the page to the shopping cart
        [ValidateInput(false)]
        [HttpPost, ActionName("Wishlist")]
        [FormValueRequired(FormValueRequirement.StartsWith, "addtocart-")]
        public ActionResult AddOneItemtoCartFromWishlist(Guid? customerGuid, FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart))
                return RedirectToRoute("HomePage");

            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist))
                return RedirectToRoute("HomePage");

            //get wishlist cart item identifier
            int sciId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("addtocart-", StringComparison.InvariantCultureIgnoreCase))
                    sciId = Convert.ToInt32(formValue.Substring("addtocart-".Length));
            //get wishlist cart item
            var pageCustomer = customerGuid.HasValue
                ? _customerService.GetCustomerByGuid(customerGuid.Value)
                : _workContext.CurrentCustomer;
            if (pageCustomer == null)
                return RedirectToRoute("HomePage");

            var pageCart = pageCustomer.ShoppingCartItems
                .Where(x => x.ShoppingCartType == ShoppingCartType.Wishlist)
                .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                .ToList();

            var sci = pageCart.FirstOrDefault(x => x.Id == sciId);
            if (sci == null)
            {
                return RedirectToRoute("Wishlist");
            }
            var warnings = _shoppingCartService.AddToCart(_workContext.CurrentCustomer,
                                           sci.Product, ShoppingCartType.ShoppingCart,
                                           _storeContext.CurrentStore.Id,
                                           sci.AttributesXml, sci.CustomerEnteredPrice, sci.Quantity, true);
            if (_shoppingCartSettings.MoveItemsFromWishlistToCart && //settings enabled
                        !customerGuid.HasValue && //own wishlist
                        warnings.Count == 0) //no warnings ( already in the cart)
            {
                //let's remove the item from wishlist
                _shoppingCartService.DeleteShoppingCartItem(sci);
            }

            if (warnings.Count == 0)
            {
                //redirect to the shopping cart page
                return RedirectToRoute("ShoppingCart");
            }
            else
            {
                //no items added. redisplay the wishlist page
                var cart = pageCustomer.ShoppingCartItems
                    .Where(x => x.ShoppingCartType == ShoppingCartType.Wishlist)
                    .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();
                var model = new WishlistModel();
                PrepareWishlistModel(model, cart, !customerGuid.HasValue);
                return View(model);
            }
        }

        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult EmailWishlist()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist) || !_shoppingCartSettings.EmailWishlistEnabled)
                return RedirectToRoute("HomePage");

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();

            if (cart.Count == 0)
                return RedirectToRoute("HomePage");

            var model = new WishlistEmailAFriendModel()
            {
                YourEmailAddress = _workContext.CurrentCustomer.Email,
                DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnEmailWishlistToFriendPage
            };
            return View(model);
        }

        [HttpPost, ActionName("EmailWishlist")]
        [FormValueRequired("send-email")]
        [CaptchaValidator]
        public ActionResult EmailWishlistSend(WishlistEmailAFriendModel model, bool captchaValid)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist) || !_shoppingCartSettings.EmailWishlistEnabled)
                return RedirectToRoute("HomePage");

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("HomePage");

            //validate CAPTCHA
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnEmailWishlistToFriendPage && !captchaValid)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Common.WrongCaptcha"));
            }

            //check whether the current customer is guest and ia allowed to email wishlist
            if (_workContext.CurrentCustomer.IsGuest() && !_shoppingCartSettings.AllowAnonymousUsersToEmailWishlist)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Wishlist.EmailAFriend.OnlyRegisteredUsers"));
            }

            if (ModelState.IsValid)
            {
                //email
                _workflowMessageService.SendWishlistEmailAFriendMessage(_workContext.CurrentCustomer,
                        _workContext.WorkingLanguage.Id, model.YourEmailAddress,
                        model.FriendEmail, Core.Html.HtmlHelper.FormatText(model.PersonalMessage, false, true, false, false, false, false));

                model.SuccessfullySent = true;
                model.Result = _localizationService.GetResource("Wishlist.EmailAFriend.SuccessfullySent");

                return View(model);
            }

            //If we got this far, something failed, redisplay form
            model.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnEmailWishlistToFriendPage;
            return View(model);
        }

        #endregion

        #region saveforlater

        //list all self relators
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult Saveforlater(Guid? customerGuid)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart))
                return RedirectToRoute("HomePage");

            Customer customer = customerGuid.HasValue ?
                _customerService.GetCustomerByGuid(customerGuid.Value)
                : _workContext.CurrentCustomer;
            if (customer == null)
                return RedirectToRoute("HomePage");
            var cart = customer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.Saveforlater)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var model = new SaveforlaterModel();
            PrepareSaveforlaterModel(model, cart, !customerGuid.HasValue);
            return View(model);
        }

        //update all self relator cart items on the page
        [ValidateInput(false)]
        [HttpPost, ActionName("Saveforlater")]
        [FormValueRequired("updatecart")]
        public ActionResult UpdateSaveforlaterAll(FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist))
                return RedirectToRoute("HomePage");

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.Saveforlater)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();

            var allIdsToRemove = form["removefromcart"] != null ? form["removefromcart"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToList() : new List<int>();

            //current warnings <cart item identifier, warnings>
            var innerWarnings = new Dictionary<int, IList<string>>();
            foreach (var sci in cart)
            {
                bool remove = allIdsToRemove.Contains(sci.Id);
                if (remove)
                    _shoppingCartService.DeleteShoppingCartItem(sci);
                else
                {
                    foreach (string formKey in form.AllKeys)
                        if (formKey.Equals(string.Format("itemquantity{0}", sci.Id), StringComparison.InvariantCultureIgnoreCase))
                        {
                            int newQuantity = sci.Quantity;
                            if (int.TryParse(form[formKey], out newQuantity))
                            {
                                var currSciWarnings = _shoppingCartService.UpdateShoppingCartItem(_workContext.CurrentCustomer,
                                    sci.Id, sci.AttributesXml, sci.CustomerEnteredPrice, newQuantity, true);
                                innerWarnings.Add(sci.Id, currSciWarnings);
                            }
                            break;
                        }
                }
            }

            //updated Saveforlater
            cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.Saveforlater)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var model = new SaveforlaterModel();
            PrepareSaveforlaterModel(model, cart);
            //update current warnings
            foreach (var kvp in innerWarnings)
            {
                //kvp = <cart item identifier, warnings>
                var sciId = kvp.Key;
                var warnings = kvp.Value;
                //find model
                var sciModel = model.Items.FirstOrDefault(x => x.Id == sciId);
                if (sciModel != null)
                    foreach (var w in warnings)
                        if (!sciModel.Warnings.Contains(w))
                            sciModel.Warnings.Add(w);
            }
            return View(model);
        }

        //update a certain wishlist cart item on the page
        [ValidateInput(false)]
        [HttpPost, ActionName("Saveforlater")]
        [FormValueRequired(FormValueRequirement.StartsWith, "updatecartitem-")]
        public ActionResult UpdateSaveforlaterItem(FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist))
                return RedirectToRoute("HomePage");

            //get wishlist cart item identifier
            int sciId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("updatecartitem-", StringComparison.InvariantCultureIgnoreCase))
                    sciId = Convert.ToInt32(formValue.Substring("updatecartitem-".Length));
            //get shopping cart item
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(x => x.ShoppingCartType == ShoppingCartType.Saveforlater)
                .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var sci = cart.FirstOrDefault(x => x.Id == sciId);
            if (sci == null)
            {
                return RedirectToRoute("Saveforlater");
            }

            //update the Saveforlater cart item
            var warnings = new List<string>();
            foreach (string formKey in form.AllKeys)
                if (formKey.Equals(string.Format("itemquantity{0}", sci.Id), StringComparison.InvariantCultureIgnoreCase))
                {
                    int newQuantity = sci.Quantity;
                    if (int.TryParse(form[formKey], out newQuantity))
                    {
                        warnings.AddRange(_shoppingCartService.UpdateShoppingCartItem(_workContext.CurrentCustomer,
                            sci.Id, sci.AttributesXml, sci.CustomerEnteredPrice, newQuantity, true));
                    }
                    break;
                }


            //updated Saveforlater
            cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(x => x.ShoppingCartType == ShoppingCartType.Saveforlater)
                .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var model = new SaveforlaterModel();
            PrepareSaveforlaterModel(model, cart);
            //update current warnings
            //find model
            var sciModel = model.Items.FirstOrDefault(x => x.Id == sciId);
            if (sciModel != null)
                foreach (var w in warnings)
                    if (!sciModel.Warnings.Contains(w))
                        sciModel.Warnings.Add(w);
            return View(model);
        }

        //remove a certain Self relator cart item on the page
        [ValidateInput(false)]
        [HttpPost, ActionName("Saveforlater")]
        [FormValueRequired(FormValueRequirement.StartsWith, "removefromcart-")]
        public ActionResult RemoveSaveforlaterItem(FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist))
                return RedirectToRoute("HomePage");

            //get wishlist cart item identifier
            int sciId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("removefromcart-", StringComparison.InvariantCultureIgnoreCase))
                    sciId = Convert.ToInt32(formValue.Substring("removefromcart-".Length));
            //get wishlist cart item
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(x => x.ShoppingCartType == ShoppingCartType.Saveforlater)
                .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var sci = cart.FirstOrDefault(x => x.Id == sciId);
            if (sci == null)
            {
                return RedirectToRoute("Saveforlater");
            }

            //remove the wishlist cart item
            _shoppingCartService.DeleteShoppingCartItem(sci);


            //updated wishlist
            cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(x => x.ShoppingCartType == ShoppingCartType.Saveforlater)
                .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            var model = new SaveforlaterModel();
            PrepareSaveforlaterModel(model, cart);
            return View(model);
        }

        //add self relator items to shopping cart
        [ValidateInput(false)]
        [HttpPost, ActionName("Saveforlater")]
        [FormValueRequired("addtocartbutton")]
        public ActionResult AddItemstoCartFromSaveforlater(Guid? customerGuid, FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart))
                return RedirectToRoute("HomePage");

            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist))
                return RedirectToRoute("HomePage");

            var pageCustomer = customerGuid.HasValue
                ? _customerService.GetCustomerByGuid(customerGuid.Value)
                : _workContext.CurrentCustomer;
            if (pageCustomer == null)
                return RedirectToRoute("HomePage");

            var pageCart = pageCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.Saveforlater)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();

            var allWarnings = new List<string>();
            var numberOfAddedItems = 0;
            var allIdsToAdd = form["addtocart"] != null ? form["addtocart"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToList() : new List<int>();
            foreach (var sci in pageCart)
            {
                if (allIdsToAdd.Contains(sci.Id))
                {
                    var warnings = _shoppingCartService.AddToCart(_workContext.CurrentCustomer,
                        sci.Product, ShoppingCartType.ShoppingCart,
                        _storeContext.CurrentStore.Id,
                        sci.AttributesXml, sci.CustomerEnteredPrice, sci.Quantity, true,
                        sci.ShoppingCartItemAddresses.ToList(), sci.SendToManyAddressSelected);
                    if (warnings.Count == 0)
                        numberOfAddedItems++;
                    if (!customerGuid.HasValue && //own save for later
                        warnings.Count == 0) //no warnings ( already in the cart)
                    {
                        //let's remove the item from wishlist
                        _shoppingCartService.DeleteShoppingCartItem(sci);
                    }
                    allWarnings.AddRange(warnings);
                }
            }

            if (numberOfAddedItems > 0)
            {
                //redirect to the shopping cart page
                return RedirectToRoute("ShoppingCart");
            }
            else
            {
                //no items added. redisplay the Saveforlater page
                var cart = pageCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.Saveforlater)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();
                var model = new SaveforlaterModel();
                PrepareSaveforlaterModel(model, cart, !customerGuid.HasValue);
                return View(model);
            }
        }

        //add a certain Saveforlater item on the page to the shopping cart
        [ValidateInput(false)]
        [HttpPost, ActionName("Saveforlater")]
        [FormValueRequired(FormValueRequirement.StartsWith, "addtocart-")]
        public ActionResult AddOneItemtoCartFromSaveforlater(Guid? customerGuid, FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart))
                return RedirectToRoute("HomePage");

            if (!_permissionService.Authorize(StandardPermissionProvider.EnableWishlist))
                return RedirectToRoute("HomePage");

            //get wishlist cart item identifier
            int sciId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("addtocart-", StringComparison.InvariantCultureIgnoreCase))
                    sciId = Convert.ToInt32(formValue.Substring("addtocart-".Length));
            //get wishlist cart item
            var pageCustomer = customerGuid.HasValue
                ? _customerService.GetCustomerByGuid(customerGuid.Value)
                : _workContext.CurrentCustomer;
            if (pageCustomer == null)
                return RedirectToRoute("HomePage");

            var pageCart = pageCustomer.ShoppingCartItems
                .Where(x => x.ShoppingCartType == ShoppingCartType.Saveforlater)
                .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                .ToList();

            var sci = pageCart.FirstOrDefault(x => x.Id == sciId);
            if (sci == null)
            {
                return RedirectToRoute("Saveforlater");
            }
            var warnings = _shoppingCartService.AddToCart(_workContext.CurrentCustomer,
                                           sci.Product, ShoppingCartType.ShoppingCart,
                                           _storeContext.CurrentStore.Id,
                                           sci.AttributesXml, sci.CustomerEnteredPrice, sci.Quantity, true,
                                           sci.ShoppingCartItemAddresses.ToList(), sci.SendToManyAddressSelected);
            if (!customerGuid.HasValue && //own save for later
                warnings.Count == 0) //no warnings ( already in the cart)
            {
                //let's remove the item from wishlist
                _shoppingCartService.DeleteShoppingCartItem(sci);
            }

            if (warnings.Count == 0)
            {
                //redirect to the shopping cart page
                return RedirectToRoute("ShoppingCart");
            }
            else
            {
                //no items added. redisplay the wishlist page
                var cart = pageCustomer.ShoppingCartItems
                    .Where(x => x.ShoppingCartType == ShoppingCartType.Saveforlater)
                    .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();
                var model = new SaveforlaterModel();
                PrepareSaveforlaterModel(model, cart, !customerGuid.HasValue);
                return View(model);
            }
        }

        #endregion

        #region Shopping Cart Services

        public JsonResult GetItemsFromShoppingCartByCustomerId(int customerId, int cartTypeId, string currencyCode)
        {
            var cartType = (ShoppingCartType)cartTypeId;
            //get list of countries and states
            var countries = EPGServices.GetDestinations("International Outgoing").Cast<DestinationModel>().ToList();
            var states = EPGServices.GetDestinations("Local").Cast<OriginModel>().ToList();

            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                return Json(_localizationService.GetResource("ShoppingCart.CustomerNotFound"), JsonRequestBehavior.AllowGet);

            Currency targetCurrency;
            if (!string.IsNullOrEmpty(currencyCode))
                targetCurrency = _currencyService.GetCurrencyByCode(currencyCode);
            else
                targetCurrency = _workContext.WorkingCurrency;

            var shoppingCarts = customer.ShoppingCartItems.Where(x => x.ShoppingCartType == cartType);

            var shoppingCartItems = new List<ShoppingCartItemSimpleInfoModel>();

            foreach (var cart in shoppingCarts)
            {
                var shoppingCartItem = new ShoppingCartItemSimpleInfoModel();

                shoppingCartItem.Id = cart.Id;
                shoppingCartItem.CustomerId = customerId;
                shoppingCartItem.ProductId = cart.Product.Id;
                shoppingCartItem.ProductName = cart.Product.Name;
                var pictures = cart.Product.ProductPictures.OrderBy(x => x.DisplayOrder);
                foreach (var pic in pictures)
                {
                    if (pic.IsTemplatePicture)
                    {
                        var URL = _pictureService.GetPictureUrl(pic.PictureId);
                        shoppingCartItem.TemplateImages.Add(URL);
                    }
                    else
                    {
                        var URL = _pictureService.GetPictureUrl(pic.PictureId);
                        shoppingCartItem.ProductPictures.Add(URL);
                    }
                }
                shoppingCartItem.SendToManyAddressSelected = cart.SendToManyAddressSelected;
                shoppingCartItem.Quantity = cart.Quantity;
                shoppingCartItem.Width = cart.Product.Width;
                shoppingCartItem.Height = cart.Product.Height;
                shoppingCartItem.IsCustomizable = cart.Product.IsCustomizable;

                shoppingCartItem.NumberOfPages = cart.Product.CardPages.Select(cardPage => cardPage.PageIndex).Distinct().Count();

                //addresses
                int domesticAddresses = 0;
                var Addresses = new List<Address>();
                foreach (var item in cart.ShoppingCartItemAddresses)
                {
                    for (int i = 0; i < item.Quantity; i++)
                    {
                        Addresses.Add(item.Address);
                    }
                }
                shoppingCartItem.Addresses = new List<SimpleAddressModel>();
                shoppingCartItem.Addresses.prepareAddressSimpleModel(Addresses, countries, states);
                foreach (var address in shoppingCartItem.Addresses)
                {
                    if (address.IsDomestic ?? _addressService.CheckDomesticCountry(address.CountryId))//_addressService.checkDomesticAddressByCountryId(address.CountryId))
                        domesticAddresses++;
                }

                //price with discount with site working currency currency international and domestic total
                int internationalAddresses = cart.Quantity - domesticAddresses;
                decimal taxRate = decimal.Zero;
                //product price
                decimal PriceWithDiscountBase = _taxService.GetProductPrice(cart.Product, _priceCalculationService.GetFinalPrice(cart.Product, true), out taxRate);
                decimal PriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(PriceWithDiscountBase, targetCurrency);

                //product International price
                decimal InternationalPriceWithDiscountBase = _taxService.GetProductPrice(cart.Product, _priceCalculationService.GetInternationalPrice(cart.Product, true), out taxRate);
                decimal InternationalPriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(InternationalPriceWithDiscountBase, targetCurrency);

                shoppingCartItem.TotalPrice = (PriceWithDiscount * domesticAddresses) + (InternationalPriceWithDiscount * internationalAddresses);

                //parsing the values of pages
                IList<string> pagesValues = new List<string>();
                var productAttributes = _productAttributeParser.ParseProductVariantAttributes(cart.AttributesXml);
                foreach (var attribute in productAttributes)
                {
                    if (attribute.ProductAttribute.Name.ToLower() == "card customization")
                    {
                        pagesValues = _productAttributeParser.ParseValues(cart.AttributesXml, attribute.Id);
                    }
                }
                if (pagesValues.Count > 0)
                    shoppingCartItem.Pages = (List<PageModel>)new JavaScriptSerializer().Deserialize(pagesValues.FirstOrDefault(), typeof(List<PageModel>));

                shoppingCartItems.Add(shoppingCartItem);
            }
            return Json(shoppingCartItems, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItemFromShoppingCartByItemId(int customerId, int ItemId, string currencyCode)
        {
            //get list of countries and states
            var countries = EPGServices.GetDestinations("International Outgoing").Cast<DestinationModel>().ToList();
            var states = EPGServices.GetDestinations("Local").Cast<OriginModel>().ToList();
            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                return Json(_localizationService.GetResource("ShoppingCart.CustomerNotFound"), JsonRequestBehavior.AllowGet);

            Currency targetCurrency;
            if (!string.IsNullOrEmpty(currencyCode))
                targetCurrency = _currencyService.GetCurrencyByCode(currencyCode);
            else
                targetCurrency = _workContext.WorkingCurrency;

            var shoppingCarts = customer.ShoppingCartItems.Where(x => x.Id == ItemId).ToList();

            if (shoppingCarts.Count <= 0)
                return Json(_localizationService.GetResource("ShoppingCart.CartItemNotFound"), JsonRequestBehavior.AllowGet);

            var shoppingCartItems = new List<ShoppingCartItemSimpleInfoModel>();

            foreach (var cart in shoppingCarts)
            {
                var shoppingCartItem = new ShoppingCartItemSimpleInfoModel();

                shoppingCartItem.Id = cart.Id;
                shoppingCartItem.CustomerId = customerId;
                shoppingCartItem.ProductId = cart.Product.Id;
                shoppingCartItem.ProductName = cart.Product.Name;
                var pictures = cart.Product.ProductPictures.OrderBy(x => x.DisplayOrder);
                foreach (var pic in pictures)
                {
                    if (pic.IsTemplatePicture)
                    {
                        var URL = _pictureService.GetPictureUrl(pic.PictureId);
                        shoppingCartItem.TemplateImages.Add(URL);
                    }
                    else
                    {
                        var URL = _pictureService.GetPictureUrl(pic.PictureId);
                        shoppingCartItem.ProductPictures.Add(URL);
                    }
                }
                shoppingCartItem.SendToManyAddressSelected = cart.SendToManyAddressSelected;
                shoppingCartItem.Quantity = cart.Quantity;
                shoppingCartItem.Width = cart.Product.Width;
                shoppingCartItem.Height = cart.Product.Height;
                shoppingCartItem.IsCustomizable = cart.Product.IsCustomizable;

                shoppingCartItem.NumberOfPages = cart.Product.CardPages.Select(cardPage => cardPage.PageIndex).Distinct().Count();

                //addresses
                int domesticAddresses = 0;
                var Addresses = new List<Address>();
                foreach (var item in cart.ShoppingCartItemAddresses)
                {
                    for (int i = 0; i < item.Quantity; i++)
                    {
                        Addresses.Add(item.Address);
                    }
                }
                shoppingCartItem.Addresses = new List<SimpleAddressModel>();
                shoppingCartItem.Addresses.prepareAddressSimpleModel(Addresses, countries, states, _workContext.WorkingLanguage.UniqueSeoCode);
                foreach (var address in shoppingCartItem.Addresses)
                {
                    if (address.IsDomestic ?? _addressService.CheckDomesticCountry(address.CountryId))//_addressService.checkDomesticAddressByCountryId(address.CountryId))
                        domesticAddresses++;
                }

                //price with discount with site working currency currency international and domestic total
                int internationalAddresses = cart.Quantity - domesticAddresses;
                decimal taxRate = decimal.Zero;
                //product price
                decimal PriceWithDiscountBase = _taxService.GetProductPrice(cart.Product, _priceCalculationService.GetFinalPrice(cart.Product, true), out taxRate);
                decimal PriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(PriceWithDiscountBase, targetCurrency);

                //product International price
                decimal InternationalPriceWithDiscountBase = _taxService.GetProductPrice(cart.Product, _priceCalculationService.GetInternationalPrice(cart.Product, true), out taxRate);
                decimal InternationalPriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(InternationalPriceWithDiscountBase, targetCurrency);

                shoppingCartItem.TotalPrice = (PriceWithDiscount * domesticAddresses) + (InternationalPriceWithDiscount * internationalAddresses);

                //parsing the values of pages
                IList<string> pagesValues = new List<string>();
                var productAttributes = _productAttributeParser.ParseProductVariantAttributes(cart.AttributesXml);
                foreach (var attribute in productAttributes)
                {
                    if (attribute.ProductAttribute.Name.ToLower() == "card customization")
                    {
                        pagesValues = _productAttributeParser.ParseValues(cart.AttributesXml, attribute.Id);
                    }
                }
                if (pagesValues.Count > 0)
                    shoppingCartItem.Pages = (List<PageModel>)new JavaScriptSerializer().Deserialize(pagesValues.FirstOrDefault(), typeof(List<PageModel>));

                shoppingCartItems.Add(shoppingCartItem);
            }
            return Json(shoppingCartItems, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddItemToCart_TEST()
        {
            int BrightnessValue = 10;
            int ContrastValue = 10;
            int RotationValue = 90;
            int ZoomValue = 200;
            int ScaleX = 10;
            int ScaleY = 10;
            int ScaleWidth = 200;
            int ScaleHeight = 200;

            var picture = _pictureService.GetPictureById(109);
            string path = _pictureService.GetPictureUrl(picture.Id);

            MemoryStream ms = new MemoryStream(picture.PictureBinary);
            Image i = Image.FromStream(ms);
            i.Save("D:\\org.jpg");

            byte[] newPicture = _pictureService.CopyAdjustPicture(picture.PictureBinary);

            if (BrightnessValue != 0)
                newPicture = _pictureService.SetBrightness(newPicture, BrightnessValue);
            if (ContrastValue != 0)
                newPicture = _pictureService.SetContrast(newPicture, ContrastValue);
            if (RotationValue != 0 && RotationValue != 360)
                newPicture = _pictureService.Rotate(newPicture, (int)RotationValue);
            if (ZoomValue != 100)
                newPicture = _pictureService.Resize(newPicture, ZoomValue);

            //cropping the picture
            ImageFormat pictureImageFormat = null;
            switch (picture.MimeType)
            {
                case "image/jpeg":
                    pictureImageFormat = ImageFormat.Jpeg;
                    break;
                case "image/bmp":
                    pictureImageFormat = ImageFormat.Bmp;
                    break;
                case "image/emf":
                    pictureImageFormat = ImageFormat.Emf;
                    break;
                case "image/exif":
                    pictureImageFormat = ImageFormat.Exif;
                    break;
                case "image/gif":
                    pictureImageFormat = ImageFormat.Gif;
                    break;
                case "image/icon":
                    pictureImageFormat = ImageFormat.Icon;
                    break;
                case "image/png":
                    pictureImageFormat = ImageFormat.Png;
                    break;
                case "image/tiff":
                    pictureImageFormat = ImageFormat.Tiff;
                    break;
                case "image/wmf":
                    pictureImageFormat = ImageFormat.Wmf;
                    break;
                default:
                    break;
            }
            newPicture = _pictureService.Crop(newPicture, ScaleX, ScaleY, ScaleWidth, ScaleHeight, pictureImageFormat);

            picture = _pictureService.InsertUploadedPicture(newPicture, string.Format("Picture_{0}.png", DateTime.Now.ToString()));
            var newPath = _pictureService.GetPictureUrl(picture.Id);

            MemoryStream ms2 = new MemoryStream(picture.PictureBinary);
            Image i2 = Image.FromStream(ms2);
            i2.Save("D:\\mod.png");

            return Json(newPath, JsonRequestBehavior.AllowGet);

            //get list of countries and states
            var countries = EPGServices.GetDestinations("International Outgoing").Cast<DestinationModel>().ToList();
            var states = EPGServices.GetDestinations("Local").Cast<OriginModel>().ToList();
            int customerId = 1;
            ///testing add item to cart
            var customer = _customerService.GetCustomerById(customerId);
            var model = new ShoppingCartItemSimpleInfoModel()
            {
                CustomerId = customerId,
                ProductId = 58,
            };
            model.Addresses = new List<SimpleAddressModel>();
            model.Addresses.prepareAddressSimpleModel(customer.Addresses.ToList(), countries, states, _workContext.WorkingLanguage.UniqueSeoCode);

            //var address = customerAddresses.First();
            //address.IsDomestic = _addressService.CheckDomesticCountry(address.Country);//_addressService.checkDomesticAddressByCountryId(address.CountryId);
            //model.Addresses.Add(address);
            //model.SendToManyAddressSelected = false;
            //model.Quantity = 5;

            foreach (var address in model.Addresses)
            {
                address.IsDomestic = _addressService.CheckDomesticCountry(address.CountryId);//_addressService.checkDomesticAddressByCountryId(address.CountryId);
            }
            model.SendToManyAddressSelected = true;

            var page = new PageModel();
            page.PlaceHolders = new List<PlaceHolderModel>();
            page.Id = 1;

            var holder = new PlaceHolderModel();
            holder.Id = 1;
            holder.Type = "text";
            holder.TextSize = 3;
            holder.TextFamily = "arial";
            holder.TextStyle = "B";
            holder.Text = "ali";

            page.PlaceHolders.Add(holder);

            holder = new PlaceHolderModel();
            holder.Id = 2;
            holder.Type = "Image";
            holder.TextSize = 4;
            holder.TextFamily = "";
            holder.TextStyle = "";
            holder.Text = "";
            holder.PictureData = Convert.ToBase64String(_pictureService.GetPictureById(1).PictureBinary);
            holder.FileName = "img.jpg";

            page.PlaceHolders.Add(holder);

            model.Pages = new List<PageModel>();
            model.Pages.Add(page);


            return AddItemToCart(1, "", model);
        }

        [HttpPost]
        public JsonResult AddItemToCart(int cartTypeId, string language, ShoppingCartItemSimpleInfoModel model)
        {
            //if no language with sent use current language of the site
            if (string.IsNullOrEmpty(language))
                language = _workContext.WorkingLanguage.UniqueSeoCode;
            int languageId = _languageService.GetLanguageIdByCode(language);

            var cartType = (ShoppingCartType)cartTypeId;
            int quantity;
            string attributes = "";
            List<Address> addresses;

            var customer = _customerService.GetCustomerById(model.CustomerId);
            if (customer == null)
                //no customer found
                return Json(_localizationService.GetResource("ShoppingCart.CustomerNotFound", languageId), JsonRequestBehavior.AllowGet);

            //mail to me option
            if (model.SendToManyAddressSelected == false)
            {
                quantity = model.Quantity;
                //could be a customer address need to be find first
                addresses = prepareAddressList(model.Addresses, customer.Id);
            }
            //mail for me option
            else
            {
                quantity = model.Addresses.Count();
                //addresses only for shipping with some dummy data for required field
                addresses = prepareNewAddressList(model.Addresses, customer.Id);
            }

            var product = _productService.GetProductById(model.ProductId);
            if (product == null)
                //no product found
                return Json(_localizationService.GetResource("ShoppingCart.ProductNotFound", languageId), JsonRequestBehavior.AllowGet);

            //we can add only simple products
            if (product.ProductType != ProductType.SimpleProduct)
            {
                return Json(_localizationService.GetResource("ShoppingCart.NotSimpleProduct", languageId), JsonRequestBehavior.AllowGet);
            }

            if (model.Pages != null && model.Pages.Count > 0)
            {
                //save the customization images in DB and attach it with customer
                foreach (var page in model.Pages)
                {
                    foreach (var holder in page.PlaceHolders)
                    {                        
                        if (holder.Type.ToLower() == "image")
                        {
                            //picture data is not null i.e. Mobile App
                            if (holder.PictureData != null)
                            {
                                var PictureBinary = Convert.FromBase64String(holder.PictureData);
                                var picture = _pictureService.InsertUploadedPicture(PictureBinary, holder.FileName);
                                holder.PictureId = picture.Id;
                                holder.ImageURL = _pictureService.GetPictureUrl(picture);
                                holder.PictureData = null;

                                var customerPicture = new CustomerPicture()
                                {
                                    CustomerId = customer.Id,
                                    Customer = customer,
                                    PictureId = picture.Id,
                                    Picture = picture
                                };

                                customer.CustomerPictures.Add(customerPicture);
                            }
                            //picture data is null i.e. Flash component
                            else
                            {

                                var currentPlaceHolder = product.CardPages.FirstOrDefault(x => x.Id == holder.Id);

                                var picture = _pictureService.GetPictureById(holder.PictureId);
                                
                                byte[] newPictureBinary = _pictureService.CopyAdjustPicture(picture.PictureBinary);                                

                                //fit picture to holder size

                                int holderWidth =Convert.ToInt32(CommonHelper.convertFromCmToPixel(currentPlaceHolder.Width));
                                int holderHeight =Convert.ToInt32(CommonHelper.convertFromCmToPixel(currentPlaceHolder.Height));

                                newPictureBinary = _pictureService.Fit(newPictureBinary, holderWidth, holderHeight);
                                
                                
                                //appling Brightness Value
                                if (holder.BrightnessValue != 0)
                                    newPictureBinary = _pictureService.SetBrightness(newPictureBinary, holder.BrightnessValue);

                                //appling Contrast Value
                                if (holder.ContrastValue != 0)
                                    newPictureBinary = _pictureService.SetContrast(newPictureBinary, holder.ContrastValue);

                                //appling Rotation Value
                                if (holder.RotationValue != 0 && holder.RotationValue != 360)
                                    newPictureBinary = _pictureService.Rotate(newPictureBinary, holder.RotationValue);

                                

                                //appling zoom Value
                                if (holder.ZoomValue != 0 && holder.ZoomValue != 100)
                                    newPictureBinary = _pictureService.Resize(newPictureBinary, holder.ZoomValue);                                
                                
                                //cropping the picture
                                newPictureBinary = _pictureService.Crop(newPictureBinary, holder.ImageScaleX, holder.ImageScaleY, holderWidth, holderHeight, ImageFormat.Png);                                

                                var newPicture = _pictureService.InsertUploadedPicture(newPictureBinary, string.Format("Picture_{0}.png", DateTime.Now.ToString()));                                

                                holder.PictureId = newPicture.Id;
                                holder.ImageURL = _pictureService.GetPictureUrl(newPicture);
                            }
                        }
                    }
                }
                _customerService.UpdateCustomer(customer);

                //save the pages values as a product attribute
                string PagesJson = new JavaScriptSerializer().Serialize(model.Pages);
                foreach (var attribute in product.ProductVariantAttributes)
                {
                    if (attribute.ProductAttribute.Name.ToLower() == "card customization")
                    {
                        var ctrlAttributes = PagesJson;
                        if (!String.IsNullOrEmpty(ctrlAttributes))
                        {
                            string enteredText = ctrlAttributes.Trim();
                            attributes = _productAttributeParser.AddProductAttribute(attributes,
                                attribute, enteredText);
                        }
                    }
                }

            }
            //get standard warnings without attribute validations
            //first, try to find existing shopping cart item
            var cart = customer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == cartType)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();

            IList<string> addToCartWarnings = new List<string>();
            //Item Editing mode
            if (model.IsEditing)
            {
                var shoppingCartItem = cart.Where(x => x.Id == model.Id).First();//_shoppingCartService.FindShoppingCartItemInTheCart(cart, cartType, product);

                addToCartWarnings = _shoppingCartService.GetShoppingCartItemWarnings(customer, cartType,
                    product, _storeContext.CurrentStore.Id, attributes,
                    decimal.Zero, quantity, false, true, false, false, false);

                if (shoppingCartItem != null)
                {
                    //if this item is edited delete the old item to add the new item with the new customization and quantity
                    this.DeleteCartItem(model.CustomerId, cartTypeId, shoppingCartItem.Id, "");
                }
            }

            if (addToCartWarnings.Count > 0)
            {
                //cannot be added to the cart
                //let's display standard warnings
                return Json(addToCartWarnings, JsonRequestBehavior.AllowGet);
            }

            //now let's try adding product to the cart (now including product attribute validation, etc)
            addToCartWarnings = _shoppingCartService.AddItemToCart(customer,
                product, cartType, _storeContext.CurrentStore.Id,
                attributes, decimal.Zero, quantity, true, addresses, model.SendToManyAddressSelected);

            if (addToCartWarnings.Count > 0)
            {
                //cannot be added to the cart
                //let's display standard warnings
                return Json(addToCartWarnings, JsonRequestBehavior.AllowGet);
            }

            return Json(_localizationService.GetResource("ShoppingCart.CartItemAdded", languageId), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteCartItem(int customerId, int cartTypeId, int shoppingCartItemId, string language)
        {
            //if no language with sent use current language of the site
            if (string.IsNullOrEmpty(language))
                language = _workContext.WorkingLanguage.UniqueSeoCode;
            int languageId = _languageService.GetLanguageIdByCode(language);

            var cartType = (ShoppingCartType)cartTypeId;

            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null)
                return Json(_localizationService.GetResource("ShoppingCart.CustomerNotFound", languageId), JsonRequestBehavior.AllowGet);

            //get shopping cart item
            var cart = customer.ShoppingCartItems
                .Where(x => x.ShoppingCartType == cartType)
                .Where(x => x.StoreId == _storeContext.CurrentStore.Id)
                .ToList();

            var sci = cart.FirstOrDefault(x => x.Id == shoppingCartItemId);

            if (sci == null)
                return Json(_localizationService.GetResource("ShoppingCart.CartItemNotFound", languageId), JsonRequestBehavior.AllowGet);

            //remove the cart item
            _shoppingCartService.DeleteShoppingCartItem(sci);


            return Json(_localizationService.GetResource("ShoppingCart.CartItemDeleted", languageId), JsonRequestBehavior.AllowGet);
        }

        protected List<Address> prepareAddressList(List<SimpleAddressModel> simpleAddresses, int customerId)
        {
            var customer = _customerService.GetCustomerById(customerId);

            var Addresses = new List<Address>();

            foreach (var adrs in simpleAddresses)
            {
                //search for the address if previously exist in customer addresses
                var address = customer.Addresses.ToList().FindAddress(
                    adrs.FirstName, adrs.LastName, adrs.PhoneNumber,
                    adrs.Email, adrs.FaxNumber, adrs.Company,
                    adrs.Address1, adrs.Address2, adrs.City,
                    adrs.StateProvinceId, adrs.ZipPostalCode, adrs.CountryId);

                if (address == null)
                {
                    address = new Address();
                    address.Id = adrs.Id;
                    address.FirstName = adrs.FirstName;
                    address.LastName = adrs.LastName;
                    address.Email = string.IsNullOrEmpty(adrs.Email) ? customer.Email : adrs.Email;
                    address.CountryName = adrs.CountryId;
                    address.Company = adrs.Company;
                    address.City = adrs.City;
                    address.Address1 = adrs.Address1;
                    address.Address2 = adrs.Address2;
                    address.ZipPostalCode = adrs.ZipPostalCode;
                    address.PhoneNumber = adrs.PhoneNumber;
                    address.FaxNumber = adrs.FaxNumber;
                    address.CreatedOnUtc = DateTime.Now;

                    if (_addressService.CheckDomesticCountry(adrs.CountryId))
                    {
                        address.IsDomestic = true;
                        address.StateName = adrs.StateProvinceId;
                    }
                    else
                    {
                        address.IsDomestic = false;
                        address.StateName = null;
                    }

                    customer.Addresses.Add(address);

                    //update customer to write changes to customer addresses and insert address to DB
                    _customerService.UpdateCustomer(customer);
                }
                Addresses.Add(address);
            }

            return Addresses;
        }

        protected List<Address> prepareNewAddressList(List<SimpleAddressModel> simpleAddresses, int customerId)
        {
            var customer = _customerService.GetCustomerById(customerId);

            var Addresses = new List<Address>();

            foreach (var adrs in simpleAddresses)
            {
                //search for the address if previously exist in customer addresses
                var address = customer.Addresses.ToList().FindAddress(
                    adrs.FirstName, adrs.LastName, adrs.PhoneNumber,
                    adrs.Email, adrs.FaxNumber, adrs.Company,
                    adrs.Address1, adrs.Address2, adrs.City,
                    adrs.StateProvinceId, adrs.ZipPostalCode, adrs.CountryId);

                if (address == null)
                {
                    address = new Address();
                    address.Id = adrs.Id;
                    address.FirstName = adrs.FirstName;
                    address.LastName = adrs.LastName;
                    address.Email = string.IsNullOrEmpty(adrs.Email) ? customer.Email : adrs.Email;
                    address.CountryName = adrs.CountryId;
                    address.Company = adrs.Company;
                    address.City = adrs.City;
                    address.Address1 = adrs.Address1;
                    address.Address2 = adrs.Address2;
                    address.ZipPostalCode = adrs.ZipPostalCode;
                    address.PhoneNumber = adrs.PhoneNumber;
                    address.FaxNumber = adrs.FaxNumber;
                    address.CreatedOnUtc = DateTime.Now;

                    if (_addressService.CheckDomesticCountry(adrs.CountryId))
                    {
                        address.IsDomestic = true;
                        address.StateName = adrs.StateProvinceId;
                    }
                    else
                    {
                        address.IsDomestic = false;
                        address.StateName = null;
                    }

                    _addressService.InsertAddress(address);
                }

                Addresses.Add(address);
            }

            return Addresses;
        }

        [HttpPost]
        public JsonResult ParseCSVFile(CSVFileModel CSVFileModel)
        {
            var product = _productService.GetProductById(CSVFileModel.ProductId);

            var parsedCSVFile = new ParsedCSVFileModel();

            parsedCSVFile.ProductId = CSVFileModel.ProductId;
            int domesticAddresses = 0;

            //country & state/province
            var countries = EPGServices.GetDestinations("International Outgoing").Cast<DestinationModel>().ToList();
            var states = EPGServices.GetDestinations("Local").Cast<OriginModel>().ToList();

            string[] lines = CSVFileModel.CSVFile.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 1; i < lines.Length; i++)
            {
                if (string.IsNullOrEmpty(lines[i]))
                    continue;

                string[] addressFields = lines[i].Split(',');
                var address = new SimpleAddressModel();

                address.FirstName = addressFields[0];
                address.LastName = addressFields[1];
                address.Email = addressFields[2];
                address.Company = addressFields[3];


                string countryName = addressFields[4].ToLower();
                foreach (var country in countries)
                {
                    //CSV file country and state names now in english
                    if (country.country_name_en.ToLower() == countryName)
                    {
                        address.CountryId = country.country_id;
                        address.CountryName = _workContext.WorkingLanguage.Rtl ? country.country_name_ar : country.country_name_en;
                        if (_addressService.CheckDomesticCountry(country.country_id))
                        {
                            domesticAddresses++;
                            address.IsDomestic = true;
                        }
                        else
                            address.IsDomestic = false;

                        //state / province
                        string stateName = addressFields[5].ToLower();
                        foreach (var state in states)
                        {
                            if (state.emirate_name_en.ToLower() == stateName)
                            {
                                address.StateProvinceId = state.emirate_id;
                                address.StateProvinceName = _workContext.WorkingLanguage.Rtl ? state.emirate_name_ar : state.emirate_name_en;
                                break;
                            }
                        }

                        break;
                    }
                }

                address.City = addressFields[6];
                address.Address1 = addressFields[7];
                address.Address2 = addressFields[8];
                address.ZipPostalCode = addressFields[9];
                address.PhoneNumber = addressFields[10];
                address.FaxNumber = addressFields[11];
                address.CreatedOnUtc = DateTime.Now;

                parsedCSVFile.Addresses.Add(address);
            }

            parsedCSVFile.Quantity = parsedCSVFile.Addresses.Count();
            int internationalAddresses = parsedCSVFile.Quantity - domesticAddresses;

            decimal taxRate = decimal.Zero;
            //product price
            decimal PriceWithDiscountBase = _taxService.GetProductPrice(product, _priceCalculationService.GetFinalPrice(product, true), out taxRate);
            decimal PriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(PriceWithDiscountBase, _workContext.WorkingCurrency);

            //product International price
            decimal InternationalPriceWithDiscountBase = _taxService.GetProductPrice(product, _priceCalculationService.GetInternationalPrice(product, true), out taxRate);
            decimal InternationalPriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(InternationalPriceWithDiscountBase, _workContext.WorkingCurrency);

            parsedCSVFile.TotalPrice = (PriceWithDiscount * domesticAddresses) + (InternationalPriceWithDiscount * internationalAddresses);

            return Json(parsedCSVFile, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
