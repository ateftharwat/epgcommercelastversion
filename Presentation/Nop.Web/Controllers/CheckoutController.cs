﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Plugins;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Web.Extensions;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Checkout;
using Nop.Web.Models.Common;
using Nop.Core.Infrastructure;
using Nop.Services.Media;
using Nop.Core.Domain.Media;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using Nop.Core.Domain.Directory;
using Nop.EPGIntegrationService;
using Nop.EPGIntegrationService.Models;
using Nop.Services.Messages;
using Nop.Core.Domain.Localization;
using System.Configuration;
using Nop.Services.Security;

namespace Nop.Web.Controllers
{
    [NopHttpsRequirement(SslRequirement.Yes)]
    public partial class CheckoutController : BasePublicController
    {
        #region Fields

        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly ILocalizationService _localizationService;
        private readonly ILanguageService _languageService;
        private readonly ITaxService _taxService;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IEncryptionService _encryptionService;
        private readonly ICustomerService _customerService;
        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IShippingService _shippingService;
        private readonly IPaymentService _paymentService;
        private readonly IMeasureService _measureService;
        private readonly IAddressService _addressService;
        private readonly IPluginFinder _pluginFinder;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly ILogger _logger;
        private readonly IOrderService _orderService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IPdfService _pdfService;
        private readonly IWebHelper _webHelper;
        private readonly HttpContextBase _httpContext;
        private readonly IMobileDeviceHelper _mobileDeviceHelper;


        private readonly OrderSettings _orderSettings;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly PaymentSettings _paymentSettings;
        private readonly ShippingSettings _shippingSettings;
        private readonly AddressSettings _addressSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MeasureSettings _measureSettings;

        private readonly IntegrationService EPGServices;

        #endregion

        #region Constructors

        public CheckoutController(IWorkContext workContext,
            IStoreContext storeContext,
            IStoreMappingService storeMappingService,
            IShoppingCartService shoppingCartService,
            ILocalizationService localizationService,
            ILanguageService languageService,
            ITaxService taxService,
            ICurrencyService currencyService,
            IPriceFormatter priceFormatter,
            IOrderProcessingService orderProcessingService,
            IEncryptionService encryptionService,
            ICustomerService customerService,
            ICustomerRegistrationService customerRegistrationService,
            IGenericAttributeService genericAttributeService,
            ICountryService countryService,
            IStateProvinceService stateProvinceService,
            IShippingService shippingService,
            IPaymentService paymentService,
            IMeasureService measureService,
            IAddressService addressService,
            IPluginFinder pluginFinder,
            IOrderTotalCalculationService orderTotalCalculationService,
            ILogger logger,
            IOrderService orderService,
            IWorkflowMessageService workflowMessageService,
            IPdfService pdfService,
            IWebHelper webHelper,
            HttpContextBase httpContext,
            IMobileDeviceHelper mobileDeviceHelper,
            OrderSettings orderSettings,
            RewardPointsSettings rewardPointsSettings,
            PaymentSettings paymentSettings,
            ShippingSettings shippingSettings,
            AddressSettings addressSettings,
            LocalizationSettings localizationSettings,
            MeasureSettings measureSettings)
        {
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._storeMappingService = storeMappingService;
            this._shoppingCartService = shoppingCartService;
            this._localizationService = localizationService;
            this._languageService = languageService;
            this._taxService = taxService;
            this._currencyService = currencyService;
            this._priceFormatter = priceFormatter;
            this._orderProcessingService = orderProcessingService;
            this._encryptionService = encryptionService;
            this._customerService = customerService;
            this._customerRegistrationService = customerRegistrationService;
            this._genericAttributeService = genericAttributeService;
            this._countryService = countryService;
            this._stateProvinceService = stateProvinceService;
            this._shippingService = shippingService;
            this._paymentService = paymentService;
            this._measureService = measureService;
            this._addressService = addressService;
            this._pluginFinder = pluginFinder;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._logger = logger;
            this._orderService = orderService;
            this._workflowMessageService = workflowMessageService;
            this._pdfService = pdfService;
            this._webHelper = webHelper;
            this._httpContext = httpContext;
            this._mobileDeviceHelper = mobileDeviceHelper;

            this._orderSettings = orderSettings;
            this._rewardPointsSettings = rewardPointsSettings;
            this._paymentSettings = paymentSettings;
            this._shippingSettings = shippingSettings;
            this._addressSettings = addressSettings;
            this._localizationSettings = localizationSettings;
            this._measureSettings = measureSettings;

            EPGServices = new IntegrationService();
        }

        #endregion

        #region Utilities

        [NonAction]
        protected bool IsPaymentWorkflowRequired(IList<ShoppingCartItem> cart, bool ignoreRewardPoints = false)
        {
            bool result = true;

            //check whether order total equals zero
            decimal? shoppingCartTotalBase = _orderTotalCalculationService.GetShoppingCartTotal(cart, ignoreRewardPoints);
            if (shoppingCartTotalBase.HasValue && shoppingCartTotalBase.Value == decimal.Zero)
                result = false;
            return result;
        }

        [NonAction]
        protected CheckoutBillingAddressModel PrepareBillingAddressModel(int customerId, int? selectedCountryId = null,
            bool prePopulateNewAddressWithCustomerFields = false)
        {
            var model = new CheckoutBillingAddressModel();
            model.CustomerId = customerId;
            var customer = _customerService.GetCustomerById(customerId);
            //existing addresses
            var addresses = customer.Addresses//_workContext.CurrentCustomer.Addresses
                //allow billing
                .Where(a => a.Country == null || a.Country.AllowsBilling)
                //enabled for the current store
                .Where(a => a.Country == null || _storeMappingService.Authorize(a.Country))
                .ToList();

            //get list of countries and states
            var countries = EPGServices.GetDestinations("International Outgoing").Cast<DestinationModel>().ToList();
            var states = EPGServices.GetDestinations("Local").Cast<OriginModel>().ToList();

            foreach (var address in addresses)
            {
                var addressModel = new AddressModel();
                //addressModel.PrepareModel(address,
                //    false,
                //    _addressSettings);
                addressModel.PrepareNewModel(address,
                    false,
                    _addressSettings,
                    countries, states,
                    language: _workContext.WorkingLanguage.UniqueSeoCode,
                    loadCountry: false);
                model.ExistingAddresses.Add(addressModel);
            }

            //new address
            model.NewAddress.CountryId = selectedCountryId;
            //model.NewAddress.PrepareModel(null,
            //    false,
            //    _addressSettings,
            //    _localizationService,
            //    _stateProvinceService,
            //    () => _countryService.GetAllCountriesForBilling(),
            //    prePopulateNewAddressWithCustomerFields,
            //    _workContext.CurrentCustomer);
            model.NewAddress.PrepareNewModel(null,
                false,
                _addressSettings,
                countries, states,
                _localizationService,
                _workContext.WorkingLanguage.UniqueSeoCode,
                prePopulateNewAddressWithCustomerFields,
                _workContext.CurrentCustomer);
            return model;
        }

        [NonAction]
        protected CheckoutShippingAddressModel PrepareShippingAddressModelProhibited(int customerId, List<ShoppingCartItem> cart, string selectedCountryId = null,
            bool prePopulateNewAddressWithCustomerFields = false)
        {
            var model = new CheckoutShippingAddressModel();
            model.CustomerId = customerId;
            model.AddressSettings = _addressSettings;
            //existing addresses
            var addresses = _workContext.CurrentCustomer.Addresses
                //allow shipping
                .Where(a => a.Country == null || a.Country.AllowsShipping)
                //enabled for the current store
                .Where(a => a.Country == null || _storeMappingService.Authorize(a.Country))
                .ToList();

            //get list of countries and states
            var countries = EPGServices.GetDestinations("International Outgoing").Cast<DestinationModel>().ToList();
            var states = EPGServices.GetDestinations("Local").Cast<OriginModel>().ToList();

            ViewBag.countries = countries;
            ViewBag.states = states;

            //get all shipping prohibited country Ids for all products
            string allShippingProhibitedCountries = string.Empty;
            foreach (var item in cart)
            {
                if (!string.IsNullOrEmpty(item.Product.ProhibitedCountriesIds))
                    allShippingProhibitedCountries += item.Product.ProhibitedCountriesIds;
            }

            string[] allProhibitedCountryIds;
            if (!string.IsNullOrEmpty(allShippingProhibitedCountries))
                allProhibitedCountryIds = allShippingProhibitedCountries
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            else
                allProhibitedCountryIds = new string[0];

            /* for single shipping address selected */
            //Existing Addresses
            foreach (var address in addresses)
            {
                //if this country is prohibited don't add it
                if (allProhibitedCountryIds.Contains(address.CountryName))
                    continue;

                var addressModel = new AddressModel();
                
                addressModel.PrepareNewModel(address,
                    false,
                    _addressSettings,
                    countries, states,
                    language: _workContext.WorkingLanguage.UniqueSeoCode,
                    loadCountry: false);
                model.ExistingAddresses.Add(addressModel);
            }

            //prohibited country names message
            model.AllProhibitedCountryNames = string.Empty;
            for (int j = 0; j < allProhibitedCountryIds.Length - 1; j++)
            {
                var country = countries.Where(x => x.country_id == allProhibitedCountryIds[j]).FirstOrDefault();
                model.AllProhibitedCountryNames += _workContext.WorkingLanguage.Rtl ? (country.country_name_ar + "، ") : (country.country_name_en + ", ");
            }
            //last item without comma
            if ((allProhibitedCountryIds.Length - 1) >= 0)
            {
                var lastProhibitedCountry = countries.Where(x => x.country_id == allProhibitedCountryIds[allProhibitedCountryIds.Length - 1]).FirstOrDefault();
                model.AllProhibitedCountryNames += _workContext.WorkingLanguage.Rtl ? lastProhibitedCountry.country_name_ar : lastProhibitedCountry.country_name_en;
            }

            //new address
            model.NewAddress.CountryName = selectedCountryId;            
            model.NewAddress.PrepareNewModel(null,
                false,
                _addressSettings,
                countries, states,
                _localizationService,
                _workContext.WorkingLanguage.UniqueSeoCode,
                prePopulateNewAddressWithCustomerFields,
                _workContext.CurrentCustomer,
                prohibitedCountriesIds: allProhibitedCountryIds);


            /* for multi shipping address selected */
            for (int i = 0; i < cart.Count; i++)
            {
                string[] prohibitedCountryIds;
                if (!string.IsNullOrEmpty(cart[i].Product.ProhibitedCountriesIds))
                    prohibitedCountryIds = cart[i].Product.ProhibitedCountriesIds
                        .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                else
                    prohibitedCountryIds = new string[0];

                //Existing Addresses
                var ExistingAddresses = new List<AddressModel>();
                foreach (var address in addresses)
                {
                    //if this country is prohibited don't add it
                    if (prohibitedCountryIds.Contains(address.CountryName))
                        continue;

                    var addressModel = new AddressModel();
                
                    addressModel.PrepareNewModel(address,
                        false,
                        _addressSettings,
                        countries, states,
                        language: _workContext.WorkingLanguage.UniqueSeoCode,
                        loadCountry: false);
                    ExistingAddresses.Add(addressModel);
                }
                model.ExistingAddressesPerProduct.Add(ExistingAddresses);

                //prohibited country names message
                string ProhibitedCountryNamesPerProduct = string.Empty;
                for (int j = 0; j < prohibitedCountryIds.Length - 1; j++)
                {
                    var country = countries.Where(x => x.country_id == prohibitedCountryIds[j]).FirstOrDefault();
                    ProhibitedCountryNamesPerProduct += _workContext.WorkingLanguage.Rtl ? (country.country_name_ar + "، ") : (country.country_name_en + ", ");
                }
                //last item without comma
                if (prohibitedCountryIds.Length - 1 >= 0)
                {
                    var lastCountry = countries.Where(x => x.country_id == prohibitedCountryIds[prohibitedCountryIds.Length - 1]).FirstOrDefault();
                    ProhibitedCountryNamesPerProduct += _workContext.WorkingLanguage.Rtl ? lastCountry.country_name_ar : lastCountry.country_name_en;
                }
                model.ProhibitedCountryNamesPerProduct.Add(ProhibitedCountryNamesPerProduct);

                //new address
                var newAddressModel = new AddressModel();
                newAddressModel.CountryName = selectedCountryId;
                newAddressModel.PrepareNewModel(null,
                    false,
                    _addressSettings,
                    countries, states,
                    _localizationService,
                    _workContext.WorkingLanguage.UniqueSeoCode,
                    prePopulateNewAddressWithCustomerFields,
                    _workContext.CurrentCustomer,
                    prohibitedCountriesIds: prohibitedCountryIds);
                model.NewAddressPerProduct.Add(newAddressModel);
            }

            return model;
        }

        [NonAction]
        protected CheckoutShippingAddressModel PrepareShippingAddressModel(int customerId, int? selectedCountryId = null,
            bool prePopulateNewAddressWithCustomerFields = false)
        {
            var model = new CheckoutShippingAddressModel();
            model.CustomerId = customerId;
            model.AddressSettings = _addressSettings;
            //existing addresses
            var addresses = _workContext.CurrentCustomer.Addresses
                //allow shipping
                .Where(a => a.Country == null || a.Country.AllowsShipping)
                //enabled for the current store
                .Where(a => a.Country == null || _storeMappingService.Authorize(a.Country))
                .ToList();

            //get list of countries and states
            var countries = EPGServices.GetDestinations("International Outgoing").Cast<DestinationModel>().ToList();
            var states = EPGServices.GetDestinations("Local").Cast<OriginModel>().ToList();

            ViewBag.countries = countries;
            ViewBag.states = states;

            foreach (var address in addresses)
            {
                var addressModel = new AddressModel();
                //addressModel.PrepareModel(address,
                //    false,
                //    _addressSettings);
                addressModel.PrepareNewModel(address,
                    false,
                    _addressSettings,
                    countries, states,
                    language: _workContext.WorkingLanguage.UniqueSeoCode,
                    loadCountry: false);
                model.ExistingAddresses.Add(addressModel);
            }

            //new address
            model.NewAddress.CountryId = selectedCountryId;
            //model.NewAddress.PrepareModel(null,
            //    false,
            //    _addressSettings,
            //    _localizationService,
            //    _stateProvinceService,
            //    () => _countryService.GetAllCountriesForShipping(),
            //    prePopulateNewAddressWithCustomerFields,
            //    _workContext.CurrentCustomer);
            model.NewAddress.PrepareNewModel(null,
                false,
                _addressSettings,
                countries, states,
                _localizationService,
                _workContext.WorkingLanguage.UniqueSeoCode,
                prePopulateNewAddressWithCustomerFields,
                _workContext.CurrentCustomer);
            return model;
        }

        [NonAction]
        protected CheckoutShippingMethodModel PrepareShippingMethodModel(IList<ShoppingCartItem> cart)
        {
            var model = new CheckoutShippingMethodModel();

            var getShippingOptionResponse = _shippingService
                .GetShippingOptions(cart, _workContext.CurrentCustomer.ShippingAddress,
                "", _storeContext.CurrentStore.Id);
            if (getShippingOptionResponse.Success)
            {
                //performance optimization. cache returned shipping options.
                //we'll use them later (after a customer has selected an option).
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                                                       SystemCustomerAttributeNames.OfferedShippingOptions,
                                                       getShippingOptionResponse.ShippingOptions,
                                                       _storeContext.CurrentStore.Id);

                foreach (var shippingOption in getShippingOptionResponse.ShippingOptions)
                {
                    var soModel = new CheckoutShippingMethodModel.ShippingMethodModel()
                                      {
                                          Name = shippingOption.Name,
                                          ID = shippingOption.ID,
                                          Description = shippingOption.Description,
                                          ShippingRateComputationMethodSystemName = shippingOption.ShippingRateComputationMethodSystemName,
                                          ShippingOption = shippingOption,
                                      };

                    //adjust rate
                    Discount appliedDiscount = null;
                    var shippingTotal = _orderTotalCalculationService.AdjustShippingRate(
                        shippingOption.Rate, cart, out appliedDiscount);

                    decimal rateBase = _taxService.GetShippingPrice(shippingTotal, _workContext.CurrentCustomer);
                    decimal rate = _currencyService.ConvertFromPrimaryStoreCurrency(rateBase,_workContext.WorkingCurrency);
                    soModel.Fee = _priceFormatter.FormatShippingPrice(rate, true);
                    model.ShippingMethods.Add(soModel);
                }

                //find a selected (previously) shipping method
                var selectedShippingOption =
                    _workContext.CurrentCustomer.GetAttribute<ShippingOption>(
                        SystemCustomerAttributeNames.SelectedShippingOption, _storeContext.CurrentStore.Id);
                if (selectedShippingOption != null)
                {
                    var shippingOptionToSelect = model.ShippingMethods.ToList()
                                                      .Find(
                                                          so =>
                                                          !String.IsNullOrEmpty(so.Name) &&
                                                          so.Name.Equals(selectedShippingOption.Name,
                                                                         StringComparison.InvariantCultureIgnoreCase) &&
                                                          !String.IsNullOrEmpty(
                                                              so.ShippingRateComputationMethodSystemName) &&
                                                          so.ShippingRateComputationMethodSystemName.Equals(
                                                              selectedShippingOption
                                                                  .ShippingRateComputationMethodSystemName,
                                                              StringComparison.InvariantCultureIgnoreCase));
                    if (shippingOptionToSelect != null)
                        shippingOptionToSelect.Selected = true;
                }
                //if no option has been selected, let's do it for the first one
                if (model.ShippingMethods.FirstOrDefault(so => so.Selected) == null)
                {
                    var shippingOptionToSelect = model.ShippingMethods.FirstOrDefault();
                    if (shippingOptionToSelect != null)
                        shippingOptionToSelect.Selected = true;
                }
            }
            else
            {
                foreach (var error in getShippingOptionResponse.Errors)
                    model.Warnings.Add(error);
            }

            return model;
        }

        [NonAction]
        protected CheckoutMultiShippingMethodModel PrepareMultiShippingMethodModel(int customerId, IList<ShoppingCartItem> cart)
        {
            Dictionary<int, ShippingDictionaryEntry> ItemsToSend = new Dictionary<int, ShippingDictionaryEntry>();
            string hashedKey = string.Empty;

            var measureDimension = _measureService.GetMeasureDimensionById(_measureSettings.BaseDimensionId);
            var measureWeight = _measureService.GetMeasureWeightById(_measureSettings.BaseWeightId);
            int weightUnitId = (measureWeight.SystemKeyword.ToLower() == "grams") ? 1 : 1000;
            string ShippingOrigin = ConfigurationManager.AppSettings["ShippingOrigin"];

            //get list of countries and states
            var countries = EPGServices.GetDestinations("International Outgoing").Cast<DestinationModel>().ToList();
            var states = EPGServices.GetDestinations("Local").Cast<OriginModel>().ToList();

            string originId = string.Empty;
            foreach (var state in states)
            {
                if (state.emirate_name_en.ToLower() == ShippingOrigin.ToLower())
                    originId = state.emirate_id;
            }

            var serviceTypes = EPGServices.GetServiceTypes();
            string LocalId = "", InternationalOutgoingId = "";
            foreach (var type in serviceTypes)
            {
                if(type.serv_type_name_en.ToLower() == "local")
                    LocalId = type.serv_type_id;
                else if (type.serv_type_name_en.ToLower() == "international outgoing")
                    InternationalOutgoingId = type.serv_type_id;
            }

            foreach (ShoppingCartItem sci in cart)
            {
                //If the product is customizable card there are no shipping methods for it
                if (sci.Product.IsCustomizable)
                    continue;
                // TODO: Change static Orgin & dimension unit lookup
                foreach (ShoppingCartItemAddress address in sci.ShoppingCartItemAddresses)
                {
                    hashedKey = sci.Product.Height.ToString() + sci.Product.Width.ToString() + sci.Product.Length.ToString() + sci.Product.Weight.ToString() + ShippingOrigin.ToLower() + (address.Address.StateName ?? address.Address.CountryName);
                    int k = hashedKey.GetHashCode();
                    //if exist item with the same specs and destination update the count with address quantity
                    if (ItemsToSend.Keys.Contains(k))
                        ItemsToSend[k].Count += address.Quantity;
                    //if not exist item with the same specs and destination add it to dictionary
                    else
                    {
                        //in case some error happened related to isDomestic (Not set) check it again
                        if (!address.Address.IsDomestic.HasValue)
                            address.Address.IsDomestic = _addressService.CheckDomesticCountry(address.Address.CountryName);
                        var deliveryDate = _shippingService.GetDeliveryDateById(sci.Product.DeliveryDateId);
                        ItemsToSend.Add(k, new ShippingDictionaryEntry()
                        {
                            Count = address.Quantity,
                            Item = new ShippedItemModel()
                            {
                                ItemId = k,
                                DimUnit = measureDimension.SystemKeyword,
                                WeightUnit = weightUnitId.ToString(),
                                DestinationId = address.Address.StateName ?? address.Address.CountryName,
                                AddressId = address.Address_Id,
                                Height = sci.Product.Height,
                                Length = sci.Product.Length,
                                OriginId = originId,
                                ServiceTypeId = address.Address.IsDomestic.Value ? LocalId : InternationalOutgoingId,
                                Weight = sci.Product.Weight,
                                Width = sci.Product.Width,
                                NumberOfDeliveryDays = deliveryDate != null ? deliveryDate.NumberOfDays : 0
                            }
                        });
                    }                    
                }
            }

            List<ShippingOptionResult> result = new List<ShippingOptionResult>();
            var language = _workContext.WorkingLanguage.UniqueSeoCode.ToLower();

            string ShippingOptionsServiceErrorMesssage = _localizationService.GetResource("shippingmethod.ShippingOptionsServiceError");
            foreach (int key in ItemsToSend.Keys)
            {
                result.Add(EPGServices.GetShippingOptions(ItemsToSend[key].Item, ShippingOptionsServiceErrorMesssage));
            }

            var model = new CheckoutMultiShippingMethodModel();
            model.CustomerId = customerId;
            var customer = _customerService.GetCustomerById(customerId);

            int hash = 0;
            foreach (ShippingOptionResult item in result)
            {
                var shippingMethodItem = new CheckoutMultiShippingMethodModel.ItemShippingMethodModel();
                shippingMethodItem.ItemId = item.ItemId;
                shippingMethodItem.Count = ItemsToSend[item.ItemId].Count;

                foreach (var sci in cart)
                {
                    foreach (var address in sci.ShoppingCartItemAddresses)
                    {
                        hash = (sci.Product.Height.ToString() + sci.Product.Width.ToString() + sci.Product.Length.ToString() + sci.Product.Weight.ToString() + ShippingOrigin.ToLower() + (address.Address.StateName ?? address.Address.CountryName)).GetHashCode();
                        if (hash == item.ItemId)
                        {
                            shippingMethodItem.ItemsNames.Add(sci.Product.Name);

                            var addressModel = new AddressModel();
                            addressModel.PrepareNewModel(address.Address, false, _addressSettings, countries, states, language: _workContext.WorkingLanguage.UniqueSeoCode, loadCountry: false);
                            string addressLine = addressModel.FirstName + " " + addressModel.LastName;
                            if (addressModel.StreetAddressEnabled && !String.IsNullOrEmpty(addressModel.Address1))
                                addressLine += ", " + addressModel.Address1;
                            if (addressModel.CityEnabled && !String.IsNullOrEmpty(addressModel.City) && addressModel.UserFriendlyStateProvinceName == null)
                                addressLine += ", " + addressModel.City;
                            if (addressModel.StateProvinceEnabled && !String.IsNullOrEmpty(addressModel.UserFriendlyStateProvinceName))
                                addressLine += ", " + addressModel.UserFriendlyStateProvinceName;
                            if (addressModel.CountryEnabled && !String.IsNullOrWhiteSpace(addressModel.UserFriendlyCountryName))
                                addressLine += ", " + addressModel.UserFriendlyCountryName;

                            //to eliminate duplicate addresses incase of single or multi address
                            if (!shippingMethodItem.ItemsAddresses.Contains(addressLine))
                                shippingMethodItem.ItemsAddresses.Add(addressLine);
                        }
                    }
                }

                for (int i = 0; i < item.options.Count; i++)
                {
                    var option = item.options[i];
                    //check if this is free shipping item
                    bool isFreeShippingItem = false;
                    foreach (var sci in cart)
                    {
                        hash = (sci.Product.Height.ToString() + sci.Product.Width.ToString() + sci.Product.Length.ToString() + sci.Product.Weight.ToString() + ShippingOrigin.ToLower() + ItemsToSend[item.ItemId].Item.DestinationId).GetHashCode();
                        if (hash == item.ItemId)
                        {
                            if (sci.IsFreeShipping)
                                isFreeShippingItem = true;
                        }
                    }

                    var charge = option.final_rate * shippingMethodItem.Count;
                    decimal rateBase = _taxService.GetShippingPrice(charge, customer);
                    decimal rate = _currencyService.ConvertFromPrimaryStoreCurrency(rateBase, _workContext.WorkingCurrency);

                    //free shipping item
                    if (isFreeShippingItem)
                        rate = 0;

                    string deliveryTime = _localizationService.GetResource("shippingmethod.expecteddeliverytime") + " ";
                    string days = " " + _localizationService.GetResource("shippingmethod.days");
                    var deliveryDate = option.del_date.Replace("day", "").Replace("days", "").Trim();
                    int numberofdelevriyDay = 0;
                    int.TryParse(deliveryDate, out numberofdelevriyDay);
                    numberofdelevriyDay = numberofdelevriyDay + ItemsToSend[item.ItemId].Item.NumberOfDeliveryDays;
                    shippingMethodItem.ShippingOptions.Add(new CheckoutMultiShippingMethodModel.ShippingOptionsModel()
                    {
                        ExpectedDeliveryTime = numberofdelevriyDay + days,//option.del_date,
                        Fee = _priceFormatter.FormatShippingPrice(rate, true),
                        Name = (language == "ar") ? option.col_desc_ar : option.col_desc_en,
                        ID = option.col_id.ToString(),
                        //select by default the first option in the shipping options
                        Selected = (i == 0),
                        ShippingRateComputationMethodSystemName = "emPost",
                        ShippingOption = new ShippingOption()
                        {
                            Name = (language == "ar") ? option.col_desc_ar : option.col_desc_en,
                            ID = option.col_id.ToString(),
                            Rate = rateBase,
                            ShippingRateComputationMethodSystemName = "emPost",
                            Description = numberofdelevriyDay + days //option.del_date,
                        }
                    });
                }

                model.ItemsShippingMethods.Add(shippingMethodItem);
            }

            customer.ShippingMethodsAttributes = new JavaScriptSerializer().Serialize(model);
            _customerService.UpdateCustomer(customer);

            return model;
        }

        [NonAction]
        protected CheckoutPaymentMethodModel PreparePaymentMethodModel(int customerId, IList<ShoppingCartItem> cart)
        {
            var model = new CheckoutPaymentMethodModel();
            model.CustomerId = customerId;
            var customer = _customerService.GetCustomerById(customerId);

            //reward points
            if (_rewardPointsSettings.Enabled && !cart.IsRecurring())
            {
                int rewardPointsBalance = customer.GetRewardPointsBalance();
                decimal rewardPointsAmountBase = _orderTotalCalculationService.ConvertRewardPointsToAmount(rewardPointsBalance);
                decimal rewardPointsAmount = _currencyService.ConvertFromPrimaryStoreCurrency(rewardPointsAmountBase, _workContext.WorkingCurrency);
                if (rewardPointsAmount > decimal.Zero &&
                    _orderTotalCalculationService.CheckMinimumRewardPointsToUseRequirement(rewardPointsBalance))
                {
                    model.DisplayRewardPoints = true;
                    model.RewardPointsAmount = _priceFormatter.FormatPrice(rewardPointsAmount, true, false);
                    model.RewardPointsBalance = rewardPointsBalance;
                }
            }

            //filter by country
            int filterByCountryId = 0;
            if (_addressSettings.CountryEnabled &&
                customer.BillingAddress != null &&
                customer.BillingAddress.Country != null)
            {
                filterByCountryId = customer.BillingAddress.Country.Id;
            }

            var boundPaymentMethods = _paymentService
                .LoadActivePaymentMethods(customer.Id, _storeContext.CurrentStore.Id, filterByCountryId)
                .Where(pm => pm.PaymentMethodType == PaymentMethodType.Standard || pm.PaymentMethodType == PaymentMethodType.Redirection)
                .ToList();
            foreach (var pm in boundPaymentMethods)
            {
                if (cart.IsRecurring() && pm.RecurringPaymentType == RecurringPaymentType.NotSupported)
                    continue;

                var pmModel = new CheckoutPaymentMethodModel.PaymentMethodModel()
                {
                    Name = pm.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id),
                    PaymentMethodSystemName = pm.PluginDescriptor.SystemName,
                    LogoUrl = pm.PluginDescriptor.GetLogoUrl(_webHelper)
                };
                //payment method additional fee
                decimal paymentMethodAdditionalFee = _paymentService.GetAdditionalHandlingFee(cart, pm.PluginDescriptor.SystemName);
                decimal rateBase = _taxService.GetPaymentMethodAdditionalFee(paymentMethodAdditionalFee, customer);
                decimal rate = _currencyService.ConvertFromPrimaryStoreCurrency(rateBase, _workContext.WorkingCurrency);
                if (rate > decimal.Zero)
                    pmModel.Fee = _priceFormatter.FormatPaymentMethodAdditionalFee(rate, true);

                model.PaymentMethods.Add(pmModel);
            }

            //find a selected (previously) payment method
            var selectedPaymentMethodSystemName = customer.GetAttribute<string>(
                SystemCustomerAttributeNames.SelectedPaymentMethod,
                _genericAttributeService, _storeContext.CurrentStore.Id);
            if (!String.IsNullOrEmpty(selectedPaymentMethodSystemName))
            {
                var paymentMethodToSelect = model.PaymentMethods.ToList()
                    .Find(pm => pm.PaymentMethodSystemName.Equals(selectedPaymentMethodSystemName, StringComparison.InvariantCultureIgnoreCase));
                if (paymentMethodToSelect != null)
                    paymentMethodToSelect.Selected = true;
            }
            //if no option has been selected, let's do it for the first one
            if (model.PaymentMethods.FirstOrDefault(so => so.Selected) == null)
            {
                var paymentMethodToSelect = model.PaymentMethods.FirstOrDefault();
                if (paymentMethodToSelect != null)
                    paymentMethodToSelect.Selected = true;
            }

            return model;
        }

        [NonAction]
        protected CheckoutPaymentInfoModel PreparePaymentInfoModel(int customerId, IPaymentMethod paymentMethod)
        {
            var model = new CheckoutPaymentInfoModel();
            model.CustomerId = customerId;
            string actionName;
            string controllerName;
            RouteValueDictionary routeValues;
            paymentMethod.GetPaymentInfoRoute(out actionName, out controllerName, out routeValues);
            model.PaymentInfoActionName = actionName;
            model.PaymentInfoControllerName = controllerName;
            model.PaymentInfoRouteValues = routeValues;
            model.DisplayOrderTotals = _orderSettings.OnePageCheckoutDisplayOrderTotalsOnPaymentInfoTab;
            return model;
        }

        [NonAction]
        protected CheckoutConfirmModel PrepareConfirmOrderModel(int customerId, IList<ShoppingCartItem> cart)
        {
            var model = new CheckoutConfirmModel();
            model.CustomerId = customerId;
            //terms of service
            model.TermsOfServiceOnOrderConfirmPage = _orderSettings.TermsOfServiceOnOrderConfirmPage;
            //min order amount validation
            bool minOrderTotalAmountOk = _orderProcessingService.ValidateMinOrderTotalAmount(cart);
            if (!minOrderTotalAmountOk)
            {
                decimal minOrderTotalAmount = _currencyService.ConvertFromPrimaryStoreCurrency(_orderSettings.MinOrderTotalAmount, _workContext.WorkingCurrency);
                model.MinOrderTotalWarning = string.Format(_localizationService.GetResource("Checkout.MinOrderTotalAmount"), _priceFormatter.FormatPrice(minOrderTotalAmount, true, false));
            }
            return model;
        }

        [NonAction]
        protected bool UseOnePageCheckout()
        {
            bool useMobileDevice = _mobileDeviceHelper.IsMobileDevice(_httpContext)
                && _mobileDeviceHelper.MobileDevicesSupported()
                && !_mobileDeviceHelper.CustomerDontUseMobileVersion();

            //mobile version doesn't support one-page checkout
            if (useMobileDevice)
                return false;

            //check the appropriate setting
            return _orderSettings.OnePageCheckoutEnabled;
        }

        [NonAction]
        protected bool IsMinimumOrderPlacementIntervalValid(Customer customer)
        {
            //prevent 2 orders being placed within an X seconds time frame
            if (_orderSettings.MinimumOrderPlacementInterval == 0)
                return true;

            var lastOrder = _orderService.SearchOrders(storeId: _storeContext.CurrentStore.Id,
                customerId: _workContext.CurrentCustomer.Id, pageSize: 1)
                .FirstOrDefault();
            if (lastOrder == null)
                return true;

            var interval = DateTime.UtcNow - lastOrder.CreatedOnUtc;
            return interval.TotalSeconds > _orderSettings.MinimumOrderPlacementInterval;
        }

        #endregion

        #region Methods (common)

        public ActionResult Index()
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //reset checkout data
            _customerService.ResetCheckoutData(_workContext.CurrentCustomer, _storeContext.CurrentStore.Id);

            //validation (cart)
            var checkoutAttributesXml = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.CheckoutAttributes, _genericAttributeService, _storeContext.CurrentStore.Id);
            var scWarnings = _shoppingCartService.GetShoppingCartWarnings(cart, checkoutAttributesXml, true);
            if (scWarnings.Count > 0)
                return RedirectToRoute("ShoppingCart");
            //validation (each shopping cart item)
            foreach (ShoppingCartItem sci in cart)
            {
                var sciWarnings = _shoppingCartService.GetShoppingCartItemWarnings(_workContext.CurrentCustomer,
                    sci.ShoppingCartType,
                    sci.Product,
                    sci.StoreId,
                    sci.AttributesXml,
                    sci.CustomerEnteredPrice,
                    sci.Quantity,
                    false);
                if (sciWarnings.Count > 0)
                    return RedirectToRoute("ShoppingCart");
            }

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");
            else
                return RedirectToRoute("CheckoutBillingAddress");
        }

        public ActionResult Completed(bool? isMobileCheckout , int? orderId)
        {
            //try to parse customer Id from session
            Customer customer = null;
            if (isMobileCheckout.HasValue && isMobileCheckout.Value == true)
            {
                //if this is a mobile checkout use the cutomer identifier in the session to get the customer
                int customerId = (_httpContext.Session["customerId"] != null) ? (int)_httpContext.Session["customerId"] : _workContext.CurrentCustomer.Id;
                customer = _customerService.GetCustomerById(customerId);
            }
            else
            {
                //if this is not a mobile checkout use the current cutomer
                customer = _workContext.CurrentCustomer;
            }

            //remove session entries
            _httpContext.Session.Remove("isMobileCheckout");
            _httpContext.Session.Remove("customerId");
            _httpContext.Session.Remove("cartTypeId");

            //validation
            if ((customer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            if (isMobileCheckout.HasValue)
                ViewBag.isMobileCheckout = isMobileCheckout;
            else
                ViewBag.isMobileCheckout = false;

            Order order = null;
            if (orderId.HasValue)
            {
                //load order by identifier (if provided)
                order = _orderService.GetOrderById(orderId.Value);
            }
            if (order == null)
            {
                order = _orderService.SearchOrders(storeId: _storeContext.CurrentStore.Id,
                customerId: customer.Id, pageSize: 1)
                    .FirstOrDefault();
            }
            if (order == null || order.Deleted || customer.Id != order.CustomerId)
            {
                return RedirectToRoute("HomePage");
            }

            //disable "order completed" page?
            if (_orderSettings.DisableOrderCompletedPage)
            {
                return RedirectToRoute("OrderDetails", new { orderId = order.Id });
            }

            if(order.PaymentStatusId == (int)PaymentStatus.Failed)
                ViewBag.isPaymentFailed = true;
            else
                ViewBag.isPaymentFailed = false;
            
            //model
            var model = new CheckoutCompletedModel()
            {
                OrderId = order.Id,
                OnePageCheckoutEnabled = _orderSettings.OnePageCheckoutEnabled
            };

            return View(model);
        }

        #endregion

        #region Methods (multistep checkout)

        public ActionResult BillingAddress()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //model
            var model = PrepareBillingAddressModel(_workContext.CurrentCustomer.Id, prePopulateNewAddressWithCustomerFields: true);

            //check whether "billing address" step is enabled
            if (_orderSettings.DisableBillingAddressCheckoutStep)
            {
                if (model.ExistingAddresses.Any())
                {
                    //choose the first one
                    return SelectBillingAddress(model.ExistingAddresses.First().Id);
                }
                else
                {
                    TryValidateModel(model);
                    TryValidateModel(model.NewAddress);
                    return NewBillingAddress(model);
                }
            }

            return View(model);
        }
        public ActionResult SelectBillingAddress(int addressId)
        {
            var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == addressId);
            if (address == null)
                return RedirectToRoute("CheckoutBillingAddress");

            _workContext.CurrentCustomer.BillingAddress = address;
            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

            return RedirectToRoute("CheckoutShippingAddress");
        }
        [HttpPost, ActionName("BillingAddress")]
        [FormValueRequired("nextstep")]
        public ActionResult NewBillingAddress(CheckoutBillingAddressModel model)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            if (ModelState.IsValid)
            {
                //try to find an address with the same values (don't duplicate records)
                var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                    model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                    model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                    model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                    model.NewAddress.StateProvinceName, model.NewAddress.ZipPostalCode, model.NewAddress.CountryName);
                if (address == null)
                {
                    //address is not found. let's create a new one
                    address = model.NewAddress.ToEntity();
                    address.CreatedOnUtc = DateTime.UtcNow;
                    //some validation
                    if (address.CountryId == 0)
                        address.CountryId = null;
                    if (address.StateProvinceId == 0)
                        address.StateProvinceId = null;
                    _workContext.CurrentCustomer.Addresses.Add(address);
                }
                _workContext.CurrentCustomer.BillingAddress = address;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                return RedirectToRoute("CheckoutShippingAddress");
            }


            //If we got this far, something failed, redisplay form
            model = PrepareBillingAddressModel(_workContext.CurrentCustomer.Id, selectedCountryId: model.NewAddress.CountryId);
            return View(model);
        }

        public ActionResult ShippingAddress()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                _workContext.CurrentCustomer.ShippingAddress = null;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                return RedirectToRoute("CheckoutShippingMethod");
            }

            //model
            var model = PrepareShippingAddressModel(_workContext.CurrentCustomer.Id, prePopulateNewAddressWithCustomerFields: true);
            return View(model);
        }
        public ActionResult SelectShippingAddress(int addressId)
        {
            var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == addressId);
            if (address == null)
                return RedirectToRoute("CheckoutShippingAddress");

            _workContext.CurrentCustomer.ShippingAddress = address;
            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

            return RedirectToRoute("CheckoutShippingMethod");
        }
        [HttpPost, ActionName("ShippingAddress")]
        [FormValueRequired("nextstep")]
        public ActionResult NewShippingAddress(CheckoutShippingAddressModel model)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                _workContext.CurrentCustomer.ShippingAddress = null;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                return RedirectToRoute("CheckoutShippingMethod");
            }

            if (ModelState.IsValid)
            {
                //try to find an address with the same values (don't duplicate records)
                var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                    model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                    model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                    model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                    model.NewAddress.StateProvinceName, model.NewAddress.ZipPostalCode, model.NewAddress.CountryName);
                if (address == null)
                {
                    address = model.NewAddress.ToEntity();
                    address.CreatedOnUtc = DateTime.UtcNow;
                    //some validation
                    if (address.CountryId == 0)
                        address.CountryId = null;
                    if (address.StateProvinceId == 0)
                        address.StateProvinceId = null;
                    _workContext.CurrentCustomer.Addresses.Add(address);
                }
                _workContext.CurrentCustomer.ShippingAddress = address;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                return RedirectToRoute("CheckoutShippingMethod");
            }


            //If we got this far, something failed, redisplay form
            model = PrepareShippingAddressModel(_workContext.CurrentCustomer.Id, selectedCountryId: model.NewAddress.CountryId);
            return View(model);
        }


        public ActionResult ShippingMethod()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, null, _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentMethod");
            }

            //model
            var model = PrepareShippingMethodModel(cart);

            if (_shippingSettings.BypassShippingMethodSelectionIfOnlyOne &&
                model.ShippingMethods.Count == 1)
            {
                //if we have only one shipping method, then a customer doesn't have to choose a shipping method
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedShippingOption,
                    model.ShippingMethods.First().ShippingOption,
                    _storeContext.CurrentStore.Id);

                return RedirectToRoute("CheckoutPaymentMethod");
            }

            return View(model);
        }
        [HttpPost, ActionName("ShippingMethod")]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult SelectShippingMethod(string shippingoption)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedShippingOption, null, _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentMethod");
            }

            //parse selected method 
            if (String.IsNullOrEmpty(shippingoption))
                return ShippingMethod();
            var splittedOption = shippingoption.Split(new string[] { "___" }, StringSplitOptions.RemoveEmptyEntries);
            if (splittedOption.Length != 3)
                return ShippingMethod();
            string selectedName = splittedOption[0];
            string shippingRateComputationMethodSystemName = splittedOption[1];
            string selectedID = splittedOption[2];
            //find it
            //performance optimization. try cache first
            var shippingOptions = _workContext.CurrentCustomer.GetAttribute<List<ShippingOption>>(SystemCustomerAttributeNames.OfferedShippingOptions, _storeContext.CurrentStore.Id);
            if (shippingOptions == null || shippingOptions.Count == 0)
            {
                //not found? let's load them using shipping service
                shippingOptions = _shippingService
                    .GetShippingOptions(cart, _workContext.CurrentCustomer.ShippingAddress, shippingRateComputationMethodSystemName, _storeContext.CurrentStore.Id)
                    .ShippingOptions
                    .ToList();
            }
            else
            {
                //loaded cached results. let's filter result by a chosen shipping rate computation method
                shippingOptions = shippingOptions.Where(so => so.ShippingRateComputationMethodSystemName.Equals(shippingRateComputationMethodSystemName, StringComparison.InvariantCultureIgnoreCase))
                    .ToList();
            }

            var shippingOption = shippingOptions.Find(so => !String.IsNullOrEmpty(so.Name) && so.Name.Equals(selectedName, StringComparison.InvariantCultureIgnoreCase));
            if (shippingOption == null)
                return ShippingMethod();

            //save
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, shippingOption, _storeContext.CurrentStore.Id);

            return RedirectToRoute("CheckoutPaymentMethod");
        }


        public ActionResult PaymentMethod()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //Check whether payment workflow is required
            //we ignore reward points during cart total calculation
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart, true);
            if (!isPaymentWorkflowRequired)
            {
                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentInfo");
            }

            //model
            var paymentMethodModel = PreparePaymentMethodModel(_workContext.CurrentCustomer.Id, cart);

            if (_paymentSettings.BypassPaymentMethodSelectionIfOnlyOne &&
                paymentMethodModel.PaymentMethods.Count == 1 && !paymentMethodModel.DisplayRewardPoints)
            {
                //if we have only one payment method and reward points are disabled or the current customer doesn't have any reward points
                //so customer doesn't have to choose a payment method

                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    paymentMethodModel.PaymentMethods[0].PaymentMethodSystemName,
                    _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentInfo");
            }

            return View(paymentMethodModel);
        }
        [HttpPost, ActionName("PaymentMethod")]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult SelectPaymentMethod(string paymentmethod, CheckoutPaymentMethodModel model)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //reward points
            if (_rewardPointsSettings.Enabled)
            {
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, model.UseRewardPoints,
                    _storeContext.CurrentStore.Id);
            }

            //Check whether payment workflow is required
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
            if (!isPaymentWorkflowRequired)
            {
                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);
                return RedirectToRoute("CheckoutPaymentInfo");
            }
            //payment method 
            if (String.IsNullOrEmpty(paymentmethod))
                return PaymentMethod();

            var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(paymentmethod);
            if (paymentMethodInst == null ||
                !paymentMethodInst.IsPaymentMethodActive(_paymentSettings) ||
                !_pluginFinder.AuthenticateStore(paymentMethodInst.PluginDescriptor, _storeContext.CurrentStore.Id))
                return PaymentMethod();

            //save
            _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                SystemCustomerAttributeNames.SelectedPaymentMethod, paymentmethod, _storeContext.CurrentStore.Id);

            return RedirectToRoute("CheckoutPaymentInfo");
        }


        public ActionResult PaymentInfo()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //Check whether payment workflow is required
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
            if (!isPaymentWorkflowRequired)
            {
                return RedirectToRoute("CheckoutConfirm");
            }

            //load payment method
            var paymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                SystemCustomerAttributeNames.SelectedPaymentMethod,
                _genericAttributeService, _storeContext.CurrentStore.Id);
            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(paymentMethodSystemName);
            if (paymentMethod == null)
                return RedirectToRoute("CheckoutPaymentMethod");

            //Check whether payment info should be skipped
            if (paymentMethod.SkipPaymentInfo)
            {
                //skip payment info page
                var paymentInfo = new ProcessPaymentRequest();
                //session save
                _httpContext.Session["OrderPaymentInfo"] = paymentInfo;

                return RedirectToRoute("CheckoutConfirm");
            }

            //model
            var model = PreparePaymentInfoModel(_workContext.CurrentCustomer.Id, paymentMethod);
            return View(model);
        }
        [HttpPost, ActionName("PaymentInfo")]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult EnterPaymentInfo(FormCollection form)
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //Check whether payment workflow is required
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
            if (!isPaymentWorkflowRequired)
            {
                return RedirectToRoute("CheckoutConfirm");
            }

            //load payment method
            var paymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                SystemCustomerAttributeNames.SelectedPaymentMethod,
                _genericAttributeService, _storeContext.CurrentStore.Id);
            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(paymentMethodSystemName);
            if (paymentMethod == null)
                return RedirectToRoute("CheckoutPaymentMethod");

            var paymentControllerType = paymentMethod.GetControllerType();
            var paymentController = DependencyResolver.Current.GetService(paymentControllerType) as BasePaymentController;
            var warnings = paymentController.ValidatePaymentForm(form);
            foreach (var warning in warnings)
                ModelState.AddModelError("", warning);
            if (ModelState.IsValid)
            {
                //get payment info
                var paymentInfo = paymentController.GetPaymentInfo(form);
                //session save
                _httpContext.Session["OrderPaymentInfo"] = paymentInfo;
                return RedirectToRoute("CheckoutConfirm");
            }

            //If we got this far, something failed, redisplay form
            //model
            var model = PreparePaymentInfoModel(_workContext.CurrentCustomer.Id, paymentMethod);
            return View(model);
        }


        public ActionResult Confirm()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //model
            var model = PrepareConfirmOrderModel(_workContext.CurrentCustomer.Id, cart);
            return View(model);
        }
        [HttpPost, ActionName("Confirm")]
        [ValidateInput(false)]
        public ActionResult ConfirmOrder()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();


            //model
            var model = PrepareConfirmOrderModel(_workContext.CurrentCustomer.Id, cart);
            try
            {
                var processPaymentRequest = _httpContext.Session["OrderPaymentInfo"] as ProcessPaymentRequest;
                if (processPaymentRequest == null)
                {
                    //Check whether payment workflow is required
                    if (IsPaymentWorkflowRequired(cart))
                        return RedirectToRoute("CheckoutPaymentInfo");
                    else
                        processPaymentRequest = new ProcessPaymentRequest();
                }

                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(_workContext.CurrentCustomer))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                //place order
                processPaymentRequest.StoreId = _storeContext.CurrentStore.Id;
                processPaymentRequest.CustomerId = _workContext.CurrentCustomer.Id;
                processPaymentRequest.PaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    _genericAttributeService, _storeContext.CurrentStore.Id);
                var placeOrderResult = _orderProcessingService.PlaceOrder(_workContext.CurrentCustomer.Id, processPaymentRequest);
                if (placeOrderResult.Success)
                {
                    _httpContext.Session["OrderPaymentInfo"] = null;
                    var postProcessPaymentRequest = new PostProcessPaymentRequest()
                    {
                        Order = placeOrderResult.PlacedOrder
                    };
                    _paymentService.PostProcessPayment(postProcessPaymentRequest);

                    if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
                    {
                        //redirection or POST has been done in PostProcessPayment
                        return Content("Redirected");
                    }
                    else
                    {
                        return RedirectToRoute("CheckoutCompleted", new { orderId = placeOrderResult.PlacedOrder.Id });
                    }
                }
                else
                {
                    foreach (var error in placeOrderResult.Errors)
                        model.Warnings.Add(error);
                }
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc);
                model.Warnings.Add(exc.Message);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }


        [ChildActionOnly]
        public ActionResult CheckoutProgress(CheckoutProgressStep step)
        {
            var model = new CheckoutProgressModel() { CheckoutProgressStep = step };
            return PartialView(model);
        }

        #endregion

        #region Methods (one page checkout)

        [NonAction]
        protected JsonResult OpcLoadStepAfterShippingMethod(List<ShoppingCartItem> cart, int cartTypeId, int customerId)
        {
            var customer = _customerService.GetCustomerById(customerId);
            //Check whether payment workflow is required
            //we ignore reward points during cart total calculation
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart, true);
            if (isPaymentWorkflowRequired)
            {
                //payment is required
                var paymentMethodModel = PreparePaymentMethodModel(customerId, cart);
                paymentMethodModel.CartTypeId = cartTypeId;

                if (_paymentSettings.BypassPaymentMethodSelectionIfOnlyOne &&
                    paymentMethodModel.PaymentMethods.Count == 1 && !paymentMethodModel.DisplayRewardPoints)
                {
                    //if we have only one payment method and reward points are disabled or the current customer doesn't have any reward points
                    //so customer doesn't have to choose a payment method

                    var selectedPaymentMethodSystemName = paymentMethodModel.PaymentMethods[0].PaymentMethodSystemName;
                    _genericAttributeService.SaveAttribute<string>(customer,
                        SystemCustomerAttributeNames.SelectedPaymentMethod,
                        selectedPaymentMethodSystemName, _storeContext.CurrentStore.Id);

                    var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(selectedPaymentMethodSystemName);
                    if (paymentMethodInst == null ||
                        !paymentMethodInst.IsPaymentMethodActive(_paymentSettings) ||
                        !_pluginFinder.AuthenticateStore(paymentMethodInst.PluginDescriptor, _storeContext.CurrentStore.Id))
                        throw new Exception(_localizationService.GetResource("Checkout.PaymentMethod.NotParsed"));

                    return OpcLoadStepAfterPaymentMethod(paymentMethodInst, cart, cartTypeId, customerId);
                }
                else
                {
                    //customer have to choose a payment method
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "payment-method",
                            html = this.RenderPartialViewToString("OpcPaymentMethods", paymentMethodModel)
                        },
                        goto_section = "payment_method"
                    });
                }
            }
            else
            {
                //payment is not required
                _genericAttributeService.SaveAttribute<string>(customer,
                    SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);

                var confirmOrderModel = PrepareConfirmOrderModel(customerId, cart);
                confirmOrderModel.CartTypeId = cartTypeId;
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "confirm-order",
                        html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                    },
                    goto_section = "confirm_order"
                });
            }
        }

        [NonAction]
        protected JsonResult OpcLoadStepAfterPaymentMethod(IPaymentMethod paymentMethod, List<ShoppingCartItem> cart, int cartTypeId, int customerId)
        {
            if (paymentMethod.SkipPaymentInfo)
            {
                //skip payment info page
                var paymentInfo = new ProcessPaymentRequest();
                //session save
                _httpContext.Session["OrderPaymentInfo"] = paymentInfo;

                var confirmOrderModel = PrepareConfirmOrderModel(customerId, cart);
                confirmOrderModel.CartTypeId = cartTypeId;
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "confirm-order",
                        html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                    },
                    goto_section = "confirm_order"
                });
            }
            else
            {
                //return payment info page
                var paymenInfoModel = PreparePaymentInfoModel(customerId, paymentMethod);
                paymenInfoModel.CartTypeId = cartTypeId;
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "payment-info",
                        html = this.RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                    },
                    goto_section = "payment_info"
                });
            }
        }

        public ActionResult OnePageCheckout(int cartTypeId = 1)
        {
            var cartType = (ShoppingCartType)cartTypeId;
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == cartType)//ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (!UseOnePageCheckout())
                return RedirectToRoute("Checkout");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            ViewBag.isMobileCheckout = false;
            var model = new OnePageCheckoutModel()
            {
                ShippingRequired = cart.RequiresShipping(),
                DisableBillingAddressCheckoutStep = _orderSettings.DisableBillingAddressCheckoutStep,
                CartTypeId = cartTypeId,
                CustomerId = _workContext.CurrentCustomer.Id
            };
            return View(model);
        }

        public ActionResult MobileOnePageCheckout(string language, int cartTypeId = 3, int customerId = 0, string email = "", string password = "")
        {
            //if no language with sent use current language of the site
            if (string.IsNullOrEmpty(language))
                language = _workContext.WorkingLanguage.UniqueSeoCode;
            int languageId = _languageService.GetLanguageIdByCode(language);
            var lang = _languageService.GetLanguageById(languageId);
            if (lang != null && lang.Published)
            {
                _workContext.WorkingLanguage = lang;
            }

            var cartType = (ShoppingCartType)cartTypeId;
            var customer = _customerService.GetCustomerById(customerId);
            var validationResult = _customerRegistrationService.ValidateCustomerById(customerId, email, password);

            switch (validationResult)
            {
                case CustomerLoginResults.Successful:
                    //validation
                    var cart = customer.ShoppingCartItems
                        .Where(sci => sci.ShoppingCartType == cartType)//ShoppingCartType.ShoppingCart)
                        .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                        .ToList();
                    if (cart.Count == 0)
                        return RedirectToRoute("ShoppingCart");

                    if (!UseOnePageCheckout())
                        return RedirectToRoute("Checkout");

                    if ((customer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                        return new HttpUnauthorizedResult();

                    ViewBag.isMobileCheckout = true;
                    var model = new OnePageCheckoutModel()
                    {
                        ShippingRequired = cart.RequiresShipping(),
                        DisableBillingAddressCheckoutStep = _orderSettings.DisableBillingAddressCheckoutStep,
                        CartTypeId = cartTypeId,
                        CustomerId = customerId
                    };
                    
                    return View("OnePageCheckout", model);

                case CustomerLoginResults.WrongPassword:
                    return Json(_localizationService.GetResource("Account.Login.WrongCredentials"), JsonRequestBehavior.AllowGet);

                default:
                    return Json(_localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist"), JsonRequestBehavior.AllowGet);
            }


        }
        [ChildActionOnly]
        public ActionResult OpcBillingForm(int customerId, int cartTypeId = 1, bool isMobileCheckout = false)
        {
            _httpContext.Session["customerId"] = customerId;
            _httpContext.Session["cartTypeId"] = cartTypeId;
            _httpContext.Session["isMobileCheckout"] = isMobileCheckout;

            var billingAddressModel = PrepareBillingAddressModel(customerId, prePopulateNewAddressWithCustomerFields: true);
            billingAddressModel.CartTypeId = cartTypeId;
            return PartialView("OpcBillingAddress", billingAddressModel);
        }

        [ValidateInput(false)]
        public ActionResult OpcSaveBilling(FormCollection form)
        {
            try
            {
                //try to parse isMobileCheckout flag from session
                bool isMobileCheckout = (_httpContext.Session["isMobileCheckout"] != null) ? (bool)_httpContext.Session["isMobileCheckout"] : false;

                //try to parse cart type Id from session
                int cartTypeId = (_httpContext.Session["cartTypeId"] != null) ? (int)_httpContext.Session["cartTypeId"] : (int)ShoppingCartType.ShoppingCart;
                var cartType = (ShoppingCartType)cartTypeId;

                //try to parse customer Id from session
                Customer customer = null;
                if (isMobileCheckout)
                {
                    //if this is a mobile checkout use the cutomer identifier in the session to get the customer
                    int customerId = (_httpContext.Session["customerId"] != null) ? (int)_httpContext.Session["customerId"] : _workContext.CurrentCustomer.Id;
                    customer = _customerService.GetCustomerById(customerId);
                }
                else
                {
                    //if this is not a mobile checkout use the current cutomer
                    customer = _workContext.CurrentCustomer;
                }

                //validation
                var cart = customer.ShoppingCartItems//_workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == cartType)//ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();

                if ((customer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception(_localizationService.GetResource("Checkout.AnonymousCheckout"));

                if (cart.Count == 0)
                    throw new Exception(_localizationService.GetResource("Checkout.EmptyCart"));

                if (!UseOnePageCheckout())
                    throw new Exception(_localizationService.GetResource("Checkout.OnePageCheckoutDisabled"));

                //save the number of cart items and the total quantities for later validations
                _httpContext.Session["cartNumberOfItems"] = cart.Count;
                _httpContext.Session["cartTotalQuantity"] = cart.Sum(x => x.Quantity);
                foreach (var item in cart)
                {
                    _httpContext.Session[item.Id.ToString()] = item.Quantity;
                }

                int billingAddressId = 0;
                int.TryParse(form["billing_address_id"], out billingAddressId);

                if (billingAddressId > 0)
                {
                    //existing address
                    var address = customer.Addresses.FirstOrDefault(a => a.Id == billingAddressId);
                    if (address == null)
                        throw new Exception(_localizationService.GetResource("Checkout.AddressNotLoaded"));

                    customer.BillingAddress = address;
                    _customerService.UpdateCustomer(customer);
                }
                else
                {
                    //new address
                    var model = new CheckoutBillingAddressModel();
                    TryUpdateModel(model.NewAddress, "BillingNewAddress");
                    //validate model
                    TryValidateModel(model.NewAddress);

                    if (!ModelState.IsValid)
                    {
                        //model is not valid. redisplay the form with errors
                        var billingAddressModel = PrepareBillingAddressModel(customer.Id, selectedCountryId: model.NewAddress.CountryId);
                        billingAddressModel.NewAddressPreselected = true;
                        billingAddressModel.CartTypeId = cartTypeId;
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "billing",
                                html = this.RenderPartialViewToString("OpcBillingAddress", billingAddressModel)
                            },
                            wrong_billing_address = true,
                        });
                    }

                    //try to find an address with the same values (don't duplicate records)
                    var address = customer.Addresses.ToList().FindAddress(
                        model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                        model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                        model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                        model.NewAddress.StateProvinceName, model.NewAddress.ZipPostalCode, model.NewAddress.CountryName);
                    if (address == null)
                    {
                        //address is not found. let's create a new one
                        address = model.NewAddress.ToEntity();

                        //newly added address from the checkout page
                        if (address.CountryName == "0")
                            address.CountryName = null;
                        if (address.StateName == "0")
                            address.StateName = null;
                        address.IsDomestic = _addressService.CheckDomesticCountry(address.CountryName);
                        address.CreatedOnUtc = DateTime.UtcNow;
                        //some validation
                        if (address.CountryId == 0)
                            address.CountryId = null;
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;
                        if (address.CountryId.HasValue && address.CountryId.Value > 0)
                        {
                            address.Country = _countryService.GetCountryById(address.CountryId.Value);
                        }
                        customer.Addresses.Add(address);
                    }
                    customer.BillingAddress = address;
                    _customerService.UpdateCustomer(customer);
                }

                if (cart.RequiresShipping())
                {
                    //shipping is required
                    var shippingAddressModel = PrepareShippingAddressModelProhibited(customer.Id, cart, prePopulateNewAddressWithCustomerFields: true);
                    shippingAddressModel.CartTypeId = cartTypeId;
                    ViewBag.cart = cart;
                    //value indicating that there is only customized products in the shopping cart
                    shippingAddressModel.OnlyCustomizedProducts = true;
                    //value indicating that there is a customized products in the shopping cart
                    shippingAddressModel.HasCustomizableProduct = false;
                    foreach (var item in cart)
                    {
                        if (!item.Product.IsCustomizable)
                        {
                            shippingAddressModel.OnlyCustomizedProducts = false;
                        }
                        else
                        {
                            shippingAddressModel.HasCustomizableProduct = true;
                        }
                    }

                    //value indicating that there is only one products in the shopping cart
                    shippingAddressModel.IsSingleProduct = false;
                    if(cart.Count == 1 && cart.First().Quantity == 1)
                    {
                        shippingAddressModel.IsSingleProduct = true;
                    }

                    var jsonResult = Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "shipping",
                            html = this.RenderPartialViewToString("OpcShippingAddressChoice", shippingAddressModel)
                        },
                        goto_section = "shipping"
                    });
                    jsonResult.MaxJsonLength = int.MaxValue;
                    return jsonResult;

                }
                else
                {
                    //shipping is not required
                    _genericAttributeService.SaveAttribute<ShippingOption>(customer, SystemCustomerAttributeNames.SelectedShippingOption, null, _storeContext.CurrentStore.Id);

                    //load next step
                    return OpcLoadStepAfterShippingMethod(cart, cartTypeId, customer.Id);
                }
            }
            catch (Exception exc)
            {
                //try to parse isMobileCheckout flag from session
                bool isMobileCheckout = (_httpContext.Session["isMobileCheckout"] != null) ? (bool)_httpContext.Session["isMobileCheckout"] : false;
                //try to parse customer Id from session
                Customer customer = null;
                if (isMobileCheckout)
                {
                    //if this is a mobile checkout use the cutomer identifier in the session to get the customer
                    int customerId = (_httpContext.Session["customerId"] != null) ? (int)_httpContext.Session["customerId"] : _workContext.CurrentCustomer.Id;
                    customer = _customerService.GetCustomerById(customerId);
                }
                else
                {
                    //if this is not a mobile checkout use the current cutomer
                    customer = _workContext.CurrentCustomer;
                }

                _logger.Warning(exc.Message, exc, customer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSaveShipping(FormCollection form)
        {
            try
            {
                //try to parse isMobileCheckout flag from session
                bool isMobileCheckout = (_httpContext.Session["isMobileCheckout"] != null) ? (bool)_httpContext.Session["isMobileCheckout"] : false;

                //try to parse cart type Id from session
                int cartTypeId = (_httpContext.Session["cartTypeId"] != null) ? (int)_httpContext.Session["cartTypeId"] : (int)ShoppingCartType.ShoppingCart;
                var cartType = (ShoppingCartType)cartTypeId;

                //try to parse customer Id from session
                Customer customer = null;
                if (isMobileCheckout)
                {
                    //if this is a mobile checkout use the cutomer identifier in the session to get the customer
                    int customerId = (_httpContext.Session["customerId"] != null) ? (int)_httpContext.Session["customerId"] : _workContext.CurrentCustomer.Id;
                    customer = _customerService.GetCustomerById(customerId);
                }
                else
                {
                    //if this is not a mobile checkout use the current cutomer
                    customer = _workContext.CurrentCustomer;
                }

                //validation
                var cart = customer.ShoppingCartItems//_workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == cartType)//ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();

                if ((customer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception(_localizationService.GetResource("Checkout.AnonymousCheckout"));

                if (cart.Count == 0)
                    throw new Exception(_localizationService.GetResource("Checkout.EmptyCart"));

                if (!UseOnePageCheckout())
                    throw new Exception(_localizationService.GetResource("Checkout.OnePageCheckoutDisabled"));

                if (!cart.RequiresShipping())
                    throw new Exception(_localizationService.GetResource("Checkout.ShippingNotRequired"));

                //validate if item added or deleted from cart
                int cartNumberOfItems = (_httpContext.Session["cartNumberOfItems"] != null) ? (int)_httpContext.Session["cartNumberOfItems"] : cart.Count;
                int cartTotalQuantity = (_httpContext.Session["cartTotalQuantity"] != null) ? (int)_httpContext.Session["cartTotalQuantity"] : cart.Sum(x => x.Quantity);
                if (cart.Count != cartNumberOfItems || cart.Sum(x => x.Quantity) != cartTotalQuantity)
                {
                    throw new Exception(_localizationService.GetResource("Checkout.ItemAddedOrDeletedError"));
                }
                else
                {
                    //validate furthur if quantity increased or decreased for item
                    foreach (var item in cart)
                    {
                        if (item.Quantity != (int)_httpContext.Session[item.Id.ToString()])
                            throw new Exception(_localizationService.GetResource("Checkout.ItemAddedOrDeletedError"));
                    }
                }

                bool OnlyCustomizableProducts = true;
                customer.SendToManyAddressSelected = null;
                foreach (var item in cart)
                {
                    if (!item.Product.IsCustomizable)
                        OnlyCustomizableProducts = false;
                }
                
                if (!OnlyCustomizableProducts)
                {
                    var multiShippingAddressModel = new CheckoutShippingAddressModel();
                    bool multiShippingAddressModelPrepared = false;
                    if (form["SendToManyAddressSelected"] == "True")
                    {
                        customer.SendToManyAddressSelected = true;
                        for (int i = 0; i < cart.Count; i++)
                        {
                            var cartItem = cart[i];
                            //escaping the customized product beacause it has its addresses
                            if (cartItem.Product.IsCustomizable)
                                continue;

                            _shoppingCartService.DeleteShoppingCartItemAddresses(cartItem.Id);
                            for (int j = 0; j < cartItem.Quantity; j++)
                            {
                                int appendedValue = cartItem.Id + cartItem.ProductId + j * 100;
                                int shippingAddressId = 0;
                                int.TryParse(form["shipping_address_id" + appendedValue.ToString()], out shippingAddressId);
                                if (shippingAddressId > 0)
                                {
                                    //existing address
                                    var address = customer.Addresses.FirstOrDefault(a => a.Id == shippingAddressId);
                                    if (address == null)
                                        throw new Exception(_localizationService.GetResource("Checkout.AddressNotLoaded"));

                                    bool sciAddressFound = false;
                                    //search for address in previous added addresses
                                    //if found update quantity of address
                                    foreach (var sciAddress in cartItem.ShoppingCartItemAddresses)
                                    {
                                        if (sciAddress.Address_Id == address.Id && address.Id != 0)
                                        {
                                            sciAddress.Quantity = sciAddress.Quantity + 1;
                                            sciAddressFound = true;
                                            break;
                                        }
                                    }
                                    //if address not found add the address to list
                                    if (!sciAddressFound)
                                    {
                                        var sciAddresses = new ShoppingCartItemAddress()
                                        {
                                            Address_Id = address.Id,
                                            Address = address,
                                            ShoppingCartItem_Id = cartItem.Id,
                                            CartItem = cartItem,
                                            Quantity = 1,
                                        };
                                        cartItem.ShoppingCartItemAddresses.Add(sciAddresses);
                                    }
                                }
                                else
                                {
                                    //new address
                                    var model = new CheckoutShippingAddressModel();
                                    TryUpdateModel(model.NewAddress, "ShippingNewAddress" + appendedValue.ToString());
                                    //validate model
                                    TryValidateModel(model.NewAddress);

                                    if (!ModelState.IsValid)
                                    {
                                        //model is not valid. redisplay the form with errors
                                        if (!multiShippingAddressModelPrepared)
                                        {
                                            //prepare the address model once but validate all the addresses
                                            multiShippingAddressModelPrepared = true;
                                            multiShippingAddressModel = PrepareShippingAddressModelProhibited(customer.Id, cart, selectedCountryId: model.NewAddress.CountryName);
                                            multiShippingAddressModel.CartTypeId = cartTypeId;
                                            multiShippingAddressModel.SendToManyAddressSelected = true;

                                            ViewBag.cart = cart;
                                            //value indicating that there is only customized products in the shopping cart
                                            multiShippingAddressModel.OnlyCustomizedProducts = OnlyCustomizableProducts;
                                            //value indicating that there is a customized products in the shopping cart
                                            multiShippingAddressModel.HasCustomizableProduct = false;
                                            foreach (var item in cart)
                                            {
                                                if (item.Product.IsCustomizable)
                                                {
                                                    multiShippingAddressModel.HasCustomizableProduct = true;
                                                    break;
                                                }
                                            }

                                            //value indicating that there is only one products in the shopping cart
                                            multiShippingAddressModel.IsSingleProduct = false;
                                            if (cart.Count == 1 && cart.First().Quantity == 1)
                                            {
                                                multiShippingAddressModel.IsSingleProduct = true;
                                            }
                                        }

                                        //append all the new addresses to render the new address form for all of them
                                        multiShippingAddressModel.NewAddressesPreselected.Add(appendedValue);

                                        //there is now rendering here for the shipping address for
                                        //as the validation on the rest of itms address still not finished
                                        //but we need to continue to avoid saving the invalid address
                                        continue;
                                    }

                                    //try to find an address with the same values (don't duplicate records)
                                    var address = customer.Addresses.ToList().FindAddress(
                                        model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                                        model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                                        model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                                        model.NewAddress.StateProvinceName, model.NewAddress.ZipPostalCode, model.NewAddress.CountryName);
                                    if (address == null)
                                    {
                                        address = model.NewAddress.ToEntity();

                                        //newly added address from the checkout page
                                        if (address.CountryName == "0")
                                            address.CountryName = null;
                                        if (address.StateName == "0")
                                            address.StateName = null;
                                        address.IsDomestic = _addressService.CheckDomesticCountry(address.CountryName);
                                        address.CreatedOnUtc = DateTime.UtcNow;
                                        //little hack here (TODO: find a better solution)
                                        //EF does not load navigation properties for newly created entities (such as this "Address").
                                        //we have to load them manually 
                                        //otherwise, "Country" property of "Address" entity will be null in shipping rate computation methods
                                        if (address.CountryId.HasValue)
                                            address.Country = _countryService.GetCountryById(address.CountryId.Value);
                                        if (address.StateProvinceId.HasValue)
                                            address.StateProvince = _stateProvinceService.GetStateProvinceById(address.StateProvinceId.Value);

                                        //other null validations
                                        if (address.CountryId == 0)
                                            address.CountryId = null;
                                        if (address.StateProvinceId == 0)
                                            address.StateProvinceId = null;

                                        customer.Addresses.Add(address);

                                        //update customer to write changes to customer addresses to have the address Id
                                        _customerService.UpdateCustomer(customer);
                                    }

                                    bool sciAddressFound = false;
                                    //search for address in previous added addresses
                                    //if found update quantity of address
                                    foreach (var sciAddress in cartItem.ShoppingCartItemAddresses)
                                    {
                                        if (sciAddress.Address_Id == address.Id && address.Id != 0)
                                        {
                                            sciAddress.Quantity = sciAddress.Quantity + 1;
                                            sciAddressFound = true;
                                            break;
                                        }
                                    }
                                    //if address not found add the address to list
                                    if (!sciAddressFound)
                                    {
                                        var sciAddresses = new ShoppingCartItemAddress()
                                        {
                                            Address_Id = address.Id,
                                            Address = address,
                                            ShoppingCartItem_Id = cartItem.Id,
                                            CartItem = cartItem,
                                            Quantity = 1,
                                        };
                                        cartItem.ShoppingCartItemAddresses.Add(sciAddresses);
                                    }
                                }
                            }
                        }
                    }
                    else //single address shipping
                    {
                        customer.SendToManyAddressSelected = false;
                        int shippingAddressId = 0;
                        int.TryParse(form["shipping_address_id"], out shippingAddressId);
                        if (shippingAddressId > 0)
                        {
                            //existing address
                            var address = customer.Addresses.FirstOrDefault(a => a.Id == shippingAddressId);
                            if (address == null)
                                throw new Exception(_localizationService.GetResource("Checkout.AddressNotLoaded"));

                            foreach (var cartItem in cart)
                            {
                                //escaping the customized product beacause it has its addresses
                                if (cartItem.Product.IsCustomizable)
                                    continue;
                                _shoppingCartService.DeleteShoppingCartItemAddresses(cartItem.Id);
                                var sciAddresses = new ShoppingCartItemAddress()
                                {
                                    Address_Id = address.Id,
                                    Address = address,
                                    ShoppingCartItem_Id = cartItem.Id,
                                    CartItem = cartItem,
                                    Quantity = cartItem.Quantity,
                                };
                                cartItem.ShoppingCartItemAddresses.Add(sciAddresses);
                            }
                        }
                        else
                        {
                            //new address
                            var model = new CheckoutShippingAddressModel();
                            TryUpdateModel(model.NewAddress, "ShippingNewAddress");
                            //validate model
                            TryValidateModel(model.NewAddress);

                            if (!ModelState.IsValid)
                            {
                                //model is not valid. redisplay the form with errors
                                var shippingAddressModel = PrepareShippingAddressModelProhibited(customer.Id, cart, selectedCountryId: model.NewAddress.CountryName);
                                shippingAddressModel.CartTypeId = cartTypeId;
                                shippingAddressModel.SendToManyAddressSelected = false;
                                shippingAddressModel.NewAddressesPreselected.Add(-1);
                                ViewBag.cart = cart;
                                //value indicating that there is only customized products in the shopping cart
                                shippingAddressModel.OnlyCustomizedProducts = OnlyCustomizableProducts;
                                //value indicating that there is a customized products in the shopping cart
                                shippingAddressModel.HasCustomizableProduct = false;
                                foreach (var item in cart)
                                {
                                    if (item.Product.IsCustomizable)
                                    {
                                        shippingAddressModel.HasCustomizableProduct = true;
                                        break;
                                    }
                                }

                                //value indicating that there is only one products in the shopping cart
                                shippingAddressModel.IsSingleProduct = false;
                                if (cart.Count == 1 && cart.First().Quantity == 1)
                                {
                                    shippingAddressModel.IsSingleProduct = true;
                                }

                                return Json(new
                                {
                                    update_section = new UpdateSectionJsonModel()
                                    {
                                        name = "shipping",
                                        html = this.RenderPartialViewToString("OpcShippingAddressChoice", shippingAddressModel)
                                    },
                                    goto_section = "shipping"
                                });
                            }

                            //try to find an address with the same values (don't duplicate records)
                            var address = customer.Addresses.ToList().FindAddress(
                                model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                                model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                                model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                                model.NewAddress.StateProvinceName, model.NewAddress.ZipPostalCode, model.NewAddress.CountryName);
                            if (address == null)
                            {
                                address = model.NewAddress.ToEntity();

                                //newly added address from the checkout page
                                if (address.CountryName == "0")
                                    address.CountryName = null;
                                if (address.StateName == "0")
                                    address.StateName = null;
                                address.IsDomestic = _addressService.CheckDomesticCountry(address.CountryName);
                                address.CreatedOnUtc = DateTime.UtcNow;
                                //little hack here (TODO: find a better solution)
                                //EF does not load navigation properties for newly created entities (such as this "Address").
                                //we have to load them manually 
                                //otherwise, "Country" property of "Address" entity will be null in shipping rate computation methods
                                if (address.CountryId.HasValue)
                                    address.Country = _countryService.GetCountryById(address.CountryId.Value);
                                if (address.StateProvinceId.HasValue)
                                    address.StateProvince = _stateProvinceService.GetStateProvinceById(address.StateProvinceId.Value);

                                //other null validations
                                if (address.CountryId == 0)
                                    address.CountryId = null;
                                if (address.StateProvinceId == 0)
                                    address.StateProvinceId = null;

                                customer.Addresses.Add(address);

                                //update customer to write changes to customer addresses to have the address Id in DB
                                _customerService.UpdateCustomer(customer);
                            }

                            foreach (var cartItem in cart)
                            {
                                //escaping the customized product beacause it has its addresses
                                if (cartItem.Product.IsCustomizable)
                                    continue;
                                _shoppingCartService.DeleteShoppingCartItemAddresses(cartItem.Id);

                                var sciAddresses = new ShoppingCartItemAddress()
                                {
                                    Address_Id = address.Id,
                                    Address = address,
                                    ShoppingCartItem_Id = cartItem.Id,
                                    CartItem = cartItem,
                                    Quantity = cartItem.Quantity,
                                };
                                cartItem.ShoppingCartItemAddresses.Add(sciAddresses);
                            }
                        }

                    }

                    //in case of the muli address shipping option selected  and the model state is not valid
                    //then render the view again but with all new address forms for new addresses shown
                    if (customer.SendToManyAddressSelected == true && !ModelState.IsValid)
                    {
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "shipping",
                                html = this.RenderPartialViewToString("OpcShippingAddressChoice", multiShippingAddressModel)
                            },
                            goto_section = "shipping"
                        });
                    }

                }                

                _customerService.UpdateCustomer(customer);

                if (OnlyCustomizableProducts)
                {
                    //load next step
                    customer.ShippingMethodsAttributes = "";
                    _customerService.UpdateCustomer(customer);
                    return OpcLoadStepAfterShippingMethod(cart, cartTypeId, customer.Id);
                }

                var shippingMethodModel = PrepareMultiShippingMethodModel(customer.Id, cart);
                shippingMethodModel.CartTypeId = cartTypeId;

                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "shipping-method",
                        html = this.RenderPartialViewToString("OpcMultiShippingMethods", shippingMethodModel)
                    },
                    goto_section = "shipping_method"
                });
            }
            catch (Exception exc)
            {
                //try to parse isMobileCheckout flag from session
                bool isMobileCheckout = (_httpContext.Session["isMobileCheckout"] != null) ? (bool)_httpContext.Session["isMobileCheckout"] : false;
                //try to parse customer Id from session
                Customer customer = null;
                if (isMobileCheckout)
                {
                    //if this is a mobile checkout use the cutomer identifier in the session to get the customer
                    int customerId = (_httpContext.Session["customerId"] != null) ? (int)_httpContext.Session["customerId"] : _workContext.CurrentCustomer.Id;
                    customer = _customerService.GetCustomerById(customerId);
                }
                else
                {
                    //if this is not a mobile checkout use the current cutomer
                    customer = _workContext.CurrentCustomer;
                }

                _logger.Warning(exc.Message, exc, customer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]        
        public ActionResult OpcSaveShippingMethod(FormCollection form)
        {
            try
            {
                //try to parse isMobileCheckout flag from session
                bool isMobileCheckout = (_httpContext.Session["isMobileCheckout"] != null) ? (bool)_httpContext.Session["isMobileCheckout"] : false;

                //try to parse cart type Id from session
                int cartTypeId = (_httpContext.Session["cartTypeId"] != null) ? (int)_httpContext.Session["cartTypeId"] : (int)ShoppingCartType.ShoppingCart;
                var cartType = (ShoppingCartType)cartTypeId;

                //try to parse customer Id from session
                Customer customer = null;
                if (isMobileCheckout)
                {
                    //if this is a mobile checkout use the cutomer identifier in the session to get the customer
                    int customerId = (_httpContext.Session["customerId"] != null) ? (int)_httpContext.Session["customerId"] : _workContext.CurrentCustomer.Id;
                    customer = _customerService.GetCustomerById(customerId);
                }
                else
                {
                    //if this is not a mobile checkout use the current cutomer
                    customer = _workContext.CurrentCustomer;
                }

                //validation
                var cart = customer.ShoppingCartItems//_workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == cartType)//ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();

                if ((customer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception(_localizationService.GetResource("Checkout.AnonymousCheckout"));

                if (cart.Count == 0)
                    throw new Exception(_localizationService.GetResource("Checkout.EmptyCart"));

                if (!UseOnePageCheckout())
                    throw new Exception(_localizationService.GetResource("Checkout.OnePageCheckoutDisabled"));

                if (!cart.RequiresShipping())
                    throw new Exception(_localizationService.GetResource("Checkout.ShippingNotRequired"));

                //validate if item added or deleted from cart
                int cartNumberOfItems = (_httpContext.Session["cartNumberOfItems"] != null) ? (int)_httpContext.Session["cartNumberOfItems"] : cart.Count;
                int cartTotalQuantity = (_httpContext.Session["cartTotalQuantity"] != null) ? (int)_httpContext.Session["cartTotalQuantity"] : cart.Sum(x => x.Quantity);
                if (cart.Count != cartNumberOfItems || cart.Sum(x => x.Quantity) != cartTotalQuantity)
                {
                    throw new Exception(_localizationService.GetResource("Checkout.ItemAddedOrDeletedError"));
                }
                else
                {
                    //validate furthur if quantity increased or decreased for item
                    foreach (var item in cart)
                    {
                        if (item.Quantity != (int)_httpContext.Session[item.Id.ToString()])
                            throw new Exception(_localizationService.GetResource("Checkout.ItemAddedOrDeletedError"));
                    }
                }

                string ShippingOrigin = ConfigurationManager.AppSettings["ShippingOrigin"];
                var ShippingMethodModel = new CheckoutMultiShippingMethodModel();
                if(!string.IsNullOrEmpty(customer.ShippingMethodsAttributes))
                    ShippingMethodModel = (CheckoutMultiShippingMethodModel)new JavaScriptSerializer().Deserialize(customer.ShippingMethodsAttributes, typeof(CheckoutMultiShippingMethodModel));

                //parse selected method 
                foreach (var sci in cart)
                {
                    //If the product is customizable card there are no shipping methods for it
                    if (sci.Product.IsCustomizable)
                        continue;
                    foreach (var address in sci.ShoppingCartItemAddresses)
                    {
                        var ItemId = (sci.Product.Height.ToString() + sci.Product.Width.ToString() + sci.Product.Length.ToString() + sci.Product.Weight.ToString() + ShippingOrigin.ToLower() + (address.Address.StateName ?? address.Address.CountryName)).GetHashCode();
                        string shippingoption = form["shippingoption_" + ItemId.ToString()];
                        if (String.IsNullOrEmpty(shippingoption))
                            throw new Exception(_localizationService.GetResource("Checkout.ShippingMethod.NotParsed"));//Selected shipping method can't be parsed

                        var splittedOption = shippingoption.Split(new string[] { "___" }, StringSplitOptions.RemoveEmptyEntries);
                        if (splittedOption.Length != 3)
                            throw new Exception(_localizationService.GetResource("Checkout.ShippingMethod.NotParsed"));
                        string selectedName = splittedOption[0];
                        string shippingRateComputationMethodSystemName = splittedOption[1];
                        string selectedID = splittedOption[2];
                        //find selected options and mark it                     
                        foreach (var method in ShippingMethodModel.ItemsShippingMethods)
                        {
                            if (method.ItemId == ItemId)
                            {
                                foreach (var option in method.ShippingOptions)
                                {
                                    if (option.Name == selectedName)
                                        option.Selected = true;
                                    else
                                        option.Selected = false;
                                }
                            }
                        }

                    }
                }

                customer.ShippingMethodsAttributes = new JavaScriptSerializer().Serialize(ShippingMethodModel);
                _customerService.UpdateCustomer(customer);

                //load next step
                return OpcLoadStepAfterShippingMethod(cart, cartTypeId, customer.Id);
            }
            catch (Exception exc)
            {
                //try to parse isMobileCheckout flag from session
                bool isMobileCheckout = (_httpContext.Session["isMobileCheckout"] != null) ? (bool)_httpContext.Session["isMobileCheckout"] : false;
                //try to parse customer Id from session
                Customer customer = null;
                if (isMobileCheckout)
                {
                    //if this is a mobile checkout use the cutomer identifier in the session to get the customer
                    int customerId = (_httpContext.Session["customerId"] != null) ? (int)_httpContext.Session["customerId"] : _workContext.CurrentCustomer.Id;
                    customer = _customerService.GetCustomerById(customerId);
                }
                else
                {
                    //if this is not a mobile checkout use the current cutomer
                    customer = _workContext.CurrentCustomer;
                }

                _logger.Warning(exc.Message, exc, customer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSavePaymentMethod(FormCollection form)
        {
            try
            {
                //try to parse isMobileCheckout flag from session
                bool isMobileCheckout = (_httpContext.Session["isMobileCheckout"] != null) ? (bool)_httpContext.Session["isMobileCheckout"] : false;

                //try to parse cart type Id from session
                int cartTypeId = (_httpContext.Session["cartTypeId"] != null) ? (int)_httpContext.Session["cartTypeId"] : (int)ShoppingCartType.ShoppingCart;
                var cartType = (ShoppingCartType)cartTypeId;

                //try to parse customer Id from session
                Customer customer = null;
                if (isMobileCheckout)
                {
                    //if this is a mobile checkout use the cutomer identifier in the session to get the customer
                    int customerId = (_httpContext.Session["customerId"] != null) ? (int)_httpContext.Session["customerId"] : _workContext.CurrentCustomer.Id;
                    customer = _customerService.GetCustomerById(customerId);
                }
                else
                {
                    //if this is not a mobile checkout use the current cutomer
                    customer = _workContext.CurrentCustomer;
                }

                //validation
                var cart = customer.ShoppingCartItems//_workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == cartType)//ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();

                if ((customer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception(_localizationService.GetResource("Checkout.AnonymousCheckout"));

                if (cart.Count == 0)
                    throw new Exception(_localizationService.GetResource("Checkout.EmptyCart"));

                if (!UseOnePageCheckout())
                    throw new Exception(_localizationService.GetResource("Checkout.OnePageCheckoutDisabled"));

                string paymentmethod = form["paymentmethod"];
                //payment method 
                if (String.IsNullOrEmpty(paymentmethod))
                    throw new Exception(_localizationService.GetResource("Checkout.PaymentMethod.NotParsed"));

                //validate if item added or deleted from cart
                int cartNumberOfItems = (_httpContext.Session["cartNumberOfItems"] != null) ? (int)_httpContext.Session["cartNumberOfItems"] : cart.Count;
                int cartTotalQuantity = (_httpContext.Session["cartTotalQuantity"] != null) ? (int)_httpContext.Session["cartTotalQuantity"] : cart.Sum(x => x.Quantity);
                if (cart.Count != cartNumberOfItems || cart.Sum(x => x.Quantity) != cartTotalQuantity)
                {
                    throw new Exception(_localizationService.GetResource("Checkout.ItemAddedOrDeletedError"));
                }
                else
                {
                    //validate furthur if quantity increased or decreased for item
                    foreach (var item in cart)
                    {
                        if (item.Quantity != (int)_httpContext.Session[item.Id.ToString()])
                            throw new Exception(_localizationService.GetResource("Checkout.ItemAddedOrDeletedError"));
                    }
                }


                var model = new CheckoutPaymentMethodModel();
                TryUpdateModel(model);

                //reward points
                if (_rewardPointsSettings.Enabled)
                {
                    _genericAttributeService.SaveAttribute(customer,
                        SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, model.UseRewardPoints,
                        _storeContext.CurrentStore.Id);
                }

                //Check whether payment workflow is required
                bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
                if (!isPaymentWorkflowRequired)
                {
                    //payment is not required
                    _genericAttributeService.SaveAttribute<string>(customer,
                        SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);

                    var confirmOrderModel = PrepareConfirmOrderModel(customer.Id, cart);
                    confirmOrderModel.CartTypeId = cartTypeId;
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "confirm-order",
                            html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                        },
                        goto_section = "confirm_order"
                    });
                }

                var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(paymentmethod);
                if (paymentMethodInst == null ||
                    !paymentMethodInst.IsPaymentMethodActive(_paymentSettings) ||
                    !_pluginFinder.AuthenticateStore(paymentMethodInst.PluginDescriptor, _storeContext.CurrentStore.Id))
                    throw new Exception(_localizationService.GetResource("Checkout.PaymentMethod.NotParsed"));

                //save
                _genericAttributeService.SaveAttribute<string>(customer,
                    SystemCustomerAttributeNames.SelectedPaymentMethod, paymentmethod, _storeContext.CurrentStore.Id);

                return OpcLoadStepAfterPaymentMethod(paymentMethodInst, cart, cartTypeId, customer.Id);
            }
            catch (Exception exc)
            {
                //try to parse isMobileCheckout flag from session
                bool isMobileCheckout = (_httpContext.Session["isMobileCheckout"] != null) ? (bool)_httpContext.Session["isMobileCheckout"] : false;
                //try to parse customer Id from session
                Customer customer = null;
                if (isMobileCheckout)
                {
                    //if this is a mobile checkout use the cutomer identifier in the session to get the customer
                    int customerId = (_httpContext.Session["customerId"] != null) ? (int)_httpContext.Session["customerId"] : _workContext.CurrentCustomer.Id;
                    customer = _customerService.GetCustomerById(customerId);
                }
                else
                {
                    //if this is not a mobile checkout use the current cutomer
                    customer = _workContext.CurrentCustomer;
                }

                _logger.Warning(exc.Message, exc, customer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSavePaymentInfo(FormCollection form)
        {
            try
            {
                //try to parse isMobileCheckout flag from session
                bool isMobileCheckout = (_httpContext.Session["isMobileCheckout"] != null) ? (bool)_httpContext.Session["isMobileCheckout"] : false;

                //try to parse cart type Id from session
                int cartTypeId = (_httpContext.Session["cartTypeId"] != null) ? (int)_httpContext.Session["cartTypeId"] : (int)ShoppingCartType.ShoppingCart;
                var cartType = (ShoppingCartType)cartTypeId;

                //try to parse customer Id from session
                Customer customer = null;
                if (isMobileCheckout)
                {
                    //if this is a mobile checkout use the cutomer identifier in the session to get the customer
                    int customerId = (_httpContext.Session["customerId"] != null) ? (int)_httpContext.Session["customerId"] : _workContext.CurrentCustomer.Id;
                    customer = _customerService.GetCustomerById(customerId);
                }
                else
                {
                    //if this is not a mobile checkout use the current cutomer
                    customer = _workContext.CurrentCustomer;
                }

                //validation
                var cart = customer.ShoppingCartItems//_workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == cartType)//ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();

                if ((customer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception(_localizationService.GetResource("Checkout.AnonymousCheckout"));

                if (cart.Count == 0)
                    throw new Exception(_localizationService.GetResource("Checkout.EmptyCart"));

                if (!UseOnePageCheckout())
                    throw new Exception(_localizationService.GetResource("Checkout.OnePageCheckoutDisabled"));

                //validate if item added or deleted from cart
                int cartNumberOfItems = (_httpContext.Session["cartNumberOfItems"] != null) ? (int)_httpContext.Session["cartNumberOfItems"] : cart.Count;
                int cartTotalQuantity = (_httpContext.Session["cartTotalQuantity"] != null) ? (int)_httpContext.Session["cartTotalQuantity"] : cart.Sum(x => x.Quantity);
                if (cart.Count != cartNumberOfItems || cart.Sum(x => x.Quantity) != cartTotalQuantity)
                {
                    throw new Exception(_localizationService.GetResource("Checkout.ItemAddedOrDeletedError"));
                }
                else
                {
                    //validate furthur if quantity increased or decreased for item
                    foreach (var item in cart)
                    {
                        if (item.Quantity != (int)_httpContext.Session[item.Id.ToString()])
                            throw new Exception(_localizationService.GetResource("Checkout.ItemAddedOrDeletedError"));
                    }
                }

                var paymentMethodSystemName = customer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    _genericAttributeService, _storeContext.CurrentStore.Id);
                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(paymentMethodSystemName);
                if (paymentMethod == null)
                    throw new Exception("Payment method is not selected");

                var paymentControllerType = paymentMethod.GetControllerType();
                var paymentController = DependencyResolver.Current.GetService(paymentControllerType) as BasePaymentController;
                var warnings = paymentController.ValidatePaymentForm(form);
                foreach (var warning in warnings)
                    ModelState.AddModelError("", warning);
                if (ModelState.IsValid)
                {
                    //get payment info
                    var paymentInfo = paymentController.GetPaymentInfo(form);

                    //Encrypt payment info
                    paymentInfo.CreditCardNumber = _encryptionService.EncryptText(paymentInfo.CreditCardNumber);
                    paymentInfo.CreditCardName = _encryptionService.EncryptText(paymentInfo.CreditCardName);
                    paymentInfo.CreditCardType = _encryptionService.EncryptText(paymentInfo.CreditCardType);

                    //session save
                    _httpContext.Session["OrderPaymentInfo"] = paymentInfo;

                    var confirmOrderModel = PrepareConfirmOrderModel(customer.Id, cart);
                    confirmOrderModel.CartTypeId = cartTypeId;

                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "confirm-order",
                            html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                        },
                        goto_section = "confirm_order"
                    });
                }

                //If we got this far, something failed, redisplay form
                var paymenInfoModel = PreparePaymentInfoModel(customer.Id, paymentMethod);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "payment-info",
                        html = this.RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                    }
                });
            }
            catch (Exception exc)
            {
                //try to parse isMobileCheckout flag from session
                bool isMobileCheckout = (_httpContext.Session["isMobileCheckout"] != null) ? (bool)_httpContext.Session["isMobileCheckout"] : false;
                //try to parse customer Id from session
                Customer customer = null;
                if (isMobileCheckout)
                {
                    //if this is a mobile checkout use the cutomer identifier in the session to get the customer
                    int customerId = (_httpContext.Session["customerId"] != null) ? (int)_httpContext.Session["customerId"] : _workContext.CurrentCustomer.Id;
                    customer = _customerService.GetCustomerById(customerId);
                }
                else
                {
                    //if this is not a mobile checkout use the current cutomer
                    customer = _workContext.CurrentCustomer;
                }

                _logger.Warning(exc.Message, exc, customer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcConfirmOrder(int customerId, int cartTypeId = 1)
        {
            try
            {
                //try to parse isMobileCheckout flag from session
                bool isMobileCheckout = (_httpContext.Session["isMobileCheckout"] != null) ? (bool)_httpContext.Session["isMobileCheckout"] : false;

                Customer customer = null;
                if (isMobileCheckout)
                    //if this is a mobile checkout use the cutomer identifier in the session to get the customer
                    customer = _customerService.GetCustomerById(customerId);
                else
                    //if this is not a mobile checkout use the current cutomer
                    customer = _workContext.CurrentCustomer;

                var cartType = (ShoppingCartType)cartTypeId;
                //validation
                var cart = customer.ShoppingCartItems//_workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == cartType)//ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();

                if ((customer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception(_localizationService.GetResource("Checkout.AnonymousCheckout"));

                if (cart.Count == 0)
                    throw new Exception(_localizationService.GetResource("Checkout.EmptyCart"));

                if (!UseOnePageCheckout())
                    throw new Exception(_localizationService.GetResource("Checkout.OnePageCheckoutDisabled"));

                //validate if item added or deleted from cart
                int cartNumberOfItems = (_httpContext.Session["cartNumberOfItems"] != null) ? (int)_httpContext.Session["cartNumberOfItems"] : cart.Count;
                int cartTotalQuantity = (_httpContext.Session["cartTotalQuantity"] != null) ? (int)_httpContext.Session["cartTotalQuantity"] : cart.Sum(x => x.Quantity);
                if (cart.Count != cartNumberOfItems || cart.Sum(x => x.Quantity) != cartTotalQuantity)
                {
                    throw new Exception(_localizationService.GetResource("Checkout.ItemAddedOrDeletedError"));
                }
                else
                {
                    //validate furthur if quantity increased or decreased for item
                    foreach (var item in cart)
                    {
                        if (item.Quantity != (int)_httpContext.Session[item.Id.ToString()])
                            throw new Exception(_localizationService.GetResource("Checkout.ItemAddedOrDeletedError"));
                    }
                }

                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(customer))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                //place order
                var processPaymentRequest = _httpContext.Session["OrderPaymentInfo"] as ProcessPaymentRequest;

                //Decrypt payment info
                if (processPaymentRequest != null)
                {
                    processPaymentRequest.CreditCardNumber = _encryptionService.DecryptText(processPaymentRequest.CreditCardNumber);
                    processPaymentRequest.CreditCardName = _encryptionService.DecryptText(processPaymentRequest.CreditCardName);
                    processPaymentRequest.CreditCardType = _encryptionService.DecryptText(processPaymentRequest.CreditCardType);
                }
                if (processPaymentRequest == null)
                {
                    //Check whether payment workflow is required
                    if (IsPaymentWorkflowRequired(cart))
                    {
                        throw new Exception(_localizationService.GetResource("Checkout.PaymentMethod.NotEntered"));
                    }
                    else
                        processPaymentRequest = new ProcessPaymentRequest();
                }

                processPaymentRequest.StoreId = _storeContext.CurrentStore.Id;
                processPaymentRequest.CustomerId = customer.Id;
                processPaymentRequest.PaymentMethodSystemName = customer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    _genericAttributeService, _storeContext.CurrentStore.Id);
                var placeOrderResult = _orderProcessingService.PlaceOrder(customerId, processPaymentRequest, cartTypeId);
                if (placeOrderResult.Success)
                {                    
                    var postProcessPaymentRequest = new PostProcessPaymentRequest()
                    {
                        Order = placeOrderResult.PlacedOrder
                    };

                    //clearing old shipping methods selection when order is placed
                    customer.ShippingMethodsAttributes = string.Empty;
                    _customerService.UpdateCustomer(customer);

                    var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(placeOrderResult.PlacedOrder.PaymentMethodSystemName);
                    if (paymentMethod != null)
                    {
                        if (paymentMethod.PaymentMethodType == PaymentMethodType.Redirection)
                        {
                            _httpContext.Session["OrderPaymentInfo"] = null;
                            //Redirection will not work because it's AJAX request.
                            //That's why we don't process it here (we redirect a user to another page where he'll be redirected)

                            //redirect
                            return Json(new { redirect = string.Format("{0}checkout/OpcCompleteRedirectionPayment", _webHelper.GetStoreLocation()) });
                        }
                        else
                        {
                            _paymentService.PostProcessPayment(postProcessPaymentRequest);
                            _httpContext.Session["OrderPaymentInfo"] = null;
                            var order = _orderService.GetOrderById(postProcessPaymentRequest.Order.Id);

                            //remove session validation entries
                            _httpContext.Session.Remove("cartNumberOfItems");
                            _httpContext.Session.Remove("cartTotalQuantity");
                            foreach (var item in cart)
                            {
                                _httpContext.Session.Remove(item.Id.ToString());
                            }

                            //send email notifications only if payment did not failed
                            if (order.OrderStatusId != (int)OrderStatus.PaymentFailed)
                            {
                                //send email notifications
                                int orderPlacedStoreOwnerNotificationQueuedEmailId = _workflowMessageService.SendOrderPlacedStoreOwnerNotification(order, _localizationSettings.DefaultAdminLanguageId);
                                if (orderPlacedStoreOwnerNotificationQueuedEmailId > 0)
                                {
                                    order.OrderNotes.Add(new OrderNote()
                                    {
                                        Note = string.Format("\"Order placed\" email (to store owner) has been queued. Queued email identifier: {0}.", orderPlacedStoreOwnerNotificationQueuedEmailId),
                                        DisplayToCustomer = false,
                                        CreatedOnUtc = DateTime.UtcNow
                                    });
                                    _orderService.UpdateOrder(order);
                                }

                                var orderPlacedAttachmentFilePath = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
                                    _pdfService.PrintOrderToPdf(order, 0) : null;
                                var orderPlacedAttachmentFileName = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
                                    "order.pdf" : null;
                                int orderPlacedCustomerNotificationQueuedEmailId = _workflowMessageService
                                    .SendOrderPlacedCustomerNotification(order, order.CustomerLanguageId, orderPlacedAttachmentFilePath, orderPlacedAttachmentFileName);
                                if (orderPlacedCustomerNotificationQueuedEmailId > 0)
                                {
                                    order.OrderNotes.Add(new OrderNote()
                                    {
                                        Note = string.Format("\"Order placed\" email (to customer) has been queued. Queued email identifier: {0}.", orderPlacedCustomerNotificationQueuedEmailId),
                                        DisplayToCustomer = false,
                                        CreatedOnUtc = DateTime.UtcNow
                                    });
                                    _orderService.UpdateOrder(order);
                                }
                            }

                            //success
                            return Json(new { success = 1 });
                        }
                    }
                    else
                    {
                        //payment method could be null if order total is 0

                        //success
                        return Json(new { success = 1 });
                    }
                }
                else
                {
                    //error
                    var confirmOrderModel = new CheckoutConfirmModel();
                    confirmOrderModel.CartTypeId = cartTypeId;
                    confirmOrderModel.CustomerId = customerId;
                    foreach (var error in placeOrderResult.Errors)
                        confirmOrderModel.Warnings.Add(error);

                    return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "confirm-order",
                                html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                            },
                            goto_section = "confirm_order"
                        });
                }
            }
            catch (Exception exc)
            {
                //try to parse isMobileCheckout flag from session
                bool isMobileCheckout = (_httpContext.Session["isMobileCheckout"] != null) ? (bool)_httpContext.Session["isMobileCheckout"] : false;
                //try to parse customer Id from session
                Customer customer = null;
                if (isMobileCheckout)
                {
                    //if this is a mobile checkout use the cutomer identifier in the session to get the customer
                    customer = _customerService.GetCustomerById(customerId);
                }
                else
                {
                    //if this is not a mobile checkout use the current cutomer
                    customer = _workContext.CurrentCustomer;
                }

                _logger.Warning(exc.Message, exc, customer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        public ActionResult OpcCompleteRedirectionPayment()
        {
            try
            {
                //validation
                if (!UseOnePageCheckout())
                    return RedirectToRoute("HomePage");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    return new HttpUnauthorizedResult();

                //get the order
                var order = _orderService.SearchOrders(storeId: _storeContext.CurrentStore.Id,
                customerId: _workContext.CurrentCustomer.Id, pageSize: 1)
                    .FirstOrDefault();
                if (order == null)
                    return RedirectToRoute("HomePage");


                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
                if (paymentMethod == null)
                    return RedirectToRoute("HomePage");
                if (paymentMethod.PaymentMethodType != PaymentMethodType.Redirection)
                    return RedirectToRoute("HomePage");

                //ensure that order has been just placed
                if ((DateTime.UtcNow - order.CreatedOnUtc).TotalMinutes > 3)
                    return RedirectToRoute("HomePage");


                //Redirection will not work on one page checkout page because it's AJAX request.
                //That's why we process it here
                var postProcessPaymentRequest = new PostProcessPaymentRequest()
                {
                    Order = order
                };

                _paymentService.PostProcessPayment(postProcessPaymentRequest);

                if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
                {
                    //redirection or POST has been done in PostProcessPayment
                    return Content("Redirected");
                }
                else
                {
                    //if no redirection has been done (to a third-party payment page)
                    //theoretically it's not possible
                    return RedirectToRoute("CheckoutCompleted", new { orderId = order.Id });
                }
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Content(exc.Message);
            }
        }

        #endregion

    }

    public class ShippingDictionaryEntry
    {
        public int Count { get; set; }
        public ShippedItemModel Item { get; set; }
    }
}
