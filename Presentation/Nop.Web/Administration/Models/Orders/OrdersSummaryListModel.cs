﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.Orders
{
    public partial class OrdersSummaryListModel : BaseNopModel
    {
        public int VendorId { get; set; }
        public bool IsLoggedInAsVendor { get; set; }

        public bool NotShippedOrders { get; set; }
    }
}