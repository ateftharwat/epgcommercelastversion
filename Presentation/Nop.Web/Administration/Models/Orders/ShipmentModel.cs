﻿using System;
using System.Collections.Generic;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using Nop.Admin.Models.Common;

namespace Nop.Admin.Models.Orders
{
    public partial class ShipmentModel : BaseNopEntityModel
    {
        public ShipmentModel()
        {
            this.Items = new List<ShipmentItemModel>();
        }
        [NopResourceDisplayName("Admin.Orders.Shipments.ID")]
        public override int Id { get; set; }
        [NopResourceDisplayName("Admin.Orders.Shipments.OrderID")]
        public int OrderId { get; set; }
        [NopResourceDisplayName("Admin.Orders.Shipments.TotalWeight")]
        public string TotalWeight { get; set; }
        [NopResourceDisplayName("Admin.Orders.Shipments.TrackingNumber")]
        public string TrackingNumber { get; set; }
        [NopResourceDisplayName("Admin.Orders.Shipments.BookingNumber")]
        public string BookingNumber { get; set; }
        public int? AddressId { get; set; }
        public AddressModel Address { get; set; }
        [NopResourceDisplayName("Admin.Orders.Shipments.Address")]
        public string FormattedAddress { get; set; }
        [NopResourceDisplayName("Admin.Orders.Shipments.ShippingMethod")]
        public string ShippingMethod { get; set; }

        public string ShippingMethodID { get; set; }

        [NopResourceDisplayName("Admin.Orders.Shipments.ReadyToBeShippedDate")]
        public string ReadyToBeShippedDate { get; set; }
        public bool CanReadyToBeShip { get; set; }
        public DateTime? ReadyToBeShippedDateUtc { get; set; }

        [NopResourceDisplayName("Admin.Orders.Shipments.ShippedDate")]
        public string ShippedDate { get; set; }
        public bool CanShip { get; set; }
        public DateTime? ShippedDateUtc { get; set; }

        [NopResourceDisplayName("Admin.Orders.Shipments.DeliveryDate")]
        public string DeliveryDate { get; set; }
        public bool CanDeliver { get; set; }
        public DateTime? DeliveryDateUtc { get; set; }

        [NopResourceDisplayName("Admin.Orders.Fields.EPGShippingStatus")]
        public string EPGShippingStatus { get; set; }

        public bool IsLoggedInAsCourier { get; set; }
        public bool IsLoggedInAsFinance { get; set; }
        [NopResourceDisplayName("Admin.Orders.Shipments.VendorAddress")]
        public string VendorFormattedAddress { get; set; }
        public List<ShipmentItemModel> Items { get; set; }

        #region Nested classes

        public partial class ShipmentItemModel : BaseNopEntityModel
        {
            public int OrderItemId { get; set; }
            public int ProductId { get; set; }
            [NopResourceDisplayName("Admin.Orders.Shipments.Products.ProductName")]
            public string ProductName { get; set; }
            public bool ProductIsCustomizable { get; set; }
            public string Sku { get; set; }
            public string AttributeInfo { get; set; }

            //weight of one item (product)
            [NopResourceDisplayName("Admin.Orders.Shipments.Products.ItemWeight")]
            public string ItemWeight { get; set; }
            [NopResourceDisplayName("Admin.Orders.Shipments.Products.ItemDimensions")]
            public string ItemDimensions { get; set; }

            public int QuantityToAdd { get; set; }
            public int QuantityOrdered { get; set; }
            [NopResourceDisplayName("Admin.Orders.Shipments.Products.QtyShipped")]
            public int QuantityInThisShipment { get; set; }
            public int QuantityInAllShipments { get; set; }
            public int? VendorAddressId { get; set; }
            public AddressModel VendorAddress { get; set; }
            [NopResourceDisplayName("Admin.Orders.Shipments.Address")]
            public string VendorFormattedAddress { get; set; }
        }
        #endregion
    }
}