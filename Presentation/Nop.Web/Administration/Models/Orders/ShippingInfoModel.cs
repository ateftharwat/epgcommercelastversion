﻿using Nop.Admin.Models.Common;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Orders
{
    public partial class ShippingInfoModel : BaseNopEntityModel
    {
        public ShippingInfoModel()
        {
            this.Items = new List<ShippingItemModel>();
        }

        [NopResourceDisplayName("Admin.Orders.Shipments.ID")]
        public override int Id { get; set; }
        [NopResourceDisplayName("Admin.Orders.Shipments.OrderID")]
        public int OrderId { get; set; }
        [NopResourceDisplayName("Admin.Orders.Shipments.TrackingNumber")]
        public string TrackingNumber { get; set; }
        public string BookingNumber { get; set; }
        public int? AddressId { get; set; }
        public AddressModel Address { get; set; }
        public string ShippingMethod { get; set; }
        public string ShippingMethodID { get; set; }
        public string ShipmentAddressGoogleMapsUrl { get; set; }
        public bool ShipmentExists { get; set; }

        [NopResourceDisplayName("Admin.Orders.Shipments.ReadyToBeShippedDate")]
        public string ReadyToBeShippedDate { get; set; }
        public bool CanReadyToBeShip { get; set; }
        public DateTime? ReadyToBeShippedDateUtc { get; set; }
        public bool CanShippedTogether { get; set; }

        [NopResourceDisplayName("Admin.Orders.Shipments.TotalWeight")]
        public decimal? TotalWeight { get; set; }
        [NopResourceDisplayName("Admin.Orders.Shipments.TotalHeight")]
        public decimal? TotalHeight { get; set; }
        [NopResourceDisplayName("Admin.Orders.Shipments.TotalWidth")]
        public decimal? TotalWidth { get; set; }
        [NopResourceDisplayName("Admin.Orders.Shipments.TotalLength")]
        public decimal? TotalLength { get; set; }
        public List<ShippingItemModel> Items { get; set; }

        #region Nested classes

        public partial class ShippingItemModel : BaseNopEntityModel
        {
            public int OrderItemId { get; set; }
            public int ProductId { get; set; }
            [NopResourceDisplayName("Admin.Orders.Shipments.Products.ProductName")]
            public string ProductName { get; set; }
            public string Sku { get; set; }
            public string AttributeInfo { get; set; }
            public bool ProductIsCustomizable { get; set; }

            //weight of one item (product)
            [NopResourceDisplayName("Admin.Orders.Shipments.Products.ItemWeight")]
            public string ItemWeight { get; set; }
            [NopResourceDisplayName("Admin.Orders.Shipments.Products.ItemDimensions")]
            public string ItemDimensions { get; set; }

            public int Quantity { get; set; }

            public int? VendorAddressId { get; set; }
            public AddressModel VendorAddress { get; set; }
            [NopResourceDisplayName("Admin.Orders.Shipments.Address")]
            public string VendorFormattedAddress { get; set; }
        }
        #endregion
    }
}