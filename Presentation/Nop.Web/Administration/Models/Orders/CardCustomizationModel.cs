﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Orders
{
    public partial class CardCustomizationModel : BaseNopEntityModel
    {
        public CardCustomizationModel()
        {
            pages = new List<PageModel>();
        }

        public int OrderId { get; set; }

        public decimal Width { get; set; }

        public decimal Height { get; set; }

        public List<PageModel> pages { get; set; }


        #region Nested Classes

        public partial class PageModel : BaseNopEntityModel
        {
            public PageModel()
            {
                PlaceHolders = new List<PlaceHolderModel>();
            }

            public string TemplatePictureURL { get; set; }

            public string PageName { get; set; }

            public List<PlaceHolderModel> PlaceHolders { get; set; }
        }

        public partial class PlaceHolderModel : BaseNopEntityModel
        {
            public string Type { get; set; }

            public string Text { get; set; }

            public string TextFamily { get; set; }

            public string TextStyle { get; set; }
            public string TextWeight { get; set; }
            public string TextDecoration { get; set; }

            public string FontColor { get; set; }

            public int TextSize { get; set; }

            public string ImageURL { get; set; }

            public string FileName { get; set; }

            public int PictureId { get; set; }

            public string PictureData { get; set; }

            public decimal XPos { get; set; }

            public decimal YPos { get; set; }

            public decimal Width { get; set; }

            public decimal Height { get; set; }
        }

        #endregion
    }
}