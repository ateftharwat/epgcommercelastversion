﻿using Nop.Admin.Models.Common;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.Vendors
{
    public partial class VendorAddressModel : BaseNopModel
    {
        public int VendorId { get; set; }

        public AddressModel Address { get; set; }
    }
}