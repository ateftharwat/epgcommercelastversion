﻿using FluentValidation.Attributes;
using Nop.Admin.Models.Customers;
using Nop.Admin.Models.Stores;
using Nop.Admin.Validators.Catalog;
using Nop.Web.Framework;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace Nop.Admin.Models.Catalog
{
    [Validator(typeof(PlaceHolderValidator))]
    public class PlaceHolderModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Admin.Catalog.PlaceHolder.Fields.Name")]
        [AllowHtml]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Catalog.PlaceHolder.Fields.TextMaxChars")]
        [AllowHtml]
        public int TextMaxChars { get; set; }
    }
}