﻿using FluentValidation.Attributes;
using Nop.Admin.Validators.Catalog;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.Catalog
{
    [Validator(typeof(PageValidators))]
    public partial class PageModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Admin.Catalog.CardPage.Fields.Name")]
        [AllowHtml]
        public string Name { get; set; }


        #region Nested Classes

        public partial class PagePlaceHoldersModel : BaseNopEntityModel
        {
            [NopResourceDisplayName("Admin.Catalog.CardPage.Fields.PageId")]
            [AllowHtml]
            public int PageId { get; set; }

            [NopResourceDisplayName("Admin.Catalog.CardPage.Fields.PlaceHolderId")]
            [AllowHtml]
            public int PlaceHolderId { get; set; }

            [NopResourceDisplayName("Admin.Catalog.CardPage.Fields.XPos")]
            [AllowHtml]
            public decimal XPos { get; set; }

            [NopResourceDisplayName("Admin.Catalog.CardPage.Fields.YPos")]
            [AllowHtml]
            public decimal YPos { get; set; }

            [NopResourceDisplayName("Admin.Catalog.CardPage.Fields.Width")]
            [AllowHtml]
            public decimal Width { get; set; }

            [NopResourceDisplayName("Admin.Catalog.CardPage.Fields.Height")]
            [AllowHtml]
            public decimal Height { get; set; }

            [NopResourceDisplayName("Admin.Catalog.CardPage.Fields.PlaceHolderName")]
            [AllowHtml]
            public string PlaceHolderName { get; set; }
        }

        #endregion
    }    
}