﻿using FluentValidation.Attributes;
using Nop.Admin.Models.Customers;
using Nop.Admin.Models.Stores;
using Nop.Admin.Validators.Catalog;
using Nop.Web.Framework;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.Catalog
{
    [Validator(typeof(CardPageValidator))]
    public class CardPageModel : BaseNopEntityModel, ILocalizedModel<CardPageLocalizedModel>
    {
        public CardPageModel()
        {
            Locales = new List<CardPageLocalizedModel>();
        }

        [NopResourceDisplayName("Admin.Catalog.CardPage.Fields.ProductId")]
        [AllowHtml]
        public int ProductId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.CardPage.Fields.PageIndex")]
        [AllowHtml]
        public int PageIndex { get; set; }

        [NopResourceDisplayName("Admin.Catalog.CardPage.Fields.Name")]
        [AllowHtml]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Catalog.CardPage.Fields.PlaceHolderId")]
        [AllowHtml]
        public int PlaceHolderId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.CardPage.Fields.XPos")]
        [AllowHtml]
        public decimal XPos { get; set; }

        [NopResourceDisplayName("Admin.Catalog.CardPage.Fields.YPos")]
        [AllowHtml]
        public decimal YPos { get; set; }

        [NopResourceDisplayName("Admin.Catalog.CardPage.Fields.Width")]
        [AllowHtml]
        public decimal Width { get; set; }

        [NopResourceDisplayName("Admin.Catalog.CardPage.Fields.Height")]
        [AllowHtml]
        public decimal Height { get; set; }

        [NopResourceDisplayName("Admin.Catalog.CardPage.Fields.PlaceHolderName")]
        [AllowHtml]
        public string PlaceHolderName { get; set; }

        public IList<CardPageLocalizedModel> Locales { get; set; }
    }

    public partial class CardPageLocalizedModel : ILocalizedModelLocal
    {
        public int LanguageId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.CardPage.Fields.Name")]
        [AllowHtml]
        public string Name { get; set; }
    }
}