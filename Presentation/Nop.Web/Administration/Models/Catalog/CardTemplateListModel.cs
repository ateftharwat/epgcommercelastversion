﻿using Nop.Web.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.Catalog
{
    public partial class CardTemplateListModel
    {
        [NopResourceDisplayName("Admin.Catalog.CardTemplate.List.SearchTemplateName")]
        [AllowHtml]
        public string SearchTemplateName { get; set; }
    }
}