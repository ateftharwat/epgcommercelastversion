﻿using Nop.Web.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.Catalog
{
    public partial class PageListModel
    {
        [NopResourceDisplayName("Admin.Catalog.CardPage.List.SearchPageName")]
        [AllowHtml]
        public string SearchPageName { get; set; }
    }
}