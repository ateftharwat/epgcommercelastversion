﻿using FluentValidation.Attributes;
using Nop.Admin.Validators.Catalog;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Models.Catalog
{
    [Validator(typeof(CardTemplateValidator))]
    public partial class CardTemplateModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Admin.Catalog.CardTemplate.Fields.Name")]
        [AllowHtml]
        public string Name { get; set; }


        #region Nested Classes

        public partial class TemplatePagesModel : BaseNopEntityModel
        {
            [NopResourceDisplayName("Admin.Catalog.CardTemplate.Fields.PageId")]
            [AllowHtml]
            public int PageId { get; set; }

            [NopResourceDisplayName("Admin.Catalog.CardTemplate.Fields.TemplateId")]
            [AllowHtml]
            public int TemplateId { get; set; }

            [NopResourceDisplayName("Admin.Catalog.CardTemplate.Fields.DisplayOrder")]
            [AllowHtml]
            public int DisplayOrder { get; set; }

            [NopResourceDisplayName("Admin.Catalog.CardTemplate.Fields.PageName")]
            [AllowHtml]
            public string PageName { get; set; }
        }

        #endregion
    }
}