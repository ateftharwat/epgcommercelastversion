﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Admin.Validators.FeaturesLists;

namespace Nop.Admin.Models.FeaturesLists
{
    public partial class FeaturesListItemModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Admin.ContentManagement.FeaturesList.Fields.FeaturesListId")]
        [AllowHtml]
        public int FeaturesListId { get; set; }

        [UIHint("Picture")]
        [NopResourceDisplayName("Admin.ContentManagement.FeaturesList.Fields.PictureId")]
        [AllowHtml]
        public int PictureId { get; set; }
        public string PictureURL { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.FeaturesList.Fields.Comment")]
        [AllowHtml]
        public string Comment { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.FeaturesList.Fields.ArabicComment")]
        [AllowHtml]
        public string ArabicComment { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.FeaturesList.Fields.Text")]
        [AllowHtml]
        public string Text { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.FeaturesList.Fields.ArabicText")]
        [AllowHtml]
        public string ArabicText { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.FeaturesList.Fields.URL")]
        [AllowHtml]
        public string URL { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.FeaturesList.Fields.DisplayOrder")]
        [AllowHtml]
        public int DisplayOrder { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.FeaturesList.Fields.Visible")]
        [AllowHtml]
        public bool Visible { get; set; }
    }
}