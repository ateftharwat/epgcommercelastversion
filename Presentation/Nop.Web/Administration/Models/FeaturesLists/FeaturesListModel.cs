﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Admin.Validators.FeaturesLists;

namespace Nop.Admin.Models.FeaturesLists
{
    [Validator(typeof(FeaturesListValidator))]
    public partial class FeaturesListModel : BaseNopEntityModel
    {
        public FeaturesListModel()
        {
            AddFeatureItem = new FeaturesListItemModel();
        }

        [NopResourceDisplayName("Admin.ContentManagement.FeaturesList.Fields.Name")]
        [AllowHtml]
        public string Name { get; set; }

        public FeaturesListItemModel AddFeatureItem { get; set; }
    }
}