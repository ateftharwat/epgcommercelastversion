﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nop.Admin.Models.Catalog;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.ExportImport;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Controllers
{
    public partial class CardPageController : BaseAdminController
    {
        #region Fields

        private readonly IPlaceHolderService _placeHolderService;
        private readonly IStoreService _storeService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly IExportManager _exportManager;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IAclService _aclService;
        private readonly IPermissionService _permissionService;
        private readonly CatalogSettings _catalogSettings;

        #endregion

        #region Constructors

        public CardPageController(IPlaceHolderService placeHoldeService,             
            IStoreService storeService,
            IStoreMappingService storeMappingService,
            IUrlRecordService urlRecordService, 
            ILanguageService languageService, 
            ILocalizationService localizationService,
            ILocalizedEntityService localizedEntityService, 
            IExportManager exportManager,
            ICustomerActivityService customerActivityService, 
            IAclService aclService,
            IPermissionService permissionService,
            CatalogSettings catalogSettings)
        {
            this._placeHolderService = placeHoldeService;            
            this._storeService = storeService;
            this._storeMappingService = storeMappingService;
            this._urlRecordService = urlRecordService;
            this._languageService = languageService;
            this._localizationService = localizationService;
            this._localizedEntityService = localizedEntityService;
            this._exportManager = exportManager;
            this._customerActivityService = customerActivityService;
            this._aclService = aclService;
            this._permissionService = permissionService;
            this._catalogSettings = catalogSettings;
        }

        #endregion

        #region Utilities

        //[NonAction]
        //protected void UpdateLocales(PlaceHolder placeHolder, PlaceHolderModel model)
        //{
        //    foreach (var localized in model.Locales)
        //    {
        //        _localizedEntityService.SaveLocalizedValue(placeHolder,
        //                                                       x => x.Name,
        //                                                       localized.Name,
        //                                                       localized.LanguageId);
        //    }
        //}

        #endregion

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public ActionResult List(DataSourceRequest command, PageListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var pages = _placeHolderService.GetAllPagesByName(model.SearchPageName, command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = pages.Select(x => x.ToModel()),
                Total = pages.TotalCount
            };

            return Json(gridModel);
        }

        public ActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var model = new PageModel();

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(PageModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var page = model.ToEntity();
                _placeHolderService.InsertPage(page);

                //activity log
                _customerActivityService.InsertActivity("AddNewPage", _localizationService.GetResource("ActivityLog.AddNewPage"), page.Name);

                SuccessNotification(_localizationService.GetResource("Admin.Catalog.CardPage.Added"));
                return continueEditing ? RedirectToAction("Edit", new { id = page.Id }) : RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var page = _placeHolderService.GetPageById(id);
            if (page == null)
                //No pages found with the specified id
                RedirectToAction("List");

            var model = page.ToModel();

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(PageModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var page = _placeHolderService.GetPageById(model.Id);
            if (page == null)
                //No pages found with the specified id
                RedirectToAction("List");

            if (ModelState.IsValid)
            {
                page = model.ToEntity(page);
                _placeHolderService.UpdatePage(page);

                //activity log
                _customerActivityService.InsertActivity("EditPage", _localizationService.GetResource("ActivityLog.EditPage"), page.Name);

                SuccessNotification(_localizationService.GetResource("Admin.Catalog.CardPage.Updated"));
                return continueEditing ? RedirectToAction("Edit", page.Id) : RedirectToAction("List");
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var page = _placeHolderService.GetPageById(id);
            if (page == null)
                //No Page found with the specified id
                return RedirectToAction("List");

            _placeHolderService.DeletePage(page);

            //activity log
            _customerActivityService.InsertActivity("DeletePage", _localizationService.GetResource("ActivityLog.DeletePage"), page.Name);

            SuccessNotification(_localizationService.GetResource("Admin.Catalog.CardPage.Deleted"));
            return RedirectToAction("List");
        }

        #region Page place holders

        [HttpPost]
        public ActionResult PlaceHoldersList(DataSourceRequest command, int pageId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var Pages = _placeHolderService.GetPlaceHoldersByPageId(pageId);
            var cardPagesModel = Pages
                .Select(x =>
                {
                    return new PageModel.PagePlaceHoldersModel()
                    {
                        Id = x.Id,
                        PlaceHolderName = _placeHolderService.GetPlaceHolderById(x.PlaceHolderId).Name,
                        PageId = x.PageId,
                        PlaceHolderId = x.PlaceHolderId,
                        XPos = x.XPos,
                        YPos = x.YPos,
                        Width = x.Width,
                        Height = x.Height,
                    };
                })
                .ToList();

            var gridModel = new DataSourceResult
            {
                Data = cardPagesModel,
                Total = cardPagesModel.Count
            };

            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult PlaceHolderInsert(PageModel.PagePlaceHoldersModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var pagePlaceHolder = new PagePlaceHolders()
            {
                PlaceHolderId = model.PlaceHolderId,
                PageId = model.PageId,
                XPos = model.XPos,
                YPos = model.YPos,
                Width = model.Width,
                Height = model.Height
            };

            _placeHolderService.InsertPagePlaceHolder(pagePlaceHolder);

            return new NullJsonResult();
        }

        [HttpPost]
        public ActionResult PlaceHolderUpdate(PageModel.PagePlaceHoldersModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var pagePlaceHolder = _placeHolderService.GetPagePlaceHolderById(model.Id);
            if (pagePlaceHolder == null)
                throw new ArgumentException("No Place Holders found with the specified id");

            pagePlaceHolder.PlaceHolderId = model.PlaceHolderId;
            pagePlaceHolder.PageId = model.PageId;
            pagePlaceHolder.XPos = model.XPos;
            pagePlaceHolder.YPos = model.YPos;
            pagePlaceHolder.Width = model.Width;
            pagePlaceHolder.Height = model.Height;

            _placeHolderService.UpdatePagePlaceHolder(pagePlaceHolder);

            return new NullJsonResult();
        }

        [HttpPost]
        public ActionResult PlaceHolderDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var pagePlaceHolder = _placeHolderService.GetPagePlaceHolderById(id);
            if (pagePlaceHolder == null)
                throw new ArgumentException("No Place Holders found with the specified id");

            _placeHolderService.DeletePagePlaceHolder(pagePlaceHolder);

            return new NullJsonResult();
        }

        #endregion
    }
}