﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nop.Admin.Models.Catalog;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.ExportImport;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Controllers
{
    public class PlaceHolderController : BaseAdminController
    {
        #region Fields

        private readonly IPlaceHolderService _placeHolderService;
        private readonly IStoreService _storeService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly IExportManager _exportManager;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IAclService _aclService;
        private readonly IPermissionService _permissionService;
        private readonly CatalogSettings _catalogSettings;

        #endregion

        #region Constructors

        public PlaceHolderController(IPlaceHolderService placeHoldeService,             
            IStoreService storeService,
            IStoreMappingService storeMappingService,
            IUrlRecordService urlRecordService, 
            ILanguageService languageService, 
            ILocalizationService localizationService,
            ILocalizedEntityService localizedEntityService, 
            IExportManager exportManager,
            ICustomerActivityService customerActivityService, 
            IAclService aclService,
            IPermissionService permissionService,
            CatalogSettings catalogSettings)
        {
            this._placeHolderService = placeHoldeService;            
            this._storeService = storeService;
            this._storeMappingService = storeMappingService;
            this._urlRecordService = urlRecordService;
            this._languageService = languageService;
            this._localizationService = localizationService;
            this._localizedEntityService = localizedEntityService;
            this._exportManager = exportManager;
            this._customerActivityService = customerActivityService;
            this._aclService = aclService;
            this._permissionService = permissionService;
            this._catalogSettings = catalogSettings;
        }

        #endregion

        #region Utilities

        //[NonAction]
        //protected void UpdateLocales(PlaceHolder placeHolder, PlaceHolderModel model)
        //{
        //    foreach (var localized in model.Locales)
        //    {
        //        _localizedEntityService.SaveLocalizedValue(placeHolder,
        //                                                       x => x.Name,
        //                                                       localized.Name,
        //                                                       localized.LanguageId);
        //    }
        //}

        #endregion


        public ActionResult Index()
        {
            return RedirectToAction("List");
        }


        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            return View();
        }


        [HttpPost]
        public ActionResult List(DataSourceRequest command, PlaceHolderListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var placeHolder = _placeHolderService.GetAllPlaceHoldersByName(model.SearchPlaceHolderName, command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = placeHolder.Select(x => x.ToModel()),
                Total = placeHolder.TotalCount
            };

            return Json(gridModel);
        }


        public ActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var model = new PlaceHolderModel();

            return View(model);
        }


        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(PlaceHolderModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var placeHolder = model.ToEntity();
                _placeHolderService.InsertPlaceHolder(placeHolder);

                //activity log
                _customerActivityService.InsertActivity("AddNewPlaceHolder", _localizationService.GetResource("ActivityLog.AddNewPlaceHolder"), placeHolder.Name);

                SuccessNotification(_localizationService.GetResource("Admin.Catalog.PlaceHolder.Added"));
                return continueEditing ? RedirectToAction("Edit", new { id = placeHolder.Id }) : RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }


        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var placeHolder = _placeHolderService.GetPlaceHolderById(id);
            if (placeHolder == null)
                //No product attribute found with the specified id
                RedirectToAction("List");

            var model = placeHolder.ToModel(); 

            return View(model);
        }


        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(PlaceHolderModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var placeHolder = _placeHolderService.GetPlaceHolderById(model.Id);
            if (placeHolder == null)
                //No product attribute found with the specified id
                RedirectToAction("List");

            if (ModelState.IsValid)
            {
                placeHolder = model.ToEntity(placeHolder);
                _placeHolderService.UpdatePlaceHolder(placeHolder);

                //activity log
                _customerActivityService.InsertActivity("EditPlaceHolder", _localizationService.GetResource("ActivityLog.EditPlaceHolder"), placeHolder.Name);

                SuccessNotification(_localizationService.GetResource("Admin.Catalog.PlaceHolder.Updated"));
                return continueEditing ? RedirectToAction("Edit", placeHolder.Id) : RedirectToAction("List");
            }

            return View(model);
        }


        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var placeHolder = _placeHolderService.GetPlaceHolderById(id);
            if (placeHolder == null)
                //No product attribute found with the specified id
                return RedirectToAction("List");

            _placeHolderService.DeletePlaceHolder(placeHolder);

            //activity log
            _customerActivityService.InsertActivity("DeletePlaceHolder", _localizationService.GetResource("ActivityLog.DeletePlaceHolder"), placeHolder.Name);

            SuccessNotification(_localizationService.GetResource("Admin.Catalog.PlaceHolder.Deleted"));
            return RedirectToAction("List");
        }
    }
}
