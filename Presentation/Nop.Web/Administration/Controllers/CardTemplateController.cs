﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nop.Admin.Models.Catalog;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.ExportImport;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Controllers
{
    public partial class CardTemplateController : BaseAdminController
    {
        #region Fields

        private readonly IPlaceHolderService _placeHolderService;
        private readonly ICardTemplateService _cardTemplateService;
        private readonly IStoreService _storeService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly IExportManager _exportManager;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IAclService _aclService;
        private readonly IPermissionService _permissionService;
        private readonly CatalogSettings _catalogSettings;

        #endregion

        #region Constructors

        public CardTemplateController(IPlaceHolderService placeHoldeService, 
            ICardTemplateService cardTemplateService,
            IStoreService storeService,
            IStoreMappingService storeMappingService,
            IUrlRecordService urlRecordService, 
            ILanguageService languageService, 
            ILocalizationService localizationService,
            ILocalizedEntityService localizedEntityService, 
            IExportManager exportManager,
            ICustomerActivityService customerActivityService, 
            IAclService aclService,
            IPermissionService permissionService,
            CatalogSettings catalogSettings)
        {
            this._placeHolderService = placeHoldeService;
            this._cardTemplateService = cardTemplateService;
            this._storeService = storeService;
            this._storeMappingService = storeMappingService;
            this._urlRecordService = urlRecordService;
            this._languageService = languageService;
            this._localizationService = localizationService;
            this._localizedEntityService = localizedEntityService;
            this._exportManager = exportManager;
            this._customerActivityService = customerActivityService;
            this._aclService = aclService;
            this._permissionService = permissionService;
            this._catalogSettings = catalogSettings;
        }

        #endregion

        #region Utilities

        //[NonAction]
        //protected void UpdateLocales(PlaceHolder placeHolder, PlaceHolderModel model)
        //{
        //    foreach (var localized in model.Locales)
        //    {
        //        _localizedEntityService.SaveLocalizedValue(placeHolder,
        //                                                       x => x.Name,
        //                                                       localized.Name,
        //                                                       localized.LanguageId);
        //    }
        //}

        #endregion

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public ActionResult List(DataSourceRequest command, CardTemplateListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var cardTemplates = _cardTemplateService.GetAllTemplatesByName(model.SearchTemplateName, command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = cardTemplates.Select(x => x.ToModel()),
                Total = cardTemplates.TotalCount
            };

            return Json(gridModel);
        }

        public ActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var model = new CardTemplateModel();

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(CardTemplateModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var cardTemplate = model.ToEntity();
                _cardTemplateService.InsertTemplate(cardTemplate);

                //activity log
                _customerActivityService.InsertActivity("AddNewTemplate", _localizationService.GetResource("ActivityLog.AddNewTemplate"), cardTemplate.Name);

                SuccessNotification(_localizationService.GetResource("Admin.Catalog.CardTemplate.Added"));
                return continueEditing ? RedirectToAction("Edit", new { id = cardTemplate.Id }) : RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var cardTemplate = _cardTemplateService.GetTemplateById(id);
            if (cardTemplate == null)
                //No template found with the specified id
                RedirectToAction("List");

            var model = cardTemplate.ToModel();

            return View(model);
        }


        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(CardTemplateModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var cardTemplate = _cardTemplateService.GetTemplateById(model.Id);
            if (cardTemplate == null)
                //No template found with the specified id
                RedirectToAction("List");

            if (ModelState.IsValid)
            {
                cardTemplate = model.ToEntity(cardTemplate);
                _cardTemplateService.UpdateTemplate(cardTemplate);

                //activity log
                _customerActivityService.InsertActivity("EditTemplate", _localizationService.GetResource("ActivityLog.EditTemplate"), cardTemplate.Name);

                SuccessNotification(_localizationService.GetResource("Admin.Catalog.CardTemplate.Updated"));
                return continueEditing ? RedirectToAction("Edit", cardTemplate.Id) : RedirectToAction("List");
            }

            return View(model);
        }


        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var cardTemplate = _cardTemplateService.GetTemplateById(id);
            if (cardTemplate == null)
                //No Page found with the specified id
                return RedirectToAction("List");

            _cardTemplateService.DeleteTemplate(cardTemplate);

            //activity log
            _customerActivityService.InsertActivity("DeleteTemplate", _localizationService.GetResource("ActivityLog.DeleteTemplate"), cardTemplate.Name);

            SuccessNotification(_localizationService.GetResource("Admin.Catalog.CardTemplate.Deleted"));
            return RedirectToAction("List");
        }

        #region Template Pages

        [HttpPost]
        public ActionResult PagesList(DataSourceRequest command, int templateId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var Pages = _cardTemplateService.GetPagesByTemplateId(templateId);
            var PagesModel = Pages
                .Select(x =>
                {
                    return new CardTemplateModel.TemplatePagesModel()
                    {
                        Id = x.Id,
                        PageName = _placeHolderService.GetPageById(x.PageId).Name,
                        PageId = x.PageId,
                        DisplayOrder = x.DisplayOrder,
                        TemplateId = templateId,
                    };
                })
                .ToList();

            var gridModel = new DataSourceResult
            {
                Data = PagesModel,
                Total = PagesModel.Count
            };

            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult PagesInsert(CardTemplateModel.TemplatePagesModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var templatePage = new TemplatePages()
            {
                PageId = model.PageId,
                TemplateId = model.TemplateId,
                DisplayOrder = model.DisplayOrder,
            };

            _cardTemplateService.InsertTemplatePage(templatePage);

            return new NullJsonResult();
        }

        [HttpPost]
        public ActionResult PagesUpdate(CardTemplateModel.TemplatePagesModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var templatePage = _cardTemplateService.GetTemplatePageById(model.Id);
            if (templatePage == null)
                throw new ArgumentException("No Page found with the specified id");

            templatePage.PageId = model.PageId;
            templatePage.DisplayOrder = model.DisplayOrder;
            templatePage.TemplateId = model.TemplateId;

            _cardTemplateService.UpdateTemplatePage(templatePage);

            return new NullJsonResult();
        }

        [HttpPost]
        public ActionResult PagesDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var templatePage = _cardTemplateService.GetTemplatePageById(id);
            if (templatePage == null)
                throw new ArgumentException("No page found with the specified id");

            _cardTemplateService.DeleteTemplatePage(templatePage);

            return new NullJsonResult();
        }

        #endregion
    }
}