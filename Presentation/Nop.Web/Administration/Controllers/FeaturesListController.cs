﻿using Nop.Admin.Models.Catalog;
using Nop.Admin.Models.FeaturesLists;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.FeaturesLists;
using Nop.Services.ExportImport;
using Nop.Services.FeaturesLists;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Admin.Controllers
{
    public partial class FeaturesListController : BaseAdminController
    {
        #region Fields

        private readonly IFeaturesListService _featuresListService;
        private readonly IPictureService _pictureService;
        private readonly IStoreService _storeService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IAclService _aclService;
        private readonly IPermissionService _permissionService;
        private readonly CatalogSettings _catalogSettings;

        #endregion

        #region Constructors

        public FeaturesListController(IFeaturesListService featuresListService,
            IPictureService pictureService,
            IStoreService storeService,
            IStoreMappingService storeMappingService,
            IUrlRecordService urlRecordService,
            ILanguageService languageService,
            ILocalizationService localizationService,
            ILocalizedEntityService localizedEntityService,
            ICustomerActivityService customerActivityService,
            IAclService aclService,
            IPermissionService permissionService,
            CatalogSettings catalogSettings)
        {
            this._featuresListService = featuresListService;
            this._pictureService = pictureService;
            this._storeService = storeService;
            this._storeMappingService = storeMappingService;
            this._urlRecordService = urlRecordService;
            this._languageService = languageService;
            this._localizationService = localizationService;
            this._localizedEntityService = localizedEntityService;
            this._customerActivityService = customerActivityService;
            this._aclService = aclService;
            this._permissionService = permissionService;
            this._catalogSettings = catalogSettings;
        }

        #endregion
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public ActionResult List(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            var featuresList = _featuresListService.GetAllFeaturesList(command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = featuresList.Select(x => x.ToModel()),
                Total = featuresList.TotalCount
            };

            return Json(gridModel);
        }

        public ActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            var model = new FeaturesListModel();

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(FeaturesListModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var featuresList = model.ToEntity();
                _featuresListService.InsertFeaturesList(featuresList);

                //activity log
                _customerActivityService.InsertActivity("AddNewFeaturesList", _localizationService.GetResource("ActivityLog.AddNewFeaturesList"), featuresList.Name);

                SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.FeaturesList.Added"));
                return continueEditing ? RedirectToAction("Edit", new { id = featuresList.Id }) : RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            var featuresList = _featuresListService.GetFeaturesListById(id);
            if (featuresList == null)
                //No Features List found with the specified id
                RedirectToAction("List");

            var model = featuresList.ToModel();

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(FeaturesListModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            var featuresList = _featuresListService.GetFeaturesListById(model.Id);
            if (featuresList == null)
                //No Features List found with the specified id
                RedirectToAction("List");

            if (ModelState.IsValid)
            {
                featuresList = model.ToEntity(featuresList);
                _featuresListService.UpdateFeaturesList(featuresList);

                //activity log
                _customerActivityService.InsertActivity("EditFeaturesList", _localizationService.GetResource("ActivityLog.EditFeaturesList"), featuresList.Name);

                SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.FeaturesList.Updated"));
                return continueEditing ? RedirectToAction("Edit", featuresList.Id) : RedirectToAction("List");
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            var featuresList = _featuresListService.GetFeaturesListById(id);
            if (featuresList == null)
                //No Page found with the specified id
                return RedirectToAction("List");

            _featuresListService.DeleteFeaturesList(featuresList);

            //activity log
            _customerActivityService.InsertActivity("DeleteFeaturesList", _localizationService.GetResource("ActivityLog.DeleteFeaturesList"), featuresList.Name);

            SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.FeaturesList.Deleted"));
            return RedirectToAction("List");
        }

        #region Features List Item

        [HttpPost]
        public ActionResult FeaturesListItemAdd(int FeaturesListId, int pictureId, string url, string comment, string arabicComment,
                                                string text, string arabicText, int displayOrder, bool visible)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            var featuresListItem = new FeaturesListItem()
            {
                FeaturesListId = FeaturesListId,
                PictureId = pictureId,
                URL = url,
                Comment = comment,
                ArabicComment = arabicComment,
                Text = text,
                ArabicText = arabicText,
                DisplayOrder = displayOrder,
                Visible = visible,
            };

            _featuresListService.InsertFeaturesListItem(featuresListItem);

            return Json(new { Result = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FeaturesListItemList(DataSourceRequest command, int FeaturesListId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            var featuresListItems = _featuresListService.GetFeaturesListItemsByFeaturesListId(FeaturesListId);
            var featuresListItemsModel = featuresListItems.Select(x => x.ToModel()).ToList();
            foreach (var item in featuresListItemsModel)
            {
                item.PictureURL = _pictureService.GetPictureUrl(item.PictureId);
            }

            var gridModel = new DataSourceResult
            {
                Data = featuresListItemsModel,
                Total = featuresListItemsModel.Count
            };

            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult FeaturesListItemUpdate(FeaturesListItemModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            var featuresListItem = _featuresListService.GetFeaturesListItemById(model.Id);
            if (featuresListItem == null)
                throw new ArgumentException("No Features List Item found with the specified id");
            
            featuresListItem.FeaturesListId = model.FeaturesListId;
            featuresListItem.PictureId = model.PictureId;
            featuresListItem.URL = model.URL;
            featuresListItem.Comment = model.Comment;
            featuresListItem.ArabicComment = model.ArabicComment;
            featuresListItem.Text = model.Text;
            featuresListItem.ArabicText = model.ArabicText;
            featuresListItem.DisplayOrder = model.DisplayOrder;
            featuresListItem.Visible = model.Visible;

            _featuresListService.UpdateFeaturesListItem(featuresListItem);

            return new NullJsonResult();
        }

        [HttpPost]
        public ActionResult FeaturesListItemsDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            var featureListItem = _featuresListService.GetFeaturesListItemById(id);
            if (featureListItem == null)
                throw new ArgumentException("No Features List Item found with the specified id");

            _featuresListService.DeleteFeaturesListItem(featureListItem);

            return new NullJsonResult();
        }

        #endregion
    }
}
