﻿using FluentValidation;
using Nop.Admin.Models.Catalog;
using Nop.Services.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Validators.Catalog
{
    public class CardPageValidator : AbstractValidator<CardPageModel>
    {
        public CardPageValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.PageIndex).NotEmpty().WithMessage(localizationService.GetResource("Admin.Catalog.CardPage.Fields.PageIndex.Required"));
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Catalog.CardPage.Fields.Name.Required"));
        }
    }
}