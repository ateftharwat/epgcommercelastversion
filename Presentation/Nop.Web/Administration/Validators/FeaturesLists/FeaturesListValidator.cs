﻿using FluentValidation;
using Nop.Admin.Models.FeaturesLists;
using Nop.Services.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Validators.FeaturesLists
{
    public class FeaturesListValidator : AbstractValidator<FeaturesListModel>
    {
        public FeaturesListValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.ContentManagement.FeaturesList.Fields.Name.Required"));
        }
    }
}