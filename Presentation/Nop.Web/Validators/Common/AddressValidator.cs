﻿using FluentValidation;
using Nop.Core.Domain.Common;
using Nop.Services.Localization;
using Nop.Web.Models.Common;
using System.Text.RegularExpressions;

namespace Nop.Web.Validators.Common
{
    public class AddressValidator : AbstractValidator<AddressModel>
    {
        public AddressValidator(ILocalizationService localizationService, AddressSettings addressSettings)
        {
            RuleFor(x => x.FirstName)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Address.Fields.FirstName.Required"));
            RuleFor(x => x.LastName)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Address.Fields.LastName.Required"));
            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Address.Fields.Email.Required"));
            RuleFor(x => x.FirstName).Length(1, 256).WithMessage(localizationService.GetResource("Account.Fields.FirstName.LengthValidation"));
            RuleFor(x => x.LastName).Length(1, 256).WithMessage(localizationService.GetResource("Account.Fields.LastName.LengthValidation"));

            RuleFor(x => x.Email)
                .EmailAddress()
                .WithMessage(localizationService.GetResource("Common.WrongEmail"));
            if (addressSettings.CountryEnabled)
            {
                //RuleFor(x => x.CountryId)
                //    .NotNull()
                //    .WithMessage(localizationService.GetResource("Address.Fields.Country.Required"));
                //RuleFor(x => x.CountryId)
                //    .NotEqual(0)
                //    .WithMessage(localizationService.GetResource("Address.Fields.Country.Required"));
                RuleFor(x => x.CountryName)
                    .NotNull()
                    .WithMessage(localizationService.GetResource("Address.Fields.Country.Required"));
                RuleFor(x => x.CountryName)
                    .NotEqual("0")
                    .WithMessage(localizationService.GetResource("Address.Fields.Country.Required"));
            }

            if (addressSettings.CompanyRequired && addressSettings.CompanyEnabled)
            {
                RuleFor(x => x.Company).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Company.Required"));
            }
            RuleFor(x => x.Company).Length(0, 256).WithMessage(localizationService.GetResource("Account.Fields.Company.LengthValidation"));
            if (addressSettings.StreetAddressRequired && addressSettings.StreetAddressEnabled)
            {
                RuleFor(x => x.Address1).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.StreetAddress.Required"));
            }
            RuleFor(x => x.Address1).Length(0, 256).WithMessage(localizationService.GetResource("Account.Fields.StreetAddress.LengthValidation"));
            if (addressSettings.StreetAddress2Required && addressSettings.StreetAddress2Enabled)
            {
                RuleFor(x => x.Address2).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.StreetAddress2.Required"));
            }
            RuleFor(x => x.Address2).Length(0, 256).WithMessage(localizationService.GetResource("Account.Fields.StreetAddress2.LengthValidation"));
            if (addressSettings.ZipPostalCodeRequired && addressSettings.ZipPostalCodeEnabled)
            {
                RuleFor(x => x.ZipPostalCode).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.ZipPostalCode.Required"));
            }
            RuleFor(x => x.ZipPostalCode).Length(0, 256).WithMessage(localizationService.GetResource("Account.Fields.ZipPostalCode.LengthValidation"));
            if (addressSettings.CityRequired && addressSettings.CityEnabled)
            {
                RuleFor(x => x.City).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.City.Required"));
            }
            RuleFor(x => x.City).Length(0, 256).WithMessage(localizationService.GetResource("Account.Fields.City.LengthValidation"));
            if (addressSettings.PhoneRequired && addressSettings.PhoneEnabled)
            {
                //phone number validation formats: [12345 or +12345]
                Regex Reg = new Regex("[\\+]{0,1}[0-9]+");
                RuleFor(x => x.PhoneNumber).Matches(Reg).WithMessage(localizationService.GetResource("Account.Fields.Phone.NotNumber"));
                RuleFor(x => x.PhoneNumber).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Phone.Required"));
            }
            RuleFor(x => x.PhoneNumber).Length(0, 256).WithMessage(localizationService.GetResource("Account.Fields.Phone.LengthValidation"));
            if (addressSettings.FaxRequired && addressSettings.FaxEnabled)
            {
                RuleFor(x => x.FaxNumber).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Fax.Required"));
            }
            RuleFor(x => x.FaxNumber).Length(0, 256).WithMessage(localizationService.GetResource("Account.Fields.Fax.LengthValidation"));
        }
    }
}