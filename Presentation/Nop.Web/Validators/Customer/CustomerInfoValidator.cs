﻿using FluentValidation;
using Nop.Core.Domain.Customers;
using Nop.Services.Localization;
using Nop.Web.Models.Customer;

namespace Nop.Web.Validators.Customer
{
    public class CustomerInfoValidator : AbstractValidator<CustomerInfoModel>
    {
        public CustomerInfoValidator(ILocalizationService localizationService, CustomerSettings customerSettings)
        {
            RuleFor(x => x.Email).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Email.Required"));
            RuleFor(x => x.Email).EmailAddress().WithMessage(localizationService.GetResource("Common.WrongEmail"));
            RuleFor(x => x.FirstName).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.FirstName.Required"));
            RuleFor(x => x.LastName).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.LastName.Required"));
            RuleFor(x => x.FirstName).Length(1, 256).WithMessage(localizationService.GetResource("Account.Fields.FirstName.LengthValidation"));
            RuleFor(x => x.LastName).Length(1, 256).WithMessage(localizationService.GetResource("Account.Fields.LastName.LengthValidation"));

            if (customerSettings.UsernamesEnabled && customerSettings.AllowUsersToChangeUsernames)
            {
                RuleFor(x => x.Username).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Username.Required"));
                RuleFor(x => x.Username).Length(1, 256).WithMessage(localizationService.GetResource("Account.Fields.Username.LengthValidation"));
            }

            //form fields
            if (customerSettings.CompanyRequired && customerSettings.CompanyEnabled)
            {
                RuleFor(x => x.Company).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Company.Required"));
            }
            RuleFor(x => x.Company).Length(0, 256).WithMessage(localizationService.GetResource("Account.Fields.Company.LengthValidation"));
            if (customerSettings.StreetAddressRequired && customerSettings.StreetAddressEnabled)
            {
                RuleFor(x => x.StreetAddress).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.StreetAddress.Required"));
            }
            RuleFor(x => x.StreetAddress).Length(0, 256).WithMessage(localizationService.GetResource("Account.Fields.StreetAddress.LengthValidation"));
            if (customerSettings.StreetAddress2Required && customerSettings.StreetAddress2Enabled)
            {
                RuleFor(x => x.StreetAddress2).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.StreetAddress2.Required"));
            }
            RuleFor(x => x.StreetAddress2).Length(0, 256).WithMessage(localizationService.GetResource("Account.Fields.StreetAddress2.LengthValidation"));
            if (customerSettings.ZipPostalCodeRequired && customerSettings.ZipPostalCodeEnabled)
            {
                RuleFor(x => x.ZipPostalCode).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.ZipPostalCode.Required"));
            }
            RuleFor(x => x.ZipPostalCode).Length(0, 256).WithMessage(localizationService.GetResource("Account.Fields.ZipPostalCode.LengthValidation"));
            if (customerSettings.CityRequired && customerSettings.CityEnabled)
            {
                RuleFor(x => x.City).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.City.Required"));
            }
            RuleFor(x => x.City).Length(0, 256).WithMessage(localizationService.GetResource("Account.Fields.City.LengthValidation"));
            if (customerSettings.PhoneRequired && customerSettings.PhoneEnabled)
            {
                RuleFor(x => x.Phone).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Phone.Required"));
            }
            RuleFor(x => x.Phone).Length(0, 256).WithMessage(localizationService.GetResource("Account.Fields.Phone.LengthValidation"));
            if (customerSettings.FaxRequired && customerSettings.FaxEnabled)
            {
                RuleFor(x => x.Fax).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Fax.Required"));
            }
            RuleFor(x => x.Fax).Length(0, 256).WithMessage(localizationService.GetResource("Account.Fields.Fax.LengthValidation"));
        }
    }
}