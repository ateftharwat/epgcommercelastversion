﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using Nop.Core.Domain.Catalog;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Catalog
{
    public class PlaceHolderMap : EntityTypeConfiguration<PlaceHolder>
    {
        public PlaceHolderMap()
        {
            this.ToTable("PlaceHolder");
            this.HasKey(p => p.Id);
            this.Property(p => p.Name).IsRequired();
            this.Property(p => p.TextMaxChars).IsOptional();
            this.Property(p => p.Deleted).IsRequired();
        }
    }
}
