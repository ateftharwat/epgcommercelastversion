﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    public partial class PagePlaceHoldersMap : EntityTypeConfiguration<PagePlaceHolders>
    {
        public PagePlaceHoldersMap()
        {
            this.ToTable("PagePlaceHolders");
            this.HasKey(pph => pph.Id);

            this.Property(pph => pph.XPos).IsRequired();
            this.Property(pph => pph.YPos).IsRequired();
            this.Property(pph => pph.Width).IsRequired();
            this.Property(pph => pph.Height).IsRequired();

            this.HasRequired(pph => pph.Page)
                .WithMany(p => p.PagePlaceHolders)
                .HasForeignKey(pph => pph.PageId);

            this.HasRequired(pph => pph.PlaceHolder)
                .WithMany()
                .HasForeignKey(pph => pph.PlaceHolderId);
        }
    }
}
