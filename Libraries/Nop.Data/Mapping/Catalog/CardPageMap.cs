﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using Nop.Core.Domain.Catalog;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Catalog
{
    class CardPageMap : EntityTypeConfiguration<CardPage>
    {
        public CardPageMap()
        {
            this.ToTable("CardPage");
            this.HasKey(c => c.Id);

            this.HasRequired(c => c.product)
                .WithMany(p => p.CardPages)
                .HasForeignKey(c => c.ProductId);

            this.HasRequired(c => c.placeHolder)
                .WithMany()
                .HasForeignKey(c => c.PlaceHolderId);

            this.Property(c => c.PageIndex).IsRequired();
            this.Property(c => c.Name).IsRequired();
            this.Property(c => c.XPos).IsRequired();
            this.Property(c => c.YPos).IsRequired();
            this.Property(c => c.Width).IsRequired();
            this.Property(c => c.Height).IsRequired();
        }
       
    }
}
