﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    public partial class TemplatePagesMap : EntityTypeConfiguration<TemplatePages>
    {
        public TemplatePagesMap()
        {
            this.ToTable("TemplatePages");
            this.HasKey(t => t.Id);

            this.Property(t => t.DisplayOrder).IsRequired();

            this.HasRequired(t => t.Page)
                .WithMany()
                .HasForeignKey(t => t.PageId);

            this.HasRequired(t => t.CardTemplate)
                .WithMany(c => c.TemplatePages)
                .HasForeignKey(t => t.TemplateId);
        }
    }
}
