﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Catalog;
using System.Data.Entity.ModelConfiguration;

namespace Nop.Data.Mapping.Catalog
{
    public partial class CardTemplateMap : EntityTypeConfiguration<CardTemplate>
    {
        public CardTemplateMap()
        {
            this.ToTable("CardTemplate");
            this.HasKey(t => t.Id);

            this.Property(c => c.Name).IsRequired();
        }
    }
}
