﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Catalog;
using System.Data.Entity.ModelConfiguration;

namespace Nop.Data.Mapping.Catalog
{
    public partial class PageMap : EntityTypeConfiguration<Page>
    {
        public PageMap()
        {
            this.ToTable("Page");
            this.HasKey(p => p.Id);

            this.Property(p => p.Name).IsRequired();
        }
    }
}
