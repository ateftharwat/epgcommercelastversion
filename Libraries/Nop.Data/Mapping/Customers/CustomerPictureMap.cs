﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Customers;

namespace Nop.Data.Mapping.Customers
{
    public partial class CustomerPictureMap : EntityTypeConfiguration<CustomerPicture>
    {
        public CustomerPictureMap()
        {
            this.ToTable("Customer_Picture_Mapping");
            this.HasKey(cp => cp.Id);

            this.HasRequired(cp => cp.Picture)
                .WithMany(p => p.CustomerPictures)
                .HasForeignKey(cp => cp.PictureId);


            this.HasRequired(cp => cp.Customer)
                .WithMany(c => c.CustomerPictures)
                .HasForeignKey(cp => cp.CustomerId);
        }
    }
}
