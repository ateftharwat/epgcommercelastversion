﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;

namespace Nop.Data.Mapping.Customers
{
    public partial class CustomerShippingAddressesMap : EntityTypeConfiguration<CustomerShippingAddresses>
    {
        public CustomerShippingAddressesMap()
        {
            this.ToTable("CustomerShippingAddresses");

            this.HasKey(c => c.Id);

            this.HasOptional(c => c.Product)
                .WithMany()
                .HasForeignKey(c => c.ProductId);

            this.HasRequired(c => c.ShippingAddress)
                .WithMany()
                .HasForeignKey(c => c.AddressId);

            this.HasRequired(c => c.Customer)
                .WithMany(cu => cu.ShippingAddresses)
                .HasForeignKey(c => c.CustomerId);
        }
    }
}
