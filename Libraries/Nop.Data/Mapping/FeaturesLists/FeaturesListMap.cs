﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.FeaturesLists;

namespace Nop.Data.Mapping.FeaturesLists
{
    public partial class FeaturesListMap : EntityTypeConfiguration<FeaturesList>
    {
        public FeaturesListMap()
        {
            this.ToTable("FeaturesList");
            this.HasKey(f => f.Id);

            this.Property(f => f.Name).IsRequired();
        }
    }
}
