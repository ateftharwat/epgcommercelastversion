﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.FeaturesLists;
using System.Data.Entity.ModelConfiguration;

namespace Nop.Data.Mapping.FeaturesLists
{
    public partial class FeaturesListItemMap : EntityTypeConfiguration<FeaturesListItem>
    {
        public FeaturesListItemMap()
        {
            this.ToTable("FeaturesListItem");
            this.HasKey(i => i.Id);

            this.HasRequired(i => i.FeaturesList)
                .WithMany(f => f.FeaturesListItems)
                .HasForeignKey(i => i.FeaturesListId);

            this.HasRequired(i => i.Picture)
                .WithMany()
                .HasForeignKey(i => i.PictureId);
        }
    }
}
