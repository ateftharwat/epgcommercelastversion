﻿using Nop.Core.Domain.Shipping;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Shipping
{
    public class ShippingRestrictionsMap : EntityTypeConfiguration<ShippingRestrictions>
    {
        public ShippingRestrictionsMap()
        {
            this.ToTable("ShippingRestrictions");
            this.HasKey(oShippingRestrictions => oShippingRestrictions.ShippingServiceID);
            this.Property(oShippingRestrictions => oShippingRestrictions.ShippingMethodId);
            this.Property(oShippingRestrictions => oShippingRestrictions.TotalWidth);
            this.Property(oShippingRestrictions => oShippingRestrictions.TotalLength);
            this.Property(oShippingRestrictions => oShippingRestrictions.TotalHeight);
            this.Property(oShippingRestrictions => oShippingRestrictions.TotalWeight);
        }
    }
}
