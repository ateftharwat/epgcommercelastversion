﻿using System.Data.Entity.ModelConfiguration;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Common;

namespace Nop.Data.Mapping.Orders
{
    public partial class ShoppingCartItemAddressMap : EntityTypeConfiguration<ShoppingCartItemAddress>
    {
        public ShoppingCartItemAddressMap()
        {
            this.ToTable("ShoppingCartItemAddresses");
            this.HasKey(sciadad => sciadad.Id);

            this.HasRequired(sciadad => sciadad.Address)
                .WithMany()
                .HasForeignKey(sciadad => sciadad.Address_Id);

            this.HasRequired(sciadad => sciadad.CartItem)
            .WithMany(sci => sci.ShoppingCartItemAddresses)
            .HasForeignKey(sciadad => sciadad.ShoppingCartItem_Id);
        }
    }
}
