using System.Data.Entity.ModelConfiguration;
using Nop.Core.Domain.Orders;

namespace Nop.Data.Mapping.Orders
{
    public partial class OrderItemShippingAddressMap : EntityTypeConfiguration<OrderItemShippingAddress>
    {
        public OrderItemShippingAddressMap()
        {
            this.ToTable("OrderItemShippingAddress");
            this.HasKey(oShippingAddress => oShippingAddress.Id);

            this.Property(oShippingAddress => oShippingAddress.OrderItemGuid);
            this.Property(oShippingAddress => oShippingAddress.ShippingMethod);
            this.Property(oShippingAddress => oShippingAddress.ShippingMethodID);
            this.HasRequired(oShippingAddress => oShippingAddress.Address)
                .WithMany()
                .HasForeignKey(oShippingAddress => oShippingAddress.AddressId);

            this.HasRequired(oShippingAddress => oShippingAddress.OrderItem)
            .WithMany(orderItem => orderItem.OrderItemShippingAddresses)
            .HasForeignKey(oShippingAddress => oShippingAddress.OrderItemId);
        }
    }
}