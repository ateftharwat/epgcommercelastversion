﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;

namespace BulkUpdateShippingStatus
{
    public partial class UpdateShippingStatus : ServiceBase
    {

        public UpdateShippingStatus()
        {
            InitializeComponent();
        }

        static System.Threading.Timer timer1;
        static System.Threading.Timer timer2;
        static System.Threading.Timer timer3;
        public static int interval;

        protected override void OnStart(string[] args)
        {
            interval = int.Parse(ConfigurationManager.AppSettings["TimeIntervalInSeconds"]);
            timer1 = new System.Threading.Timer(new TimerCallback(updateShippingStatus), null, interval * 1000, Timeout.Infinite);
            timer2 = new System.Threading.Timer(new TimerCallback(AddProductsFromInventory), null, interval * 1000, Timeout.Infinite);
            timer3 = new System.Threading.Timer(new TimerCallback(UpdateProductsQauntityFromInventory), null, interval * 1000, Timeout.Infinite);
        }

        protected override void OnStop()
        {
        }

        private static void updateShippingStatus(object sender)
        {
            try
            {
                timer1.Change(interval * 1000, Timeout.Infinite);
                string url = ConfigurationManager.AppSettings["UpdateShippingStatusServiceURL"];
                new WebClient().DownloadString(url);
            }
            catch (Exception ex)
            {

            }
        }

        private static void AddProductsFromInventory(object sender)
        {
            try
            {
                timer2.Change(interval * 1000, Timeout.Infinite);
                string url = ConfigurationManager.AppSettings["AddProductsFromInventory"];
                new WebClient().DownloadString(url);
            }
            catch (Exception ex)
            {

            }
        }

        private static void UpdateProductsQauntityFromInventory(object sender)
        {
            try
            {
                timer3.Change(interval * 1000, Timeout.Infinite);
                string url = ConfigurationManager.AppSettings["UpdateProductsQauntityFromInventory"];
                new WebClient().DownloadString(url);
            }
            catch (Exception ex)
            {

            }
        }

    }
}