
namespace Nop.Core.Domain.Orders
{
    /// <summary>
    /// Represents a shoping cart type
    /// </summary>
    public enum ShoppingCartType
    {
        /// <summary>
        /// Shopping cart
        /// </summary>
        ShoppingCart = 1,
        /// <summary>
        /// Wishlist
        /// </summary>
        Wishlist = 2,
        /// <summary>
        /// Mobile App
        /// </summary>
        MobileAppShoppingCart = 3,
        /// <summary>
        /// Self Relator
        /// </summary>
        Saveforlater = 4
    }
}
