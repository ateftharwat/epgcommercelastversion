﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Orders
{
    [Serializable]
    public class BookingVM
    {
        public int contractID { get; set; }
        public int senderSectionID { get; set; }
        public int contractCustomerID { get; set; }
        public String senderContractNumber { get; set; }
        public String receiverContractNumber { get; set; }
        public String userID { get; set; }
        public int shipmentTypeID { get; set; }
        public int originID { get; set; }
        public int desinationID { get; set; }
        public int serviceTypeID { get; set; }
        public int contentTypeID { get; set; }
        public int numberOfItems { get; set; }
        public int length { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public int dimenstionUnit { get; set; }
        public double totalWeight { get; set; }
        public int weightUnit { get; set; }
        public double customsValue { get; set; }
        public String customsCurrencyID { get; set; }
        public String contentDescription { get; set; }
        public AddressVM pickupAddress { get; set; }
        public AddressVM deliveryAddress { get; set; }
        public String shippingInstructions { get; set; }
        public int payMethodID { get; set; }
        public String callerContact { get; set; }
        public String callerContactPhone { get; set; }
        public string sender_customer_id { get; set; }
        public string receiver_customer_id { get; set; }
        public int earliestHour { get; set; }
        public int earliestMinute { get; set; }
        public int latestHour { get; set; }
        public int latestMinute { get; set; }
        public String preferredDate { get; set; }
    }

    public class AddressVM
    {
        public String addresseeName { get; set; }
        public String companyName { get; set; }
        public String addressLine1 { get; set; }
        public String addressLine2 { get; set; }
        public String addressLine3 { get; set; }
        public int cityID { get; set; }
        public String countryID { get; set; }
        public String zipCode { get; set; }
        public String phone { get; set; }
        public String email { get; set; }
    }
}
