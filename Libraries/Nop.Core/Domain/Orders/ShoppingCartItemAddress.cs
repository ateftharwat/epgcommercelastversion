﻿using Nop.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Orders
{
    public partial class ShoppingCartItemAddress : BaseEntity
    {
        /// <summary>
        /// Gets or sets the Shopping Cart item identifier
        /// </summary>
        public int ShoppingCartItem_Id { get; set; }

        /// <summary>
        /// Gets or sets the Shopping Cart item address Quantity
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the address identifier
        /// </summary>
        public int Address_Id { get; set; }

        /// <summary>
        /// Gets or sets the address
        /// </summary>
        public virtual Address Address { get; set; }

        /// <summary>
        /// Gets or sets the Cart Item
        /// </summary>
        public virtual ShoppingCartItem CartItem { get; set; }
    }
}
