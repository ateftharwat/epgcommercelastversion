﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Orders
{
    public partial class OrderItemPagePlaceHolder : BaseEntity
    {
        public string Type { get; set; }

        public string Text { get; set; }

        public string TextFamily { get; set; }

        public string TextStyle { get; set; }

        public string FontColor { get; set; }

        public int TextSize { get; set; }

        public int BrightnessValue { get; set; }

        public int ContrastValue { get; set; }

        public int ZoomValue { get; set; }

        public float RotationValue { get; set; }

        public string ImageURL { get; set; }

        public string FileName { get; set; }

        public int PictureId { get; set; }

        public string PictureData { get; set; }

        public decimal XPos { get; set; }

        public decimal YPos { get; set; }

        public decimal Width { get; set; }

        public decimal Height { get; set; }
    }
}
