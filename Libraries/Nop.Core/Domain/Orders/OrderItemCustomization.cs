﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Orders
{
    public partial class OrderItemCustomization : BaseEntity
    {
        public OrderItemCustomization()
        {
            pages = new List<OrderItemPage>();
        }

        public decimal Width { get; set; }

        public decimal Height { get; set; }

        public List<OrderItemPage> pages { get; set; }
    }
}
