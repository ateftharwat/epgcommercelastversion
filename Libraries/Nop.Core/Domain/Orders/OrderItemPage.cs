﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Orders
{
    public partial class OrderItemPage : BaseEntity
    {
        public OrderItemPage()
        {
            PlaceHolders = new List<OrderItemPagePlaceHolder>();
        }

        public string TemplatePictureURL { get; set; }

        public List<OrderItemPagePlaceHolder> PlaceHolders { get; set; }        
    }
}
