﻿using Nop.Core.Domain.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.FeaturesLists
{
    public partial class FeaturesListItem : BaseEntity
    {
        /// <summary>
        /// Gets or sets the Comment
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the Arabic Comment
        /// </summary>
        public string ArabicComment { get; set; }

        /// <summary>
        /// Gets or sets the URL Text
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets URL Arabic Text
        /// </summary>
        public string ArabicText { get; set; }

        // <summary>
        /// Gets or sets URL
        /// </summary>
        public string URL { get; set; }

        /// <summary>
        /// Gets or sets the Display Order
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets the value indicates the visiblity of the picture
        /// </summary>
        public bool Visible { get; set; }


        /// <summary>
        /// Gets or sets the FeaturesList Identifier
        /// </summary>
        public int FeaturesListId { get; set; }

        /// <summary>
        /// Gets or sets the Picture identifier
        /// </summary>
        public int PictureId { get; set; }


        /// <summary>
        /// Gets or sets the FeaturesList
        /// </summary>
        public virtual FeaturesList FeaturesList { get; set; }
        
        /// <summary>
        /// Gets or sets the Picture
        /// </summary>
        public virtual Picture Picture { get; set; }
    }
}
