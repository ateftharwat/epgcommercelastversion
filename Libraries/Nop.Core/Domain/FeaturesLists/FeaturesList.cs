﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.FeaturesLists
{
    public partial class FeaturesList : BaseEntity
    {
        private ICollection<FeaturesListItem> _featuresListItems;


        /// <summary>
        /// Gets or sets the Features List Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the FeaturesList Items
        /// </summary>
        public virtual ICollection<FeaturesListItem> FeaturesListItems
        {
            get { return _featuresListItems ?? (_featuresListItems = new List<FeaturesListItem>()); }
            protected set { _featuresListItems = value; }
        }
    }
}
