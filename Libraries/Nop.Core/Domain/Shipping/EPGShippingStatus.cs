namespace Nop.Core.Domain.Shipping
{
    /// <summary>
    /// Represents the epg shipping status enumeration
    /// </summary>
    public enum EPGShippingStatus : int
    {
        /// <summary>
        /// Not Shipped Yet
        /// </summary>
        NotShippedYet = 0,
        /// <summary>
        /// On Collection
        /// </summary>
        OnCollection = 1,
        /// <summary>
        /// At Hub
        /// </summary>
        AtHub = 2,
        /// <summary>
        /// In Transit
        /// </summary>
        InTransit = 3,
        /// <summary>
        /// On Hold
        /// </summary>
        OnHold = 4,
        /// <summary>
        /// Out For Delivery
        /// </summary>
        OutForDelivery = 5,
        /// <summary>
        /// Delivered
        /// </summary>
        Delivered = 6,
        /// <summary>
        /// Return To Sender
        /// </summary>
        ReturnToSender = 9,
        /// <summary>
        /// Out For Processing
        /// </summary>
        OutForProcessing = 10,
        /// <summary>
        /// Cancelled
        /// </summary>
        Cancelled = 11,
        /// <summary>
        /// Partially Delivered
        /// </summary>
        PartiallyDelivered = 12,
    }
}
