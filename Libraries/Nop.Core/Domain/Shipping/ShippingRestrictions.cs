﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Shipping
{
   public class ShippingRestrictions : BaseEntity
    {
       public int ShippingServiceID { get; set; }
       public int ShippingMethodId { get; set; }
       public decimal? TotalWidth { get; set; }
       public decimal? TotalLength { get; set; }
       public decimal? TotalHeight { get; set; }
       public decimal? TotalWeight { get; set; }

    }
}
