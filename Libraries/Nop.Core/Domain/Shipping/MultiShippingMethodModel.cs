﻿using System.Collections.Generic;
using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace Nop.Core.Domain.Shipping
{
    public partial class MultiShippingMethodModel
    {
        public MultiShippingMethodModel()
        {
            ItemsShippingMethods = new List<ItemShippingMethodModel>();
        }

        public IList<ItemShippingMethodModel> ItemsShippingMethods { get; set; }


        #region Nested classes

        public partial class ItemShippingMethodModel
        {
            public ItemShippingMethodModel()
            {
                ShippingOptions = new List<ShippingOptionsModel>();
                Warnings = new List<string>();
                ItemsNames = new List<string>();
                ItemsAddresses = new List<string>();
            }
            public List<string> ItemsNames { get; set; }

            public List<string> ItemsAddresses { get; set; }

            public int Count { get; set; }

            public int ItemId { get; set; }

            public List<ShippingOptionsModel> ShippingOptions { get; set; }

            public List<string> Warnings { get; set; }
        }
        public partial class ShippingOptionsModel
        {
            public string ShippingRateComputationMethodSystemName { get; set; }
            public string Name { get; set; }
            public string ID { get; set; }
            public string ExpectedDeliveryTime { get; set; }
            public string Fee { get; set; }
            public bool Selected { get; set; }

            public ShippingOption ShippingOption { get; set; } 
        }
        #endregion
    }
}