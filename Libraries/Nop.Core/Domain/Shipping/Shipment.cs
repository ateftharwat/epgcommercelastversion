using System;
using System.Collections.Generic;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Common;

namespace Nop.Core.Domain.Shipping
{
    /// <summary>
    /// Represents a shipment
    /// </summary>
    public partial class Shipment : BaseEntity
    {
        private ICollection<ShipmentItem> _shipmentItems;

        /// <summary>
        /// Gets or sets the order identifier
        /// </summary>
        public int OrderId { get; set; }
        
        /// <summary>
        /// Gets or sets the tracking number of this shipment
        /// </summary>
        public string TrackingNumber { get; set; }

        /// <summary>
        /// Gets or sets the Address identifier
        /// </summary>
        public int? AddressId { get; set; }

        /// <summary>
        /// Gets or sets the Shipping Method
        /// </summary>
        public string ShippingMethod { get; set; }

        public string ShippingMethodID { get; set; }

        /// <summary>
        /// Gets or sets the total weight of this shipment
        /// It's nullable for compatibility with the previous version of nopCommerce where was no such property
        /// </summary>
        public decimal? TotalWeight { get; set; }

        /// <summary>
        /// Gets or sets the Ready To Be Shipped date and time
        /// </summary>
        public DateTime? ReadyToBeShippedDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the shipped date and time
        /// </summary>
        public DateTime? ShippedDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the delivery date and time
        /// </summary>
        public DateTime? DeliveryDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the entity creation date
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets an epg shipping status identifier
        /// </summary>
        public int EPGShippingStatusId { get; set; }

        /// <summary>
        /// Gets the order
        /// </summary>
        public virtual Order Order { get; set; }

        /// <summary>
        /// Gets the address
        /// </summary>
        public virtual Address Address { get; set; }

        /// <summary>
        /// Gets or sets the shipment items
        /// </summary>
        public virtual ICollection<ShipmentItem> ShipmentItems
        {
            get { return _shipmentItems ?? (_shipmentItems = new List<ShipmentItem>()); }
            protected set { _shipmentItems = value; }
        }
        

        #region Custom properties

        /// <summary>
        /// Gets or sets the order status
        /// </summary>
        public EPGShippingStatus EPGShippingStatus
        {
            get
            {
                return (EPGShippingStatus)this.EPGShippingStatusId;
            }
            set
            {
                this.EPGShippingStatusId = (int)value;
            }
        }
        
        /// <summary>
        /// Booking Number of that return from EPG Service 
        /// </summary>
        public string BookingNumber { get; set; }

        #endregion
    }
}