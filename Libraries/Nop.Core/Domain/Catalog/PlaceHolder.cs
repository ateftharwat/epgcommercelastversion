﻿using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Seo;
using Nop.Core.Domain.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Catalog
{
    public class PlaceHolder : BaseEntity, ILocalizedEntity
    {
        /// <summary>
        /// Gets or sets the type
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the text maximum chars
        /// </summary>
        public int TextMaxChars { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity has been deleted
        /// </summary>
        public bool Deleted { get; set; }
        
    }
}
