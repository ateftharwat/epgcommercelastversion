﻿using Nop.Core.Domain.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Catalog
{
    public partial class Page : BaseEntity, ILocalizedEntity
    {
        private ICollection<PagePlaceHolders> _pagePlaceHolders;

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Page place Holders
        /// </summary>
        public virtual ICollection<PagePlaceHolders> PagePlaceHolders
        {
            get { return _pagePlaceHolders ?? (_pagePlaceHolders = new List<PagePlaceHolders>()); }
            protected set { _pagePlaceHolders = value; }
        }
    }
}
