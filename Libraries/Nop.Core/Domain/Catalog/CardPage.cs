﻿using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Seo;
using Nop.Core.Domain.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Catalog
{
    public partial class CardPage : BaseEntity, ILocalizedEntity
    {
        /// <summary>
        /// Gets or sets the product identifier
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the pages id
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// Gets or sets the pages name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the place holder id
        /// </summary>
        public int PlaceHolderId { get; set; }

        /// <summary>
        /// Gets or sets the x position
        /// </summary>
        public decimal XPos { get; set; }

        /// <summary>
        /// Gets or sets the y position
        /// </summary>
        public decimal YPos { get; set; }

        /// <summary>
        /// Gets or sets the width
        /// </summary>
        public decimal Width { get; set; }

        /// <summary>
        /// Gets or sets the height
        /// </summary>
        public decimal Height { get; set; }

        /// <summary>
        /// Gets product
        /// </summary>
        public virtual Product product { get; set; }

        /// <summary>
        /// Gets place Holder
        /// </summary>
        public virtual PlaceHolder placeHolder { get; set; }
    }
}
