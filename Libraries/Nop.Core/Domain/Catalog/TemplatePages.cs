﻿using Nop.Core.Domain.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Catalog
{
    public partial class TemplatePages : BaseEntity, ILocalizedEntity
    {
        /// <summary>
        /// Gets or sets the template id
        /// </summary>
        public int TemplateId { get; set; }

        /// <summary>
        /// Gets or sets the page id
        /// </summary>
        public int PageId { get; set; }        

        /// <summary>
        /// Gets or sets the Display Order
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets Card Template
        /// </summary>
        public virtual CardTemplate CardTemplate { get; set; }

        /// <summary>
        /// Gets Page
        /// </summary>
        public virtual Page Page { get; set; }
    }
}
