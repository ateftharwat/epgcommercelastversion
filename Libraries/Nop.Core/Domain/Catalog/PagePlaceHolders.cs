﻿using Nop.Core.Domain.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Catalog
{
    public partial class PagePlaceHolders : BaseEntity, ILocalizedEntity
    {
        /// <summary>
        /// Gets or sets the page id
        /// </summary>
        public int PageId { get; set; }

        /// <summary>
        /// Gets or sets the place holder id
        /// </summary>
        public int PlaceHolderId { get; set; }

        /// <summary>
        /// Gets or sets the x position
        /// </summary>
        public decimal XPos { get; set; }

        /// <summary>
        /// Gets or sets the y position
        /// </summary>
        public decimal YPos { get; set; }

        /// <summary>
        /// Gets or sets the width
        /// </summary>
        public decimal Width { get; set; }

        /// <summary>
        /// Gets or sets the height
        /// </summary>
        public decimal Height { get; set; }

        /// <summary>
        /// Gets place Holder
        /// </summary>
        public virtual PlaceHolder PlaceHolder { get; set; }

        /// <summary>
        /// Gets Page
        /// </summary>
        public virtual Page Page { get; set; }
    }
}
