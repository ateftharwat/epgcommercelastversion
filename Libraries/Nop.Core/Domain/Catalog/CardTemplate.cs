﻿using Nop.Core.Domain.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Catalog
{
    public partial class CardTemplate : BaseEntity, ILocalizedEntity
    {
        private ICollection<TemplatePages> _templatePages;

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Page place Holders
        /// </summary>
        public virtual ICollection<TemplatePages> TemplatePages
        {
            get { return _templatePages ?? (_templatePages = new List<TemplatePages>()); }
            protected set { _templatePages = value; }
        }
    }
}
