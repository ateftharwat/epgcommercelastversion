﻿using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Customers
{
    public partial class CustomerShippingAddresses : BaseEntity
    {
        public int CustomerId { get; set; }

        public int AddressId { get; set; }

        public int? ProductId { get; set; }

        public virtual Customer Customer { get; set; }

        public virtual Address ShippingAddress { get; set; }

        public virtual Product Product { get; set; }
    }
}
