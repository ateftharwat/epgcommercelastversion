﻿using Nop.Core.Domain.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Customers
{
    /// <summary>
    /// Represents a customer picture mapping
    /// </summary>
    public partial class CustomerPicture : BaseEntity
    {
        /// <summary>
        /// Gets or sets the Customer identifier
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the picture identifier
        /// </summary>
        public int PictureId { get; set; }

        /// <summary>
        /// Gets the picture
        /// </summary>
        public virtual Picture Picture { get; set; }

        /// <summary>
        /// Gets the Customer
        /// </summary>
        public virtual Customer Customer { get; set; }
    }
}
