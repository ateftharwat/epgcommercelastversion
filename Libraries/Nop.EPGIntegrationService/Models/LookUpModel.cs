﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.EPGIntegrationService.Models
{
    public class LookUpModel
    {
        public string LookUpId { get; set; }

        public string NameAr { get; set; }

        public string NameEn { get; set; }
    }
}