﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.EPGIntegrationService.Models
{
    public class OriginModel
    {
        public string emirate_id { get; set; }

        public string emirate_name_en { get; set; }

        public string emirate_name_ar { get; set; }
    }
}
