﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.EPGIntegrationService.Models
{
    public class ServiceTypeModel
    {
        public string serv_type_id { get; set; }

        public string serv_type_name_en { get; set; }

        public string serv_type_name_ar { get; set; }
    }
}
