﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.EPGIntegrationService.Models
{
    public class ShippedItemModel
    {
        /// <summary>
        /// Gets or sets the Item Id
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// Gets or sets the Service Type
        /// </summary>
        public string ServiceTypeId { get; set; }

        /// <summary>
        /// Gets or sets the weight
        /// </summary>
        public decimal Weight { get; set; }

        /// <summary>
        /// Gets or sets the weight unit
        /// </summary>
        public string WeightUnit { get; set; }

        /// <summary>
        /// Gets or sets the length
        /// </summary>
        public decimal Length { get; set; }

        /// <summary>
        /// Gets or sets the width
        /// </summary>
        public decimal Width { get; set; }

        /// <summary>
        /// Gets or sets the height
        /// </summary>
        public decimal Height { get; set; }

        /// <summary>
        /// Gets or sets the Dimension unit
        /// </summary>
        public string DimUnit { get; set; }

        /// <summary>
        /// Gets or sets shopping cart item shipping address Id
        /// </summary>
        public int AddressId { get; set; }

        /// <summary>
        /// Gets or sets shopping cart item shipping address destination Id
        /// </summary>
        public string DestinationId { get; set; }

        /// <summary>
        /// Shipped from address
        /// </summary>
        public string OriginId { get; set; }

        public int NumberOfDeliveryDays { get; set; }
    }
}