﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.EPGIntegrationService.Models
{
    public class EPGProduct
    {

        public string Itemcode { get; set; }

        public string InventoryItemID { get; set; }

        public string ItemName { get; set; }

        public string ItemDesc { get; set; }

        public string Price { get; set; }

        public string CreationDate { get; set; }

        public string UpdatedDate { get; set; }

        public string Quantity { get; set; }
    }
}
