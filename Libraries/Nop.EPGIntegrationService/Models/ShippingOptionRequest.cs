﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.EPGIntegrationService.Models
{
    public class ShippingOptionRequest
    {
        public ShippingOptionRequest()
        {
            items = new List<ShippedItemModel>();
        }

        public List<ShippedItemModel> items { get; set; }
    }
}