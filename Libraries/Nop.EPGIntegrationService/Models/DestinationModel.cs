﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.EPGIntegrationService.Models
{
    public class DestinationModel
    {
        public string country_id { get; set; }

        public string country_name_en { get; set; }

        public string country_name_ar { get; set; }
    }
}
