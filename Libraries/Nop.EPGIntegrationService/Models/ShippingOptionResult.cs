﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.EPGIntegrationService.Models
{
    public class ShippingOptionResult
    {
        public ShippingOptionResult()
        {
            options = new List<EPGShippingOption>();
        }

        public int ItemId { get; set; }

        public List<EPGShippingOption> options { get; set; }

    }
}