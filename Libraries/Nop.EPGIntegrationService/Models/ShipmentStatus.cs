﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.EPGIntegrationService.Models
{
    public class ShipmentStatus
    {
        public List<Consignment> consignment { get; set; }
        public List<ConsignmentShipmentlog> consignment_shipmentlog { get; set; }

        #region Nested Classes

        public class Consignment
        {
            public double? consignment_id { get; set; }
            public double? cons_status_id { get; set; }
            public string awb_num { get; set; }
            public double? ext_awb_num { get; set; }
            public string ref_number { get; set; }
            public string sender_country { get; set; }
            public string sender_emirate { get; set; }
            public string recpt_country { get; set; }
            public string recpt_emirate { get; set; }
            public string sub_service { get; set; }
            public string content_type { get; set; }
            public string dest { get; set; }
            public string origin { get; set; }
            public string status { get; set; }
            public DateTime? delivered_date { get; set; }
            public string cur_location { get; set; }
        }

        public class ConsignmentShipmentlog
        {
            public string dispatch_key { get; set; }
            public DateTime? dispatch_date { get; set; }
            public string dispatch_date_format { get; set; }
            public string src_section_name_en { get; set; }
            public string src_section_name_ar { get; set; }
            public string origin_scan_time { get; set; }
            public string des_section_name_en { get; set; }
            public string des_section_name_ar { get; set; }
            public string dest_scan_time { get; set; }
            public string remarks { get; set; }
            public string status_desc_en { get; set; }
            public string status_desc_ar { get; set; }
            public string cond_type_desc_en { get; set; }
            public string cond_type_desc_ar { get; set; }
            public string disp_type_name_en { get; set; }
            public string disp_type_name_ar { get; set; }
            public string tp_site_name_ar { get; set; }
            public string tp_site_name_en { get; set; }
            public string tp_site_name_ar1 { get; set; }
            public string tp_site_name_en1 { get; set; }
            public string assign_courier_login { get; set; }
            public double? item_type_id { get; set; }
            public string item_type_desc_en { get; set; }
            public string item_type_desc_ar { get; set; }
            public string phone_num { get; set; }
            public double? cons_status_id { get; set; }
            public double? dispatch_type { get; set; }
            public string full_name { get; set; }
            public string non_del_reason_en { get; set; }
            public string non_del_reason_ar { get; set; }
            public string sender_emirate_ar { get; set; }
            public string sender_emirate_en { get; set; }
            public string recpt_emirate_ar { get; set; }
            public string recpt_emirate_en { get; set; }
        }

        #endregion
    }
}
