﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.EPGIntegrationService.Models
{
    public class EPGProductQuantity
    {
        public string Itemcode { get; set; }
        public string TransactionID { get; set; }
        public string Quantity { get; set; }
        public string UpdatedDate { get; set; }
    }
}
