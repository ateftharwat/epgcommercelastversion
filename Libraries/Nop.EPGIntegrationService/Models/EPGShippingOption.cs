﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.EPGIntegrationService.Models
{
    public class EPGShippingOption
    {
        public double col_id { get; set; }
        /// <summary>
        /// Gets or sets Service Description
        /// </summary>
        public string col_desc_en { get; set; }
        public string col_desc_ar { get; set; }
        /// <summary>
        /// Gets or sets the Total Charge
        /// </summary>
        public decimal final_rate { get; set; }
        /// <summary>
        /// Gets or sets the Expected Delivery Time
        /// </summary>
        public string del_date { get; set; }

        public string content_type_id { get; set; }
        public string content_type { get; set; }
    }
}