﻿using Newtonsoft.Json.Linq;
using Nop.EPGIntegrationService.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace Nop.EPGIntegrationService
{
    public class IntegrationService
    {
        #region Utlities

        public static string SendRequest(string URL, string methodType, Dictionary<string, string> requestParamters)
        {
            try
            {
                string postString = null;
                HttpWebRequest httpWebRequest = null;
                string posturl = URL;
                if (methodType == "GET")
                {
                    if (requestParamters != null)
                    {
                        posturl += "?";
                        foreach (var postVariable in requestParamters)
                        {
                            posturl = posturl + postVariable.Key + "=" + postVariable.Value + "&";
                        }
                    }
                    httpWebRequest = (HttpWebRequest)WebRequest.Create(posturl);
                    httpWebRequest.Method = methodType;
                }
                else
                {

                    httpWebRequest = (HttpWebRequest)WebRequest.Create(posturl);
                    httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                    httpWebRequest.Method = methodType;
                    postString = dictionaryToPostString(requestParamters);
                    byte[] byteData = Encoding.UTF8.GetBytes(postString);
                    httpWebRequest.ContentLength = byteData.Length;
                    Stream requestStream = httpWebRequest.GetRequestStream();
                    requestStream.Write(byteData, 0, byteData.Length);
                    requestStream.Close();

                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                string result = string.Empty;
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                return result;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }

        }

        public static string SendRequest(string URL, string methodType, Dictionary<string, string> requestParamters, Dictionary<string, string> headersParamters)
        {
            try
            {
                string postString = string.Empty;
                HttpWebRequest httpWebRequest = null;
                string posturl = URL;
                if (methodType == "GET")
                {
                    if (requestParamters != null)
                    {
                        posturl += "?";
                        foreach (var postVariable in requestParamters)
                        {
                            posturl = posturl + postVariable.Key + "=" + postVariable.Value + "&";
                        }
                    }
                    httpWebRequest = (HttpWebRequest)WebRequest.Create(posturl);
                    httpWebRequest.Method = methodType;
                }
                else
                {

                    httpWebRequest = (HttpWebRequest)WebRequest.Create(posturl);
                    httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                    httpWebRequest.Method = methodType;
                    if (headersParamters.Count > 0)
                    {
                        foreach (var item in headersParamters)
                        {
                            httpWebRequest.Headers.Add(item.Key,item.Value);
                        }
                    }
                    if (requestParamters.Count > 0)
                    {
                        postString = dictionaryToPostString(requestParamters);
                    }
                    byte[] byteData = Encoding.UTF8.GetBytes(postString);
                    httpWebRequest.ContentLength = byteData.Length;
                    Stream requestStream = httpWebRequest.GetRequestStream();
                    requestStream.Write(byteData, 0, byteData.Length);
                    requestStream.Close();

                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                string result = string.Empty;
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                return result;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }

        }

        public static string dictionaryToPostString(Dictionary<string, string> postVariables)
        {
            string postString = "";
            foreach (KeyValuePair<string, string> pair in postVariables)
            {
                postString += HttpUtility.UrlEncode(pair.Key) + "=" + pair.Value + "&";
            }

            return postString;
        }

        public static string dictionaryToGetString(Dictionary<string, string> postVariables)
        {
            string postString = "?";
            foreach (KeyValuePair<string, string> pair in postVariables)
            {
                postString += HttpUtility.UrlEncode(pair.Key) + "=" + pair.Value + "&";
            }

            return postString;
        }

        #endregion

        /// <summary>
        /// Convert Currency Integration Service
        /// </summary>
        /// <param name="fromCurrencyCode">currency to convert from</param>
        /// <param name="toCurrencyCode">currency to convert to</param>
        /// <param name="amount">value</param>
        /// <returns>converted value</returns>
        public decimal ConvertCurrency(string fromCurrencyCode, string toCurrencyCode, decimal amount)
        {
            try
            {
                Dictionary<string, string> RequestParamters = new Dictionary<string, string>();

                RequestParamters.Add("fromCurrencyCode", fromCurrencyCode);
                RequestParamters.Add("toCurrencyCode", toCurrencyCode);
                RequestParamters.Add("amount", amount.ToString());

                string URL = ConfigurationManager.AppSettings["ConvertCurrencyServiceURL"];
                string responseContent = SendRequest(URL, "GET", RequestParamters);

                ConvertedCurrency ConvertedCurrency = new ConvertedCurrency();
                if (!string.IsNullOrEmpty(responseContent))
                {
                    ConvertedCurrency = (ConvertedCurrency)new JavaScriptSerializer().Deserialize(responseContent, typeof(ConvertedCurrency));
                }

                return ConvertedCurrency.Result;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Get service types Integration Service
        /// </summary>
        /// <returns>collection of service types</returns>
        public List<ServiceTypeModel> GetServiceTypes()
        {
            try
            {
                var Serviceypes = new List<ServiceTypeModel>();

                //Service Integration
                string url = ConfigurationManager.AppSettings["GetServiceTypesServiceURL"];
                string ServiceypesList = SendRequest(url, "GET", null);

                if (!string.IsNullOrEmpty(ServiceypesList))
                    Serviceypes = (List<ServiceTypeModel>)new JavaScriptSerializer().Deserialize(ServiceypesList, typeof(List<ServiceTypeModel>));

                return Serviceypes;
            }
            catch (Exception ex)
            {
                return new List<ServiceTypeModel>();
            }
        }

        public int GetshipmentTypeIdByType(bool isLocal)
        {
            List<ServiceTypeModel> lstServiceTypes = GetServiceTypes();
            foreach (var serviceType in lstServiceTypes)
            {
                if (isLocal)
                {
                    if (serviceType.serv_type_name_en.ToLower() == "local")
                    {
                        return Convert.ToInt32(decimal.Parse(serviceType.serv_type_id));
                    }
                }
                else
                {
                    if (serviceType.serv_type_name_en.ToLower() == "international outgoing")
                    {
                        return Convert.ToInt32(decimal.Parse(serviceType.serv_type_id));
                    }
                }

            }
            return 0;
        }
  
        /// <summary>
        /// Get List of origins Integration Service
        /// </summary>
        /// <returns>collection of origins</returns>
        public List<OriginModel> GetOrigins()
        {
            try
            {
                var origins = new List<OriginModel>();

                //Service Integration
                string url = ConfigurationManager.AppSettings["GetOriginsServiceURL"];
                string originList = SendRequest(url, "GET", null);

                if (!string.IsNullOrEmpty(originList))
                    origins = (List<OriginModel>)new JavaScriptSerializer().Deserialize(originList, typeof(List<OriginModel>));

                return origins;
            }
            catch (Exception ex)
            {
                return new List<OriginModel>();
            }
        }

        /// <summary>
        /// Get List of Destination Integration Service
        /// </summary>
        /// <param name="serviceTypeParam">service type</param>
        /// <returns>collection of local or international destinations</returns>
        public List<object> GetDestinations(string serviceTypeParam)
        {
            try
            {
                List<object> Destinations = new List<object>();

                if (serviceTypeParam == "Local")
                {
                    string url = ConfigurationManager.AppSettings["GetOriginsServiceURL"];
                    string originList = SendRequest(url, "GET", null);

                    if (!string.IsNullOrEmpty(originList))
                    {
                        var dest = (List<OriginModel>)new JavaScriptSerializer().Deserialize(originList, typeof(List<OriginModel>));
                        Destinations = dest.Cast<object>().ToList();
                    }
                }
                else if (serviceTypeParam == "International Outgoing")
                {
                    string url = ConfigurationManager.AppSettings["GetDestinationsServiceURL"];
                    string destinationList = SendRequest(url, "GET", null);

                    if (!string.IsNullOrEmpty(destinationList))
                    {
                        var dest = (List<DestinationModel>)new JavaScriptSerializer().Deserialize(destinationList, typeof(List<DestinationModel>));
                        Destinations = dest.Cast<object>().ToList();
                    }
                }
                return Destinations;
            }
            catch (Exception ex)
            {
                return new List<object>();
            }
        }

        /// <summary>
        /// Get Shipping Options Integration Service
        /// </summary>
        /// <param name="request">shipping option request</param>
        /// <returns>collection of shipping options</returns>
        public ShippingOptionResult GetShippingOptions(ShippedItemModel request, string localizedErrorMessage)
        {
            try
            {
                var result = new ShippingOptionResult()
                {
                    ItemId = request.ItemId,
                };

                //Service Integration
                string url = ConfigurationManager.AppSettings["GetShippingOptionsServiceURL"];

                Dictionary<string, string> PostData = new Dictionary<string, string>();
                PostData["shipmentID"] = request.ItemId.ToString();
                decimal serviceType = 0;
                decimal.TryParse(request.ServiceTypeId, out serviceType);
                PostData["serviceTypeID"] = ((int)serviceType).ToString();
                PostData["totalWeight"] = request.Weight.ToString();
                PostData["length"] = request.Length.ToString();
                PostData["width"] = request.Width.ToString();
                PostData["height"] = request.Height.ToString();
                PostData["dimensionUnit"] = request.WeightUnit;
                decimal originId = 0;
                decimal.TryParse(request.OriginId, out originId);
                PostData["originID"] = ((int)originId).ToString();
                PostData["originAreaID"] = "-1";
                decimal destinationId = 0;
                decimal.TryParse(request.DestinationId, out destinationId);
                PostData["destinationID"] = ((int)destinationId).ToString();
                PostData["destinationAreaID"] = "-1";
                PostData["contractID"] = "-1";

                string shippingOptions = SendRequest(url, "POST", PostData);

                if (!string.IsNullOrEmpty(shippingOptions))
                    result.options = (List<EPGShippingOption>)new JavaScriptSerializer().Deserialize(shippingOptions, typeof(List<EPGShippingOption>));
                else
                    throw new Exception(localizedErrorMessage);

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(localizedErrorMessage);
            }
        }

        /// <summary>
        /// Get Shipping Status Integration Service
        /// </summary>
        /// <param name="trackingNumber">shipment tracking number</param>
        /// <returns>shipment status</returns>
        public ShipmentStatus GetShippingStatus(string trackingNumber)
        {
            try
            {
                var shippingStatus = new ShipmentStatus();

                //Service Integration
                string url = ConfigurationManager.AppSettings["GetOrderShippingStatusServiceURL"];

                Dictionary<string, string> PostData = new Dictionary<string, string>();
                PostData["AWBNumber"] = trackingNumber;

                string status = SendRequest(url, "GET", PostData);

                if (!string.IsNullOrEmpty(status))
                    shippingStatus = (ShipmentStatus)new JavaScriptSerializer().Deserialize(status, typeof(ShipmentStatus));

                return shippingStatus;
            }
            catch (Exception ex)
            {
                return new ShipmentStatus();
            }
        }

        public List<EPGProduct> GetProductsFromInventory()
        {
            List<EPGProduct> products = new List<EPGProduct>();
            //Service Integration
            string url = ConfigurationManager.AppSettings["GetItemMasterURL"];
            Dictionary<string, string> PostData = new Dictionary<string, string>();
            string result = SendRequest(url, "GET", PostData);

            if (result.StartsWith("["))
            {
                var arr = JArray.Parse(result);
                var count = arr.Count() >= 1000 ? 1000 : arr.Count();
                for (int i = 0; i < count; i++)
                {
                    EPGProduct product = new EPGProduct();
                    dynamic data = JObject.Parse(arr[i].ToString());
                    product.ItemName = data.item_description;
                    product.ItemDesc = data.item_description;
                    product.InventoryItemID = data.inventory_item_id;
                    product.Itemcode = data.item_code;
                    product.Price = data.list_price_per_unit;
                    product.CreationDate = data.creation_date;
                    product.UpdatedDate = data.last_update_date;
                    products.Add(product);
                } 
            }
            return products;
        }

        public void UpdateItemsFetchedFromInventory(string lstitemCodes)
        {
            string url = ConfigurationManager.AppSettings["UpdateItemsFetchedFromInventory"];
            Dictionary<string, string> PostData = new Dictionary<string, string>();
            PostData.Add("oracle_item_code_list", lstitemCodes);
            string result = SendRequest(url, "Post", PostData);
        }

        public List<EPGProductQuantity> GetProductsQuantityFromInventory()
        {
            List<EPGProductQuantity> productsQuantity = new List<EPGProductQuantity>();
            string url = ConfigurationManager.AppSettings["Inventory"];
            Dictionary<string, string> PostData = new Dictionary<string, string>();
            string result = SendRequest(url, "GET", PostData);

            if (result.StartsWith("["))
            {
                var arr = JArray.Parse(result);
                var count = arr.Count() >= 1000 ? 1000 : arr.Count();
                for (int i = 0; i < count; i++)
                {
                    EPGProductQuantity epgProductQuantity = new EPGProductQuantity();
                    dynamic data = JObject.Parse(arr[i].ToString());
                    epgProductQuantity.Itemcode = data.oracle_item_code;
                    epgProductQuantity.TransactionID = data.transaction_id;
                    epgProductQuantity.UpdatedDate = data.last_update_date;
                    epgProductQuantity.Quantity = data.quantity;
                    productsQuantity.Add(epgProductQuantity);
                }
            }
            return productsQuantity;
        }

        public void UpdateProductsQuantityinventoryfetched(string lsttransactionIds)
        {
            string url = ConfigurationManager.AppSettings["UpdateInventoryFetched"];
            Dictionary<string, string> PostData = new Dictionary<string, string>();
            PostData.Add("transaction_id_list", lsttransactionIds);
            string result = SendRequest(url, "Post", PostData);
        }

        public bool AddTransactioninInventory(string ItemCode, int quantity,int orderID,string price)
        {
            string url = ConfigurationManager.AppSettings["AddInventoryTransaction"];
            Dictionary<string, string> PostData = new Dictionary<string, string>();
            PostData.Add("oracle_item_code", ItemCode);
            PostData.Add("quantity", quantity.ToString());
            PostData.Add("txn_reference", orderID.ToString());
            PostData.Add("unit_selling_price", price.ToString());
            string result = SendRequest(url, "Post", PostData);
             if(result.ToLower().Contains("error"))
             {
                 return false;
             }
            else
             {
                 return true;
             }
        }
       
        public void TransactionAudit(string orderID,string type,string amount,string data)
        {
            string url = ConfigurationManager.AppSettings["TransactionAudit"];
            Dictionary<string, string> PostData = new Dictionary<string, string>();
            PostData.Add("txn_reference", orderID);
            PostData.Add("txn_type", type);
            PostData.Add("amount", amount);
            PostData.Add("txn_data", data);
            string result = SendRequest(url, "Post", PostData);
        }
    }
}
