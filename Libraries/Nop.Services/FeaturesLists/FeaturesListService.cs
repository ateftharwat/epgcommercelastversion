﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.FeaturesLists;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Stores;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.FeaturesLists
{
    public partial class FeaturesListService : IFeaturesListService
    {
        #region Fields

        private readonly IRepository<FeaturesList> _featuresListRepository;
        private readonly IRepository<FeaturesListItem> _featuresListItemRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<AclRecord> _aclRepository;
        private readonly IRepository<StoreMapping> _storeMappingRepository;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;
        private readonly CatalogSettings _catalogSettings;


        #endregion

        #region Ctor

        public FeaturesListService(ICacheManager cacheManager,
            IRepository<FeaturesList> featuresListRepository,
            IRepository<FeaturesListItem> featuresListItemRepository,
            IRepository<Product> productRepository,
            IRepository<AclRecord> aclRepository,
            IRepository<StoreMapping> storeMappingRepository,
            IWorkContext workContext,
            IStoreContext storeContext,
            CatalogSettings catalogSettings,
            IEventPublisher eventPublisher)
        {
            this._cacheManager = cacheManager;
            this._featuresListRepository = featuresListRepository;
            this._featuresListItemRepository = featuresListItemRepository;
            this._productRepository = productRepository;
            this._aclRepository = aclRepository;
            this._storeMappingRepository = storeMappingRepository;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._catalogSettings = catalogSettings;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Features List

        /// <summary>
        /// Gets all Feature Lists
        /// </summary>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>Feature List collection</returns>
        public virtual IPagedList<FeaturesList> GetAllFeaturesList(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = from f in _featuresListRepository.Table
                        orderby f.Name
                        select f;
            var featuresList = new PagedList<FeaturesList>(query, pageIndex, pageSize);

            return featuresList;
        }

        /// <summary>
        /// Gets a Feature List by identifier
        /// </summary>
        /// <param name="featuresListId">Feature List identifier</param>
        /// <returns>Feature List collection</returns>
        public virtual FeaturesList GetFeaturesListById(int featuresListId)
        {
            if (featuresListId == 0)
                return null;

            return _featuresListRepository.GetById(featuresListId);
        }

        /// <summary>
        /// Gets a Feature List by name
        /// </summary>
        /// <param name="featuresListId">Feature List name</param>
        /// <returns>Feature List collection</returns>
        public virtual FeaturesList GetFeaturesListByName(string featuresListName)
        {
            if (featuresListName == null)
                return null;

            var query = from f in _featuresListRepository.Table
                        where f.Name == featuresListName
                        select f;
            var featuresList = query.ToList().FirstOrDefault();

            return featuresList;
        }

        /// <summary>
        /// Inserts a Feature List
        /// </summary>
        /// <param name="featuresList">featuresList</param>
        public virtual void InsertFeaturesList(FeaturesList featuresList)
        {
            if (featuresList == null)
                throw new ArgumentNullException("FeaturesList");

            _featuresListRepository.Insert(featuresList);

            //event notification
            _eventPublisher.EntityInserted(featuresList);
        }

        /// <summary>
        /// Updates a Feature List
        /// </summary>
        /// <param name="featuresList">featuresList</param>
        public virtual void UpdateFeaturesList(FeaturesList featuresList)
        {
            if (featuresList == null)
                throw new ArgumentNullException("FeaturesList");

            _featuresListRepository.Update(featuresList);

            //event notification
            _eventPublisher.EntityUpdated(featuresList);
        }

        /// <summary>
        /// Deletes a Feature List
        /// </summary>
        /// <param name="featuresList">featuresList</param>
        public virtual void DeleteFeaturesList(FeaturesList featuresList)
        {
            if (featuresList == null)
                throw new ArgumentNullException("FeaturesList");

            _featuresListRepository.Delete(featuresList);

            //event notification
            _eventPublisher.EntityDeleted(featuresList);
        }
        
        #endregion

        #region Features List Item

        /// <summary>
        /// get all features List Items for specific features List
        /// </summary>
        /// <param name="featuresListId">features List Identifier</param>
        /// <returns>features List Items collection</returns>
        public virtual IList<FeaturesListItem> GetFeaturesListItemsByFeaturesListId(int featuresListId)
        {
            if (featuresListId == 0)
                return new List<FeaturesListItem>();

            var query = from fi in _featuresListItemRepository.Table
                        where fi.FeaturesListId == featuresListId
                        orderby fi.DisplayOrder
                        select fi;

            var featuresListItems = query.ToList();

            return featuresListItems;
        }

        /// <summary>
        /// Inserts a Features List Item
        /// </summary>
        /// <param name="featuresListItem">features List Item</param>
        public virtual void InsertFeaturesListItem(FeaturesListItem featuresListItem)
        {
            if (featuresListItem == null)
                throw new ArgumentNullException("FeaturesListItem");

            _featuresListItemRepository.Insert(featuresListItem);

            //event notification
            _eventPublisher.EntityInserted(featuresListItem);
        }

        /// <summary>
        /// Gets a Features List Item
        /// </summary>
        /// <param name="featuresListItemId">Features List Item identifier</param>
        /// <returns>Features List Itemr</returns>
        public virtual FeaturesListItem GetFeaturesListItemById(int featuresListItemId)
        {
            if (featuresListItemId == 0)
                return null;

            return _featuresListItemRepository.GetById(featuresListItemId);
        }

        /// <summary>
        /// Updates the Features List Item
        /// </summary>
        /// <param name="featuresListItem">Features List Item</param>
        public virtual void UpdateFeaturesListItem(FeaturesListItem featuresListItem)
        {
            if (featuresListItem == null)
                throw new ArgumentNullException("FeaturesListItem");

            _featuresListItemRepository.Update(featuresListItem);

            //event notification
            _eventPublisher.EntityUpdated(featuresListItem);
        }

        /// <summary>
        /// Deletes a Features List Item
        /// </summary>
        /// <param name="featuresListItem">Features List Item</param>
        public virtual void DeleteFeaturesListItem(FeaturesListItem featuresListItem)
        {
            if (featuresListItem == null)
                throw new ArgumentNullException("FeaturesListItem");

            _featuresListItemRepository.Delete(featuresListItem);

            //event notification
            _eventPublisher.EntityDeleted(featuresListItem);
        }

        #endregion
    }
}
