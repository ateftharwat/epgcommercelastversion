﻿using Nop.Core;
using Nop.Core.Domain.FeaturesLists;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.FeaturesLists
{
    public partial interface IFeaturesListService
    {
        #region Features List

        /// <summary>
        /// Gets all Feature Lists
        /// </summary>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>Feature List collection</returns>
        IPagedList<FeaturesList> GetAllFeaturesList(int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Gets a Feature List by identifier
        /// </summary>
        /// <param name="featuresListId">Feature List identifier</param>
        /// <returns>Feature List collection</returns>
        FeaturesList GetFeaturesListById(int featuresListId);

        /// <summary>
        /// Gets a Feature List by name
        /// </summary>
        /// <param name="featuresListId">Feature List name</param>
        /// <returns>Feature List collection</returns>
        FeaturesList GetFeaturesListByName(string featuresListName);

        /// <summary>
        /// Inserts a Feature List
        /// </summary>
        /// <param name="featuresList">featuresList</param>
        void InsertFeaturesList(FeaturesList featuresList);

        /// <summary>
        /// Updates a Feature List
        /// </summary>
        /// <param name="featuresList">featuresList</param>
        void UpdateFeaturesList(FeaturesList featuresList);

        /// <summary>
        /// Deletes a Feature List
        /// </summary>
        /// <param name="featuresList">featuresList</param>
        void DeleteFeaturesList(FeaturesList featuresList);

        #endregion

        #region Features List Item

        /// <summary>
        /// get all features List Items for specific features List
        /// </summary>
        /// <param name="featuresListId">features List Identifier</param>
        /// <returns>features List Items collection</returns>
        IList<FeaturesListItem> GetFeaturesListItemsByFeaturesListId(int featuresListId);

        /// <summary>
        /// Inserts a Features List Item
        /// </summary>
        /// <param name="featuresListItem">features List Item</param>
        void InsertFeaturesListItem(FeaturesListItem featuresListItem);

        /// <summary>
        /// Gets a Features List Item
        /// </summary>
        /// <param name="featuresListItemId">Features List Item identifier</param>
        /// <returns>Features List Itemr</returns>
        FeaturesListItem GetFeaturesListItemById(int featuresListItemId);

        /// <summary>
        /// Updates the Features List Item
        /// </summary>
        /// <param name="featuresListItem">Features List Item</param>
        void UpdateFeaturesListItem(FeaturesListItem featuresListItem);

        /// <summary>
        /// Deletes a Features List Item
        /// </summary>
        /// <param name="featuresListItem">Features List Item</param>
        void DeleteFeaturesListItem(FeaturesListItem featuresListItem);

        #endregion
    }
}
