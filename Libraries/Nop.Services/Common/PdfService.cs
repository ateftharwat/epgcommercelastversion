﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Core.Html;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Stores;
using Nop.EPGIntegrationService.Models;
using Nop.EPGIntegrationService;
using Nop.Services.Shipping;
using iTextSharp.text.html.simpleparser;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;


namespace Nop.Services.Common
{
    /// <summary>
    /// PDF service
    /// </summary>
    public partial class PdfService : IPdfService
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly ILanguageService _languageService;
        private readonly IWorkContext _workContext;
        private readonly IAddressService _addressService;
        private readonly IOrderService _orderService;
        private readonly IShipmentService _shipmentService;
        private readonly IPaymentService _paymentService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPriceFormatter _priceFormatter;
        private readonly ICurrencyService _currencyService;
        private readonly IMeasureService _measureService;
        private readonly IPictureService _pictureService;
        private readonly IProductService _productService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IStoreService _storeService;
        private readonly IStoreContext _storeContext;
        private readonly ISettingService _settingContext;
        private readonly IWebHelper _webHelper;

        private readonly CatalogSettings _catalogSettings;
        private readonly CurrencySettings _currencySettings;
        private readonly MeasureSettings _measureSettings;
        private readonly PdfSettings _pdfSettings;
        private readonly TaxSettings _taxSettings;
        private readonly AddressSettings _addressSettings;

        private readonly IntegrationService EPGServices;

        #endregion

        #region Ctor

        public PdfService(ILocalizationService localizationService,
            ILanguageService languageService,
            IWorkContext workContext,
            IAddressService addressService,
            IOrderService orderService,
            IShipmentService shipmentService,
            IPaymentService paymentService,
            IDateTimeHelper dateTimeHelper,
            IPriceFormatter priceFormatter,
            ICurrencyService currencyService,
            IMeasureService measureService,
            IPictureService pictureService,
            IProductService productService,
            IProductAttributeParser productAttributeParser,
            IStoreService storeService,
            IStoreContext storeContext,
            ISettingService settingContext,
            IWebHelper webHelper,
            CatalogSettings catalogSettings,
            CurrencySettings currencySettings,
            MeasureSettings measureSettings,
            PdfSettings pdfSettings,
            TaxSettings taxSettings,
            AddressSettings addressSettings)
        {
            this._localizationService = localizationService;
            this._languageService = languageService;
            this._workContext = workContext;
            this._addressService = addressService;
            this._orderService = orderService;
            this._shipmentService = shipmentService;
            this._paymentService = paymentService;
            this._dateTimeHelper = dateTimeHelper;
            this._priceFormatter = priceFormatter;
            this._currencyService = currencyService;
            this._measureService = measureService;
            this._pictureService = pictureService;
            this._productService = productService;
            this._productAttributeParser = productAttributeParser;
            this._storeService = storeService;
            this._storeContext = storeContext;
            this._settingContext = settingContext;
            this._webHelper = webHelper;
            this._currencySettings = currencySettings;
            this._catalogSettings = catalogSettings;
            this._measureSettings = measureSettings;
            this._pdfSettings = pdfSettings;
            this._taxSettings = taxSettings;
            this._addressSettings = addressSettings;

            this.EPGServices = new IntegrationService();
        }

        #endregion

        #region Utilities

        protected virtual iTextSharp.text.Font GetFont()
        {
            //nopCommerce supports unicode characters
            //nopCommerce uses Free Serif font by default (~/App_Data/Pdf/FreeSerif.ttf file)
            //It was downloaded from http://savannah.gnu.org/projects/freefont
            //string fontPath = Path.Combine(_webHelper.MapPath("~/App_Data/Pdf/"), _pdfSettings.FontFileName);
            string fontPath = Environment.GetEnvironmentVariable("SystemRoot") + "\\fonts\\times.ttf";
            var baseFont = BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            var font = new Font(baseFont, 10, Font.NORMAL);
            return font;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Print an order to PDF
        /// </summary>
        /// <param name="order">Order</param>
        /// <param name="languageId">Language identifier; 0 to use a language used when placing an order</param>
        /// <returns>A path of generates file</returns>
        public virtual string PrintOrderToPdf(Order order, int languageId)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            string fileName = string.Format("order_{0}_{1}.pdf", order.OrderGuid, CommonHelper.GenerateRandomDigitCode(4));
            string filePath = Path.Combine(_webHelper.MapPath("~/content/files/ExportImport"), fileName);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                var orders = new List<Order>();
                orders.Add(order);
                PrintOrdersToPdf(fileStream, orders, languageId);
            }
            return filePath;
        }



        /// <summary>
        /// Print orders to PDF
        /// </summary>
        /// <param name="stream">Stream</param>
        /// <param name="orders">Orders</param>
        /// <param name="languageId">Language identifier; 0 to use a language used when placing an order</param>
        public virtual void PrintOrdersToPdf(Stream stream, IList<Order> orders, int languageId = 0)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            if (orders == null)
                throw new ArgumentNullException("orders");

            var pageSize = PageSize.A4;

            if (_pdfSettings.LetterPageSizeEnabled)
            {
                pageSize = PageSize.LETTER;
            }

            var countries = EPGServices.GetDestinations("International Outgoing").Cast<DestinationModel>().ToList();
            var states = EPGServices.GetDestinations("Local").Cast<OriginModel>().ToList();

            var doc = new Document(pageSize);
            var pdfWriter = PdfWriter.GetInstance(doc, stream);
            doc.Open();

            //fonts
            var titleFont = GetFont();
            titleFont.SetStyle(Font.BOLD);
            titleFont.Color = BaseColor.BLACK;
            var font = GetFont();
            var attributesFont = GetFont();
            attributesFont.SetStyle(Font.ITALIC);

            int ordCount = orders.Count;
            int ordNum = 0;

            foreach (var order in orders)
            {
                //by default _pdfSettings contains settings for the current active store
                //and we need PdfSettings for the store which was used to place an order
                //so let's load it based on a store of the current order
                var pdfSettingsByStore = _settingContext.LoadSetting<PdfSettings>(order.StoreId);


                var lang = _languageService.GetLanguageById(languageId == 0 ? order.CustomerLanguageId : languageId);
                if (lang == null || !lang.Published)
                    lang = _workContext.WorkingLanguage;
                bool useRTL = lang.Rtl;

                #region Header

                //logo
                var logoPicture = _pictureService.GetPictureById(pdfSettingsByStore.LogoPictureId);
                var logoExists = logoPicture != null;

                //header
                var headerTable = new PdfPTable(logoExists ? 2 : 1);
                
                headerTable.WidthPercentage = 100f;
                if (logoExists)
                    headerTable.SetWidths(new[] { 50, 50 });

                //logo
                if (logoExists)
                {
                    var logoFilePath = _pictureService.GetThumbLocalPath(logoPicture, 0, false);
                    var cellLogo = new PdfPCell(Image.GetInstance(logoFilePath));
                    if (useRTL)
                        cellLogo.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    cellLogo.Border = Rectangle.NO_BORDER;
                    headerTable.AddCell(cellLogo);
                }
                //store info
                var cell = new PdfPCell();
                if (useRTL)
                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                cell.Border = Rectangle.NO_BORDER;
                cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Order#", lang.Id), order.Id), titleFont, useRTL));
                var store = _storeService.GetStoreById(order.StoreId) ?? _storeContext.CurrentStore;
                var anchor = new Anchor(store.Url.Trim(new char[] { '/' }), font);
                anchor.Reference = store.Url;
                cell.AddElement(new Paragraph(anchor));
                cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.OrderDate", lang.Id), _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc).ToString("D", new CultureInfo(lang.LanguageCulture))), font, useRTL));
                cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.OrderStatus", lang.Id), order.OrderStatus.GetLocalizedEnum(_localizationService, lang.Id)), font, useRTL));
                headerTable.AddCell(cell);
              
                doc.Add(headerTable);

                #endregion

                #region Addresses

                var addressTable = new PdfPTable(2);
                if (useRTL)
                    addressTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                addressTable.WidthPercentage = 100f;
                addressTable.SetWidths(new[] { 50, 50 });

                //billing info
                cell = new PdfPCell();
                if (useRTL)
                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                cell.Border = Rectangle.NO_BORDER;
                cell.AddElement(GenerateParagraph(_localizationService.GetResource("PDFInvoice.BillingInformation", lang.Id), titleFont, useRTL));

                if (_addressSettings.CompanyEnabled && !String.IsNullOrEmpty(order.BillingAddress.Company))
                    cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Company", lang.Id), order.BillingAddress.Company), font, useRTL));

                cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Name", lang.Id), order.BillingAddress.FirstName + " " + order.BillingAddress.LastName), font, useRTL));
                if (_addressSettings.PhoneEnabled)
                    cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Phone", lang.Id), order.BillingAddress.PhoneNumber), font, useRTL));
                if (_addressSettings.FaxEnabled && !String.IsNullOrEmpty(order.BillingAddress.FaxNumber))
                    cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Fax", lang.Id), order.BillingAddress.FaxNumber), font, useRTL));
                if (_addressSettings.StreetAddressEnabled)
                    cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Address", lang.Id), order.BillingAddress.Address1), font, useRTL));
                if (_addressSettings.StreetAddress2Enabled && !String.IsNullOrEmpty(order.BillingAddress.Address2))
                    cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Address2", lang.Id), order.BillingAddress.Address2), font, useRTL));
                if (_addressSettings.CityEnabled || _addressSettings.StateProvinceEnabled || _addressSettings.ZipPostalCodeEnabled)
                {
                    string stateName = null;
                    if (order.BillingAddress.StateName != null)
                    {
                        foreach (var state in states)
                        {
                            if (state.emirate_id == order.BillingAddress.StateName)
                            {
                                if (useRTL)
                                    stateName = state.emirate_name_ar;
                                else
                                    stateName = state.emirate_name_en;
                            }
                        }
                    }
                    cell.AddElement(GenerateParagraph(String.Format("{0}{1} {2}", stateName == null ? order.BillingAddress.City + ", " : "", stateName != null ? stateName : "", order.BillingAddress.ZipPostalCode), font, useRTL));
                }
                if (_addressSettings.CountryEnabled && order.BillingAddress.CountryName != null)
                {
                    string countryName = null;
                    foreach (var country in countries)
                    {
                        if (country.country_id == order.BillingAddress.CountryName)
                        {
                            if (useRTL)
                                countryName = country.country_name_ar;
                            else
                                countryName = country.country_name_en;
                        }
                    }
                    cell.AddElement(GenerateParagraph(String.Format("{0}", countryName != null ? countryName : ""), font, useRTL));
                }

                //VAT number
                if (!String.IsNullOrEmpty(order.VatNumber))
                    cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.VATNumber", lang.Id), order.VatNumber), font, useRTL));

                //payment method
                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
                string paymentMethodStr = paymentMethod != null ? paymentMethod.GetLocalizedFriendlyName(_localizationService, lang.Id) : order.PaymentMethodSystemName;
                if (!String.IsNullOrEmpty(paymentMethodStr))
                {
                    cell.AddElement(GenerateParagraph());
                    cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.PaymentMethod", lang.Id), paymentMethodStr), font, useRTL));
                    cell.AddElement(GenerateParagraph());
                }

                //purchase order number (we have to find a better to inject this information because it's related to a certain plugin)
                if (paymentMethod != null && paymentMethod.PluginDescriptor.SystemName.Equals("Payments.PurchaseOrder", StringComparison.InvariantCultureIgnoreCase))
                {
                    cell.AddElement(GenerateParagraph());
                    cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.PurchaseOrderNumber", lang.Id), order.PurchaseOrderNumber), font, useRTL));
                    cell.AddElement(GenerateParagraph());
                }

                addressTable.AddCell(cell);

                bool SendToManyAddressSelected = order.SendToManyAddressSelected ?? true;

                //shipping info
                if (order.ShippingStatus != ShippingStatus.ShippingNotRequired && !SendToManyAddressSelected)
                {
                    cell = new PdfPCell();
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.AddElement(GenerateParagraph(_localizationService.GetResource("PDFInvoice.ShippingInformation", lang.Id), titleFont, useRTL));

                    Address shippingAddress = null;
                    foreach (var item in order.OrderItems)
                    {
                        if (!item.Product.IsCustomizable)
                            shippingAddress = item.OrderItemShippingAddresses.First().Address;
                    }

                    if (shippingAddress == null)
                        throw new NopException(string.Format("Shipping is required, but address is not available. Order ID = {0}", order.Id));

                    if (!String.IsNullOrEmpty(shippingAddress.Company))
                        cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Company", lang.Id), shippingAddress.Company), font, useRTL));
                    cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Name", lang.Id), shippingAddress.FirstName + " " + shippingAddress.LastName), font, useRTL));
                    if (_addressSettings.PhoneEnabled)
                        cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Phone", lang.Id), shippingAddress.PhoneNumber), font, useRTL));
                    if (_addressSettings.FaxEnabled && !String.IsNullOrEmpty(shippingAddress.FaxNumber))
                        cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Fax", lang.Id), shippingAddress.FaxNumber), font, useRTL));
                    if (_addressSettings.StreetAddressEnabled)
                        cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Address", lang.Id), shippingAddress.Address1), font, useRTL));
                    if (_addressSettings.StreetAddress2Enabled && !String.IsNullOrEmpty(shippingAddress.Address2))
                        cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Address2", lang.Id), shippingAddress.Address2), font, useRTL));
                    if (_addressSettings.CityEnabled || _addressSettings.StateProvinceEnabled || _addressSettings.ZipPostalCodeEnabled)
                    {
                        string stateName = null;
                        if (shippingAddress.StateName != null)
                        {
                            foreach (var state in states)
                            {
                                if (state.emirate_id == shippingAddress.StateName)
                                {
                                    if (useRTL)
                                        stateName = state.emirate_name_ar;
                                    else
                                        stateName = state.emirate_name_en;
                                }
                            }
                        }
                        cell.AddElement(GenerateParagraph(String.Format("{0}{1} {2}", stateName == null ? shippingAddress.City + ", " : "", stateName != null ? stateName : "", shippingAddress.ZipPostalCode), font, useRTL));
                    }
                    if (_addressSettings.CountryEnabled && shippingAddress.CountryName != null)
                    {
                        string countryName = null;
                        foreach (var country in countries)
                        {
                            if (country.country_id == shippingAddress.CountryName)
                            {
                                if (useRTL)
                                    countryName = country.country_name_ar;
                                else
                                    countryName = country.country_name_en;
                            }
                        }
                        cell.AddElement(GenerateParagraph(String.Format("{0}", countryName != null ? countryName : ""), font, useRTL));
                    }
                    cell.AddElement(GenerateParagraph());

                    //cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.ShippingMethod", lang.Id), order.ShippingMethod), font));
                    //cell.AddElement(new Paragraph());

                    addressTable.AddCell(cell);
                }
                else
                {
                    cell = new PdfPCell(new Phrase(" "));
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    cell.Border = Rectangle.NO_BORDER;
                    addressTable.AddCell(cell);
                }

                doc.Add(addressTable);
                doc.Add(GenerateParagraph());

                #endregion

                #region Products
                //products
                var titleTable = new PdfPTable(1);
                cell = new PdfPCell(GenerateParagraph(_localizationService.GetResource("PDFInvoice.Product(s)", lang.Id), titleFont, useRTL));
                cell.BorderColor = BaseColor.WHITE;
                if (useRTL)
                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                
                titleTable.AddCell(cell);
                doc.Add(titleTable);
                doc.Add(GenerateParagraph());


                var orderItems = _orderService.GetAllOrderItems(order.Id, null, null, null, null, null, null);

                var productsTable = new PdfPTable(1);
               
                if (SendToManyAddressSelected)
                {
                    productsTable = new PdfPTable(_catalogSettings.ShowProductSku ? 6 : 5);
                    productsTable.WidthPercentage = 100f;
                    productsTable.SetWidths(_catalogSettings.ShowProductSku ? new[] { 30, 5, 15, 5, 15, 30 } : new[] { 30, 15, 10, 15, 30 });
                }
                else
                {
                    productsTable = new PdfPTable(_catalogSettings.ShowProductSku ? 6 : 5);
                    productsTable.WidthPercentage = 100f;
                    productsTable.SetWidths(_catalogSettings.ShowProductSku ? new[] { 30, 5, 15, 5, 15, 30 } : new[] { 30, 15, 10, 15, 30 });
                }
                 if (useRTL)
                    productsTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

                //product name
                cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.ProductName", lang.Id), font));
                cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                if (useRTL)
                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                productsTable.AddCell(cell);

                //SKU
                if (_catalogSettings.ShowProductSku)
                {
                    cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.SKU", lang.Id), font));
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    productsTable.AddCell(cell);
                }

                //price
                cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.ProductPrice", lang.Id), font));
                cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                if (useRTL)
                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                productsTable.AddCell(cell);

                //qty
                cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.ProductQuantity", lang.Id), font));
                cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                if (useRTL)
                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                productsTable.AddCell(cell);

                //total
                cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.ProductTotal", lang.Id), font));
                cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                if (useRTL)
                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                productsTable.AddCell(cell);

                //multi shipping address
                //if (SendToManyAddressSelected)                    
                cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.ShippingDetails", lang.Id), font));
                cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                if (useRTL)
                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                productsTable.AddCell(cell);

                for (int i = 0; i < orderItems.Count; i++)
                {
                    var orderItem = orderItems[i];
                    var p = orderItem.Product;

                    //product name
                    string name = p.GetLocalized(x => x.Name, lang.Id);
                    cell = new PdfPCell();
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        
                    cell.AddElement(GenerateParagraph(name, font, useRTL));
                    
                    if (!orderItem.Product.IsCustomizable)
                    {
                        var attributesParagraph = GenerateParagraph(HtmlHelper.ConvertHtmlToPlainText(orderItem.AttributeDescription, true, true), attributesFont, useRTL);
                        cell.AddElement(attributesParagraph);
                    }
                    productsTable.AddCell(cell);

                    //SKU
                    if (_catalogSettings.ShowProductSku)
                    {
                        var sku = p.FormatSku(orderItem.AttributesXml, _productAttributeParser);
                        cell = new PdfPCell(new Phrase(sku ?? String.Empty, font));
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        productsTable.AddCell(cell);
                    }

                    //domestic unit price
                    string unitPrice = string.Empty;
                    if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
                    {
                        //including tax
                        var unitPriceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceInclTax, order.CurrencyRate);
                        unitPrice = _priceFormatter.FormatPrice(unitPriceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, true);
                    }
                    else
                    {
                        //excluding tax
                        var unitPriceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceExclTax, order.CurrencyRate);
                        unitPrice = _priceFormatter.FormatPrice(unitPriceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, false);
                    }
                    //international unit price
                    string internationalPrice = string.Empty;
                    if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
                    {
                        //including tax
                        var internationalPriceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.InternationalPriceInclTax, order.CurrencyRate);
                        internationalPrice = _priceFormatter.FormatPrice(internationalPriceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, true);
                    }
                    else
                    {
                        //excluding tax
                        var internationalPriceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.InternationalPriceExclTax, order.CurrencyRate);
                        internationalPrice = _priceFormatter.FormatPrice(internationalPriceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, false);
                    }
                    //unit price and displayed for non customizable products
                    if (!orderItem.Product.IsCustomizable)
                    {
                        cell = new PdfPCell(new Phrase(unitPrice, font));
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        productsTable.AddCell(cell);
                    }
                    else
                    {
                        cell = new PdfPCell(new Phrase(" "));
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        productsTable.AddCell(cell);
                    }

                    //qty
                    cell = new PdfPCell(new Phrase(orderItem.Quantity.ToString(), font));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    productsTable.AddCell(cell);

                    //total
                    string subTotal = string.Empty;
                    if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
                    {
                        //including tax
                        var priceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceInclTax, order.CurrencyRate);
                        subTotal = _priceFormatter.FormatPrice(priceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, true);
                    }
                    else
                    {
                        //excluding tax
                        var priceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceExclTax, order.CurrencyRate);
                        subTotal = _priceFormatter.FormatPrice(priceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, false);
                    }
                    cell = new PdfPCell(new Phrase(subTotal, font));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;                    
                    productsTable.AddCell(cell);

                    //multi shipping address
                    var addressesTable = new PdfPTable(1);
                    if (useRTL)
                        addressTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    if (order.ShippingStatus != ShippingStatus.ShippingNotRequired)
                    {
                        //send to many address order or customized product
                        if (SendToManyAddressSelected || orderItem.Product.IsCustomizable)
                        {
                            for (int j = 0; j < orderItem.OrderItemShippingAddresses.Count; j++)
                            {
                                var orderItemAddress = orderItem.OrderItemShippingAddresses.ElementAt(j);
                                for (int k = 0; k < orderItemAddress.Quantity; k++)
                                {
                                    if (orderItem.OrderItemShippingAddresses.Count() <= 0)
                                        throw new NopException(string.Format("Shipping is required, but address is not available. Order ID = {0}", order.Id));
                                    cell = new PdfPCell();
                                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    if (useRTL)
                                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                                    cell.Border = Rectangle.NO_BORDER;

                                    if (!String.IsNullOrEmpty(orderItemAddress.Address.Company))
                                        cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Company", lang.Id), orderItemAddress.Address.Company), font, useRTL));
                                    cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Name", lang.Id), orderItemAddress.Address.FirstName + " " + orderItemAddress.Address.LastName), font, useRTL));
                                    if (_addressSettings.PhoneEnabled)
                                        cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Phone", lang.Id), orderItemAddress.Address.PhoneNumber), font, useRTL));
                                    if (_addressSettings.FaxEnabled && !String.IsNullOrEmpty(orderItemAddress.Address.FaxNumber))
                                        cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Fax", lang.Id), orderItemAddress.Address.FaxNumber), font, useRTL));
                                    if (_addressSettings.StreetAddressEnabled)
                                        cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Address", lang.Id), orderItemAddress.Address.Address1), font, useRTL));
                                    if (_addressSettings.StreetAddress2Enabled && !String.IsNullOrEmpty(orderItemAddress.Address.Address2))
                                        cell.AddElement(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.Address2", lang.Id), orderItemAddress.Address.Address2), font, useRTL));
                                    if (_addressSettings.CityEnabled || _addressSettings.StateProvinceEnabled || _addressSettings.ZipPostalCodeEnabled)
                                    {
                                        string stateName = null;
                                        if (orderItemAddress.Address.StateName != null)
                                        {
                                            foreach (var state in states)
                                            {
                                                if (state.emirate_id == orderItemAddress.Address.StateName)
                                                {
                                                    if (useRTL)
                                                        stateName = state.emirate_name_ar;
                                                    else
                                                        stateName = state.emirate_name_en;
                                                }
                                            }
                                        }
                                        cell.AddElement(GenerateParagraph(String.Format("{0}{1} {2}", stateName == null ? orderItemAddress.Address.City + ", " : "", stateName != null ? stateName : "", orderItemAddress.Address.ZipPostalCode), font, useRTL));
                                    }
                                    if (_addressSettings.CountryEnabled && orderItemAddress.Address.CountryName != null)
                                    {
                                        string countryName = null;
                                        foreach (var country in countries)
                                        {
                                            if (country.country_id == orderItemAddress.Address.CountryName)
                                            {
                                                if (useRTL)
                                                    countryName = country.country_name_ar;
                                                else
                                                    countryName = country.country_name_en;
                                            }
                                        }
                                        cell.AddElement(GenerateParagraph(String.Format("{0}", countryName != null ? countryName : ""), font, useRTL));
                                    }                                    

                                    addressesTable.AddCell(cell);

                                    cell = new PdfPCell(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.ShippingMethod", lang.Id), orderItemAddress.ShippingMethod), font, useRTL));
                                    cell.Border = Rectangle.NO_BORDER;
                                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    if (useRTL)
                                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                                    
                                    addressesTable.AddCell(cell);

                                    //customized product price depending on the address
                                    if (orderItem.Product.IsCustomizable)
                                    {
                                        var address = orderItemAddress.Address;
                                        if (address.IsDomestic ?? _addressService.CheckDomesticCountry(address.CountryName))
                                        {
                                            cell = new PdfPCell(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.CustomizedProductPrice", lang.Id), unitPrice), font, useRTL));
                                        }
                                        else
                                        {
                                            cell = new PdfPCell(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.CustomizedProductPrice", lang.Id), internationalPrice), font, useRTL));
                                        }
                                        cell.Border = Rectangle.NO_BORDER;
                                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                        if (useRTL)
                                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;                                        
                                        addressesTable.AddCell(cell);

                                        //empty line
                                        if (orderItem.OrderItemShippingAddresses.Count > 1 && j != orderItem.OrderItemShippingAddresses.Count - 1)
                                            cell.AddElement(GenerateParagraph());
                                    }
                                    else
                                    {
                                        cell = new PdfPCell(new Phrase(" "));
                                        if (useRTL)
                                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                                        cell.Border = Rectangle.NO_BORDER;
                                        addressesTable.AddCell(cell);
                                    }
                                }
                            }
                        }
                        //send to single address order
                        else
                        {
                            var shippingMethod = orderItem.OrderItemShippingAddresses.FirstOrDefault().ShippingMethod;
                            cell = new PdfPCell(GenerateParagraph(String.Format(_localizationService.GetResource("PDFInvoice.ShippingMethod", lang.Id), orderItem.OrderItemShippingAddresses.FirstOrDefault().ShippingMethod), font, useRTL));
                            if (useRTL)
                                cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                            cell.Border = Rectangle.NO_BORDER;
                            addressesTable.AddCell(cell);
                        }                        
                    }
                    else
                    {
                        cell = new PdfPCell(new Phrase(" "));
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        cell.Border = Rectangle.NO_BORDER;
                        addressesTable.AddCell(cell);
                    }

                    addressesTable.HorizontalAlignment = Element.ALIGN_LEFT;
                    productsTable.AddCell(addressesTable);
                }
                doc.Add(productsTable);

                #endregion

                #region Checkout attributes

                if (!String.IsNullOrEmpty(order.CheckoutAttributeDescription))
                {
                    doc.Add(GenerateParagraph(" "));
                    string attributes = HtmlHelper.ConvertHtmlToPlainText(order.CheckoutAttributeDescription, true, true);
                    var pCheckoutAttributes = GenerateParagraph(attributes, font, useRTL);
                    pCheckoutAttributes.Alignment = Element.ALIGN_RIGHT;
                    var checkoutTable = new PdfPTable(1);
                    cell = new PdfPCell(pCheckoutAttributes);
                    cell.BorderColor = BaseColor.WHITE;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    checkoutTable.AddCell(cell);
                    doc.Add(GenerateParagraph(" "));
                }

                #endregion

                #region Totals

                //subtotal
                doc.Add(GenerateParagraph());
                var totalTable = new PdfPTable(1);
                //order subtotal
                if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal)
                {
                    //including tax

                    var orderSubtotalInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalInclTax, order.CurrencyRate);
                    string orderSubtotalInclTaxStr = _priceFormatter.FormatPrice(orderSubtotalInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, true);

                    var p = GenerateParagraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.Sub-Total", lang.Id), orderSubtotalInclTaxStr), font, useRTL);
                    p.Alignment = Element.ALIGN_RIGHT;
                    cell = new PdfPCell(p);                  
                }
                else
                {
                    //excluding tax

                    var orderSubtotalExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalExclTax, order.CurrencyRate);
                    string orderSubtotalExclTaxStr = _priceFormatter.FormatPrice(orderSubtotalExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, false);

                    var p = GenerateParagraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.Sub-Total", lang.Id), orderSubtotalExclTaxStr), font, useRTL);
                    p.Alignment = Element.ALIGN_RIGHT;
                    cell = new PdfPCell(p); 
                }
                cell.BorderColor = BaseColor.WHITE;
                if (useRTL)
                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                totalTable.AddCell(cell); 

                //discount (applied to order subtotal)
                if (order.OrderSubTotalDiscountExclTax > decimal.Zero)
                {
                    //order subtotal
                    if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal)
                    {
                        //including tax

                        var orderSubTotalDiscountInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountInclTax, order.CurrencyRate);
                        string orderSubTotalDiscountInCustomerCurrencyStr = _priceFormatter.FormatPrice(-orderSubTotalDiscountInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, true);

                        var p = GenerateParagraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.Discount", lang.Id), orderSubTotalDiscountInCustomerCurrencyStr), font, useRTL);
                        p.Alignment = Element.ALIGN_RIGHT;
                        cell = new PdfPCell(p);                        
                    }
                    else
                    {
                        //excluding tax

                        var orderSubTotalDiscountExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountExclTax, order.CurrencyRate);
                        string orderSubTotalDiscountInCustomerCurrencyStr = _priceFormatter.FormatPrice(-orderSubTotalDiscountExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, false);

                        var p = GenerateParagraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.Discount", lang.Id), orderSubTotalDiscountInCustomerCurrencyStr), font, useRTL);
                        p.Alignment = Element.ALIGN_RIGHT;
                        cell = new PdfPCell(p);
                    }
                    cell.BorderColor = BaseColor.WHITE;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    totalTable.AddCell(cell);
                }

                //shipping
                if (order.ShippingStatus != ShippingStatus.ShippingNotRequired)
                {
                    if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
                    {
                        //including tax
                        var orderShippingInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingInclTax, order.CurrencyRate);
                        string orderShippingInclTaxStr = _priceFormatter.FormatShippingPrice(orderShippingInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, true);

                        var p = GenerateParagraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.Shipping", lang.Id), orderShippingInclTaxStr), font, useRTL);
                        p.Alignment = Element.ALIGN_RIGHT;
                        cell = new PdfPCell(p);
                    }
                    else
                    {
                        //excluding tax
                        var orderShippingExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingExclTax, order.CurrencyRate);
                        string orderShippingExclTaxStr = _priceFormatter.FormatShippingPrice(orderShippingExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, false);

                        var p = GenerateParagraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.Shipping", lang.Id), orderShippingExclTaxStr), font, useRTL);
                        p.Alignment = Element.ALIGN_RIGHT;
                        cell = new PdfPCell(p);
                    }
                    cell.BorderColor = BaseColor.WHITE;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    totalTable.AddCell(cell);
                }

                //payment fee
                if (order.PaymentMethodAdditionalFeeExclTax > decimal.Zero)
                {
                    if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
                    {
                        //including tax
                        var paymentMethodAdditionalFeeInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeInclTax, order.CurrencyRate);
                        string paymentMethodAdditionalFeeInclTaxStr = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, true);

                        var p = GenerateParagraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.PaymentMethodAdditionalFee", lang.Id), paymentMethodAdditionalFeeInclTaxStr), font, useRTL);
                        p.Alignment = Element.ALIGN_RIGHT;
                        cell = new PdfPCell(p);
                    }
                    else
                    {
                        //excluding tax
                        var paymentMethodAdditionalFeeExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeExclTax, order.CurrencyRate);
                        string paymentMethodAdditionalFeeExclTaxStr = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, false);

                        var p = GenerateParagraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.PaymentMethodAdditionalFee", lang.Id), paymentMethodAdditionalFeeExclTaxStr), font, useRTL);
                        p.Alignment = Element.ALIGN_RIGHT;
                        cell = new PdfPCell(p);
                    }
                    cell.BorderColor = BaseColor.WHITE;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    totalTable.AddCell(cell);
                }

                //tax
                string taxStr = string.Empty;
                var taxRates = new SortedDictionary<decimal, decimal>();
                bool displayTax = true;
                bool displayTaxRates = true;
                if (_taxSettings.HideTaxInOrderSummary && order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
                {
                    displayTax = false;
                }
                else
                {
                    if (order.OrderTax == 0 && _taxSettings.HideZeroTax)
                    {
                        displayTax = false;
                        displayTaxRates = false;
                    }
                    else
                    {
                        taxRates = order.TaxRatesDictionary;

                        displayTaxRates = _taxSettings.DisplayTaxRates && taxRates.Count > 0;
                        displayTax = !displayTaxRates;

                        var orderTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTax, order.CurrencyRate);
                        taxStr = _priceFormatter.FormatPrice(orderTaxInCustomerCurrency, true, order.CustomerCurrencyCode, false, lang);
                    }
                }
                if (displayTax)
                {
                    var p = GenerateParagraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.Tax", lang.Id), taxStr), font, useRTL);
                    p.Alignment = Element.ALIGN_RIGHT;
                    cell = new PdfPCell(p);
                    cell.BorderColor = BaseColor.WHITE;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    totalTable.AddCell(cell);
                }
                if (displayTaxRates)
                {
                    foreach (var item in taxRates)
                    {
                        string taxRate = String.Format(_localizationService.GetResource("PDFInvoice.TaxRate", lang.Id), _priceFormatter.FormatTaxRate(item.Key));
                        string taxValue = _priceFormatter.FormatPrice(_currencyService.ConvertCurrency(item.Value, order.CurrencyRate), true, order.CustomerCurrencyCode, false, lang);

                        var p = GenerateParagraph(String.Format("{0} {1}", taxRate, taxValue), font, useRTL);
                        p.Alignment = Element.ALIGN_RIGHT;
                        cell = new PdfPCell(p);
                        cell.BorderColor = BaseColor.WHITE;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        totalTable.AddCell(cell);
                    }
                }

                //discount (applied to order total)
                if (order.OrderDiscount > decimal.Zero)
                {
                    var orderDiscountInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderDiscount, order.CurrencyRate);
                    string orderDiscountInCustomerCurrencyStr = _priceFormatter.FormatPrice(-orderDiscountInCustomerCurrency, true, order.CustomerCurrencyCode, false, lang);

                    var p = GenerateParagraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.Discount", lang.Id), orderDiscountInCustomerCurrencyStr), font, useRTL);
                    p.Alignment = Element.ALIGN_RIGHT;
                    cell = new PdfPCell(p);
                    cell.BorderColor = BaseColor.WHITE;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    totalTable.AddCell(cell);
                }

                //gift cards
                foreach (var gcuh in order.GiftCardUsageHistory)
                {
                    string gcTitle = string.Format(_localizationService.GetResource("PDFInvoice.GiftCardInfo", lang.Id), gcuh.GiftCard.GiftCardCouponCode);
                    string gcAmountStr = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(gcuh.UsedValue, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, lang);

                    var p = GenerateParagraph(String.Format("{0} {1}", gcTitle, gcAmountStr), font, useRTL);
                    p.Alignment = Element.ALIGN_RIGHT;
                    cell = new PdfPCell(p);
                    cell.BorderColor = BaseColor.WHITE;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    totalTable.AddCell(cell);
                }

                //reward points
                if (order.RedeemedRewardPointsEntry != null)
                {
                    string rpTitle = string.Format(_localizationService.GetResource("PDFInvoice.RewardPoints", lang.Id), -order.RedeemedRewardPointsEntry.Points);
                    string rpAmount = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(order.RedeemedRewardPointsEntry.UsedAmount, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, lang);

                    var p = GenerateParagraph(String.Format("{0} {1}", rpTitle, rpAmount), font, useRTL);
                    p.Alignment = Element.ALIGN_RIGHT;
                    cell = new PdfPCell(p);
                    cell.BorderColor = BaseColor.WHITE;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    totalTable.AddCell(cell);
                }

                //order total
                var orderTotalInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTotal, order.CurrencyRate);
                string orderTotalStr = _priceFormatter.FormatPrice(orderTotalInCustomerCurrency, true, order.CustomerCurrencyCode, false, lang);


                var pTotal = GenerateParagraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.OrderTotal", lang.Id), orderTotalStr), titleFont, useRTL);
                pTotal.Alignment = Element.ALIGN_RIGHT;
                cell = new PdfPCell(pTotal);
                cell.BorderColor = BaseColor.WHITE;
                if (useRTL)
                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                totalTable.AddCell(cell);

                doc.Add(totalTable);
                doc.Add(GenerateParagraph());

                #endregion

                #region shipment
                //shipment
                titleTable = new PdfPTable(1);
                cell = new PdfPCell(GenerateParagraph(_localizationService.GetResource("PDFInvoice.Shipment(s)", lang.Id), titleFont, useRTL));
                cell.BorderColor = BaseColor.WHITE;
                if (useRTL)
                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

                titleTable.AddCell(cell);
                doc.Add(titleTable);
                doc.Add(GenerateParagraph());


                var shipments = _shipmentService.GetShipmentsByOrderId(order.Id);

                var shipmentsTable = new PdfPTable(4);
                shipmentsTable.WidthPercentage = 100f;
                //Id coloumn should be small in both cultures
                if (useRTL)
                    shipmentsTable.SetWidths(new[] { 30, 30, 30, 10 });
                else
                    shipmentsTable.SetWidths(new[] { 10, 30, 30, 30 });

                if (useRTL)
                    shipmentsTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

                //Shipment identifier
                cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.Shipment.Id", lang.Id), font));
                cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                if (useRTL)
                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                shipmentsTable.AddCell(cell);

                //Shipment tracking number
                cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.Shipment.TrackingNumber", lang.Id), font));
                cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                if (useRTL)
                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                shipmentsTable.AddCell(cell);

                //Shipping Date
                cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.Shipment.ShippedDate", lang.Id), font));
                cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                if (useRTL)
                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                shipmentsTable.AddCell(cell);

                //Delivery Date
                cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.Shipment.DeliveryDate", lang.Id), font));
                cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                if (useRTL)
                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                shipmentsTable.AddCell(cell);

                for (int i = 0; i < shipments.Count; i++)
                {
                    var shipment = shipments[i];

                    //Shipment identifier
                    cell = new PdfPCell();
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

                    cell.AddElement(GenerateParagraph(shipment.Id.ToString(), font, useRTL));
                    shipmentsTable.AddCell(cell);

                    //Shipment tracking number
                    cell = new PdfPCell();
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;

                    cell.AddElement(GenerateParagraph(shipment.TrackingNumber, font, useRTL));
                    shipmentsTable.AddCell(cell);

                    //Shipping Date
                    cell = new PdfPCell();
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    if (shipment.ShippedDateUtc.HasValue)
                        cell.AddElement(GenerateParagraph(_dateTimeHelper.ConvertToUserTime(shipment.ShippedDateUtc.Value, DateTimeKind.Utc).ToString("D", new CultureInfo(lang.LanguageCulture)), font, useRTL));
                    else
                        cell.AddElement(GenerateParagraph(_localizationService.GetResource("PDFInvoice.Shipment.ShippedDate.NotYet"), font, useRTL));
                    shipmentsTable.AddCell(cell);

                    //Delivery Date
                    cell = new PdfPCell();
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    if (shipment.DeliveryDateUtc.HasValue)
                        cell.AddElement(GenerateParagraph(_dateTimeHelper.ConvertToUserTime(shipment.DeliveryDateUtc.Value, DateTimeKind.Utc).ToString("D", new CultureInfo(lang.LanguageCulture)), font, useRTL));
                    else
                        cell.AddElement(GenerateParagraph(_localizationService.GetResource("PDFInvoice.Shipment.DeliveryDate.NotYet"), font, useRTL));
                    shipmentsTable.AddCell(cell);                    

                }
                doc.Add(shipmentsTable);
                doc.Add(GenerateParagraph());

                #endregion

                #region Order notes

                if (pdfSettingsByStore.RenderOrderNotes)
                {
                    var orderNotes = order.OrderNotes
                        .Where(on => on.DisplayToCustomer)
                        .OrderByDescending(on => on.CreatedOnUtc)
                        .ToList();
                    if (orderNotes.Count > 0)
                    {
                        titleTable = new PdfPTable(1);
                        cell = new PdfPCell(GenerateParagraph(_localizationService.GetResource("PDFInvoice.OrderNotes", lang.Id), titleFont, useRTL));
                        cell.BorderColor = BaseColor.WHITE;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        titleTable.AddCell(cell);
                        doc.Add(titleTable);

                        doc.Add(GenerateParagraph());

                        var notesTable = new PdfPTable(2);
                        if (useRTL)
                            notesTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        notesTable.WidthPercentage = 100f;
                        notesTable.SetWidths(new[] { 30, 70 });

                        //created on
                        cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.OrderNotes.CreatedOn", lang.Id), font));
                        cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        notesTable.AddCell(cell);

                        //note
                        cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.OrderNotes.Note", lang.Id), font));
                        cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        notesTable.AddCell(cell);

                        foreach (var orderNote in orderNotes)
                        {
                            cell = new PdfPCell();
                            cell.AddElement(GenerateParagraph(_dateTimeHelper.ConvertToUserTime(orderNote.CreatedOnUtc, DateTimeKind.Utc).ToString("D", new CultureInfo(lang.LanguageCulture)), font, useRTL));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            if (useRTL)
                                cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                            notesTable.AddCell(cell);

                            cell = new PdfPCell();
                            cell.AddElement(GenerateParagraph(HtmlHelper.ConvertHtmlToPlainText(orderNote.FormatOrderNoteText(), true, true), font, useRTL));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            if (useRTL)
                                cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                            notesTable.AddCell(cell);

                            //should we display a link to downloadable files here?
                            //I think, no. Onyway, PDFs are printable documents and links (files) are useful here
                        }
                        doc.Add(notesTable);
                    }
                }

                #endregion

                #region Footer

                if (!String.IsNullOrEmpty(pdfSettingsByStore.InvoiceFooterTextColumn1) || !String.IsNullOrEmpty(pdfSettingsByStore.InvoiceFooterTextColumn2))
                {
                    var column1Lines = String.IsNullOrEmpty(pdfSettingsByStore.InvoiceFooterTextColumn1) ?
                        new List<string>() :
                        pdfSettingsByStore.InvoiceFooterTextColumn1
                        .Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                        .ToList();
                    var column2Lines = String.IsNullOrEmpty(pdfSettingsByStore.InvoiceFooterTextColumn2) ?
                        new List<string>() :
                        pdfSettingsByStore.InvoiceFooterTextColumn2
                        .Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                        .ToList();
                    if (column1Lines.Count > 0 || column2Lines.Count > 0)
                    {
                        var totalLines = Math.Max(column1Lines.Count, column2Lines.Count);
                        const float margin = 43;

                        //if you have really a lot of lines in the footer, then replace 9 with 10 or 11
                        int footerHeight = totalLines * 9;
                        var directContent = pdfWriter.DirectContent;
                        directContent.MoveTo(pageSize.GetLeft(margin), pageSize.GetBottom(margin) + footerHeight);
                        directContent.LineTo(pageSize.GetRight(margin), pageSize.GetBottom(margin) + footerHeight);
                        directContent.Stroke();


                        var footerTable = new PdfPTable(2);
                        if (useRTL)
                            footerTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        footerTable.WidthPercentage = 100f;
                        footerTable.SetTotalWidth(new float[] { 250, 250 });

                        //column 1
                        if (column1Lines.Count > 0)
                        {
                            var column1 = new PdfPCell();
                            column1.Border = Rectangle.NO_BORDER;
                            column1.HorizontalAlignment = Element.ALIGN_LEFT;
                            if (useRTL)
                                column1.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                            foreach (var footerLine in column1Lines)
                            {
                                column1.AddElement(new Phrase(footerLine, font));
                            }
                            footerTable.AddCell(column1);
                        }
                        else
                        {
                            var column = new PdfPCell(new Phrase(" "));
                            column.Border = Rectangle.NO_BORDER;
                            if (useRTL)
                                column.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                            footerTable.AddCell(column);
                        }

                        //column 2
                        if (column2Lines.Count > 0)
                        {
                            var column2 = new PdfPCell();
                            column2.Border = Rectangle.NO_BORDER;
                            column2.HorizontalAlignment = Element.ALIGN_LEFT;
                            if (useRTL)
                                column2.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                            foreach (var footerLine in column2Lines)
                            {
                                column2.AddElement(new Phrase(footerLine, font));
                            }
                            footerTable.AddCell(column2);
                        }
                        else
                        {
                            var column = new PdfPCell(new Phrase(" "));
                            column.Border = Rectangle.NO_BORDER;
                            if (useRTL)
                                column.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                            footerTable.AddCell(column);
                        }

                        footerTable.WriteSelectedRows(0, totalLines, pageSize.GetLeft(margin), pageSize.GetBottom(margin) + footerHeight, directContent);
                    }
                }

                #endregion

                ordNum++;
                if (ordNum < ordCount)
                {
                    doc.NewPage();
                }
            }
            doc.Close();
        }

        private Paragraph GenerateParagraph(string text = " ", iTextSharp.text.Font font = null, bool useRTL = false)
        {
            try
            {
                if (text == " " && font == null && useRTL == false)
                    return new Paragraph(" ");
                Paragraph paragraphObject = new Paragraph(text, font);
                if (useRTL)
                {
                    paragraphObject.Alignment = Element.ALIGN_LEFT;
                }
                return paragraphObject;
            }
            catch (Exception ex)
            {
                return new Paragraph(" ");
            }
        }

        /// <summary>
        /// Print packaging slips to PDF
        /// </summary>
        /// <param name="stream">Stream</param>
        /// <param name="shipments">Shipments</param>
        /// <param name="languageId">Language identifier; 0 to use a language used when placing an order</param>
        public virtual void PrintPackagingSlipsToPdf(Stream stream, IList<Shipment> shipments, int languageId = 0)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            if (shipments == null)
                throw new ArgumentNullException("shipments");

            var lang = _languageService.GetLanguageById(languageId);
            if (lang == null)
                throw new ArgumentException(string.Format("Cannot load language. ID={0}", languageId));

            var pageSize = PageSize.A4;

            if (_pdfSettings.LetterPageSizeEnabled)
            {
                pageSize = PageSize.LETTER;
            }

            var doc = new Document(pageSize);
            PdfWriter.GetInstance(doc, stream);
            doc.Open();

            //fonts
            var titleFont = GetFont();
            titleFont.SetStyle(Font.BOLD);
            titleFont.Color = BaseColor.BLACK;
            var font = GetFont();
            var attributesFont = GetFont();
            attributesFont.SetStyle(Font.ITALIC);

            var countries = EPGServices.GetDestinations("International Outgoing").Cast<DestinationModel>().ToList();
            var states = EPGServices.GetDestinations("Local").Cast<OriginModel>().ToList();

            int shipmentCount = shipments.Count;
            int shipmentNum = 0;

            foreach (var shipment in shipments)
            {
                var order = shipment.Order;

                if (languageId == 0)
                {
                    lang = _languageService.GetLanguageById(order.CustomerLanguageId);
                    if (lang == null || !lang.Published)
                        lang = _workContext.WorkingLanguage;
                }
                bool useRTL = lang.Rtl;

                if (shipment.Address != null)
                {
                    var headerTable = new PdfPTable(1);
                    var cell = new PdfPCell(new Paragraph(String.Format(_localizationService.GetResource("PDFPackagingSlip.Shipment", lang.Id), shipment.Id), titleFont));
                    cell.BorderColor = BaseColor.WHITE;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    headerTable.AddCell(cell);
                    cell = new PdfPCell(new Paragraph(String.Format(_localizationService.GetResource("PDFPackagingSlip.Order", lang.Id), order.Id), titleFont));
                    cell.BorderColor = BaseColor.WHITE;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    headerTable.AddCell(cell);

                    if (_addressSettings.CompanyEnabled && !String.IsNullOrEmpty(shipment.Address.Company))
                    {
                        cell = new PdfPCell(new Paragraph(String.Format(_localizationService.GetResource("PDFPackagingSlip.Company", lang.Id), shipment.Address.Company), font));
                        cell.BorderColor = BaseColor.WHITE;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        headerTable.AddCell(cell);
                    }
                    cell = new PdfPCell(new Paragraph(String.Format(_localizationService.GetResource("PDFPackagingSlip.Name", lang.Id), shipment.Address.FirstName + " " + shipment.Address.LastName), font));
                    cell.BorderColor = BaseColor.WHITE;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    headerTable.AddCell(cell);

                    if (_addressSettings.PhoneEnabled)
                    {
                        cell = new PdfPCell(new Paragraph(String.Format(_localizationService.GetResource("PDFPackagingSlip.Phone", lang.Id), shipment.Address.PhoneNumber), font));
                        cell.BorderColor = BaseColor.WHITE;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        headerTable.AddCell(cell);
                    }

                    if (_addressSettings.StreetAddressEnabled)
                    {
                        cell = new PdfPCell(new Paragraph(String.Format(_localizationService.GetResource("PDFPackagingSlip.Address", lang.Id), shipment.Address.Address1), font));
                        cell.BorderColor = BaseColor.WHITE;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        headerTable.AddCell(cell);
                    }

                    if (_addressSettings.StreetAddress2Enabled && !String.IsNullOrEmpty(shipment.Address.Address2))
                    {
                        cell = new PdfPCell(new Paragraph(String.Format(_localizationService.GetResource("PDFPackagingSlip.Address2", lang.Id), shipment.Address.Address2), font));
                        cell.BorderColor = BaseColor.WHITE;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        headerTable.AddCell(cell);
                    }

                    if (_addressSettings.CityEnabled || _addressSettings.StateProvinceEnabled || _addressSettings.ZipPostalCodeEnabled)
                    {
                        //get the name of the state by the state Id
                        string stateName = null;
                        if (shipment.Address.StateName != null)
                        {
                            foreach (var state in states)
                            {
                                if (state.emirate_id == shipment.Address.StateName)
                                {
                                    if (useRTL)
                                        stateName = state.emirate_name_ar;
                                    else
                                        stateName = state.emirate_name_en;
                                }
                            }
                        }
                        cell = new PdfPCell(new Paragraph(String.Format("{0}{1} {2}", stateName == null ? shipment.Address.City + ", " : "", stateName != null ? stateName : "", shipment.Address.ZipPostalCode), font));
                        cell.BorderColor = BaseColor.WHITE;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        headerTable.AddCell(cell);
                    }

                    if (_addressSettings.CountryEnabled && shipment.Address.CountryName != null)
                    {
                        //get the name of the country by the country Id
                        string countryName = null;
                        foreach (var country in countries)
                        {
                            if (country.country_id == shipment.Address.CountryName)
                            {
                                if(useRTL)
                                    countryName = country.country_name_ar;
                                else
                                    countryName = country.country_name_en;
                            }
                        }

                        cell = new PdfPCell(new Paragraph(String.Format("{0}", countryName != null ? countryName : ""), font));
                        cell.BorderColor = BaseColor.WHITE;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        headerTable.AddCell(cell);
                    }

                    cell = new PdfPCell(new Paragraph(" "));
                    cell.Border = Rectangle.NO_BORDER;
                    headerTable.AddCell(cell);

                    cell = new PdfPCell(new Paragraph(String.Format(_localizationService.GetResource("PDFPackagingSlip.ShippingMethod", lang.Id), shipment.ShippingMethod), font));
                    cell.BorderColor = BaseColor.WHITE;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    headerTable.AddCell(cell);

                    cell = new PdfPCell(new Paragraph(String.Format(_localizationService.GetResource("PDFPackagingSlip.ShippingStatus", lang.Id), shipment.EPGShippingStatus.GetLocalizedEnum(_localizationService, lang.Id)), font));
                    cell.BorderColor = BaseColor.WHITE;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    headerTable.AddCell(cell);

                    cell = new PdfPCell(new Paragraph(" "));
                    cell.Border = Rectangle.NO_BORDER;
                    headerTable.AddCell(cell);

                    doc.Add(headerTable);

                    var productsTable = new PdfPTable(3);
                    if (useRTL)
                        productsTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL; 
                    productsTable.WidthPercentage = 100f;
                    productsTable.SetWidths(new[] { 60, 20, 20 });

                    //product name
                    cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFPackagingSlip.ProductName", lang.Id), font));
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL; 
                    productsTable.AddCell(cell);

                    //SKU
                    cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFPackagingSlip.SKU", lang.Id), font));
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL; 
                    productsTable.AddCell(cell);

                    //qty
                    cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFPackagingSlip.QTY", lang.Id), font));
                    cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL; 
                    productsTable.AddCell(cell);

                    foreach (var si in shipment.ShipmentItems)
                    {
                        //product name
                        var orderItem = _orderService.GetOrderItemById(si.OrderItemId);
                        if (orderItem == null)
                            continue;

                        var p = orderItem.Product;
                        string name = p.GetLocalized(x => x.Name, lang.Id);
                        cell = new PdfPCell();
                        cell.AddElement(new Paragraph(name, font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL; 
                        var attributesParagraph = new Paragraph(HtmlHelper.ConvertHtmlToPlainText(orderItem.AttributeDescription, true, true), attributesFont);
                        cell.AddElement(attributesParagraph);
                        productsTable.AddCell(cell);

                        //SKU
                        var sku = p.FormatSku(orderItem.AttributesXml, _productAttributeParser);
                        cell = new PdfPCell(new Phrase(sku ?? String.Empty, font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL; 
                        productsTable.AddCell(cell);

                        //qty
                        cell = new PdfPCell(new Phrase(si.Quantity.ToString(), font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL; 
                        productsTable.AddCell(cell);
                    }
                    doc.Add(productsTable);
                }

                shipmentNum++;
                if (shipmentNum < shipmentCount)
                {
                    doc.NewPage();
                }
            }


            doc.Close();
        }

        /// <summary>
        /// Print product collection to PDF
        /// </summary>
        /// <param name="stream">Stream</param>
        /// <param name="products">Products</param>
        public virtual void PrintProductsToPdf(Stream stream, IList<Product> products)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            if (products == null)
                throw new ArgumentNullException("products");

            var lang = _workContext.WorkingLanguage;
            bool useRTL = lang.Rtl;

            var pageSize = PageSize.A4;

            if (_pdfSettings.LetterPageSizeEnabled)
            {
                pageSize = PageSize.LETTER;
            }

            var doc = new Document(pageSize);
            PdfWriter.GetInstance(doc, stream);
            doc.Open();

            //fonts
            var titleFont = GetFont();
            titleFont.SetStyle(Font.BOLD);
            titleFont.Color = BaseColor.BLACK;
            var font = GetFont();

            int productNumber = 1;
            int prodCount = products.Count;

            foreach (var product in products)
            {
                string productName = product.GetLocalized(x => x.Name, lang.Id);
                string productFullDescription = product.GetLocalized(x => x.FullDescription, lang.Id);

                var headerTable = new PdfPTable(1);

                var cell = new PdfPCell(new Paragraph(String.Format("{0}. {1}", productNumber, productName), titleFont));
                cell.BorderColor = BaseColor.WHITE;
                if (useRTL)
                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                headerTable.AddCell(cell);

                cell = new PdfPCell(new Paragraph(" "));
                cell.Border = Rectangle.NO_BORDER;
                headerTable.AddCell(cell);

                cell = new PdfPCell(new Paragraph(HtmlHelper.StripTags(HtmlHelper.ConvertHtmlToPlainText(productFullDescription)), font));
                cell.BorderColor = BaseColor.WHITE;
                if (useRTL)
                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                headerTable.AddCell(cell);

                cell = new PdfPCell(new Paragraph(" "));
                cell.Border = Rectangle.NO_BORDER;
                headerTable.AddCell(cell);

                if (product.ProductType == ProductType.SimpleProduct)
                {
                    //simple product
                    //render its properties such as price, weight, etc
                    cell = new PdfPCell(new Paragraph(String.Format("{0}: {1} {2}", _localizationService.GetResource("PDFProductCatalog.Price", lang.Id), product.Price.ToString("0.00"), _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode), font));
                    cell.BorderColor = BaseColor.WHITE;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    headerTable.AddCell(cell);
                    cell = new PdfPCell(new Paragraph(String.Format("{0}: {1}", _localizationService.GetResource("PDFProductCatalog.SKU", lang.Id), product.Sku), font));
                    cell.BorderColor = BaseColor.WHITE;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    headerTable.AddCell(cell);

                    if (product.IsShipEnabled && product.Weight > Decimal.Zero)
                        cell = new PdfPCell(new Paragraph(String.Format("{0}: {1} {2}", _localizationService.GetResource("PDFProductCatalog.Weight", lang.Id), product.Weight.ToString("0.00"), _measureService.GetMeasureWeightById(_measureSettings.BaseWeightId).Name), font));
                    cell.BorderColor = BaseColor.WHITE;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    headerTable.AddCell(cell);

                    if (product.ManageInventoryMethod == ManageInventoryMethod.ManageStock)
                        cell = new PdfPCell(new Paragraph(String.Format("{0}: {1}", _localizationService.GetResource("PDFProductCatalog.StockQuantity", lang.Id), product.StockQuantity), font));
                    cell.BorderColor = BaseColor.WHITE;
                    if (useRTL)
                        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    headerTable.AddCell(cell);
                }
                doc.Add(headerTable);
                doc.Add(new Paragraph(" "));

                var pictures = _pictureService.GetPicturesByProductId(product.Id);
                if (pictures.Count > 0)
                {
                    var table = new PdfPTable(2);
                    if (useRTL)
                        table.RunDirection = PdfWriter.RUN_DIRECTION_RTL; 
                    table.WidthPercentage = 100f;

                    for (int i = 0; i < pictures.Count; i++)
                    {
                        var pic = pictures[i];
                        if (pic != null)
                        {
                            var picBinary = _pictureService.LoadPictureBinary(pic);
                            if (picBinary != null && picBinary.Length > 0)
                            {
                                var pictureLocalPath = _pictureService.GetThumbLocalPath(pic, 200, false);
                                cell = new PdfPCell(Image.GetInstance(pictureLocalPath));
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.Border = Rectangle.NO_BORDER;
                                if (useRTL)
                                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL; 
                                table.AddCell(cell);
                            }
                        }
                    }

                    if (pictures.Count % 2 > 0)
                    {
                        cell = new PdfPCell(new Phrase(" "));
                        cell.Border = Rectangle.NO_BORDER;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL; 
                        table.AddCell(cell);
                    }

                    doc.Add(table);
                    doc.Add(new Paragraph(" "));
                }


                if (product.ProductType == ProductType.GroupedProduct)
                {
                    //grouped product. render its associated products
                    int pvNum = 1;
                    foreach (var associatedProduct in _productService.SearchProducts(parentGroupedProductId: product.Id,
                        showHidden: true))
                    {
                        var associatedProductTable = new PdfPTable(1);

                        cell = new PdfPCell(new Paragraph(String.Format("{0}-{1}. {2}", productNumber, pvNum, associatedProduct.GetLocalized(x => x.Name, lang.Id)), font));
                        cell.BorderColor = BaseColor.WHITE;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        associatedProductTable.AddCell(cell);

                        cell = new PdfPCell(new Paragraph(" "));
                        associatedProductTable.AddCell(cell);

                        //uncomment to render associated product description
                        //string apDescription = associatedProduct.GetLocalized(x => x.ShortDescription, lang.Id);
                        //if (!String.IsNullOrEmpty(apDescription))
                        //{
                        //    cell = new PdfPCell(new Paragraph(HtmlHelper.StripTags(HtmlHelper.ConvertHtmlToPlainText(apDescription)), font));
                        //    cell.BorderColor = BaseColor.WHITE;
                        //    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        //    associatedProductTable.AddCell(cell);
                        //    cell = new PdfPCell(new Paragraph(" "));
                        //    associatedProductTable.AddCell(cell);
                        //}

                        //uncomment to render associated product picture
                        //var apPicture = _pictureService.GetPicturesByProductId(associatedProduct.Id).FirstOrDefault();
                        //if (apPicture != null)
                        //{
                        //    var picBinary = _pictureService.LoadPictureBinary(apPicture);
                        //    if (picBinary != null && picBinary.Length > 0)
                        //    {
                        //        var pictureLocalPath = _pictureService.GetThumbLocalPath(apPicture, 200, false);
                        //        doc.Add(Image.GetInstance(pictureLocalPath));
                        //        cell = new PdfPCell(Image.GetInstance(pictureLocalPath));
                        //        cell.BorderColor = BaseColor.WHITE;
                        //        cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        //        associatedProductTable.AddCell(cell);
                        //    }
                        //}

                        cell = new PdfPCell(new Paragraph(String.Format("{0}: {1} {2}", _localizationService.GetResource("PDFProductCatalog.Price", lang.Id), associatedProduct.Price.ToString("0.00"), _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode), font));
                        cell.BorderColor = BaseColor.WHITE;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        associatedProductTable.AddCell(cell);
                        cell = new PdfPCell(new Paragraph(String.Format("{0}: {1}", _localizationService.GetResource("PDFProductCatalog.SKU", lang.Id), associatedProduct.Sku), font));
                        cell.BorderColor = BaseColor.WHITE;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        associatedProductTable.AddCell(cell);

                        if (associatedProduct.IsShipEnabled && associatedProduct.Weight > Decimal.Zero)
                            cell = new PdfPCell(new Paragraph(String.Format("{0}: {1} {2}", _localizationService.GetResource("PDFProductCatalog.Weight", lang.Id), associatedProduct.Weight.ToString("0.00"), _measureService.GetMeasureWeightById(_measureSettings.BaseWeightId).Name), font));
                        cell.BorderColor = BaseColor.WHITE;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        associatedProductTable.AddCell(cell);

                        if (associatedProduct.ManageInventoryMethod == ManageInventoryMethod.ManageStock)
                            cell = new PdfPCell(new Paragraph(String.Format("{0}: {1}", _localizationService.GetResource("PDFProductCatalog.StockQuantity", lang.Id), associatedProduct.StockQuantity), font));
                        cell.BorderColor = BaseColor.WHITE;
                        if (useRTL)
                            cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        associatedProductTable.AddCell(cell);

                        doc.Add(headerTable);
                        doc.Add(new Paragraph(" "));

                        pvNum++;
                    }
                }


                productNumber++;

                if (productNumber <= prodCount)
                {
                    doc.NewPage();
                }
            }

            doc.Close();
        }

        #endregion

        /// <summary>
        /// Print customized card to PDF
        /// </summary>
        /// <param name="stream">Stream</param>
        /// <param name="customization">card customization object</param>
        /// <param name="appPath">app location path</param>
        /// <param name="Server">Server utlity to map paths</param>
        /// <param name="languageId">Language identifier; 0 to use a language used when placing an order</param>
        public virtual void PrintCardToPdf(Stream stream, OrderItemCustomization customization, HttpServerUtilityBase Server, string appPath, int languageId = 0)
        {
            // Create a Document object
            var document = new Document(new Rectangle(CommonHelper.convertFromCmToPixel(customization.Width), CommonHelper.convertFromCmToPixel(customization.Height)),0,0,0,0);
            try
            {                
                // Create a new PdfWriter object, specifying the output stream
                var writer = PdfWriter.GetInstance(document, stream);
                // Open the Document for writing
                document.Open();

                int pageNum = 0;
                foreach (var page in customization.pages)
                {
                    foreach (var placeHolder in page.PlaceHolders)
                    {
                        if (placeHolder.Type.ToLower() == "image")
                        {
                            string imagePlaceholderFilePath = Server.MapPath(placeHolder.ImageURL.Replace(appPath, @"~/").Replace(@"\", "/"));
                            Image Placeholderjpg = Image.GetInstance(imagePlaceholderFilePath);
                            Placeholderjpg.SetAbsolutePosition(CommonHelper.convertFromCmToPixel(placeHolder.XPos), document.PageSize.Height - (CommonHelper.convertFromCmToPixel(placeHolder.YPos) + CommonHelper.convertFromCmToPixel(placeHolder.Height)));
                            Placeholderjpg.ScaleToFit(CommonHelper.convertFromCmToPixel(placeHolder.Width), CommonHelper.convertFromCmToPixel(placeHolder.Height));
                            Placeholderjpg.ScaleAbsolute(CommonHelper.convertFromCmToPixel(placeHolder.Width), CommonHelper.convertFromCmToPixel(placeHolder.Height));
                           // Placeholderjpg.BorderColor = BaseColor.YELLOW;
                            Placeholderjpg.BorderWidth = 5f;
                            //If you want to choose image as background then,
                            document.Add(Placeholderjpg);
                        }
                    }
                    string imageFilePath = Server.MapPath(page.TemplatePictureURL.Replace(appPath, @"~/").Replace(@"\", "/"));
                    Image jpg = Image.GetInstance(imageFilePath);
                    jpg.ScaleToFit(CommonHelper.convertFromCmToPixel(customization.Width), CommonHelper.convertFromCmToPixel(customization.Height));
                    //If you want to choose image as background then,
                    jpg.Alignment = Image.UNDERLYING;
                    document.Add(jpg);
                    foreach (var placeHolder in page.PlaceHolders)
                    {
                        if (placeHolder.Type.ToLower() == "text")
                        {
                            BaseFont bfUniCode;
                            //set the text font family
                            switch (placeHolder.TextFamily.ToLower())
                            {
                                case "arial":
                                    bfUniCode = BaseFont.CreateFont(Server.MapPath(@"~/App_Data/Pdf/arial.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                                    break;
                                case "cour":
                                    bfUniCode = BaseFont.CreateFont(Server.MapPath(@"~/App_Data/Pdf/cour.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                                    break;
                                case "serif":
                                    bfUniCode = BaseFont.CreateFont(Server.MapPath(@"~/App_Data/Pdf/FreeSerif.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                                    break;
                                case "taho":
                                    bfUniCode = BaseFont.CreateFont(Server.MapPath(@"~/App_Data/Pdf/tahoma.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                                    break;
                                case "tradar":
                                    bfUniCode = BaseFont.CreateFont(Server.MapPath(@"~/App_Data/Pdf/trado.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                                    break;
                                case "impact":
                                    bfUniCode = BaseFont.CreateFont(Server.MapPath(@"~/App_Data/Pdf/Impact.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                                    break;
                                case "comic sans ms":
                                    bfUniCode = BaseFont.CreateFont(Server.MapPath(@"~/App_Data/Pdf/ComicSansMS-Bold - Comic Sans MS - Bold.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                                    break;
                                default:
                                    bfUniCode = BaseFont.CreateFont(Server.MapPath(@"~/App_Data/Pdf/arial.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                                    break;
                            }
                            Font font = new Font(bfUniCode);
                            //set the text font color
                            System.Drawing.Color color = System.Drawing.ColorTranslator.FromHtml(placeHolder.FontColor);
                            font.Color = new BaseColor(color);
                            font.Size = (float)placeHolder.TextSize;
                            //set the text font style
                            switch (placeHolder.TextStyle.ToLower())
                            {
                                case "b":
                                    font.SetStyle(Font.BOLD);
                                    break;
                                case "i":
                                    font.SetStyle(Font.ITALIC);
                                    break;
                                case "u":
                                    font.SetStyle(Font.UNDERLINE);
                                    break;
                                default:
                                    font.SetStyle(Font.NORMAL);
                                    break;
                            }
                            var cb = writer.DirectContent;
                            var col = new ColumnText(cb);
                            //Create a regex expression to detect hebrew or arabic code points 
                            const string regex_match_arabic_hebrew = @"[\u0600-\u06FF,\u0590-\u05FF]+";
                            if (Regex.IsMatch(placeHolder.Text, regex_match_arabic_hebrew, RegexOptions.IgnoreCase))
                            {
                                col.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                            }

                            float llx, lly, urx, ury;
                            llx = CommonHelper.convertFromCmToPixel(placeHolder.XPos);
                            lly = document.PageSize.Height - (CommonHelper.convertFromCmToPixel(placeHolder.YPos) + CommonHelper.convertFromCmToPixel(placeHolder.Height));
                            urx = llx + CommonHelper.convertFromCmToPixel(placeHolder.Width);
                            ury = lly + CommonHelper.convertFromCmToPixel(placeHolder.Height);
                            iTextSharp.text.Rectangle x = new Rectangle(llx, lly, urx, ury);
                            col.SetSimpleColumn(x);
                            document.Add(x);
                            var c = new Chunk(placeHolder.Text, font);
                            c.setLineHeight((float)placeHolder.TextSize);
                            col.AddText(c);
                            col.Alignment = Element.ALIGN_CENTER;
                            col.Go(false);
                        }
                    }
                    pageNum++;
                    if (pageNum < customization.pages.Count)
                    {
                        document.NewPage();
                    }                    
                }
                document.Close();
            }
            catch (Exception ex)
            {
                if (document.IsOpen())
                    document.Close();
            }
        }

        /// <summary>
        /// Print Image to PDF
        /// </summary>
        /// <param name="stream">Stream</param>
        /// <param name="image">card image</param>
        /// <param name="languageId">Language identifier; 0 to use a language used when placing an order</param>
        public virtual void PrintImageToPdf(Stream stream, System.Drawing.Image image, int languageId = 0)
        {
            Document doc = new Document();
            try
            {
                PdfWriter.GetInstance(doc, stream);

                doc.Open();

                Image Bmp = Image.GetInstance(image, image.RawFormat);

                doc.Add(Bmp);
            }

            catch (Exception ex)
            {
            }

            finally
            {
                doc.Close();
            }
        }
    }
}