﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Catalog;
using Nop.Core;

namespace Nop.Services.Catalog
{
    public partial interface ICardTemplateService
    {
        #region Card Templates (Admin Menu)

        /// <summary>
        /// Gets all card Templates
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>card Templates collection</returns>
        IPagedList<CardTemplate> GetAllTemplates(int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Gets all card Templates by specific name
        /// </summary>
        /// <param name="holderName">card Template Name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>card Templates collection</returns>
        IPagedList<CardTemplate> GetAllTemplatesByName(string templateName = "", int pageIndex = 0, int pageSize = int.MaxValue);
        
        /// <summary>
        /// Gets a Card Template by Id
        /// </summary>
        /// <param name="templateId">Card Template identifier</param>
        /// <returns>Card Template</returns>
        CardTemplate GetTemplateById(int templateId);
        
        /// <summary>
        /// Inserts a Card Template
        /// </summary>
        /// <param name="cardTemplate">CardTemplate</param>
        void InsertTemplate(CardTemplate cardTemplate);
        
        /// <summary>
        /// Updates the Card Template
        /// </summary>
        /// <param name="cardTemplate">Card Template</param>
        void UpdateTemplate(CardTemplate cardTemplate);
        
        /// <summary>
        /// Deletes a card Template
        /// </summary>
        /// <param name="page">card Template</param>
        void DeleteTemplate(CardTemplate cardTemplate);
        
        #endregion

        #region Template Page

        /// <summary>
        /// get all pages for specific template
        /// </summary>
        /// <param name="templateId">template Identifier</param>
        /// <returns>Template Pages collelction</returns>
        IList<TemplatePages> GetPagesByTemplateId(int templateId);

        /// <summary>
        /// Inserts a template Page
        /// </summary>
        /// <param name="templatePage">template Page</param>
        void InsertTemplatePage(TemplatePages templatePage);

        /// <summary>
        /// Gets a Template Pages by Identifier
        /// </summary>
        /// <param name="templatePageId">template Page identifier</param>
        /// <returns>Template Page</returns>
        TemplatePages GetTemplatePageById(int templatePageId);

        /// <summary>
        /// Updates the Template Page
        /// </summary>
        /// <param name="templatePage">Template Page</param>
        void UpdateTemplatePage(TemplatePages templatePage);

        /// <summary>
        /// Deletes a template Page
        /// </summary>
        /// <param name="templatePage">template Page</param>
        void DeleteTemplatePage(TemplatePages templatePage);

        #endregion
    }
}
