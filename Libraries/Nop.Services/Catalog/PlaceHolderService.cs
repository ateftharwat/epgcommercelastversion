﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Services.Events;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Stores;

namespace Nop.Services.Catalog
{
    public partial class PlaceHolderService : IPlaceHolderService
    {
        #region Constants
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : page index
        /// {1} : page size
        /// </remarks>
        private const string PLACEHOLDDERS_ALL_KEY = "Nop.placeholder.all-{0}-{1}";
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : product attribute ID
        /// </remarks>
        private const string PLACEHOLDDERS_BY_ID_KEY = "Nop.placeholder.id-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string PLACEHOLDDERS_PATTERN_KEY = "Nop.placeholder.";

        #endregion

        #region Fields

        private readonly IRepository<PlaceHolder> _placeHolderRepository;
        private readonly IRepository<CardPage> _cardPageRepository;
        private readonly IRepository<Page> _pageRepository;
        private readonly IRepository<PagePlaceHolders> _pagePlaceHoldersRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<AclRecord> _aclRepository;
        private readonly IRepository<StoreMapping> _storeMappingRepository;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;
        private readonly CatalogSettings _catalogSettings;


        #endregion

        #region Ctor

        public PlaceHolderService(ICacheManager cacheManager,
            IRepository<PlaceHolder> placeHolderRepository,
            IRepository<CardPage> cardPageRepository,
            IRepository<Page> pageRepository,
            IRepository<PagePlaceHolders> pagePlaceHoldersRepository,
            IRepository<Product> productRepository,
            IRepository<AclRecord> aclRepository,
            IRepository<StoreMapping> storeMappingRepository,
            IWorkContext workContext,
            IStoreContext storeContext,
            CatalogSettings catalogSettings,
            IEventPublisher eventPublisher)
        {
            this._cacheManager = cacheManager;
            this._placeHolderRepository = placeHolderRepository;
            this._cardPageRepository = cardPageRepository;
            this._pageRepository = pageRepository;
            this._pagePlaceHoldersRepository = pagePlaceHoldersRepository;
            this._productRepository = productRepository;
            this._aclRepository = aclRepository;
            this._storeMappingRepository = storeMappingRepository;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._catalogSettings = catalogSettings;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Place Holders

        /// <summary>
        /// Deletes a PlaceHolder
        /// </summary>
        /// <param name="productAttribute">PlaceHolder</param>
        public virtual void DeletePlaceHolder(PlaceHolder placeHolder)
        {
            if (placeHolder == null)
                throw new ArgumentNullException("placeHolder");

            placeHolder.Deleted = true;
            _placeHolderRepository.Update(placeHolder);

            //cache
            _cacheManager.RemoveByPattern(PLACEHOLDDERS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityDeleted(placeHolder);
        }

        /// <summary>
        /// Gets all PlaceHolder
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>PlaceHolder collection</returns>
        public virtual IPagedList<PlaceHolder> GetAllPlaceHolders(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            string key = string.Format(PLACEHOLDDERS_ALL_KEY, pageIndex, pageSize);
            return _cacheManager.Get(key, () =>
            {
                var query = from ph in _placeHolderRepository.Table
                            where !ph.Deleted
                            orderby ph.Name
                            select ph;
                var PlaceHolders = new PagedList<PlaceHolder>(query, pageIndex, pageSize);
                return PlaceHolders;
            });
        }

        /// <summary>
        /// Gets all PlaceHolder by specific name
        /// </summary>
        /// <param name="holderName">holder Name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>PlaceHolder collection</returns>
        public virtual IPagedList<PlaceHolder> GetAllPlaceHoldersByName(string holderName = "", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            string key = string.Format(PLACEHOLDDERS_ALL_KEY, pageIndex, pageSize);
            return _cacheManager.Get(key, () =>
            {
                var query = _placeHolderRepository.Table;
                query = query.Where(h => !h.Deleted);
                if (!String.IsNullOrWhiteSpace(holderName))
                {
                    query = query.Where(h => h.Name.Contains(holderName));
                }
                query = query.OrderBy(h => h.Name);
                var PlaceHolders = new PagedList<PlaceHolder>(query, pageIndex, pageSize);
                return PlaceHolders;
            });
        }

        /// <summary>
        /// Gets a PlaceHolder
        /// </summary>
        /// <param name="productAttributeId">PlaceHolder identifier</param>
        /// <returns>PlaceHolder </returns>
        public virtual PlaceHolder GetPlaceHolderById(int placeHolderId)
        {
            if (placeHolderId == 0)
                return null;

            string key = string.Format(PLACEHOLDDERS_BY_ID_KEY, placeHolderId);
            return _cacheManager.Get(key, () => { return _placeHolderRepository.GetById(placeHolderId); });
        }

        /// <summary>
        /// Inserts a PlaceHolder
        /// </summary>
        /// <param name="productAttribute">PlaceHolder</param>
        public virtual void InsertPlaceHolder(PlaceHolder placeHolder)
        {
            if (placeHolder == null)
                throw new ArgumentNullException("placeHolder");

            _placeHolderRepository.Insert(placeHolder);

            //cache
            _cacheManager.RemoveByPattern(PLACEHOLDDERS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(placeHolder);
        }

        /// <summary>
        /// Updates the PlaceHolder
        /// </summary>
        /// <param name="placeHolder">PlaceHolder</param>
        public virtual void UpdatePlaceHolder(PlaceHolder placeHolder)
        {
            if (placeHolder == null)
                throw new ArgumentNullException("placeHolder");

            _placeHolderRepository.Update(placeHolder);

            //cache
            _cacheManager.RemoveByPattern(PLACEHOLDDERS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(placeHolder);
        }

        #endregion

        #region Card Pages

        /// <summary>
        /// Gets all card pages
        /// </summary>
        /// <param name="productId">Product identifier</param>
        /// <returns>card pages collection </returns>
        public virtual IList<CardPage> GetCardPagesByProductId(int productId)
        {
            if (productId == 0)
                return new List<CardPage>();

            //string key = string.Format(PRODUCTCATEGORIES_ALLBYPRODUCTID_KEY, showHidden, productId, _workContext.CurrentCustomer.Id, _storeContext.CurrentStore.Id);
            //return _cacheManager.Get(key, () =>
            //{
                var query = from cp in _cardPageRepository.Table
                            join ph in _placeHolderRepository.Table on cp.PlaceHolderId equals ph.Id
                            where cp.ProductId == productId
                            orderby cp.PageIndex
                            select cp;

                var allCardPages = query.ToList();
                var result = new List<CardPage>();
                result.AddRange(allCardPages);

                return result;
            //});
        }

        /// <summary>
        /// Inserts a card page
        /// </summary>
        /// <param name="cardPage">>card page</param>
        public virtual void InsertCardPage(CardPage cardPage)
        {
            if (cardPage == null)
                throw new ArgumentNullException("cardPage");

            _cardPageRepository.Insert(cardPage);

            //cache
            //_cacheManager.RemoveByPattern(CATEGORIES_PATTERN_KEY);
            //_cacheManager.RemoveByPattern(PRODUCTCATEGORIES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(cardPage);
        }

        /// <summary>
        /// Gets a card Page
        /// </summary>
        /// <param name="cardPageId">card Page identifier</param>
        /// <returns>card Page</returns>
        public virtual CardPage GetCardPageById(int cardPageId)
        {
            if (cardPageId == 0)
                return null;

            return _cardPageRepository.GetById(cardPageId);
        }

        /// <summary>
        /// Updates the card Page
        /// </summary>
        /// <param name="cardPage">>Card Page</param>
        public virtual void UpdateCardPage(CardPage cardPage)
        {
            if (cardPage == null)
                throw new ArgumentNullException("cardPage");

            _cardPageRepository.Update(cardPage);

            //cache
            //_cacheManager.RemoveByPattern(CATEGORIES_PATTERN_KEY);
            //_cacheManager.RemoveByPattern(PRODUCTCATEGORIES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(cardPage);
        }

        /// <summary>
        /// Deletes a Card Page
        /// </summary>
        /// <param name="cardPage">Card Page</param>
        public virtual void DeleteCardPage(CardPage cardPage)
        {
            if (cardPage == null)
                throw new ArgumentNullException("CardPage");

            _cardPageRepository.Delete(cardPage);

            //cache
            //_cacheManager.RemoveByPattern(CATEGORIES_PATTERN_KEY);
            //_cacheManager.RemoveByPattern(PRODUCTCATEGORIES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityDeleted(cardPage);
        }

        #endregion

        #region Pages (Admin Menu)

        /// <summary>
        /// Gets all Pages
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>page collection</returns>
        public virtual IPagedList<Page> GetAllPages(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = from p in _pageRepository.Table
                        orderby p.Name
                        select p;
            var pages = new PagedList<Page>(query, pageIndex, pageSize);

            return pages;
        }

        /// <summary>
        /// Gets all pages by specific name
        /// </summary>
        /// <param name="holderName">page Name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>pages collection</returns>
        public virtual IPagedList<Page> GetAllPagesByName(string pageName = "", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _pageRepository.Table;
            if (!String.IsNullOrWhiteSpace(pageName))
            {
                query = query.Where(h => h.Name.Contains(pageName));
            }
            query = query.OrderBy(h => h.Name);
            var pages = new PagedList<Page>(query, pageIndex, pageSize);

            return pages;
        }

        /// <summary>
        /// Gets a page by Id
        /// </summary>
        /// <param name="pageId">Page identifier</param>
        /// <returns>page</returns>
        public virtual Page GetPageById(int pageId)
        {
            if (pageId == 0)
                return null;

            return _pageRepository.GetById(pageId);
        }

        /// <summary>
        /// Inserts a page
        /// </summary>
        /// <param name="Page">page</param>
        public virtual void InsertPage(Page Page)
        {
            if (Page == null)
                throw new ArgumentNullException("Page");

            _pageRepository.Insert(Page);

            //event notification
            _eventPublisher.EntityInserted(Page);
        }

        /// <summary>
        /// Updates the page
        /// </summary>
        /// <param name="page">page</param>
        public virtual void UpdatePage(Page page)
        {
            if (page == null)
                throw new ArgumentNullException("page");

            _pageRepository.Update(page);

            //event notification
            _eventPublisher.EntityUpdated(page);
        }

        /// <summary>
        /// Deletes a Page
        /// </summary>
        /// <param name="page">Page</param>
        public virtual void DeletePage(Page page)
        {
            if (page == null)
                throw new ArgumentNullException("Page");

            _pageRepository.Delete(page);

            //event notification
            _eventPublisher.EntityDeleted(page);
        }

        #endregion

        #region Page place holders

        /// <summary>
        /// get all place holders for specific page
        /// </summary>
        /// <param name="pageId">page Identifier</param>
        /// <returns>Page PlaceHolders collection</returns>
        public virtual IList<PagePlaceHolders> GetPlaceHoldersByPageId(int pageId)
        {
            if (pageId == 0)
                return new List<PagePlaceHolders>();

            var query = from pp in _pagePlaceHoldersRepository.Table
                        join ph in _placeHolderRepository.Table on pp.PlaceHolderId equals ph.Id
                        where pp.PageId == pageId
                        select pp;

            var allPlaceHolders = query.ToList();

            return allPlaceHolders;
        }

        /// <summary>
        /// Inserts a page Place Holder 
        /// </summary>
        /// <param name="pagePlaceHolder">page Place Holder</param>
        public virtual void InsertPagePlaceHolder(PagePlaceHolders pagePlaceHolder)
        {
            if (pagePlaceHolder == null)
                throw new ArgumentNullException("pagePlaceHolder");

            _pagePlaceHoldersRepository.Insert(pagePlaceHolder);

            //event notification
            _eventPublisher.EntityInserted(pagePlaceHolder);
        }

        /// <summary>
        /// Gets a page Place Holder
        /// </summary>
        /// <param name="pagePlaceHolderId">page Place Holder identifier</param>
        /// <returns>page Place Holder</returns>
        public virtual PagePlaceHolders GetPagePlaceHolderById(int pagePlaceHolderId)
        {
            if (pagePlaceHolderId == 0)
                return null;

            return _pagePlaceHoldersRepository.GetById(pagePlaceHolderId);
        }

        /// <summary>
        /// Updates the page Place Holder
        /// </summary>
        /// <param name="pagePlaceHolder">>page Place Holder</param>
        public virtual void UpdatePagePlaceHolder(PagePlaceHolders pagePlaceHolder)
        {
            if (pagePlaceHolder == null)
                throw new ArgumentNullException("PagePlaceHolder");

            _pagePlaceHoldersRepository.Update(pagePlaceHolder);

            //event notification
            _eventPublisher.EntityUpdated(pagePlaceHolder);
        }

        /// <summary>
        /// Deletes a page Place Holder
        /// </summary>
        /// <param name="pagePlaceHolder">page Place Holder</param>
        public virtual void DeletePagePlaceHolder(PagePlaceHolders pagePlaceHolder)
        {
            if (pagePlaceHolder == null)
                throw new ArgumentNullException("PagePlaceHolder");

            _pagePlaceHoldersRepository.Delete(pagePlaceHolder);

            //event notification
            _eventPublisher.EntityDeleted(pagePlaceHolder);
        }

        #endregion
    }
}
