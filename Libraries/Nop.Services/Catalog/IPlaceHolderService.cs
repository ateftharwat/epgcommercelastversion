﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Catalog;
using Nop.Core;

namespace Nop.Services.Catalog
{
    public partial interface IPlaceHolderService
    {
        #region Place Holders

        /// <summary>
        /// Deletes a PlaceHolder
        /// </summary>
        /// <param name="pageHolder">Place Holder</param>
        void DeletePlaceHolder(PlaceHolder pageHolder);

        /// <summary>
        /// Gets all Place Holders
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>PlaceHolder collection</returns>
        IPagedList<PlaceHolder> GetAllPlaceHolders(int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Gets all PlaceHolder by specific name
        /// </summary>
        /// <param name="holderName">holder Name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>PlaceHolder collection</returns>
        IPagedList<PlaceHolder> GetAllPlaceHoldersByName(string holderName = "", int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Gets a Place Holder
        /// </summary>
        /// <param name="placeHolderId">Product attribute identifier</param>
        /// <returns>Place Holder </returns>
        PlaceHolder GetPlaceHolderById(int placeHolderId);

        /// <summary>
        /// Inserts a PlaceHolder
        /// </summary>
        /// <param name="placeHolder">Place Holder</param>
        void InsertPlaceHolder(PlaceHolder placeHolder);

        /// <summary>
        /// Updates the product attribute
        /// </summary>
        /// <param name="placeHolder">Product attribute</param>
        void UpdatePlaceHolder(PlaceHolder placeHolder);

        #endregion

        #region Card Pages

        /// <summary>
        /// Gets all card pages
        /// </summary>
        /// <param name="productId">Product identifier</param>
        /// <returns>card pages collection </returns>
        IList<CardPage> GetCardPagesByProductId(int productId);

        /// <summary>
        /// Inserts a card page
        /// </summary>
        /// <param name="cardPage">>card page</param>
        void InsertCardPage(CardPage cardPage);

        /// <summary>
        /// Gets a card Page
        /// </summary>
        /// <param name="cardPageId">card Page identifier</param>
        /// <returns>card Page</returns>
        CardPage GetCardPageById(int cardPageId);

        /// <summary>
        /// Updates the card Page
        /// </summary>
        /// <param name="cardPage">>Card Page</param>
        void UpdateCardPage(CardPage cardPage);

        /// <summary>
        /// Deletes a Card Page
        /// </summary>
        /// <param name="cardPage">Card Page</param>
        void DeleteCardPage(CardPage cardPage);

        #endregion

        #region Pages

        /// <summary>
        /// Gets all Pages
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>page collection</returns>
        IPagedList<Page> GetAllPages(int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Gets all pages by specific name
        /// </summary>
        /// <param name="holderName">page Name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>pages collection</returns>
        IPagedList<Page> GetAllPagesByName(string pageName = "", int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Gets a page by Id
        /// </summary>
        /// <param name="productAttributeId">Page identifier</param>
        /// <returns>page</returns>
        Page GetPageById(int pageId);

        /// <summary>
        /// Inserts a card page
        /// </summary>
        /// <param name="Page">>card page</param>
        void InsertPage(Page Page);

        /// <summary>
        /// Updates the page
        /// </summary>
        /// <param name="page">page</param>
        void UpdatePage(Page page);

        /// <summary>
        /// Deletes a Page
        /// </summary>
        /// <param name="page">Page</param>
        void DeletePage(Page page);

        #endregion

        #region Page place holders

        /// <summary>
        /// get all place holders for specific page
        /// </summary>
        /// <param name="pageId">page Identifier</param>
        /// <returns>Page PlaceHolders collection</returns>
        IList<PagePlaceHolders> GetPlaceHoldersByPageId(int pageId);

        /// <summary>
        /// Inserts a page Place Holder 
        /// </summary>
        /// <param name="pagePlaceHolder">page Place Holder</param>
        void InsertPagePlaceHolder(PagePlaceHolders pagePlaceHolder);

        /// <summary>
        /// Gets a page Place Holder
        /// </summary>
        /// <param name="pagePlaceHolderId">page Place Holder identifier</param>
        /// <returns>page Place Holder</returns>
        PagePlaceHolders GetPagePlaceHolderById(int pagePlaceHolderId);

        /// <summary>
        /// Updates the page Place Holder
        /// </summary>
        /// <param name="pagePlaceHolder">>page Place Holder</param>
        void UpdatePagePlaceHolder(PagePlaceHolders pagePlaceHolder);

        /// <summary>
        /// Deletes a page Place Holder
        /// </summary>
        /// <param name="pagePlaceHolder">page Place Holder</param>
        void DeletePagePlaceHolder(PagePlaceHolders pagePlaceHolder);

        #endregion
    }
}
