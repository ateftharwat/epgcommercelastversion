﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Services.Events;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Stores;

namespace Nop.Services.Catalog
{
    public partial class CardTemplateService : ICardTemplateService
    {
        #region Constants
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : page index
        /// {1} : page size
        /// </remarks>
        private const string PLACEHOLDDERS_ALL_KEY = "Nop.placeholder.all-{0}-{1}";
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : product attribute ID
        /// </remarks>
        private const string PLACEHOLDDERS_BY_ID_KEY = "Nop.placeholder.id-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string PLACEHOLDDERS_PATTERN_KEY = "Nop.placeholder.";

        #endregion

        #region Fields

        private readonly IRepository<Page> _pageRepository;
        private readonly IRepository<PagePlaceHolders> _pagePlaceHoldersRepository;
        private readonly IRepository<CardTemplate> _cardTemplateRepository;
        private readonly IRepository<TemplatePages> _templatePagesRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<AclRecord> _aclRepository;
        private readonly IRepository<StoreMapping> _storeMappingRepository;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;
        private readonly CatalogSettings _catalogSettings;


        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="productAttributeRepository">Product attribute repository</param>
        /// <param name="productVariantAttributeRepository">Product variant attribute mapping repository</param>
        /// <param name="productVariantAttributeCombinationRepository">Product variant attribute combination repository</param>
        /// <param name="productVariantAttributeValueRepository">Product variant attribute value repository</param>
        /// <param name="eventPublisher">Event published</param>
        public CardTemplateService(ICacheManager cacheManager,
            IRepository<Page> pageRepository,
            IRepository<PagePlaceHolders> pagePlaceHoldersRepository,
            IRepository<CardTemplate> cardTemplateRepository,
            IRepository<TemplatePages> templatePagesRepository,
            IRepository<Product> productRepository,
            IRepository<AclRecord> aclRepository,
            IRepository<StoreMapping> storeMappingRepository,
            IWorkContext workContext,
            IStoreContext storeContext,
            CatalogSettings catalogSettings,
            IEventPublisher eventPublisher)
        {
            this._cacheManager = cacheManager;
            this._pageRepository = pageRepository;
            this._pagePlaceHoldersRepository = pagePlaceHoldersRepository;
            this._cardTemplateRepository = cardTemplateRepository;
            this._templatePagesRepository = templatePagesRepository;
            this._productRepository = productRepository;
            this._aclRepository = aclRepository;
            this._storeMappingRepository = storeMappingRepository;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._catalogSettings = catalogSettings;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Card Templates (Admin Menu)

        /// <summary>
        /// Gets all card Templates
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>card Templates collection</returns>
        public virtual IPagedList<CardTemplate> GetAllTemplates(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = from p in _cardTemplateRepository.Table
                        orderby p.Name
                        select p;

            var cardTemplates = new PagedList<CardTemplate>(query, pageIndex, pageSize);

            return cardTemplates;
        }

        /// <summary>
        /// Gets all card Templates by specific name
        /// </summary>
        /// <param name="holderName">card Template Name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>card Templates collection</returns>
        public virtual IPagedList<CardTemplate> GetAllTemplatesByName(string templateName = "", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _cardTemplateRepository.Table;
            if (!String.IsNullOrWhiteSpace(templateName))
            {
                query = query.Where(h => h.Name.Contains(templateName));
            }
            query = query.OrderBy(h => h.Name);
            var cardTemplates = new PagedList<CardTemplate>(query, pageIndex, pageSize);

            return cardTemplates;
        }

        /// <summary>
        /// Gets a Card Template by Id
        /// </summary>
        /// <param name="templateId">Card Template identifier</param>
        /// <returns>Card Template</returns>
        public virtual CardTemplate GetTemplateById(int templateId)
        {
            if (templateId == 0)
                return null;

            return _cardTemplateRepository.GetById(templateId);
        }

        /// <summary>
        /// Inserts a Card Template
        /// </summary>
        /// <param name="cardTemplate">CardTemplate</param>
        public virtual void InsertTemplate(CardTemplate cardTemplate)
        {
            if (cardTemplate == null)
                throw new ArgumentNullException("CardTemplate");

            _cardTemplateRepository.Insert(cardTemplate);

            //event notification
            _eventPublisher.EntityInserted(cardTemplate);
        }

        /// <summary>
        /// Updates the Card Template
        /// </summary>
        /// <param name="cardTemplate">Card Template</param>
        public virtual void UpdateTemplate(CardTemplate cardTemplate)
        {
            if (cardTemplate == null)
                throw new ArgumentNullException("CardTemplate");

            _cardTemplateRepository.Update(cardTemplate);

            //event notification
            _eventPublisher.EntityUpdated(cardTemplate);
        }

        /// <summary>
        /// Deletes a card Template
        /// </summary>
        /// <param name="page">card Template</param>
        public virtual void DeleteTemplate(CardTemplate cardTemplate)
        {
            if (cardTemplate == null)
                throw new ArgumentNullException("CardTemplate");

            _cardTemplateRepository.Delete(cardTemplate);

            //event notification
            _eventPublisher.EntityDeleted(cardTemplate);
        }

        #endregion

        #region Template Page

        /// <summary>
        /// get all pages for specific template
        /// </summary>
        /// <param name="templateId">template Identifier</param>
        /// <returns>Template Pages collelction</returns>
        public virtual IList<TemplatePages> GetPagesByTemplateId(int templateId)
        {
            if (templateId == 0)
                return new List<TemplatePages>();

            var query = from tp in _templatePagesRepository.Table
                        join p in _pageRepository.Table on tp.PageId equals p.Id
                        where tp.TemplateId == templateId
                        select tp;

            var allPages = query.ToList();

            return allPages;

        }

        /// <summary>
        /// Inserts a template Page
        /// </summary>
        /// <param name="templatePage">template Page</param>
        public virtual void InsertTemplatePage(TemplatePages templatePage)
        {
            if (templatePage == null)
                throw new ArgumentNullException("TemplatePages");

            _templatePagesRepository.Insert(templatePage);

            //event notification
            _eventPublisher.EntityInserted(templatePage);
        }

        /// <summary>
        /// Gets a Template Pages by Identifier
        /// </summary>
        /// <param name="templatePageId">template Page identifier</param>
        /// <returns>Template Page</returns>
        public virtual TemplatePages GetTemplatePageById(int templatePageId)
        {
            if (templatePageId == 0)
                return null;

            return _templatePagesRepository.GetById(templatePageId);
        }

        /// <summary>
        /// Updates the Template Page
        /// </summary>
        /// <param name="templatePage">Template Page</param>
        public virtual void UpdateTemplatePage(TemplatePages templatePage)
        {
            if (templatePage == null)
                throw new ArgumentNullException("TemplatePages");

            _templatePagesRepository.Update(templatePage);

            //event notification
            _eventPublisher.EntityUpdated(templatePage);
        }

        /// <summary>
        /// Deletes a template Page
        /// </summary>
        /// <param name="templatePage">template Page</param>
        public virtual void DeleteTemplatePage(TemplatePages templatePage)
        {
            if (templatePage == null)
                throw new ArgumentNullException("templatePage");

            _templatePagesRepository.Delete(templatePage);

            //event notification
            _eventPublisher.EntityDeleted(templatePage);
        }

        #endregion
    }
}
