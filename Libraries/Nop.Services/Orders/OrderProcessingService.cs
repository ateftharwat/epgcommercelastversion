using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Logging;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.Vendors;
using Nop.Services.Affiliates;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Collections.Specialized;
using System.Net;
using System.IO;
using System.Web;
using Newtonsoft.Json;
using Nop.EPGIntegrationService;

namespace Nop.Services.Orders
{
    /// <summary>
    /// Order processing service
    /// </summary>
    public partial class OrderProcessingService : IOrderProcessingService
    {
        #region Fields
        
        private readonly IOrderService _orderService;
        private readonly IWebHelper _webHelper;
        private readonly ILocalizationService _localizationService;
        private readonly ILanguageService _languageService;
        private readonly IProductService _productService;
        private readonly IPaymentService _paymentService;
        private readonly ILogger _logger;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IGiftCardService _giftCardService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly ICheckoutAttributeFormatter _checkoutAttributeFormatter;
        private readonly IShippingService _shippingService;
        private readonly IShipmentService _shipmentService;
        private readonly ITaxService _taxService;
        private readonly ICustomerService _customerService;
        private readonly IDiscountService _discountService;
        private readonly IEncryptionService _encryptionService;
        private readonly IWorkContext _workContext;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IVendorService _vendorService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ICurrencyService _currencyService;
        private readonly IAffiliateService _affiliateService;
        private readonly IEventPublisher _eventPublisher;
        private readonly IPdfService _pdfService;

        private readonly PaymentSettings _paymentSettings;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly OrderSettings _orderSettings;
        private readonly TaxSettings _taxSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly CurrencySettings _currencySettings;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="orderService">Order service</param>
        /// <param name="webHelper">Web helper</param>
        /// <param name="localizationService">Localization service</param>
        /// <param name="languageService">Language service</param>
        /// <param name="productService">Product service</param>
        /// <param name="paymentService">Payment service</param>
        /// <param name="logger">Logger</param>
        /// <param name="orderTotalCalculationService">Order total calculationservice</param>
        /// <param name="priceCalculationService">Price calculation service</param>
        /// <param name="priceFormatter">Price formatter</param>
        /// <param name="productAttributeParser">Product attribute parser</param>
        /// <param name="productAttributeFormatter">Product attribute formatter</param>
        /// <param name="giftCardService">Gift card service</param>
        /// <param name="shoppingCartService">Shopping cart service</param>
        /// <param name="checkoutAttributeFormatter">Checkout attribute service</param>
        /// <param name="shippingService">Shipping service</param>
        /// <param name="shipmentService">Shipment service</param>
        /// <param name="taxService">Tax service</param>
        /// <param name="customerService">Customer service</param>
        /// <param name="discountService">Discount service</param>
        /// <param name="encryptionService">Encryption service</param>
        /// <param name="workContext">Work context</param>
        /// <param name="workflowMessageService">Workflow message service</param>
        /// <param name="vendorService">Vendor service</param>
        /// <param name="customerActivityService">Customer activity service</param>
        /// <param name="currencyService">Currency service</param>
        /// <param name="affiliateService">Affiliate service</param>
        /// <param name="eventPublisher">Event published</param>
        /// <param name="pdfService">PDF service</param>
        /// <param name="paymentSettings">Payment settings</param>
        /// <param name="rewardPointsSettings">Reward points settings</param>
        /// <param name="orderSettings">Order settings</param>
        /// <param name="taxSettings">Tax settings</param>
        /// <param name="localizationSettings">Localization settings</param>
        /// <param name="currencySettings">Currency settings</param>
        public OrderProcessingService(IOrderService orderService,
            IWebHelper webHelper,
            ILocalizationService localizationService,
            ILanguageService languageService,
            IProductService productService,
            IPaymentService paymentService,
            ILogger logger,
            IOrderTotalCalculationService orderTotalCalculationService,
            IPriceCalculationService priceCalculationService,
            IPriceFormatter priceFormatter,
            IProductAttributeParser productAttributeParser,
            IProductAttributeFormatter productAttributeFormatter,
            IGiftCardService giftCardService,
            IShoppingCartService shoppingCartService,
            ICheckoutAttributeFormatter checkoutAttributeFormatter,
            IShippingService shippingService,
            IShipmentService shipmentService,
            ITaxService taxService,
            ICustomerService customerService,
            IDiscountService discountService,
            IEncryptionService encryptionService,
            IWorkContext workContext,
            IWorkflowMessageService workflowMessageService,
            IVendorService vendorService,
            ICustomerActivityService customerActivityService,
            ICurrencyService currencyService,
            IAffiliateService affiliateService,
            IEventPublisher eventPublisher,
            IPdfService pdfService,
            PaymentSettings paymentSettings,
            RewardPointsSettings rewardPointsSettings,
            OrderSettings orderSettings,
            TaxSettings taxSettings,
            LocalizationSettings localizationSettings,
            CurrencySettings currencySettings)
        {
            this._orderService = orderService;
            this._webHelper = webHelper;
            this._localizationService = localizationService;
            this._languageService = languageService;
            this._productService = productService;
            this._paymentService = paymentService;
            this._logger = logger;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._priceCalculationService = priceCalculationService;
            this._priceFormatter = priceFormatter;
            this._productAttributeParser = productAttributeParser;
            this._productAttributeFormatter = productAttributeFormatter;
            this._giftCardService = giftCardService;
            this._shoppingCartService = shoppingCartService;
            this._checkoutAttributeFormatter = checkoutAttributeFormatter;
            this._workContext = workContext;
            this._workflowMessageService = workflowMessageService;
            this._vendorService = vendorService;
            this._shippingService = shippingService;
            this._shipmentService = shipmentService;
            this._taxService = taxService;
            this._customerService = customerService;
            this._discountService = discountService;
            this._encryptionService = encryptionService;
            this._customerActivityService = customerActivityService;
            this._currencyService = currencyService;
            this._affiliateService = affiliateService;
            this._eventPublisher = eventPublisher;
            this._pdfService = pdfService;
            this._paymentSettings = paymentSettings;
            this._rewardPointsSettings = rewardPointsSettings;
            this._orderSettings = orderSettings;
            this._taxSettings = taxSettings;
            this._localizationSettings = localizationSettings;
            this._currencySettings = currencySettings;
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Award reward points
        /// </summary>
        /// <param name="order">Order</param>
        protected virtual void AwardRewardPoints(Order order)
        {
            if (!_rewardPointsSettings.Enabled)
                return;

            if (_rewardPointsSettings.PointsForPurchases_Amount <= decimal.Zero)
                return;

            //Ensure that reward points are applied only to registered users
            if (order.Customer == null || order.Customer.IsGuest())
                return;

            int points = (int)Math.Truncate(order.OrderTotal / _rewardPointsSettings.PointsForPurchases_Amount * _rewardPointsSettings.PointsForPurchases_Points);
            if (points == 0)
                return;

            //Ensure that reward points were not added before. We should not add reward points if they were already earned for this order
            if (order.RewardPointsWereAdded)
                return;

            //add reward points
            order.Customer.AddRewardPointsHistoryEntry(points, string.Format(_localizationService.GetResource("RewardPoints.Message.EarnedForOrder"), order.Id));
            order.RewardPointsWereAdded = true;
            _orderService.UpdateOrder(order);
        }

        /// <summary>
        /// Award reward points
        /// </summary>
        /// <param name="order">Order</param>
        protected virtual void ReduceRewardPoints(Order order)
        {
            if (!_rewardPointsSettings.Enabled)
                return;

            if (_rewardPointsSettings.PointsForPurchases_Amount <= decimal.Zero)
                return;

            //Ensure that reward points are applied only to registered users
            if (order.Customer == null || order.Customer.IsGuest())
                return;

            int points = (int)Math.Truncate(order.OrderTotal / _rewardPointsSettings.PointsForPurchases_Amount * _rewardPointsSettings.PointsForPurchases_Points);
            if (points == 0)
                return;

            //ensure that reward points were already earned for this order before
            if (!order.RewardPointsWereAdded)
                return;

            //reduce reward points
            order.Customer.AddRewardPointsHistoryEntry(-points, string.Format(_localizationService.GetResource("RewardPoints.Message.ReducedForOrder"), order.Id));
            _orderService.UpdateOrder(order);
        }

        /// <summary>
        /// Set IsActivated value for purchase gift cards for particular order
        /// </summary>
        /// <param name="order">Order</param>
        /// <param name="activate">A value indicating whether to activate gift cards; true - activate, false - deactivate</param>
        protected virtual void SetActivatedValueForPurchasedGiftCards(Order order, bool activate)
        {
            var giftCards = _giftCardService.GetAllGiftCards(order.Id, null, null, !activate, "", 0, int.MaxValue);
            foreach (var gc in giftCards)
            {
                if (activate)
                {
                    //activate
                    bool isRecipientNotified = gc.IsRecipientNotified;
                    if (gc.GiftCardType == GiftCardType.Virtual)
                    {
                        //send email for virtual gift card
                        if (!String.IsNullOrEmpty(gc.RecipientEmail) &&
                            !String.IsNullOrEmpty(gc.SenderEmail))
                        {
                            var customerLang = _languageService.GetLanguageById(order.CustomerLanguageId);
                            if (customerLang == null)
                                customerLang = _languageService.GetAllLanguages().FirstOrDefault();
                            if (customerLang == null)
                                throw new Exception("No languages could be loaded");
                            int queuedEmailId = _workflowMessageService.SendGiftCardNotification(gc, customerLang.Id);
                            if (queuedEmailId > 0)
                                isRecipientNotified = true;
                        }
                    }
                    gc.IsGiftCardActivated = true;
                    gc.IsRecipientNotified = isRecipientNotified;
                    _giftCardService.UpdateGiftCard(gc);
                }
                else
                {
                    //deactivate
                    gc.IsGiftCardActivated = false;
                    _giftCardService.UpdateGiftCard(gc);
                }
            }
        }

        /// <summary>
        /// Sets an order status
        /// </summary>
        /// <param name="order">Order</param>
        /// <param name="os">New order status</param>
        /// <param name="notifyCustomer">True to notify customer</param>
        protected virtual void SetOrderStatus(Order order, OrderStatus os, bool notifyCustomer)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            OrderStatus prevOrderStatus = order.OrderStatus;
            if (prevOrderStatus == os)
                return;

            //set and save new order status
            order.OrderStatusId = (int)os;
            _orderService.UpdateOrder(order);

            //order notes, notifications
            order.OrderNotes.Add(new OrderNote()
                {
                    Note = string.Format("Order status has been changed to {0}", os.ToString()),
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });
            _orderService.UpdateOrder(order);


            if (prevOrderStatus != OrderStatus.Complete &&
                os == OrderStatus.Complete
                && notifyCustomer)
            {
                //notification
                var orderCompletedAttachmentFilePath = _orderSettings.AttachPdfInvoiceToOrderCompletedEmail ?
                    _pdfService.PrintOrderToPdf(order, 0) : null;
                var orderCompletedAttachmentFileName = _orderSettings.AttachPdfInvoiceToOrderCompletedEmail ?
                    "order.pdf" : null;
                int orderCompletedCustomerNotificationQueuedEmailId = _workflowMessageService
                    .SendOrderCompletedCustomerNotification(order, order.CustomerLanguageId, orderCompletedAttachmentFilePath,
                    orderCompletedAttachmentFileName);
                if (orderCompletedCustomerNotificationQueuedEmailId > 0)
                {
                    order.OrderNotes.Add(new OrderNote()
                    {
                        Note = string.Format("\"Order completed\" email (to customer) has been queued. Queued email identifier: {0}.", orderCompletedCustomerNotificationQueuedEmailId),
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow
                    });
                    _orderService.UpdateOrder(order);
                }
            }

            if (prevOrderStatus != OrderStatus.Cancelled &&
                os == OrderStatus.Cancelled
                && notifyCustomer)
            {
                //notification
                int orderCancelledCustomerNotificationQueuedEmailId = _workflowMessageService.SendOrderCancelledCustomerNotification(order, order.CustomerLanguageId);
                if (orderCancelledCustomerNotificationQueuedEmailId > 0)
                {
                    order.OrderNotes.Add(new OrderNote()
                    {
                        Note = string.Format("\"Order cancelled\" email (to customer) has been queued. Queued email identifier: {0}.", orderCancelledCustomerNotificationQueuedEmailId),
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow
                    });
                    _orderService.UpdateOrder(order);
                }
            }

            //reward points
            if (_rewardPointsSettings.PointsForPurchases_Awarded == order.OrderStatus)
            {
                AwardRewardPoints(order);
            }
            if (_rewardPointsSettings.PointsForPurchases_Canceled == order.OrderStatus)
            {
                ReduceRewardPoints(order);
            }

            //gift cards activation
            if (_orderSettings.GiftCards_Activated_OrderStatusId > 0 &&
               _orderSettings.GiftCards_Activated_OrderStatusId == (int)order.OrderStatus)
            {
                SetActivatedValueForPurchasedGiftCards(order, true);
            }

            //gift cards deactivation
            if (_orderSettings.GiftCards_Deactivated_OrderStatusId > 0 &&
               _orderSettings.GiftCards_Deactivated_OrderStatusId == (int)order.OrderStatus)
            {
                SetActivatedValueForPurchasedGiftCards(order, false);
            }
        }

        /// <summary>
        /// Process order paid status
        /// </summary>
        /// <param name="order">Order</param>
        protected virtual void ProcessOrderPaid(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            //raise event
            _eventPublisher.PublishOrderPaid(order);

            //order paid email notification
            if (order.OrderTotal != decimal.Zero)
            {
                //we should not send it for free ($0 total) orders?
                //remove this "if" statement if you want to send it in this case
                _workflowMessageService.SendOrderPaidStoreOwnerNotification(order, _localizationSettings.DefaultAdminLanguageId);
            }

            //customer roles with "purchased with product" specified
            ProcessCustomerRolesWithPurchasedProductSpecified(order, true);
        }

        /// <summary>
        /// Process customer roles with "Purchased with Product" property configured
        /// </summary>
        /// <param name="order">Order</param>
        /// <param name="add">A value indicating whether to add configured customer role; true - add, false - remove</param>
        protected virtual void ProcessCustomerRolesWithPurchasedProductSpecified(Order order, bool add)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            //purchased product IDs
            //UNDONE: should we add associated (bundled) products to this list?
            var purchasedProductIds = order.OrderItems.Select(oi => oi.ProductId).ToList();

            //list of customer roles
            var customerRoles = _customerService
                .GetAllCustomerRoles(true)
                .Where(cr => purchasedProductIds.Contains(cr.PurchasedWithProductId))
                .ToList();

            if (customerRoles.Count > 0)
            {
                var customer = order.Customer;
                foreach (var customerRole in customerRoles)
                {
                    if (customer.CustomerRoles.Count(cr => cr.Id == customerRole.Id) == 0)
                    {
                        //not in the list yet
                        if (add)
                        {
                            //add
                            customer.CustomerRoles.Add(customerRole);
                        }
                    }
                    else
                    {
                        //already in the list
                        if (!add)
                        {
                            //remove
                            customer.CustomerRoles.Remove(customerRole);
                        }
                    }
                }
                _customerService.UpdateCustomer(customer);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Checks order status
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Validated order</returns>
        public virtual void CheckOrderStatus(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.PaymentStatus == PaymentStatus.Paid && !order.PaidDateUtc.HasValue)
            {
                //ensure that paid date is set
                order.PaidDateUtc = DateTime.UtcNow;
                _orderService.UpdateOrder(order);
            }

            if (order.OrderStatus == OrderStatus.Pending)
            {
                if (order.PaymentStatus == PaymentStatus.Authorized ||
                    order.PaymentStatus == PaymentStatus.Paid)
                {
                    SetOrderStatus(order, OrderStatus.Processing, false);
                }
            }

            if (order.OrderStatus == OrderStatus.Pending)
            {
                if (order.ShippingStatus == ShippingStatus.PartiallyReadyToBeShipped ||
                    order.ShippingStatus == ShippingStatus.ReadyToBeShipped ||
                    order.ShippingStatus == ShippingStatus.PartiallyShipped ||
                    order.ShippingStatus == ShippingStatus.Shipped ||
                    order.ShippingStatus == ShippingStatus.Delivered)
                {
                    SetOrderStatus(order, OrderStatus.Processing, false);
                }
            }

            if (order.OrderStatus != OrderStatus.Cancelled &&
                order.OrderStatus != OrderStatus.Complete)
            {
                if (order.PaymentStatus == PaymentStatus.Paid)
                {
                    if (order.ShippingStatus == ShippingStatus.ShippingNotRequired || order.ShippingStatus == ShippingStatus.Delivered)
                    {
                        SetOrderStatus(order, OrderStatus.Complete, true);
                    }
                }
            }
        }

        /// <summary>
        /// Places an order
        /// </summary>
        /// <param name="processPaymentRequest">Process payment request</param>
        /// <returns>Place order result</returns>
        public virtual PlaceOrderResult PlaceOrder(int customerId, ProcessPaymentRequest processPaymentRequest, int cartTypeId = 1)
        {
            //think about moving functionality of processing recurring orders (after the initial order was placed) to ProcessNextRecurringPayment() method
            if (processPaymentRequest == null)
                throw new ArgumentNullException("processPaymentRequest");

            if (processPaymentRequest.OrderGuid == Guid.Empty)
                processPaymentRequest.OrderGuid = Guid.NewGuid();

            var result = new PlaceOrderResult();
            try
            {
                #region Order details (customer, addresses, totals)



                //Recurring orders. Load initial order
                Order initialOrder = _orderService.GetOrderById(processPaymentRequest.InitialOrderId);
                if (processPaymentRequest.IsRecurringPayment)
                {
                    if (initialOrder == null)
                        throw new ArgumentException("Initial order is not set for recurring payment");

                    processPaymentRequest.PaymentMethodSystemName = initialOrder.PaymentMethodSystemName;
                }

                //customer
                var customer = _customerService.GetCustomerById(processPaymentRequest.CustomerId);
                if (customer == null)
                    throw new ArgumentException("Customer is not set");
                
                //affilites
                int affiliateId = 0;
                var affiliate = _affiliateService.GetAffiliateById(customer.AffiliateId);
                if (affiliate != null && affiliate.Active && !affiliate.Deleted)
                    affiliateId = affiliate.Id;

                //customer currency
                string customerCurrencyCode = "";
                decimal customerCurrencyRate = decimal.Zero;
                if (!processPaymentRequest.IsRecurringPayment)
                {
                    var currencyTmp = _currencyService.GetCurrencyById(customer.GetAttribute<int>(SystemCustomerAttributeNames.CurrencyId, processPaymentRequest.StoreId));
                    var customerCurrency = (currencyTmp != null && currencyTmp.Published) ? currencyTmp : _workContext.WorkingCurrency;
                    customerCurrencyCode = customerCurrency.CurrencyCode;
                    var primaryStoreCurrency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
                    customerCurrencyRate = customerCurrency.Rate / primaryStoreCurrency.Rate;
                }
                else
                {
                    customerCurrencyCode = initialOrder.CustomerCurrencyCode;
                    customerCurrencyRate = initialOrder.CurrencyRate;
                }
                //customer language
                Language customerLanguage = null;
                if (!processPaymentRequest.IsRecurringPayment)
                {
                    customerLanguage = _languageService.GetLanguageById(customer.GetAttribute<int>(
                        SystemCustomerAttributeNames.LanguageId, processPaymentRequest.StoreId));
                }
                else
                {
                    customerLanguage = _languageService.GetLanguageById(initialOrder.CustomerLanguageId);
                }
                if (customerLanguage == null || !customerLanguage.Published)
                    customerLanguage = _workContext.WorkingLanguage;

                //check whether customer is guest
                if (customer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
                    throw new NopException(_localizationService.GetResource("Checkout.AnonymousCheckout"));

                //billing address
                Address billingAddress = null;
                if (!processPaymentRequest.IsRecurringPayment)
                {
                    if (customer.BillingAddress == null)
                        throw new NopException("Billing address is not provided");

                    if (!CommonHelper.IsValidEmail(customer.BillingAddress.Email))
                        throw new NopException("Email is not valid");

                    //clone billing address
                    billingAddress = (Address)customer.BillingAddress.Clone();
                    if (billingAddress.Country != null && !billingAddress.Country.AllowsBilling)
                        throw new NopException(string.Format("Country '{0}' is not allowed for billing", billingAddress.Country.Name));
                }
                else
                {
                    if (initialOrder.BillingAddress == null)
                        throw new NopException("Billing address is not available");

                    //clone billing address
                    billingAddress = (Address)initialOrder.BillingAddress.Clone();
                    if (billingAddress.Country != null && !billingAddress.Country.AllowsBilling)
                        throw new NopException(string.Format("Country '{0}' is not allowed for billing", billingAddress.Country.Name));
                }

                //checkout attributes
                string checkoutAttributeDescription, checkoutAttributesXml;
                if (!processPaymentRequest.IsRecurringPayment)
                {
                    checkoutAttributesXml = customer.GetAttribute<string>(SystemCustomerAttributeNames.CheckoutAttributes, processPaymentRequest.StoreId);
                    checkoutAttributeDescription = _checkoutAttributeFormatter.FormatAttributes(checkoutAttributesXml, customer);
                }
                else
                {
                    checkoutAttributesXml = initialOrder.CheckoutAttributesXml;
                    checkoutAttributeDescription = initialOrder.CheckoutAttributeDescription;
                }

                //load and validate customer shopping cart
                IList<ShoppingCartItem> cart = null;
                if (!processPaymentRequest.IsRecurringPayment)
                {
                    //load shopping cart
                    var cartType = (ShoppingCartType)cartTypeId;
                    cart = customer.ShoppingCartItems
                        .Where(sci => sci.ShoppingCartType == cartType)//ShoppingCartType.ShoppingCart)
                        .Where(sci => sci.StoreId == processPaymentRequest.StoreId)
                        .ToList();

                    if (cart.Count == 0)
                        throw new NopException(_localizationService.GetResource("checkout.emptycart"));

                    //validate the entire shopping cart
                    var warnings = _shoppingCartService.GetShoppingCartWarnings(cart,
                        checkoutAttributesXml,
                        true);
                    if (warnings.Count > 0)
                    {
                        var warningsSb = new StringBuilder();
                        foreach (string warning in warnings)
                        {
                            warningsSb.Append(warning);
                            warningsSb.Append(";");
                        }
                        throw new NopException(warningsSb.ToString());
                    }

                    //validate individual cart items
                    foreach (var sci in cart)
                    {
                        var sciWarnings = _shoppingCartService.GetShoppingCartItemWarnings(customer, sci.ShoppingCartType,
                            sci.Product, processPaymentRequest.StoreId, sci.AttributesXml,
                            sci.CustomerEnteredPrice, sci.Quantity, false);
                        if (sciWarnings.Count > 0)
                        {
                            var warningsSb = new StringBuilder();
                            foreach (string warning in sciWarnings)
                            {
                                warningsSb.Append(warning);
                                warningsSb.Append(";");
                            }
                            throw new NopException(warningsSb.ToString());
                        }
                    }
                }

                //min totals validation
                if (!processPaymentRequest.IsRecurringPayment)
                {
                    bool minOrderSubtotalAmountOk = ValidateMinOrderSubtotalAmount(cart);
                    if (!minOrderSubtotalAmountOk)
                    {
                        decimal minOrderSubtotalAmount = _currencyService.ConvertFromPrimaryStoreCurrency(_orderSettings.MinOrderSubtotalAmount, _workContext.WorkingCurrency);
                        throw new NopException(string.Format(_localizationService.GetResource("Checkout.MinOrderSubtotalAmount"), _priceFormatter.FormatPrice(minOrderSubtotalAmount, true, false)));
                    }
                    bool minOrderTotalAmountOk = ValidateMinOrderTotalAmount(cart);
                    if (!minOrderTotalAmountOk)
                    {
                        decimal minOrderTotalAmount = _currencyService.ConvertFromPrimaryStoreCurrency(_orderSettings.MinOrderTotalAmount, _workContext.WorkingCurrency);
                        throw new NopException(string.Format(_localizationService.GetResource("Checkout.MinOrderTotalAmount"), _priceFormatter.FormatPrice(minOrderTotalAmount, true, false)));
                    }
                }

                //tax display type
                var customerTaxDisplayType = TaxDisplayType.IncludingTax;
                if (!processPaymentRequest.IsRecurringPayment)
                {
                    if (_taxSettings.AllowCustomersToSelectTaxDisplayType)
                        customerTaxDisplayType = (TaxDisplayType)customer.GetAttribute<int>(SystemCustomerAttributeNames.TaxDisplayTypeId, processPaymentRequest.StoreId);
                    else
                        customerTaxDisplayType = _taxSettings.TaxDisplayType;
                }
                else
                {
                    customerTaxDisplayType = initialOrder.CustomerTaxDisplayType;
                }

                //applied discount (used to store discount usage history)
                var appliedDiscounts = new List<Discount>();

                //sub total
                decimal orderSubTotalInclTax, orderSubTotalExclTax;
                decimal orderSubTotalDiscountInclTax = 0, orderSubTotalDiscountExclTax = 0;
                if (!processPaymentRequest.IsRecurringPayment)
                {
                    //sub total (incl tax)
                    decimal orderSubTotalDiscountAmount1 = decimal.Zero;
                    Discount orderSubTotalAppliedDiscount1 = null;
                    decimal subTotalWithoutDiscountBase1 = decimal.Zero;
                    decimal subTotalWithDiscountBase1 = decimal.Zero;
                    _orderTotalCalculationService.GetShoppingCartSubTotal(cart,
                        true, out orderSubTotalDiscountAmount1, out orderSubTotalAppliedDiscount1,
                        out subTotalWithoutDiscountBase1, out subTotalWithDiscountBase1);
                    orderSubTotalInclTax = subTotalWithoutDiscountBase1;
                    orderSubTotalDiscountInclTax = orderSubTotalDiscountAmount1;

                    //discount history
                    if (orderSubTotalAppliedDiscount1 != null && !appliedDiscounts.ContainsDiscount(orderSubTotalAppliedDiscount1))
                        appliedDiscounts.Add(orderSubTotalAppliedDiscount1);

                    //sub total (excl tax)
                    decimal orderSubTotalDiscountAmount2 = decimal.Zero;
                    Discount orderSubTotalAppliedDiscount2 = null;
                    decimal subTotalWithoutDiscountBase2 = decimal.Zero;
                    decimal subTotalWithDiscountBase2 = decimal.Zero;
                    _orderTotalCalculationService.GetShoppingCartSubTotal(cart,
                        false, out orderSubTotalDiscountAmount2, out orderSubTotalAppliedDiscount2,
                        out subTotalWithoutDiscountBase2, out subTotalWithDiscountBase2);
                    orderSubTotalExclTax = subTotalWithoutDiscountBase2;
                    orderSubTotalDiscountExclTax = orderSubTotalDiscountAmount2;
                }
                else
                {
                    orderSubTotalInclTax = initialOrder.OrderSubtotalInclTax;
                    orderSubTotalExclTax = initialOrder.OrderSubtotalExclTax;
                }


                //shipping info
                bool shoppingCartRequiresShipping = false;
                if (!processPaymentRequest.IsRecurringPayment)
                {
                    shoppingCartRequiresShipping = cart.RequiresShipping();
                }
                else
                {
                    shoppingCartRequiresShipping = initialOrder.ShippingStatus != ShippingStatus.ShippingNotRequired;
                }

                bool OnlyCustomizedProducts = true;
                if (customer.SendToManyAddressSelected != null)
                    OnlyCustomizedProducts = false;

                string ShippingOrigin = ConfigurationManager.AppSettings["ShippingOrigin"];
                var ShippingMethodModel = (MultiShippingMethodModel)new JavaScriptSerializer().Deserialize(customer.ShippingMethodsAttributes, typeof(MultiShippingMethodModel));
                //string shippingMethodName = "";
                string shippingRateComputationMethodSystemName = "";
                if (shoppingCartRequiresShipping)
                {
                    if (!processPaymentRequest.IsRecurringPayment)
                    {                        
                        //var shippingOption = customer.GetAttribute<ShippingOption>(SystemCustomerAttributeNames.SelectedShippingOption, processPaymentRequest.StoreId);
                        //if (shippingOption != null)
                        {
                            //shippingMethodName = shippingOption.Name;
                            //shippingRateComputationMethodSystemName = shippingOption.ShippingRateComputationMethodSystemName;
                            if (ShippingMethodModel != null)
                            {
                                shippingRateComputationMethodSystemName = ShippingMethodModel.ItemsShippingMethods.FirstOrDefault().ShippingOptions.FirstOrDefault().ShippingRateComputationMethodSystemName;
                            }
                            else
                            {
                                shippingRateComputationMethodSystemName = "emPost";
                            }
                        }
                    }
                    else
                    {
                        //shippingMethodName = initialOrder.ShippingMethod;
                        shippingRateComputationMethodSystemName = initialOrder.ShippingRateComputationMethodSystemName;
                    }
                }


                ////shipping total
                //decimal? orderShippingTotalInclTax, orderShippingTotalExclTax = null;
                //if (!processPaymentRequest.IsRecurringPayment)
                //{
                //    decimal taxRate = decimal.Zero;
                //    Discount shippingTotalDiscount = null;
                //    orderShippingTotalInclTax = _orderTotalCalculationService.GetShoppingCartShippingTotal(cart, true, out taxRate, out shippingTotalDiscount);
                //    orderShippingTotalExclTax = _orderTotalCalculationService.GetShoppingCartShippingTotal(cart, false);
                //    if (!orderShippingTotalInclTax.HasValue || !orderShippingTotalExclTax.HasValue)
                //        throw new NopException("Shipping total couldn't be calculated");

                //    if (shippingTotalDiscount != null && !appliedDiscounts.ContainsDiscount(shippingTotalDiscount))
                //        appliedDiscounts.Add(shippingTotalDiscount);
                //}
                //else
                //{
                //    orderShippingTotalInclTax = initialOrder.OrderShippingInclTax;
                //    orderShippingTotalExclTax = initialOrder.OrderShippingExclTax;
                //}

                //shipping total
                decimal? orderShippingTotalInclTax, orderShippingTotalExclTax = null;
                if (!processPaymentRequest.IsRecurringPayment)
                {
                    decimal taxRate = decimal.Zero;
                    Discount shippingTotalDiscount = null;
                    orderShippingTotalInclTax = _orderTotalCalculationService.GetShoppingCartMultiShippingTotal(cart, true, out taxRate, out shippingTotalDiscount);
                    orderShippingTotalExclTax = _orderTotalCalculationService.GetShoppingCartMultiShippingTotal(cart, false);
                    //Shipping method have been skipped
                    if(ShippingMethodModel == null)
                    {
                        orderShippingTotalInclTax = 0;
                        orderShippingTotalExclTax = 0;
                    }
                    if (!orderShippingTotalInclTax.HasValue || !orderShippingTotalExclTax.HasValue)
                        throw new NopException("Shipping total couldn't be calculated");

                    if (shippingTotalDiscount != null && !appliedDiscounts.ContainsDiscount(shippingTotalDiscount))
                        appliedDiscounts.Add(shippingTotalDiscount);
                }
                else
                {
                    orderShippingTotalInclTax = initialOrder.OrderShippingInclTax;
                    orderShippingTotalExclTax = initialOrder.OrderShippingExclTax;
                }


                //payment total
                decimal paymentAdditionalFeeInclTax, paymentAdditionalFeeExclTax;
                if (!processPaymentRequest.IsRecurringPayment)
                {
                    decimal paymentAdditionalFee = _paymentService.GetAdditionalHandlingFee(cart, processPaymentRequest.PaymentMethodSystemName);
                    paymentAdditionalFeeInclTax = _taxService.GetPaymentMethodAdditionalFee(paymentAdditionalFee, true, customer);
                    paymentAdditionalFeeExclTax = _taxService.GetPaymentMethodAdditionalFee(paymentAdditionalFee, false, customer);
                }
                else
                {
                    paymentAdditionalFeeInclTax = initialOrder.PaymentMethodAdditionalFeeInclTax;
                    paymentAdditionalFeeExclTax = initialOrder.PaymentMethodAdditionalFeeExclTax;
                }


                //tax total
                decimal orderTaxTotal = decimal.Zero;
                string vatNumber = "", taxRates = "";
                if (!processPaymentRequest.IsRecurringPayment)
                {
                    //tax amount
                    SortedDictionary<decimal, decimal> taxRatesDictionary = null;
                    orderTaxTotal = _orderTotalCalculationService.GetTaxTotal(cart, out taxRatesDictionary);

                    //VAT number
                    var customerVatStatus = (VatNumberStatus)customer.GetAttribute<int>(SystemCustomerAttributeNames.VatNumberStatusId);
                    if (_taxSettings.EuVatEnabled && customerVatStatus == VatNumberStatus.Valid)
                        vatNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.VatNumber);

                    //tax rates
                    foreach (var kvp in taxRatesDictionary)
                    {
                        var taxRate = kvp.Key;
                        var taxValue = kvp.Value;
                        taxRates += string.Format("{0}:{1};   ", taxRate.ToString(CultureInfo.InvariantCulture), taxValue.ToString(CultureInfo.InvariantCulture));
                    }
                }
                else
                {
                    orderTaxTotal = initialOrder.OrderTax;
                    //VAT number
                    vatNumber = initialOrder.VatNumber;
                }


                //order total (and applied discounts, gift cards, reward points)
                decimal? orderTotal = null;
                decimal orderDiscountAmount = decimal.Zero;
                List<AppliedGiftCard> appliedGiftCards = null;
                int redeemedRewardPoints = 0;
                decimal redeemedRewardPointsAmount = decimal.Zero;
                if (!processPaymentRequest.IsRecurringPayment)
                {
                    Discount orderAppliedDiscount = null;
                    orderTotal = _orderTotalCalculationService.GetShoppingCartTotal(cart,
                        out orderDiscountAmount, out orderAppliedDiscount, out appliedGiftCards,
                        out redeemedRewardPoints, out redeemedRewardPointsAmount);
                    if (!orderTotal.HasValue)
                        throw new NopException("Order total couldn't be calculated");

                    //discount history
                    if (orderAppliedDiscount != null && !appliedDiscounts.ContainsDiscount(orderAppliedDiscount))
                        appliedDiscounts.Add(orderAppliedDiscount);
                }
                else
                {
                    orderDiscountAmount = initialOrder.OrderDiscount;
                    orderTotal = initialOrder.OrderTotal;
                }
                processPaymentRequest.OrderTotal = orderTotal.Value;

                #endregion

                #region Payment workflow

                //skip payment workflow if order total equals zero
                bool skipPaymentWorkflow = orderTotal.Value == decimal.Zero;

                //payment workflow
                IPaymentMethod paymentMethod = null;
                if (!skipPaymentWorkflow)
                {
                    paymentMethod = _paymentService.LoadPaymentMethodBySystemName(processPaymentRequest.PaymentMethodSystemName);
                    if (paymentMethod == null)
                        throw new NopException("Payment method couldn't be loaded");

                    //ensure that payment method is active
                    if (!paymentMethod.IsPaymentMethodActive(_paymentSettings))
                        throw new NopException("Payment method is not active");
                }
                else
                    processPaymentRequest.PaymentMethodSystemName = "";

                //recurring or standard shopping cart?
                bool isRecurringShoppingCart = false;
                if (!processPaymentRequest.IsRecurringPayment)
                {
                    isRecurringShoppingCart = cart.IsRecurring();
                    if (isRecurringShoppingCart)
                    {
                        int recurringCycleLength = 0;
                        RecurringProductCyclePeriod recurringCyclePeriod;
                        int recurringTotalCycles = 0;
                        string recurringCyclesError = cart.GetRecurringCycleInfo(_localizationService,
                            out recurringCycleLength, out recurringCyclePeriod, out recurringTotalCycles);
                        if (!string.IsNullOrEmpty(recurringCyclesError))
                            throw new NopException(recurringCyclesError);
                        processPaymentRequest.RecurringCycleLength = recurringCycleLength;
                        processPaymentRequest.RecurringCyclePeriod = recurringCyclePeriod;
                        processPaymentRequest.RecurringTotalCycles = recurringTotalCycles;
                    }
                }
                else
                    isRecurringShoppingCart = true;


                //process payment
                ProcessPaymentResult processPaymentResult = null;
                if (!skipPaymentWorkflow)
                {
                    if (!processPaymentRequest.IsRecurringPayment)
                    {
                        if (isRecurringShoppingCart)
                        {
                            //recurring cart
                            var recurringPaymentType = _paymentService.GetRecurringPaymentType(processPaymentRequest.PaymentMethodSystemName);
                            switch (recurringPaymentType)
                            {
                                case RecurringPaymentType.NotSupported:
                                    throw new NopException("Recurring payments are not supported by selected payment method");
                                case RecurringPaymentType.Manual:
                                case RecurringPaymentType.Automatic:
                                    processPaymentResult = _paymentService.ProcessRecurringPayment(processPaymentRequest);
                                    break;
                                default:
                                    throw new NopException("Not supported recurring payment type");
                            }
                        }
                        else
                        {
                            //standard cart
                            processPaymentResult = _paymentService.ProcessPayment(processPaymentRequest);
                        }
                    }
                    else
                    {
                        if (isRecurringShoppingCart)
                        {
                            //Old credit card info
                            processPaymentRequest.CreditCardType = initialOrder.AllowStoringCreditCardNumber ? _encryptionService.DecryptText(initialOrder.CardType) : "";
                            processPaymentRequest.CreditCardName = initialOrder.AllowStoringCreditCardNumber ? _encryptionService.DecryptText(initialOrder.CardName) : "";
                            processPaymentRequest.CreditCardNumber = initialOrder.AllowStoringCreditCardNumber ? _encryptionService.DecryptText(initialOrder.CardNumber) : "";
                            //MaskedCreditCardNumber 
                            processPaymentRequest.CreditCardCvv2 = initialOrder.AllowStoringCreditCardNumber ? _encryptionService.DecryptText(initialOrder.CardCvv2) : "";
                            try
                            {
                                processPaymentRequest.CreditCardExpireMonth = initialOrder.AllowStoringCreditCardNumber ? Convert.ToInt32(_encryptionService.DecryptText(initialOrder.CardExpirationMonth)) : 0;
                                processPaymentRequest.CreditCardExpireYear = initialOrder.AllowStoringCreditCardNumber ? Convert.ToInt32(_encryptionService.DecryptText(initialOrder.CardExpirationYear)) : 0;
                            }
                            catch {}

                            var recurringPaymentType = _paymentService.GetRecurringPaymentType(processPaymentRequest.PaymentMethodSystemName);
                            switch (recurringPaymentType)
                            {
                                case RecurringPaymentType.NotSupported:
                                    throw new NopException("Recurring payments are not supported by selected payment method");
                                case RecurringPaymentType.Manual:
                                    processPaymentResult = _paymentService.ProcessRecurringPayment(processPaymentRequest);
                                    break;
                                case RecurringPaymentType.Automatic:
                                    //payment is processed on payment gateway site
                                    processPaymentResult = new ProcessPaymentResult();
                                    break;
                                default:
                                    throw new NopException("Not supported recurring payment type");
                            }
                        }
                        else
                        {
                            throw new NopException("No recurring products");
                        }
                    }
                }
                else
                {
                    //payment is not required
                    if (processPaymentResult == null)
                        processPaymentResult = new ProcessPaymentResult();
                    processPaymentResult.NewPaymentStatus = PaymentStatus.Paid;
                }

                if (processPaymentResult == null)
                    throw new NopException("processPaymentResult is not available");

                #endregion

                if (processPaymentResult.Success)
                {

                    //save order in data storage
                    //uncomment this line to support transactions
                    //using (var scope = new System.Transactions.TransactionScope())
                    {
                        #region Save order details

                        var shippingStatus = ShippingStatus.NotYetShipped;
                        if (!shoppingCartRequiresShipping)
                            shippingStatus = ShippingStatus.ShippingNotRequired;

                        var order = new Order()
                        {
                            StoreId = processPaymentRequest.StoreId,
                            OrderGuid = processPaymentRequest.OrderGuid,
                            CustomerId = customer.Id,
                            CustomerLanguageId = customerLanguage.Id,
                            CustomerTaxDisplayType = customerTaxDisplayType,
                            CustomerIp = _webHelper.GetCurrentIpAddress(),
                            OrderSubtotalInclTax = orderSubTotalInclTax,
                            OrderSubtotalExclTax = orderSubTotalExclTax,
                            OrderSubTotalDiscountInclTax = orderSubTotalDiscountInclTax,
                            OrderSubTotalDiscountExclTax = orderSubTotalDiscountExclTax,
                            OrderShippingInclTax = orderShippingTotalInclTax.Value,
                            OrderShippingExclTax = orderShippingTotalExclTax.Value,
                            PaymentMethodAdditionalFeeInclTax = paymentAdditionalFeeInclTax,
                            PaymentMethodAdditionalFeeExclTax = paymentAdditionalFeeExclTax,
                            TaxRates = taxRates,
                            OrderTax = orderTaxTotal,
                            OrderTotal = orderTotal.Value,
                            RefundedAmount = decimal.Zero,
                            OrderDiscount = orderDiscountAmount,
                            CheckoutAttributeDescription = checkoutAttributeDescription,
                            CheckoutAttributesXml = checkoutAttributesXml,
                            CustomerCurrencyCode = customerCurrencyCode,
                            CurrencyRate = customerCurrencyRate,
                            AffiliateId = affiliateId,
                            OrderStatus = OrderStatus.Pending,
                            AllowStoringCreditCardNumber = processPaymentResult.AllowStoringCreditCardNumber,
                            CardType = processPaymentResult.AllowStoringCreditCardNumber ? _encryptionService.EncryptText(processPaymentRequest.CreditCardType) : string.Empty,
                            CardName = processPaymentResult.AllowStoringCreditCardNumber ? _encryptionService.EncryptText(processPaymentRequest.CreditCardName) : string.Empty,
                            CardNumber = processPaymentResult.AllowStoringCreditCardNumber ? _encryptionService.EncryptText(processPaymentRequest.CreditCardNumber) : string.Empty,
                            MaskedCreditCardNumber = _encryptionService.EncryptText(_paymentService.GetMaskedCreditCardNumber(processPaymentRequest.CreditCardNumber)),
                            CardCvv2 = processPaymentResult.AllowStoringCreditCardNumber ? _encryptionService.EncryptText(processPaymentRequest.CreditCardCvv2) : string.Empty,
                            CardExpirationMonth = processPaymentResult.AllowStoringCreditCardNumber ? _encryptionService.EncryptText(processPaymentRequest.CreditCardExpireMonth.ToString()) : string.Empty,
                            CardExpirationYear = processPaymentResult.AllowStoringCreditCardNumber ? _encryptionService.EncryptText(processPaymentRequest.CreditCardExpireYear.ToString()) : string.Empty,
                            PaymentMethodSystemName = processPaymentRequest.PaymentMethodSystemName,
                            AuthorizationTransactionId = processPaymentResult.AuthorizationTransactionId,
                            AuthorizationTransactionCode = processPaymentResult.AuthorizationTransactionCode,
                            AuthorizationTransactionResult = processPaymentResult.AuthorizationTransactionResult,
                            CaptureTransactionId = processPaymentResult.CaptureTransactionId,
                            CaptureTransactionResult = processPaymentResult.CaptureTransactionResult,
                            SubscriptionTransactionId = processPaymentResult.SubscriptionTransactionId,
                            PurchaseOrderNumber = processPaymentRequest.PurchaseOrderNumber,
                            PaymentStatus = processPaymentResult.NewPaymentStatus,
                            PaidDateUtc = null,
                            BillingAddress = billingAddress,
                            ShippingStatus = shippingStatus,
                            //ShippingMethod = shippingMethodName,
                            ShippingRateComputationMethodSystemName = shippingRateComputationMethodSystemName,
                            CustomValuesXml = processPaymentRequest.SerializeCustomValues(),
                            VatNumber = vatNumber,
                            SendToManyAddressSelected = customer.SendToManyAddressSelected,
                            CreatedOnUtc = DateTime.UtcNow
                        };
                        //if (order.SendToManyAddressSelected == false)
                        //    order.ShippingAddress = shippingAddress;
                        _orderService.InsertOrder(order);

                        result.PlacedOrder = order;

                        Shipment shipment = null;
                        decimal? totalWeight = null;
                        bool singleTrackingNumber = false;

                        if (!processPaymentRequest.IsRecurringPayment)
                        {
                            //move shopping cart items to order items
                            foreach (var sc in cart)
                            {
                                //prices
                                decimal taxRate = decimal.Zero;
                                decimal scUnitPrice = _priceCalculationService.GetUnitPrice(sc, true);
                                decimal scSubTotal = _priceCalculationService.GetSubTotal(sc, true, sc.SendToManyAddressSelected ?? false);
                                decimal scInternatiolPrice = _priceCalculationService.GetInternationalPrice(sc.Product, true);
                                decimal scUnitPriceInclTax = _taxService.GetProductPrice(sc.Product, scUnitPrice, true, customer, out taxRate);
                                decimal scUnitPriceExclTax = _taxService.GetProductPrice(sc.Product, scUnitPrice, false, customer, out taxRate);
                                decimal scInternatiolPriceInclTax = _taxService.GetProductPrice(sc.Product, scInternatiolPrice, true, customer, out taxRate);
                                decimal scInternatiolPriceExclTax = _taxService.GetProductPrice(sc.Product, scInternatiolPrice, false, customer, out taxRate);
                                decimal scSubTotalInclTax = _taxService.GetProductPrice(sc.Product, scSubTotal, true, customer, out taxRate);
                                decimal scSubTotalExclTax = _taxService.GetProductPrice(sc.Product, scSubTotal, false, customer, out taxRate);

                                //discounts
                                Discount scDiscount = null;
                                decimal discountAmount = _priceCalculationService.GetDiscountAmount(sc, out scDiscount);
                                decimal discountAmountInclTax = _taxService.GetProductPrice(sc.Product, discountAmount, true, customer, out taxRate);
                                decimal discountAmountExclTax = _taxService.GetProductPrice(sc.Product, discountAmount, false, customer, out taxRate);
                                if (scDiscount != null && !appliedDiscounts.ContainsDiscount(scDiscount))
                                    appliedDiscounts.Add(scDiscount);

                                //attributes
                                string attributeDescription = _productAttributeFormatter.FormatAttributes(sc.Product, sc.AttributesXml, customer);

                                var itemWeight = _shippingService.GetShoppingCartItemWeight(sc);

                                //save order item
                                var orderItem = new OrderItem()
                                {
                                    OrderItemGuid = Guid.NewGuid(),
                                    Order = order,
                                    ProductId = sc.ProductId,
                                    UnitPriceInclTax = scUnitPriceInclTax,
                                    UnitPriceExclTax = scUnitPriceExclTax,
                                    InternationalPriceInclTax = scInternatiolPriceInclTax,
                                    InternationalPriceExclTax = scInternatiolPriceExclTax,
                                    PriceInclTax = scSubTotalInclTax,
                                    PriceExclTax = scSubTotalExclTax,
                                    OriginalProductCost = _priceCalculationService.GetProductCost(sc.Product, sc.AttributesXml),
                                    AttributeDescription = attributeDescription,
                                    AttributesXml = sc.AttributesXml,
                                    Quantity = sc.Quantity,
                                    DiscountAmountInclTax = discountAmountInclTax,
                                    DiscountAmountExclTax = discountAmountExclTax,
                                    DownloadCount = 0,
                                    IsDownloadActivated = false,
                                    LicenseDownloadId = 0,
                                    ItemWeight = itemWeight,
                                    SendToManyAddressSelected = sc.SendToManyAddressSelected,
                                    CustomerCanEdit = false,
                                };
                               
                                //customizable Products Shipping Addresses
                                if (sc.Product.IsCustomizable)
                                {
                                    //multiple shipping addresses
                                    if ( sc.SendToManyAddressSelected == true)
                                    {
                                        if (!(sc.ShoppingCartItemAddresses.Count() > 0))
                                            throw new NopException("Shipping address is not provided");

                                        foreach (var sciAddress in sc.ShoppingCartItemAddresses)
                                        {
                                            var address = sciAddress.Address;

                                            if (!CommonHelper.IsValidEmail(address.Email))
                                                throw new NopException("Email is not valid");

                                            var orderItemShippingAddress = new OrderItemShippingAddress()
                                            {
                                                AddressId = sciAddress.Address_Id,
                                                Address = (Address)address.Clone(),
                                                Quantity = sciAddress.Quantity,
                                                OrderItemGuid = orderItem.OrderItemGuid,
                                                OrderItemId = orderItem.Id,
                                                OrderItem = orderItem,
                                                ShippingMethod = _localizationService.GetResource("ShoppingCart.CustomizedProducts.ShippingMethod"),
                                            };

                                            if (orderItemShippingAddress.Address.Country != null && !orderItemShippingAddress.Address.Country.AllowsShipping)
                                                throw new NopException(string.Format("Country '{0}' is not allowed for shipping", orderItemShippingAddress.Address.Country.Name));

                                            orderItem.OrderItemShippingAddresses.Add(orderItemShippingAddress);
                                        }
                                    }
                                    //single shipping address
                                    else if (sc.Product.IsShipEnabled == true && sc.SendToManyAddressSelected == false)
                                    {
                                        if (!(sc.ShoppingCartItemAddresses.Count() > 0))
                                            throw new NopException("Shipping address is not provided");

                                        var address = sc.ShoppingCartItemAddresses.FirstOrDefault().Address;

                                        if (!CommonHelper.IsValidEmail(address.Email))
                                            throw new NopException("Email is not valid");

                                        var orderItemShippingAddress = new OrderItemShippingAddress()
                                        {
                                            AddressId = sc.ShoppingCartItemAddresses.FirstOrDefault().Address_Id,
                                            Address = (Address)address.Clone(),
                                            Quantity = sc.ShoppingCartItemAddresses.FirstOrDefault().Quantity,
                                            OrderItemGuid = orderItem.OrderItemGuid,
                                            OrderItemId = orderItem.Id,
                                            OrderItem = orderItem,
                                            ShippingMethod = _localizationService.GetResource("ShoppingCart.CustomizedProducts.ShippingMethod"),
                                        };

                                        if (orderItemShippingAddress.Address.Country != null && !orderItemShippingAddress.Address.Country.AllowsShipping)
                                            throw new NopException(string.Format("Country '{0}' is not allowed for shipping", orderItemShippingAddress.Address.Country.Name));

                                        orderItem.OrderItemShippingAddresses.Add(orderItemShippingAddress);
                                    }
                                }
                                //non customizable Products Shipping Addresses
                                else
                                {
                                    //multiple shipping addresses
                                    if (order.SendToManyAddressSelected == true)
                                    {
                                        if ((sc.ShoppingCartItemAddresses.Count() > 0))
                                        {
                                            foreach (var sciAddress in sc.ShoppingCartItemAddresses)
                                            {
                                                var address = sciAddress.Address;
                                                var ItemId = (sc.Product.Height.ToString() + sc.Product.Width.ToString() + sc.Product.Length.ToString() + sc.Product.Weight.ToString() + ShippingOrigin.ToLower() + (address.StateName ?? address.CountryName)).GetHashCode();

                                                if (!CommonHelper.IsValidEmail(address.Email))
                                                    throw new NopException("Email is not valid");

                                                var orderItemShippingAddress = new OrderItemShippingAddress()
                                                {
                                                    AddressId = sciAddress.Address_Id,
                                                    Address = (Address)address.Clone(),
                                                    Quantity = sciAddress.Quantity,
                                                    OrderItemGuid = orderItem.OrderItemGuid,
                                                    OrderItemId = orderItem.Id,
                                                    OrderItem = orderItem,
                                                };
                                                //find selected options 
                                                if (ShippingMethodModel != null)
                                                {
                                                    foreach (var method in ShippingMethodModel.ItemsShippingMethods)
                                                    {
                                                        if (method.ItemId == ItemId)
                                                        {
                                                            foreach (var option in method.ShippingOptions)
                                                            {
                                                                if (option.Selected)
                                                                {
                                                                    orderItemShippingAddress.ShippingMethod = option.Name + " - " + option.ExpectedDeliveryTime;
                                                                    orderItemShippingAddress.ShippingMethodID = option.ID;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    orderItemShippingAddress.ShippingMethod = _localizationService.GetResource("ShoppingCart.CustomizedProducts.ShippingMethod");
                                                }

                                                if (orderItemShippingAddress.Address.Country != null && !orderItemShippingAddress.Address.Country.AllowsShipping)
                                                    throw new NopException(string.Format("Country '{0}' is not allowed for shipping", orderItemShippingAddress.Address.Country.Name));

                                                orderItem.OrderItemShippingAddresses.Add(orderItemShippingAddress);
                                            }
                                        }
                                    }
                                    //single shipping address
                                    else if (order.SendToManyAddressSelected == false)
                                    {
                                        if ((sc.ShoppingCartItemAddresses.Count() > 0))
                                        {
                                            var address = sc.ShoppingCartItemAddresses.FirstOrDefault().Address;
                                            var ItemId = (sc.Product.Height.ToString() + sc.Product.Width.ToString() + sc.Product.Length.ToString() + sc.Product.Weight.ToString() + ShippingOrigin.ToLower() + (address.StateName ?? address.CountryName)).GetHashCode();

                                            if (!CommonHelper.IsValidEmail(address.Email))
                                                throw new NopException("Email is not valid");

                                            var orderItemShippingAddress = new OrderItemShippingAddress()
                                            {
                                                AddressId = sc.ShoppingCartItemAddresses.FirstOrDefault().Address_Id,
                                                Address = (Address)address.Clone(),
                                                Quantity = sc.ShoppingCartItemAddresses.FirstOrDefault().Quantity,
                                                OrderItemGuid = orderItem.OrderItemGuid,
                                                OrderItemId = orderItem.Id,
                                                OrderItem = orderItem,
                                            };
                                            //find selected options 
                                            if (ShippingMethodModel != null)
                                            {
                                                foreach (var method in ShippingMethodModel.ItemsShippingMethods)
                                                {
                                                    if (method.ItemId == ItemId)
                                                    {
                                                        foreach (var option in method.ShippingOptions)
                                                        {
                                                            if (option.Selected)
                                                            {
                                                                orderItemShippingAddress.ShippingMethod = option.Name + " - " + option.ExpectedDeliveryTime;
                                                                orderItemShippingAddress.ShippingMethodID = option.ID;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                orderItemShippingAddress.ShippingMethod = "Emirates Post";
                                            }

                                            if (orderItemShippingAddress.Address.Country != null && !orderItemShippingAddress.Address.Country.AllowsShipping)
                                                throw new NopException(string.Format("Country '{0}' is not allowed for shipping", orderItemShippingAddress.Address.Country.Name));

                                            orderItem.OrderItemShippingAddresses.Add(orderItemShippingAddress);
                                        }
                                    }
                                }
                                
                                order.OrderItems.Add(orderItem);
                                _orderService.UpdateOrder(order);


                                //Add shippment For each item
                                //only cutomized products in shopping cart
                                if(OnlyCustomizedProducts)
                                {
                                    //in case of multiple addresses a tracking number is added for each//add element by element with its address quantity
                                    //in case of single address a single tracking number is added//add all elements with its single address
                                    foreach (var orderItemAddress in orderItem.OrderItemShippingAddresses)
                                    {
                                        var orderItemWeight = orderItem.ItemWeight.HasValue ? orderItem.ItemWeight * orderItemAddress.Quantity : null;
                                        var trackingNumber = Guid.NewGuid().ToString();
                                        shipment = new Shipment()
                                        {
                                            OrderId = order.Id,
                                            TrackingNumber = trackingNumber,
                                            TotalWeight = orderItemWeight,
                                            ReadyToBeShippedDateUtc = null,
                                            ShippedDateUtc = null,
                                            DeliveryDateUtc = null,
                                            CreatedOnUtc = DateTime.UtcNow,
                                            EPGShippingStatusId = (int)EPGShippingStatus.NotShippedYet,
                                            AddressId = orderItemAddress.AddressId,
                                            Address = orderItemAddress.Address,
                                            ShippingMethod = orderItemAddress.ShippingMethod,
                                            ShippingMethodID = orderItemAddress.ShippingMethodID
                                        };

                                        //create a shipment item for each item in the quentity                                    
                                        var shipmentItem = new ShipmentItem()
                                        {
                                            OrderItemId = orderItem.Id,
                                            Quantity = orderItemAddress.Quantity,
                                        };
                                        shipment.ShipmentItems.Add(shipmentItem);

                                        //_shipmentService.InsertShipment(shipment);
                                    }
                                }
                                //not only cutomized products in shopping cart
                                else
                                {
                                    //in case of multiple addresses a tracking number is added for each
                                    if (order.SendToManyAddressSelected == true)
                                    {
                                        //a send to me customizable product in case of multiple addresses cart
                                        if (sc.Product.IsShipEnabled==true && sc.Product.IsCustomizable )
                                        {
                                            var orderItemWeight = orderItem.ItemWeight.HasValue ? orderItem.ItemWeight * orderItem.Quantity : null;

                                            var trackingNumber = Guid.NewGuid().ToString();
                                            shipment = new Shipment()
                                            {
                                                OrderId = order.Id,
                                                TrackingNumber = trackingNumber,
                                                TotalWeight = orderItemWeight,
                                                ReadyToBeShippedDateUtc = null,
                                                ShippedDateUtc = null,
                                                DeliveryDateUtc = null,
                                                CreatedOnUtc = DateTime.UtcNow,
                                                EPGShippingStatusId = (int)EPGShippingStatus.NotShippedYet,
                                                AddressId = orderItem.OrderItemShippingAddresses.FirstOrDefault().AddressId,
                                                Address = orderItem.OrderItemShippingAddresses.FirstOrDefault().Address,
                                                ShippingMethod = orderItem.OrderItemShippingAddresses.FirstOrDefault().ShippingMethod,
                                                ShippingMethodID = orderItem.OrderItemShippingAddresses.FirstOrDefault().ShippingMethodID
                                            };

                                            //create a shipment items for each item in the quantity
                                            var shipmentItem = new ShipmentItem()
                                            {
                                                OrderItemId = orderItem.Id,
                                                Quantity = orderItem.Quantity,
                                            };
                                            shipment.ShipmentItems.Add(shipmentItem);

                                            //_shipmentService.InsertShipment(shipment);
                                        }
                                        else
                                        {
                                            foreach (var orderItemAddress in orderItem.OrderItemShippingAddresses)
                                            {
                                                var orderItemWeight = orderItem.ItemWeight.HasValue ? orderItem.ItemWeight * orderItemAddress.Quantity : null;
                                                var trackingNumber = Guid.NewGuid().ToString();
                                                shipment = new Shipment()
                                                {
                                                    OrderId = order.Id,
                                                    TrackingNumber = trackingNumber,
                                                    TotalWeight = orderItemWeight,
                                                    ReadyToBeShippedDateUtc = null,
                                                    ShippedDateUtc = null,
                                                    DeliveryDateUtc = null,
                                                    CreatedOnUtc = DateTime.UtcNow,
                                                    EPGShippingStatusId = (int)EPGShippingStatus.NotShippedYet,
                                                    AddressId = orderItemAddress.AddressId,
                                                    Address = orderItemAddress.Address,
                                                    ShippingMethod = orderItemAddress.ShippingMethod,
                                                    ShippingMethodID = orderItemAddress.ShippingMethodID
                                                };

                                                //create a shipment item for each item in the quentity                                    
                                                var shipmentItem = new ShipmentItem()
                                                {
                                                    OrderItemId = orderItem.Id,
                                                    Quantity = orderItemAddress.Quantity,
                                                };
                                                shipment.ShipmentItems.Add(shipmentItem);

                                                //_shipmentService.InsertShipment(shipment);
                                            }
                                        }
                                    }
                                    //in case of single address a single tracking number is added
                                    else if (order.SendToManyAddressSelected == false)
                                    {
                                        //a customizable product in case of single address cart
                                        if (sc.Product.IsCustomizable)
                                        {
                                            //a send for me customizable product in case of single address cart//add element by element with its address quantity
                                            //a send to me customizable product in case of single address cart//add all elements with its single address
                                            foreach (var orderItemAddress in orderItem.OrderItemShippingAddresses)
                                            {
                                                var orderItemWeight = orderItem.ItemWeight.HasValue ? orderItem.ItemWeight * orderItemAddress.Quantity : null;
                                                var trackingNumber = Guid.NewGuid().ToString();
                                                var manyShipments = new Shipment()
                                                {
                                                    OrderId = order.Id,
                                                    TrackingNumber = trackingNumber,
                                                    TotalWeight = orderItemWeight,
                                                    ReadyToBeShippedDateUtc = null,
                                                    ShippedDateUtc = null,
                                                    DeliveryDateUtc = null,
                                                    CreatedOnUtc = DateTime.UtcNow,
                                                    EPGShippingStatusId = (int)EPGShippingStatus.NotShippedYet,
                                                    AddressId = orderItemAddress.AddressId,
                                                    Address = orderItemAddress.Address,
                                                    ShippingMethod = orderItemAddress.ShippingMethod,
                                                    ShippingMethodID = orderItemAddress.ShippingMethodID
                                                };

                                                //create a shipment item for each item in the quentity                                    
                                                var shipmentItem = new ShipmentItem()
                                                {
                                                    OrderItemId = orderItem.Id,
                                                    Quantity = orderItemAddress.Quantity,
                                                };
                                                manyShipments.ShipmentItems.Add(shipmentItem);

                                                //_shipmentService.InsertShipment(manyShipments);
                                            }
                                        }
                                        else
                                        {
                                            singleTrackingNumber = true;
                                            var orderItemTotalWeight = orderItem.ItemWeight.HasValue ? orderItem.ItemWeight * orderItem.Quantity : null;
                                            if (orderItemTotalWeight.HasValue)
                                            {
                                                if (!totalWeight.HasValue)
                                                    totalWeight = 0;
                                                totalWeight += orderItemTotalWeight.Value;
                                            }
                                            if (shipment == null)
                                            {
                                                var trackingNumber = Guid.NewGuid().ToString();
                                                if (orderItem.OrderItemShippingAddresses.Count() > 0)
                                                {
                                                    shipment = new Shipment()
                                                    {
                                                        OrderId = order.Id,
                                                        TrackingNumber = trackingNumber,
                                                        TotalWeight = orderItemTotalWeight,
                                                        ReadyToBeShippedDateUtc = null,
                                                        ShippedDateUtc = null,
                                                        DeliveryDateUtc = null,
                                                        CreatedOnUtc = DateTime.UtcNow,
                                                        EPGShippingStatusId = (int)EPGShippingStatus.NotShippedYet,
                                                        AddressId = orderItem.OrderItemShippingAddresses.FirstOrDefault().AddressId,
                                                        Address = orderItem.OrderItemShippingAddresses.FirstOrDefault().Address,
                                                        ShippingMethod = orderItem.OrderItemShippingAddresses.FirstOrDefault().ShippingMethod,
                                                        ShippingMethodID = orderItem.OrderItemShippingAddresses.FirstOrDefault().ShippingMethodID
                                                    };
                                                    //create a shipment item
                                                    var shipmentItem = new ShipmentItem()
                                                    {
                                                        OrderItemId = orderItem.Id,
                                                        Quantity = orderItem.Quantity,
                                                    };
                                                    shipment.ShipmentItems.Add(shipmentItem);
                                                }
                                            }
                                        }
                                    }
                                }
                                

                                //gift cards
                                if (sc.Product.IsGiftCard)
                                {
                                    string giftCardRecipientName, giftCardRecipientEmail,
                                        giftCardSenderName, giftCardSenderEmail, giftCardMessage;
                                    _productAttributeParser.GetGiftCardAttribute(sc.AttributesXml,
                                        out giftCardRecipientName, out giftCardRecipientEmail,
                                        out giftCardSenderName, out giftCardSenderEmail, out giftCardMessage);

                                    for (int i = 0; i < sc.Quantity; i++)
                                    {
                                        var gc = new GiftCard()
                                        {
                                            GiftCardType = sc.Product.GiftCardType,
                                            PurchasedWithOrderItem = orderItem,
                                            Amount = scUnitPriceExclTax,
                                            IsGiftCardActivated = false,
                                            GiftCardCouponCode = _giftCardService.GenerateGiftCardCode(),
                                            RecipientName = giftCardRecipientName,
                                            RecipientEmail = giftCardRecipientEmail,
                                            SenderName = giftCardSenderName,
                                            SenderEmail = giftCardSenderEmail,
                                            Message = giftCardMessage,
                                            IsRecipientNotified = false,
                                            CreatedOnUtc = DateTime.UtcNow
                                        };
                                        _giftCardService.InsertGiftCard(gc);
                                    }
                                }

                                //inventory
                                _productService.AdjustInventory(sc.Product, true, sc.Quantity, sc.AttributesXml);
                            }

                            //if we have at least one item in the shipment, then save it
                            if (shipment != null && shipment.ShipmentItems.Count > 0 && singleTrackingNumber)
                            {
                                shipment.TotalWeight = totalWeight;
                                //_shipmentService.InsertShipment(shipment);
                            }

                            //clear shopping cart
                            cart.ToList().ForEach(sci => _shoppingCartService.DeleteShoppingCartItem(sci, false));
                        }
                        else
                        {
                            //recurring payment
                            var initialOrderItems = initialOrder.OrderItems;
                            foreach (var orderItem in initialOrderItems)
                            {
                                //save item
                                var newOrderItem = new OrderItem()
                                {
                                    OrderItemGuid = Guid.NewGuid(),
                                    Order = order,
                                    ProductId = orderItem.ProductId,
                                    UnitPriceInclTax = orderItem.UnitPriceInclTax,
                                    UnitPriceExclTax = orderItem.UnitPriceExclTax,
                                    InternationalPriceInclTax = orderItem.InternationalPriceInclTax,
                                    InternationalPriceExclTax = orderItem.InternationalPriceExclTax,
                                    PriceInclTax = orderItem.PriceInclTax,
                                    PriceExclTax = orderItem.PriceExclTax,
                                    OriginalProductCost = orderItem.OriginalProductCost,
                                    AttributeDescription = orderItem.AttributeDescription,
                                    AttributesXml = orderItem.AttributesXml,
                                    Quantity = orderItem.Quantity,
                                    DiscountAmountInclTax = orderItem.DiscountAmountInclTax,
                                    DiscountAmountExclTax = orderItem.DiscountAmountExclTax,
                                    DownloadCount = 0,
                                    IsDownloadActivated = false,
                                    LicenseDownloadId = 0,
                                    ItemWeight = orderItem.ItemWeight,
                                    SendToManyAddressSelected = orderItem.SendToManyAddressSelected,
                                    CustomerCanEdit = orderItem.CustomerCanEdit,
                                };

                                //customizable Products Shipping Addresses
                                if (orderItem.Product.IsCustomizable)
                                {
                                    //multiple shipping addresses
                                    if (orderItem.SendToManyAddressSelected == true)
                                    {
                                        if (!(orderItem.OrderItemShippingAddresses.Count() > 0))
                                            throw new NopException("Shipping address is not provided");

                                        foreach (var orderItemAddress in orderItem.OrderItemShippingAddresses)
                                        {
                                            var address = orderItemAddress.Address;

                                            if (!CommonHelper.IsValidEmail(address.Email))
                                                throw new NopException("Email is not valid");

                                            var orderItemShippingAddress = new OrderItemShippingAddress()
                                            {
                                                AddressId = orderItemAddress.AddressId,
                                                Address = (Address)address.Clone(),
                                                Quantity = orderItemAddress.Quantity,
                                                OrderItemGuid = orderItem.OrderItemGuid,
                                                OrderItemId = orderItem.Id,
                                                OrderItem = orderItem,
                                                ShippingMethod = orderItemAddress.ShippingMethod,
                                                ShippingMethodID = orderItemAddress.ShippingMethodID
                                            };

                                            if (orderItemShippingAddress.Address.Country != null && !orderItemShippingAddress.Address.Country.AllowsShipping)
                                                throw new NopException(string.Format("Country '{0}' is not allowed for shipping", orderItemShippingAddress.Address.Country.Name));

                                            newOrderItem.OrderItemShippingAddresses.Add(orderItemShippingAddress);
                                        }
                                    }
                                    //single shipping address
                                    else if (orderItem.SendToManyAddressSelected == false)
                                    {
                                        if (!(orderItem.OrderItemShippingAddresses.Count() > 0))
                                            throw new NopException("Shipping address is not provided");

                                        var address = orderItem.OrderItemShippingAddresses.FirstOrDefault().Address;

                                        if (!CommonHelper.IsValidEmail(address.Email))
                                            throw new NopException("Email is not valid");

                                        var orderItemShippingAddress = new OrderItemShippingAddress()
                                        {
                                            AddressId = orderItem.OrderItemShippingAddresses.FirstOrDefault().AddressId,
                                            Address = (Address)address.Clone(),
                                            Quantity = orderItem.OrderItemShippingAddresses.FirstOrDefault().Quantity,
                                            OrderItemGuid = orderItem.OrderItemGuid,
                                            OrderItemId = orderItem.Id,
                                            OrderItem = orderItem,
                                            ShippingMethod = orderItem.OrderItemShippingAddresses.FirstOrDefault().ShippingMethod,
                                            ShippingMethodID = orderItem.OrderItemShippingAddresses.FirstOrDefault().ShippingMethodID
                                        };

                                        if (orderItemShippingAddress.Address.Country != null && !orderItemShippingAddress.Address.Country.AllowsShipping)
                                            throw new NopException(string.Format("Country '{0}' is not allowed for shipping", orderItemShippingAddress.Address.Country.Name));

                                        newOrderItem.OrderItemShippingAddresses.Add(orderItemShippingAddress);
                                    }
                                }
                                //Non customizable Products Shipping Addresses
                                else
                                {
                                    //multiple shipping addresses
                                    if (order.SendToManyAddressSelected == true)
                                    {
                                        if (!(orderItem.OrderItemShippingAddresses.Count() > 0))
                                            throw new NopException("Shipping address is not provided");

                                        foreach (var orderItemAddress in orderItem.OrderItemShippingAddresses)
                                        {
                                            var address = orderItemAddress.Address;

                                            if (!CommonHelper.IsValidEmail(address.Email))
                                                throw new NopException("Email is not valid");

                                            var orderItemShippingAddress = new OrderItemShippingAddress()
                                            {
                                                AddressId = orderItemAddress.AddressId,
                                                Address = (Address)address.Clone(),
                                                Quantity = orderItemAddress.Quantity,
                                                OrderItemGuid = orderItem.OrderItemGuid,
                                                OrderItemId = orderItem.Id,
                                                OrderItem = orderItem,
                                                ShippingMethod = orderItemAddress.ShippingMethod,
                                                ShippingMethodID = orderItemAddress.ShippingMethodID
                                            };

                                            if (orderItemShippingAddress.Address.Country != null && !orderItemShippingAddress.Address.Country.AllowsShipping)
                                                throw new NopException(string.Format("Country '{0}' is not allowed for shipping", orderItemShippingAddress.Address.Country.Name));

                                            newOrderItem.OrderItemShippingAddresses.Add(orderItemShippingAddress);
                                        }
                                    }
                                    //single shipping address
                                    else if (order.SendToManyAddressSelected == false)
                                    {
                                        if (!(orderItem.OrderItemShippingAddresses.Count() > 0))
                                            throw new NopException("Shipping address is not provided");

                                        var address = orderItem.OrderItemShippingAddresses.FirstOrDefault().Address;

                                        if (!CommonHelper.IsValidEmail(address.Email))
                                            throw new NopException("Email is not valid");

                                        var orderItemShippingAddress = new OrderItemShippingAddress()
                                        {
                                            AddressId = orderItem.OrderItemShippingAddresses.FirstOrDefault().AddressId,
                                            Address = (Address)address.Clone(),
                                            Quantity = orderItem.OrderItemShippingAddresses.FirstOrDefault().Quantity,
                                            OrderItemGuid = orderItem.OrderItemGuid,
                                            OrderItemId = orderItem.Id,
                                            OrderItem = orderItem,
                                            ShippingMethod = orderItem.OrderItemShippingAddresses.FirstOrDefault().ShippingMethod,
                                            ShippingMethodID = orderItem.OrderItemShippingAddresses.FirstOrDefault().ShippingMethodID
                                        };

                                        if (orderItemShippingAddress.Address.Country != null && !orderItemShippingAddress.Address.Country.AllowsShipping)
                                            throw new NopException(string.Format("Country '{0}' is not allowed for shipping", orderItemShippingAddress.Address.Country.Name));

                                        newOrderItem.OrderItemShippingAddresses.Add(orderItemShippingAddress);
                                    }
                                }                                                               

                                order.OrderItems.Add(newOrderItem);
                                _orderService.UpdateOrder(order);


                                //Add shippment For each item
                                //only cutomized products in shopping cart
                                if (OnlyCustomizedProducts)
                                {
                                    //in case of multiple addresses a tracking number is added for each//add element by element with its address quantity
                                    //in case of single address a single tracking number is added//add all elements with its single address
                                    foreach (var newOrderItemAddress in newOrderItem.OrderItemShippingAddresses)
                                    {
                                        var orderItemWeight = newOrderItem.ItemWeight.HasValue ? newOrderItem.ItemWeight * newOrderItemAddress.Quantity : null;
                                        var trackingNumber = Guid.NewGuid().ToString();
                                        shipment = new Shipment()
                                        {
                                            OrderId = order.Id,
                                            TrackingNumber = trackingNumber,
                                            TotalWeight = orderItemWeight,
                                            ReadyToBeShippedDateUtc = null,
                                            ShippedDateUtc = null,
                                            DeliveryDateUtc = null,
                                            CreatedOnUtc = DateTime.UtcNow,
                                            EPGShippingStatusId = (int)EPGShippingStatus.NotShippedYet,
                                            AddressId = newOrderItemAddress.AddressId,
                                            Address = newOrderItemAddress.Address,
                                            ShippingMethod = newOrderItemAddress.ShippingMethod,
                                            ShippingMethodID = newOrderItemAddress.ShippingMethodID
                                        };

                                        //create a shipment item for each item in the quentity                                    
                                        var shipmentItem = new ShipmentItem()
                                        {
                                            OrderItemId = newOrderItem.Id,
                                            Quantity = newOrderItemAddress.Quantity,
                                        };
                                        shipment.ShipmentItems.Add(shipmentItem);

                                        //_shipmentService.InsertShipment(shipment);
                                    }
                                }
                                //not only cutomized products in shopping cart
                                else
                                {
                                    //in case of multiple addresses a tracking number is added for each
                                    if (order.SendToManyAddressSelected == true)
                                    {
                                        //a send to me customizable product in case of multiple addresses cart
                                        if (orderItem.Product.IsCustomizable && newOrderItem.SendToManyAddressSelected == false)
                                        {
                                            var orderItemWeight = newOrderItem.ItemWeight.HasValue ? newOrderItem.ItemWeight * newOrderItem.Quantity : null;
                                            if (newOrderItem.OrderItemShippingAddresses.Count() > 0)
                                            {
                                                var trackingNumber = Guid.NewGuid().ToString();
                                                shipment = new Shipment()
                                                {
                                                    OrderId = order.Id,
                                                    TrackingNumber = trackingNumber,
                                                    TotalWeight = orderItemWeight,
                                                    ReadyToBeShippedDateUtc = null,
                                                    ShippedDateUtc = null,
                                                    DeliveryDateUtc = null,
                                                    CreatedOnUtc = DateTime.UtcNow,
                                                    EPGShippingStatusId = (int)EPGShippingStatus.NotShippedYet,
                                                    AddressId = newOrderItem.OrderItemShippingAddresses.FirstOrDefault().AddressId,
                                                    Address = newOrderItem.OrderItemShippingAddresses.FirstOrDefault().Address,
                                                    ShippingMethod = newOrderItem.OrderItemShippingAddresses.FirstOrDefault().ShippingMethod,
                                                    ShippingMethodID = newOrderItem.OrderItemShippingAddresses.FirstOrDefault().ShippingMethodID
                                                };

                                                //create a shipment item for each item in the quentity                                    
                                                var shipmentItem = new ShipmentItem()
                                                {
                                                    OrderItemId = newOrderItem.Id,
                                                    Quantity = newOrderItem.Quantity,
                                                };
                                                shipment.ShipmentItems.Add(shipmentItem);
                                            }
                                            //_shipmentService.InsertShipment(shipment);
                                        }
                                        else
                                        {
                                            foreach (var newOrderItemAddress in newOrderItem.OrderItemShippingAddresses)
                                            {
                                                var orderItemWeight = newOrderItem.ItemWeight.HasValue ? newOrderItem.ItemWeight * newOrderItemAddress.Quantity: null;
                                                var trackingNumber = Guid.NewGuid().ToString();
                                                shipment = new Shipment()
                                                {
                                                    OrderId = order.Id,
                                                    TrackingNumber = trackingNumber,
                                                    TotalWeight = orderItemWeight,
                                                    ReadyToBeShippedDateUtc = null,
                                                    ShippedDateUtc = null,
                                                    DeliveryDateUtc = null,
                                                    CreatedOnUtc = DateTime.UtcNow,
                                                    EPGShippingStatusId = (int)EPGShippingStatus.NotShippedYet,
                                                    AddressId = newOrderItemAddress.AddressId,
                                                    Address = newOrderItemAddress.Address,
                                                    ShippingMethod = newOrderItemAddress.ShippingMethod,
                                                    ShippingMethodID = newOrderItemAddress.ShippingMethodID
                                                };

                                                //create a shipment item for each item in the quentity                                    
                                                var shipmentItem = new ShipmentItem()
                                                {
                                                    OrderItemId = newOrderItem.Id,
                                                    Quantity = newOrderItemAddress.Quantity,
                                                };
                                                shipment.ShipmentItems.Add(shipmentItem);

                                                //_shipmentService.InsertShipment(shipment);
                                            }
                                        }
                                    }
                                    //in case of single address a single tracking number is added
                                    else if (order.SendToManyAddressSelected == false)
                                    {
                                        //a customizable product in case of single address cart
                                        if (orderItem.Product.IsCustomizable)
                                        {
                                            //a send for me customizable product in case of single address cart//add element by element with its address quantity
                                            //a send to me customizable product in case of single address cart//add all elements with its single address
                                            foreach (var newOrderItemAddress in newOrderItem.OrderItemShippingAddresses)
                                            {
                                                var orderItemWeight = newOrderItem.ItemWeight.HasValue ? newOrderItem.ItemWeight * newOrderItemAddress.Quantity : null;
                                                var trackingNumber = Guid.NewGuid().ToString();
                                                var manyShipments = new Shipment()
                                                {
                                                    OrderId = order.Id,
                                                    TrackingNumber = trackingNumber,
                                                    TotalWeight = orderItemWeight,
                                                    ReadyToBeShippedDateUtc = null,
                                                    ShippedDateUtc = null,
                                                    DeliveryDateUtc = null,
                                                    CreatedOnUtc = DateTime.UtcNow,
                                                    EPGShippingStatusId = (int)EPGShippingStatus.NotShippedYet,
                                                    AddressId = newOrderItemAddress.AddressId,
                                                    Address = newOrderItemAddress.Address,
                                                    ShippingMethod = newOrderItemAddress.ShippingMethod,
                                                    ShippingMethodID = newOrderItemAddress.ShippingMethodID
                                                };

                                                //create a shipment item for each item in the quentity                                    
                                                var shipmentItem = new ShipmentItem()
                                                {
                                                    OrderItemId = newOrderItem.Id,
                                                    Quantity = newOrderItemAddress.Quantity,
                                                };
                                                manyShipments.ShipmentItems.Add(shipmentItem);

                                                //_shipmentService.InsertShipment(manyShipments);
                                            }
                                        }
                                        else
                                        {
                                            singleTrackingNumber = true;
                                            var orderItemTotalWeight = newOrderItem.ItemWeight.HasValue ? newOrderItem.ItemWeight * newOrderItem.Quantity : null;
                                            if (orderItemTotalWeight.HasValue)
                                            {
                                                if (!totalWeight.HasValue)
                                                    totalWeight = 0;
                                                totalWeight += orderItemTotalWeight.Value;
                                            }
                                            if (shipment == null)
                                            {
                                                if (newOrderItem.OrderItemShippingAddresses.Count() > 0)
                                                {
                                                    var trackingNumber = Guid.NewGuid().ToString();
                                                    shipment = new Shipment()
                                                    {
                                                        OrderId = order.Id,
                                                        TrackingNumber = trackingNumber,
                                                        TotalWeight = orderItemTotalWeight,
                                                        ReadyToBeShippedDateUtc = null,
                                                        ShippedDateUtc = null,
                                                        DeliveryDateUtc = null,
                                                        CreatedOnUtc = DateTime.UtcNow,
                                                        EPGShippingStatusId = (int)EPGShippingStatus.NotShippedYet,
                                                        AddressId = newOrderItem.OrderItemShippingAddresses.FirstOrDefault().AddressId,
                                                        Address = newOrderItem.OrderItemShippingAddresses.FirstOrDefault().Address,
                                                        ShippingMethod = newOrderItem.OrderItemShippingAddresses.FirstOrDefault().ShippingMethod,
                                                        ShippingMethodID = newOrderItem.OrderItemShippingAddresses.FirstOrDefault().ShippingMethodID
                                                    };
                                                    //create a shipment item
                                                    var shipmentItem = new ShipmentItem()
                                                    {
                                                        OrderItemId = newOrderItem.Id,
                                                        Quantity = newOrderItem.Quantity,
                                                    };
                                                    shipment.ShipmentItems.Add(shipmentItem);
                                                }
                                            }

                                           
                                        }
                                    }
                                }

                                //gift cards
                                if (orderItem.Product.IsGiftCard)
                                {
                                    string giftCardRecipientName, giftCardRecipientEmail,
                                        giftCardSenderName, giftCardSenderEmail, giftCardMessage;
                                    _productAttributeParser.GetGiftCardAttribute(orderItem.AttributesXml,
                                        out giftCardRecipientName, out giftCardRecipientEmail,
                                        out giftCardSenderName, out giftCardSenderEmail, out giftCardMessage);

                                    for (int i = 0; i < orderItem.Quantity; i++)
                                    {
                                        var gc = new GiftCard()
                                        {
                                            GiftCardType = orderItem.Product.GiftCardType,
                                            PurchasedWithOrderItem = newOrderItem,
                                            Amount = orderItem.UnitPriceExclTax,
                                            IsGiftCardActivated = false,
                                            GiftCardCouponCode = _giftCardService.GenerateGiftCardCode(),
                                            RecipientName = giftCardRecipientName,
                                            RecipientEmail = giftCardRecipientEmail,
                                            SenderName = giftCardSenderName,
                                            SenderEmail = giftCardSenderEmail,
                                            Message = giftCardMessage,
                                            IsRecipientNotified = false,
                                            CreatedOnUtc = DateTime.UtcNow
                                        };
                                        _giftCardService.InsertGiftCard(gc);
                                    }
                                }

                                //inventory
                                _productService.AdjustInventory(orderItem.Product, true, orderItem.Quantity, orderItem.AttributesXml);
                            }

                            //if we have at least one item in the shipment, then save it
                            if (shipment != null && shipment.ShipmentItems.Count > 0 && singleTrackingNumber)
                            {
                                shipment.TotalWeight = totalWeight;
                                //_shipmentService.InsertShipment(shipment);
                            }
                        }

                        //discount usage history
                        if (!processPaymentRequest.IsRecurringPayment)
                            foreach (var discount in appliedDiscounts)
                            {
                                var duh = new DiscountUsageHistory()
                                {
                                    Discount = discount,
                                    Order = order,
                                    CreatedOnUtc = DateTime.UtcNow
                                };
                                _discountService.InsertDiscountUsageHistory(duh);
                            }

                        //gift card usage history
                        if (!processPaymentRequest.IsRecurringPayment)
                            if (appliedGiftCards != null)
                                foreach (var agc in appliedGiftCards)
                                {
                                    decimal amountUsed = agc.AmountCanBeUsed;
                                    var gcuh = new GiftCardUsageHistory()
                                    {
                                        GiftCard = agc.GiftCard,
                                        UsedWithOrder = order,
                                        UsedValue = amountUsed,
                                        CreatedOnUtc = DateTime.UtcNow
                                    };
                                    agc.GiftCard.GiftCardUsageHistory.Add(gcuh);
                                    _giftCardService.UpdateGiftCard(agc.GiftCard);
                                }

                        //reward points history
                        if (redeemedRewardPointsAmount > decimal.Zero)
                        {
                            customer.AddRewardPointsHistoryEntry(-redeemedRewardPoints,
                                string.Format(_localizationService.GetResource("RewardPoints.Message.RedeemedForOrder", order.CustomerLanguageId), order.Id),
                                order,
                                redeemedRewardPointsAmount);
                            _customerService.UpdateCustomer(customer);
                        }

                        //recurring orders
                        if (!processPaymentRequest.IsRecurringPayment && isRecurringShoppingCart)
                        {
                            //create recurring payment (the first payment)
                            var rp = new RecurringPayment()
                            {
                                CycleLength = processPaymentRequest.RecurringCycleLength,
                                CyclePeriod = processPaymentRequest.RecurringCyclePeriod,
                                TotalCycles = processPaymentRequest.RecurringTotalCycles,
                                StartDateUtc = DateTime.UtcNow,
                                IsActive = true,
                                CreatedOnUtc = DateTime.UtcNow,
                                InitialOrder = order,
                            };
                            _orderService.InsertRecurringPayment(rp);


                            var recurringPaymentType = _paymentService.GetRecurringPaymentType(processPaymentRequest.PaymentMethodSystemName);
                            switch (recurringPaymentType)
                            {
                                case RecurringPaymentType.NotSupported:
                                    {
                                        //not supported
                                    }
                                    break;
                                case RecurringPaymentType.Manual:
                                    {
                                        //first payment
                                        var rph = new RecurringPaymentHistory()
                                        {
                                            RecurringPayment = rp,
                                            CreatedOnUtc = DateTime.UtcNow,
                                            OrderId = order.Id,
                                        };
                                        rp.RecurringPaymentHistory.Add(rph);
                                        _orderService.UpdateRecurringPayment(rp);
                                    }
                                    break;
                                case RecurringPaymentType.Automatic:
                                    {
                                        //will be created later (process is automated)
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }

                        #endregion

                        #region Notifications & notes

                        //notes, messages
                        if (_workContext.OriginalCustomerIfImpersonated != null)
                        {
                            //this order is placed by a store administrator impersonating a customer
                            order.OrderNotes.Add(new OrderNote()
                            {
                                Note = string.Format( "Order placed by a store owner ('{0}'. ID = {1}) impersonating the customer.",
                                    _workContext.OriginalCustomerIfImpersonated.Email, _workContext.OriginalCustomerIfImpersonated.Id),
                                DisplayToCustomer = false,
                                CreatedOnUtc = DateTime.UtcNow
                            });
                            _orderService.UpdateOrder(order);
                        }
                        else
                        {
                            order.OrderNotes.Add(new OrderNote()
                            {
                                Note = "Order placed",
                                DisplayToCustomer = false,
                                CreatedOnUtc = DateTime.UtcNow
                            });
                            _orderService.UpdateOrder(order);
                        }


                        //send email notifications after the payment process happens to include the order status
                        //int orderPlacedStoreOwnerNotificationQueuedEmailId = _workflowMessageService.SendOrderPlacedStoreOwnerNotification(order, _localizationSettings.DefaultAdminLanguageId);
                        //if (orderPlacedStoreOwnerNotificationQueuedEmailId > 0)
                        //{
                        //    order.OrderNotes.Add(new OrderNote()
                        //    {
                        //        Note = string.Format("\"Order placed\" email (to store owner) has been queued. Queued email identifier: {0}.", orderPlacedStoreOwnerNotificationQueuedEmailId),
                        //        DisplayToCustomer = false,
                        //        CreatedOnUtc = DateTime.UtcNow
                        //    });
                        //    _orderService.UpdateOrder(order);
                        //}

                        //var orderPlacedAttachmentFilePath = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
                        //    _pdfService.PrintOrderToPdf(order, 0) : null;
                        //var orderPlacedAttachmentFileName = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
                        //    "order.pdf" : null;
                        //int orderPlacedCustomerNotificationQueuedEmailId = _workflowMessageService
                        //    .SendOrderPlacedCustomerNotification(order, order.CustomerLanguageId, orderPlacedAttachmentFilePath, orderPlacedAttachmentFileName);
                        //if (orderPlacedCustomerNotificationQueuedEmailId > 0)
                        //{
                        //    order.OrderNotes.Add(new OrderNote()
                        //    {
                        //        Note = string.Format("\"Order placed\" email (to customer) has been queued. Queued email identifier: {0}.", orderPlacedCustomerNotificationQueuedEmailId),
                        //        DisplayToCustomer = false,
                        //        CreatedOnUtc = DateTime.UtcNow
                        //    });
                        //    _orderService.UpdateOrder(order);
                        //}

                        var vendors = new List<Vendor>();
                        foreach (var orderItem in order.OrderItems)
                        {
                            var vendorId = orderItem.Product.VendorId;
                            //find existing
                            var vendor = vendors.FirstOrDefault(v => v.Id == vendorId);
                            if (vendor == null)
                            {
                                //not found. load by Id
                                vendor = _vendorService.GetVendorById(vendorId);
                                if (vendor != null && !vendor.Deleted && vendor.Active)
                                {
                                    vendors.Add(vendor);
                                }
                            }
                        }
                        foreach (var vendor in vendors)
                        {
                            int orderPlacedVendorNotificationQueuedEmailId = _workflowMessageService.SendOrderPlacedVendorNotification(order, vendor, order.CustomerLanguageId);
                            if (orderPlacedVendorNotificationQueuedEmailId > 0)
                            {
                                order.OrderNotes.Add(new OrderNote()
                                {
                                    Note = string.Format("\"Order placed\" email (to vendor) has been queued. Queued email identifier: {0}.", orderPlacedVendorNotificationQueuedEmailId),
                                    DisplayToCustomer = false,
                                    CreatedOnUtc = DateTime.UtcNow
                                });
                                _orderService.UpdateOrder(order);
                            }
                        }

                        //check order status
                        CheckOrderStatus(order);

                        //reset checkout data
                        if (!processPaymentRequest.IsRecurringPayment)
                            _customerService.ResetCheckoutData(customer, processPaymentRequest.StoreId, clearCouponCodes: true, clearCheckoutAttributes: true);

                        if (!processPaymentRequest.IsRecurringPayment)
                        {
                            _customerActivityService.InsertActivity(
                                "PublicStore.PlaceOrder",
                                _localizationService.GetResource("ActivityLog.PublicStore.PlaceOrder"),
                                order.Id);
                        }

                        //uncomment this line to support transactions
                        //scope.Complete();

                        //raise event       
                        _eventPublisher.PublishOrderPlaced(order);

                        if (order.PaymentStatus == PaymentStatus.Paid)
                        {
                            ProcessOrderPaid(order);
                        }
                        #endregion
                    }
                }
                else
                {
                    foreach (var paymentError in processPaymentResult.Errors)
                        result.AddError(string.Format("Payment error: {0}", paymentError));
                }
            }
            catch (Exception exc)
            {
                _logger.Error(exc.Message, exc);
                result.AddError(exc.Message);
            }

            #region Process errors

            string error = "";
            for (int i = 0; i < result.Errors.Count; i++)
            {
                error += string.Format("Error {0}: {1}", i + 1, result.Errors[i]);
                if (i != result.Errors.Count - 1)
                    error += ". ";
            }
            if (!String.IsNullOrEmpty(error))
            {
                //log it
                string logError = string.Format("Error while placing order. {0}", error);
                _logger.Error(logError);
            }

            #endregion

            return result;
        }

        /// <summary>
        /// Deletes an order
        /// </summary>
        /// <param name="order">The order</param>
        public virtual void DeleteOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            //check whether the order wasn't cancelled before
            // if it already was cancelled, then there's no need to make the following adjustments
            //(such as reward poitns, inventory, recurring payments)
            //they already was done when cancelling the order
            if (order.OrderStatus != OrderStatus.Cancelled)
            {
                //reward points
                ReduceRewardPoints(order);

                //cancel recurring payments
                var recurringPayments = _orderService.SearchRecurringPayments(0, 0, order.Id, null, 0, int.MaxValue);
                foreach (var rp in recurringPayments)
                {
                    //use errors?
                    var errors = CancelRecurringPayment(rp);
                }

                //Adjust inventory
                foreach (var orderItem in order.OrderItems)
                    _productService.AdjustInventory(orderItem.Product, false, orderItem.Quantity, orderItem.AttributesXml);
            }

            //add a note
            order.OrderNotes.Add(new OrderNote()
            {
                Note = "Order has been deleted",
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);
            
            //now delete an order
            _orderService.DeleteOrder(order);
        }


        /// <summary>
        /// Process next recurring psayment
        /// </summary>
        /// <param name="recurringPayment">Recurring payment</param>
        public virtual void ProcessNextRecurringPayment(RecurringPayment recurringPayment)
        {
            if (recurringPayment == null)
                throw new ArgumentNullException("recurringPayment");
            try
            {
                if (!recurringPayment.IsActive)
                    throw new NopException("Recurring payment is not active");

                var initialOrder = recurringPayment.InitialOrder;
                if (initialOrder == null)
                    throw new NopException("Initial order could not be loaded");

                var customer = initialOrder.Customer;
                if (customer == null)
                    throw new NopException("Customer could not be loaded");

                var nextPaymentDate = recurringPayment.NextPaymentDate;
                if (!nextPaymentDate.HasValue)
                    throw new NopException("Next payment date could not be calculated");

                //payment info
                var paymentInfo = new ProcessPaymentRequest()
                {
                    StoreId = initialOrder.StoreId,
                    CustomerId = customer.Id,
                    OrderGuid = Guid.NewGuid(),
                    IsRecurringPayment = true,
                    InitialOrderId = initialOrder.Id,
                    RecurringCycleLength = recurringPayment.CycleLength,
                    RecurringCyclePeriod = recurringPayment.CyclePeriod,
                    RecurringTotalCycles = recurringPayment.TotalCycles,
                };

                //place a new order
                var result = this.PlaceOrder(customer.Id, paymentInfo);
                if (result.Success)
                {
                    if (result.PlacedOrder == null)
                        throw new NopException("Placed order could not be loaded");

                    var rph = new RecurringPaymentHistory()
                    {
                        RecurringPayment = recurringPayment,
                        CreatedOnUtc = DateTime.UtcNow,
                        OrderId = result.PlacedOrder.Id,
                    };
                    recurringPayment.RecurringPaymentHistory.Add(rph);
                    _orderService.UpdateRecurringPayment(recurringPayment);
                }
                else
                {
                    string error = "";
                    for (int i = 0; i < result.Errors.Count; i++)
                    {
                        error += string.Format("Error {0}: {1}", i, result.Errors[i]);
                        if (i != result.Errors.Count - 1)
                            error += ". ";
                    }
                    throw new NopException(error);
                }
            }
            catch (Exception exc)
            {
                _logger.Error(string.Format("Error while processing recurring order. {0}", exc.Message), exc);
                throw;
            }
        }

        /// <summary>
        /// Cancels a recurring payment
        /// </summary>
        /// <param name="recurringPayment">Recurring payment</param>
        public virtual IList<string> CancelRecurringPayment(RecurringPayment recurringPayment)
        {
            if (recurringPayment == null)
                throw new ArgumentNullException("recurringPayment");

            var initialOrder = recurringPayment.InitialOrder;
            if (initialOrder == null)
                return new List<string>() { "Initial order could not be loaded" };


            var request = new CancelRecurringPaymentRequest();
            CancelRecurringPaymentResult result = null;
            try
            {
                request.Order = initialOrder;
                result = _paymentService.CancelRecurringPayment(request);
                if (result.Success)
                {
                    //update recurring payment
                    recurringPayment.IsActive = false;
                    _orderService.UpdateRecurringPayment(recurringPayment);


                    //add a note
                    initialOrder.OrderNotes.Add(new OrderNote()
                    {
                        Note = "Recurring payment has been cancelled",
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow
                    });
                    _orderService.UpdateOrder(initialOrder);

                    //notify a store owner
                    _workflowMessageService
                        .SendRecurringPaymentCancelledStoreOwnerNotification(recurringPayment, 
                        _localizationSettings.DefaultAdminLanguageId);
                }
            }
            catch (Exception exc)
            {
                if (result == null)
                    result = new CancelRecurringPaymentResult();
                result.AddError(string.Format("Error: {0}. Full exception: {1}", exc.Message, exc.ToString()));
            }


            //process errors
            string error = "";
            for (int i = 0; i < result.Errors.Count; i++)
            {
                error += string.Format("Error {0}: {1}", i, result.Errors[i]);
                if (i != result.Errors.Count - 1)
                    error += ". ";
            }
            if (!String.IsNullOrEmpty(error))
            {
                //add a note
                initialOrder.OrderNotes.Add(new OrderNote()
                {
                    Note = string.Format("Unable to cancel recurring payment. {0}", error),
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });
                _orderService.UpdateOrder(initialOrder);

                //log it
                string logError = string.Format("Error cancelling recurring payment. Order #{0}. Error: {1}", initialOrder.Id, error);
                _logger.InsertLog(LogLevel.Error, logError, logError);
            }
            return result.Errors;
        }

        /// <summary>
        /// Gets a value indicating whether a customer can cancel recurring payment
        /// </summary>
        /// <param name="customerToValidate">Customer</param>
        /// <param name="recurringPayment">Recurring Payment</param>
        /// <returns>value indicating whether a customer can cancel recurring payment</returns>
        public virtual bool CanCancelRecurringPayment(Customer customerToValidate, RecurringPayment recurringPayment)
        {
            if (recurringPayment == null)
                return false;

            if (customerToValidate == null)
                return false;

            var initialOrder = recurringPayment.InitialOrder;
            if (initialOrder == null)
                return false;

            var customer = recurringPayment.InitialOrder.Customer;
            if (customer == null)
                return false;

            if (initialOrder.OrderStatus == OrderStatus.Cancelled)
                return false;

            if (!customerToValidate.IsAdmin())
            {
                if (customer.Id != customerToValidate.Id)
                    return false;
            }

            if (!recurringPayment.NextPaymentDate.HasValue)
                return false;

            return true;
        }


        /// <summary>
        /// Set a Ready To Ship shipment
        /// </summary>
        /// <param name="shipment">Shipment</param>
        /// <param name="notifyCustomer">True to notify customer</param>
        public virtual void ReadyToShip(Shipment shipment, bool notifyCustomer)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            var order = _orderService.GetOrderById(shipment.OrderId);
            if (order == null)
                throw new Exception("Order cannot be loaded");

            if (shipment.ReadyToBeShippedDateUtc.HasValue)
                throw new Exception("This shipment is already ready to be shipped");

            shipment.ReadyToBeShippedDateUtc = DateTime.UtcNow;
            shipment.BookingNumber = ReserveBookingNumber(shipment);
            shipment.TrackingNumber = GetAWBNumber(shipment.BookingNumber);
            if (!string.IsNullOrEmpty(shipment.BookingNumber))
            {
                _shipmentService.UpdateShipment(shipment);
              
                //check whether we have more items to set ready to ship and the shipping status is not partially shipped
                if ((order.HasItemsToAddToShipment() || order.HasItemsToSetReadyToShip()) && order.ShippingStatusId != (int)ShippingStatus.PartiallyShipped)
                    order.ShippingStatusId = (int)ShippingStatus.PartiallyReadyToBeShipped;
                //the shipping status is not partially shipped
                else if (order.ShippingStatusId != (int)ShippingStatus.PartiallyShipped)
                    order.ShippingStatusId = (int)ShippingStatus.ReadyToBeShipped;
                _orderService.UpdateOrder(order);

                //add a note
                order.OrderNotes.Add(new OrderNote()
                {
                    Note = string.Format("Shipment# {0} is ready to be shipped", shipment.Id),
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });
                _orderService.UpdateOrder(order);

                //event
                _eventPublisher.PublishShipmentReady(shipment);

                //check order status
                CheckOrderStatus(order);
            }
            else
            {
                throw new Exception("Can't genrate booking number for this Shipment");
            }

        }

        /// <summary>
        /// Reserve Booking Number From EPGServer
        /// </summary>
        /// <param name="shipment"></param>
        /// <returns></returns>
        public virtual string ReserveBookingNumber(Shipment shipment)
        {
            IntegrationService integrationService = new IntegrationService();
            string bookingNumber = string.Empty;
            var booking = new BookingVM();
            booking.contractID = -1;
            booking.senderSectionID = -1;
            booking.contractCustomerID = -1;
            booking.senderContractNumber = "-1";
            booking.receiverContractNumber = "-1";
            booking.sender_customer_id = "-1";
            booking.receiver_customer_id = "-1";
            booking.userID = "-1";

            booking.shipmentTypeID = integrationService.GetshipmentTypeIdByType(shipment.Address.IsDomestic.HasValue ? shipment.Address.IsDomestic.Value : false);
            booking.originID = 1;
            if (shipment.Address.IsDomestic.HasValue && shipment.Address.IsDomestic.Value)
            {
                booking.desinationID = Convert.ToInt32(decimal.Parse(shipment.Address.StateName));  
            }
            else
            {
                booking.desinationID = Convert.ToInt32(decimal.Parse(shipment.Address.CountryName));
            }
            booking.serviceTypeID = string.IsNullOrEmpty(shipment.ShippingMethodID) ? 0 :int.Parse(shipment.ShippingMethodID);//44; //integrationService.GetServiceTypeIdByType(shipment.Address.IsDomestic.HasValue ? shipment.Address.IsDomestic.Value : false );//44;
            booking.contentTypeID = 198;
           
            decimal totalLength =0 , totalWidth =0, totalheight = 0;
            StringBuilder contentDescription = new StringBuilder();
            int count = 0;
            int numbetofItems =0 ;
            foreach (var item in shipment.ShipmentItems)
            {
                var product = _orderService.GetOrderItemById(item.OrderItemId).Product;
                totalLength = totalLength + product.Length;
                totalWidth = totalWidth + product.Width;
                totalheight = totalheight + product.Height;
                contentDescription.Append(product.Name);
                contentDescription.Append(" - ");
                contentDescription.Append(product.ShortDescription);
                if (count > 0 && count < shipment.ShipmentItems.Count())
                    contentDescription.Append(" | ");
                count = count + 1;
                numbetofItems = numbetofItems + item.Quantity ;
            }
            booking.length = Convert.ToInt32(totalLength);
            booking.width = Convert.ToInt32(totalWidth);
            booking.height =Convert.ToInt32(totalheight);
            booking.numberOfItems = numbetofItems;
            booking.dimenstionUnit = 1;
            booking.totalWeight = shipment.TotalWeight.HasValue ? double.Parse(shipment.TotalWeight.Value.ToString()) : 0;
            booking.weightUnit = 0;
            booking.customsValue = 0.00;
            booking.customsCurrencyID = "AED";
            booking.contentDescription = contentDescription.ToString().Replace("'","");

            booking.pickupAddress = new AddressVM
            {
                addresseeName = _localizationService.GetResource("pickupAddress.addresseeName"),
                addressLine1 = _localizationService.GetResource("pickupAddress.addressLine1"),
                addressLine2 = _localizationService.GetResource("pickupAddress.addressLine2"),
                addressLine3 = _localizationService.GetResource("pickupAddress.addressLine3"),
                cityID = int.Parse(_localizationService.GetResource("pickupAddress.cityID")),
                companyName = _localizationService.GetResource("pickupAddress.companyName"),
                phone = _localizationService.GetResource("pickupAddress.phone")
            };
            booking.deliveryAddress = new AddressVM
            {
                addresseeName = shipment.Address.FirstName + " " + shipment.Address.LastName,
                addressLine1 = string.IsNullOrEmpty(shipment.Address.Address1) ? "" : shipment.Address.Address1,
                addressLine2 = string.IsNullOrEmpty(shipment.Address.Address2) ? "a" : shipment.Address.Address2,
                addressLine3 = "b",
                cityID = (shipment.Address.IsDomestic.HasValue && shipment.Address.IsDomestic.Value) ? Convert.ToInt32(decimal.Parse(shipment.Address.StateName)) : 0,
                countryID = Convert.ToInt32(decimal.Parse(shipment.Address.CountryName)).ToString(),
                companyName = string.IsNullOrEmpty(shipment.Address.Company) ? "" : shipment.Address.Company,
                phone = string.IsNullOrEmpty(shipment.Address.PhoneNumber) ? "" : shipment.Address.PhoneNumber
            };
            if (!string.IsNullOrEmpty(shipment.Order.CheckoutAttributeDescription))
            {
                booking.preferredDate = DateTime.Parse(shipment.Order.CheckoutAttributeDescription.Split(':')[1].Trim()).ToString("dd/MM/yyyy");
                booking.earliestHour = 5;
                booking.earliestMinute = 30;
                booking.latestHour = 5;
                booking.latestMinute = 30;
            }
            booking.shippingInstructions = "Order Number: " + shipment.OrderId;
            booking.payMethodID = 2;
            booking.callerContact = string.IsNullOrEmpty(shipment.Address.FirstName) ? "" : shipment.Address.FirstName;
            booking.callerContactPhone = string.IsNullOrEmpty(shipment.Address.PhoneNumber) ? "" : shipment.Address.PhoneNumber;
           

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["ReserveBookingNumberURL"]);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "Post";
            byte[] bytedata = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(booking));
            httpWebRequest.ContentLength = bytedata.Length;
            Stream requestStream = httpWebRequest.GetRequestStream();
            requestStream.Write(bytedata, 0, bytedata.Length);
            requestStream.Close();
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            string result = string.Empty;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                bookingNumber = streamReader.ReadToEnd().ToString().Replace("\"","");
            }
            return bookingNumber;
        }

        /// <summary>
        /// Get AWB Number by booking number from EPG Service
        /// </summary>
        /// <param name="bookingNumber"></param>
        /// <returns></returns>
        public virtual string GetAWBNumber(string bookingNumber)
        {
           string awbNumber = string.Empty;
            Dictionary<string, string> getVariables = new Dictionary<string, string>();
            getVariables.Add("bookingNumber", bookingNumber);
           string resultawbNumber = CommonHelper.HttpGetRequest(System.Configuration.ConfigurationManager.AppSettings["GetAWBNumberURL"], getVariables);
           if (string.IsNullOrEmpty(resultawbNumber) ||  resultawbNumber == "null")
            {
                CommonHelper.HttpGetRequest(string.Format(System.Configuration.ConfigurationManager.AppSettings["DownLoadBookingPDF"], bookingNumber), new Dictionary<string, string>());
                resultawbNumber = CommonHelper.HttpGetRequest(System.Configuration.ConfigurationManager.AppSettings["GetAWBNumberURL"], getVariables);
            }
           awbNumber = resultawbNumber.Replace("[\"","").Replace("\"]","");
            return awbNumber;
        }

        /// <summary>
        /// Send a shipment
        /// </summary>
        /// <param name="shipment">Shipment</param>
        /// <param name="notifyCustomer">True to notify customer</param>
        public virtual void Ship(Shipment shipment, bool notifyCustomer)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            var order = _orderService.GetOrderById(shipment.OrderId);
            if (order == null)
                throw new Exception("Order cannot be loaded");

            if (shipment.ShippedDateUtc.HasValue)
                throw new Exception("This shipment is already shipped");

            shipment.ShippedDateUtc = DateTime.UtcNow;
            _shipmentService.UpdateShipment(shipment);

            //check whether we have more items to ship
            if (order.HasItemsToAddToShipment() || order.HasItemsToSetReadyToShip() || order.HasItemsToShip())
                order.ShippingStatusId = (int)ShippingStatus.PartiallyShipped;
            else
                order.ShippingStatusId = (int)ShippingStatus.Shipped;
            _orderService.UpdateOrder(order);

            //add a note
            order.OrderNotes.Add(new OrderNote()
                {
                    Note = string.Format("Shipment# {0} has been sent", shipment.Id),
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });
            _orderService.UpdateOrder(order);

            if (notifyCustomer)
            {
                //notify customer
                int queuedEmailId = _workflowMessageService.SendShipmentSentCustomerNotification(shipment, order.CustomerLanguageId);
                if (queuedEmailId > 0)
                {
                    order.OrderNotes.Add(new OrderNote()
                    {
                        Note = string.Format("\"Shipped\" email (to customer) has been queued. Queued email identifier: {0}.", queuedEmailId),
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow
                    });
                    _orderService.UpdateOrder(order);
                }
            }

            //event
            _eventPublisher.PublishShipmentSent(shipment);

            //check order status
            CheckOrderStatus(order);
        }

        /// <summary>
        /// Send a shipment by a date
        /// </summary>
        /// <param name="shipment">Shipment</param>
        /// <param name="shipDate">shipping date</param>
        /// <param name="notifyCustomer">True to notify customer</param>
        public virtual void Ship(Shipment shipment, DateTime shipDate, bool notifyCustomer)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            var order = _orderService.GetOrderById(shipment.OrderId);
            if (order == null)
                throw new Exception("Order cannot be loaded");

            if (shipment.ShippedDateUtc.HasValue)
                throw new Exception("This shipment is already shipped");

            shipment.ShippedDateUtc = shipDate;
            _shipmentService.UpdateShipment(shipment);

            //check whether we have more items to ship
            if (order.HasItemsToAddToShipment() || order.HasItemsToSetReadyToShip() || order.HasItemsToShip())
                order.ShippingStatusId = (int)ShippingStatus.PartiallyShipped;
            else
                order.ShippingStatusId = (int)ShippingStatus.Shipped;
            _orderService.UpdateOrder(order);

            //add a note
            order.OrderNotes.Add(new OrderNote()
            {
                Note = string.Format("Shipment# {0} has been sent", shipment.Id),
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);

            if (notifyCustomer)
            {
                //notify customer
                int queuedEmailId = _workflowMessageService.SendShipmentSentCustomerNotification(shipment, order.CustomerLanguageId);
                if (queuedEmailId > 0)
                {
                    order.OrderNotes.Add(new OrderNote()
                    {
                        Note = string.Format("\"Shipped\" email (to customer) has been queued. Queued email identifier: {0}.", queuedEmailId),
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow
                    });
                    _orderService.UpdateOrder(order);
                }
            }

            //event
            _eventPublisher.PublishShipmentSent(shipment);

            //check order status
            CheckOrderStatus(order);
        }

        /// <summary>
        /// Marks a shipment as delivered
        /// </summary>
        /// <param name="shipment">Shipment</param>
        /// <param name="notifyCustomer">True to notify customer</param>
        public virtual void Deliver(Shipment shipment, bool notifyCustomer)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            var order = shipment.Order;
            if (order == null)
                throw new Exception("Order cannot be loaded");

            if (!shipment.ShippedDateUtc.HasValue)
                throw new Exception("This shipment is not shipped yet");

            if (shipment.DeliveryDateUtc.HasValue)
                throw new Exception("This shipment is already delivered");

            shipment.DeliveryDateUtc = DateTime.UtcNow;
            _shipmentService.UpdateShipment(shipment);

            if (!order.HasItemsToAddToShipment() && !order.HasItemsToShip() && !order.HasItemsToDeliver())
                order.ShippingStatusId = (int)ShippingStatus.Delivered;
            _orderService.UpdateOrder(order);

            //add a note
            order.OrderNotes.Add(new OrderNote()
            {
                Note = string.Format("Shipment# {0} has been delivered", shipment.Id),
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);

            if (notifyCustomer)
            {
                //send email notification
                int queuedEmailId = _workflowMessageService.SendShipmentDeliveredCustomerNotification(shipment, order.CustomerLanguageId);
                if (queuedEmailId > 0)
                {
                    order.OrderNotes.Add(new OrderNote()
                    {
                        Note = string.Format("\"Delivered\" email (to customer) has been queued. Queued email identifier: {0}.", queuedEmailId),
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow
                    });
                    _orderService.UpdateOrder(order);
                }
            }

            //event
            _eventPublisher.PublishShipmentDelivered(shipment);

            //check order status
            CheckOrderStatus(order);
        }

        /// <summary>
        /// Marks a shipment as delivered by a date
        /// </summary>
        /// <param name="shipment">Shipment</param>
        /// <param name="deliverDate">deliver date</param>
        /// <param name="notifyCustomer">True to notify customer</param>
        public virtual void Deliver(Shipment shipment, DateTime deliverDate, bool notifyCustomer)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            var order = shipment.Order;
            if (order == null)
                throw new Exception("Order cannot be loaded");

            if (!shipment.ShippedDateUtc.HasValue)
                throw new Exception("This shipment is not shipped yet");

            if (shipment.DeliveryDateUtc.HasValue)
                throw new Exception("This shipment is already delivered");

            shipment.DeliveryDateUtc = deliverDate;
            _shipmentService.UpdateShipment(shipment);

            if (!order.HasItemsToAddToShipment() && !order.HasItemsToShip() && !order.HasItemsToDeliver())
                order.ShippingStatusId = (int)ShippingStatus.Delivered;
            _orderService.UpdateOrder(order);

            //add a note
            order.OrderNotes.Add(new OrderNote()
            {
                Note = string.Format("Shipment# {0} has been delivered", shipment.Id),
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);

            if (notifyCustomer)
            {
                //send email notification
                int queuedEmailId = _workflowMessageService.SendShipmentDeliveredCustomerNotification(shipment, order.CustomerLanguageId);
                if (queuedEmailId > 0)
                {
                    order.OrderNotes.Add(new OrderNote()
                    {
                        Note = string.Format("\"Delivered\" email (to customer) has been queued. Queued email identifier: {0}.", queuedEmailId),
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow
                    });
                    _orderService.UpdateOrder(order);
                }
            }

            //event
            _eventPublisher.PublishShipmentDelivered(shipment);

            //check order status
            CheckOrderStatus(order);
        }



        /// <summary>
        /// Gets a value indicating whether cancel is allowed
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>A value indicating whether cancel is allowed</returns>
        public virtual bool CanCancelOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderStatus == OrderStatus.Cancelled)
                return false;

            return true;
        }

        /// <summary>
        /// Cancels order
        /// </summary>
        /// <param name="order">Order</param>
        /// <param name="notifyCustomer">True to notify customer</param>
        public virtual void CancelOrder(Order order, bool notifyCustomer)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (!CanCancelOrder(order))
                throw new NopException("Cannot do cancel for order.");

            //Cancel order
            SetOrderStatus(order, OrderStatus.Cancelled, notifyCustomer);

            //add a note
            order.OrderNotes.Add(new OrderNote()
            {
                Note = "Order has been cancelled",
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);

            //cancel recurring payments
            var recurringPayments = _orderService.SearchRecurringPayments(0, 0, order.Id, null, 0, int.MaxValue);
            foreach (var rp in recurringPayments)
            {
                //use errors?
                var errors = CancelRecurringPayment(rp);
            }

            //Adjust inventory
            foreach (var orderItem in order.OrderItems)
                _productService.AdjustInventory(orderItem.Product, false, orderItem.Quantity, orderItem.AttributesXml);

            _eventPublisher.PublishOrderCancelled(order);

        }

        /// <summary>
        /// Gets a value indicating whether order can be marked as authorized
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>A value indicating whether order can be marked as authorized</returns>
        public virtual bool CanMarkOrderAsAuthorized(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderStatus == OrderStatus.Cancelled)
                return false;

            if (order.PaymentStatus == PaymentStatus.Pending)
                return true;

            return false;
        }

        /// <summary>
        /// Marks order as authorized
        /// </summary>
        /// <param name="order">Order</param>
        public virtual void MarkAsAuthorized(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            order.PaymentStatusId = (int)PaymentStatus.Authorized;
            _orderService.UpdateOrder(order);

            //add a note
            order.OrderNotes.Add(new OrderNote()
            {
                Note = "Order has been marked as authorized",
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);

            //check order status
            CheckOrderStatus(order);
        }



        /// <summary>
        /// Gets a value indicating whether capture from admin panel is allowed
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>A value indicating whether capture from admin panel is allowed</returns>
        public virtual bool CanCapture(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderStatus == OrderStatus.Cancelled ||
                order.OrderStatus == OrderStatus.Pending)
                return false;

            if (order.PaymentStatus == PaymentStatus.Authorized &&
                _paymentService.SupportCapture(order.PaymentMethodSystemName))
                return true;

            return false;
        }

        /// <summary>
        /// Capture an order (from admin panel)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>A list of errors; empty list if no errors</returns>
        public virtual IList<string> Capture(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (!CanCapture(order))
                throw new NopException("Cannot do capture for order.");

            var request = new CapturePaymentRequest();
            CapturePaymentResult result = null;
            try
            {
                //old info from placing order
                request.Order = order;
                result = _paymentService.Capture(request);

                if (result.Success)
                {
                    var paidDate = order.PaidDateUtc;
                    if (result.NewPaymentStatus == PaymentStatus.Paid)
                        paidDate = DateTime.UtcNow;

                    order.CaptureTransactionId = result.CaptureTransactionId;
                    order.CaptureTransactionResult = result.CaptureTransactionResult;
                    order.PaymentStatus = result.NewPaymentStatus;
                    order.PaidDateUtc = paidDate;
                    _orderService.UpdateOrder(order);

                    //add a note
                    order.OrderNotes.Add(new OrderNote()
                    {
                        Note = "Order has been captured",
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow
                    });
                    _orderService.UpdateOrder(order);

                    CheckOrderStatus(order);
     
                    if (order.PaymentStatus == PaymentStatus.Paid)
                    {
                        ProcessOrderPaid(order);
                    }
                }
            }
            catch (Exception exc)
            {
                if (result == null)
                    result = new CapturePaymentResult();
                result.AddError(string.Format("Error: {0}. Full exception: {1}", exc.Message, exc.ToString()));
            }


            //process errors
            string error = "";
            for (int i = 0; i < result.Errors.Count; i++)
            {
                error += string.Format("Error {0}: {1}", i, result.Errors[i]);
                if (i != result.Errors.Count - 1)
                    error += ". ";
            }
            if (!String.IsNullOrEmpty(error))
            {
                //add a note
                order.OrderNotes.Add(new OrderNote()
                {
                    Note = string.Format("Unable to capture order. {0}", error),
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });
                _orderService.UpdateOrder(order);

                //log it
                string logError = string.Format("Error capturing order #{0}. Error: {1}", order.Id, error);
                _logger.InsertLog(LogLevel.Error, logError, logError);
            }
            return result.Errors;
        }

        /// <summary>
        /// Gets a value indicating whether order can be marked as paid
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>A value indicating whether order can be marked as paid</returns>
        public virtual bool CanMarkOrderAsPaid(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderStatus == OrderStatus.Cancelled)
                return false;

            if (order.PaymentStatus == PaymentStatus.Paid ||
                order.PaymentStatus == PaymentStatus.Refunded ||
                order.PaymentStatus == PaymentStatus.Voided)
                return false;

            return true;
        }

        /// <summary>
        /// Marks order as paid
        /// </summary>
        /// <param name="order">Order</param>
        public virtual void MarkOrderAsPaid(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (!CanMarkOrderAsPaid(order))
                throw new NopException("You can't mark this order as paid");

            order.PaymentStatusId = (int)PaymentStatus.Paid;
            order.PaidDateUtc = DateTime.UtcNow;
            _orderService.UpdateOrder(order);

            //add a note
            order.OrderNotes.Add(new OrderNote()
            {
                Note = "Order has been marked as paid",
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);

            CheckOrderStatus(order);
   
            if (order.PaymentStatus == PaymentStatus.Paid)
            {
                ProcessOrderPaid(order);
            }
        }



        /// <summary>
        /// Gets a value indicating whether refund from admin panel is allowed
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>A value indicating whether refund from admin panel is allowed</returns>
        public virtual bool CanRefund(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderTotal == decimal.Zero)
                return false;

            //uncomment the lines below in order to allow this operation for cancelled orders
            //if (order.OrderStatus == OrderStatus.Cancelled)
            //    return false;

            if (order.PaymentStatus == PaymentStatus.Paid &&
                _paymentService.SupportRefund(order.PaymentMethodSystemName))
                return true;

            return false;
        }
        
        /// <summary>
        /// Refunds an order (from admin panel)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>A list of errors; empty list if no errors</returns>
        public virtual IList<string> Refund(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (!CanRefund(order))
                throw new NopException("Cannot do refund for order.");

            var request = new RefundPaymentRequest();
            RefundPaymentResult result = null;
            try
            {
                request.Order = order;
                request.AmountToRefund = order.OrderTotal;
                request.IsPartialRefund = false;
                result = _paymentService.Refund(request);
                if (result.Success)
                {
                    //total amount refunded
                    decimal totalAmountRefunded = order.RefundedAmount + request.AmountToRefund;

                    //update order info
                    order.RefundedAmount = totalAmountRefunded;
                    order.PaymentStatus = result.NewPaymentStatus;
                    _orderService.UpdateOrder(order);

                    //add a note
                    order.OrderNotes.Add(new OrderNote()
                    {
                        Note = string.Format("Order has been refunded. Amount = {0}", _priceFormatter.FormatPrice(request.AmountToRefund, true, false)),
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow
                    });
                    _orderService.UpdateOrder(order);

                    //check order status
                    CheckOrderStatus(order);
                }

            }
            catch (Exception exc)
            {
                if (result == null)
                    result = new RefundPaymentResult();
                result.AddError(string.Format("Error: {0}. Full exception: {1}", exc.Message, exc.ToString()));
            }

            //process errors
            string error = "";
            for (int i = 0; i < result.Errors.Count; i++)
            {
                error += string.Format("Error {0}: {1}", i, result.Errors[i]);
                if (i != result.Errors.Count - 1)
                    error += ". ";
            }
            if (!String.IsNullOrEmpty(error))
            {
                //add a note
                order.OrderNotes.Add(new OrderNote()
                {
                    Note = string.Format("Unable to refund order. {0}", error),
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });
                _orderService.UpdateOrder(order);

                //log it
                string logError = string.Format("Error refunding order #{0}. Error: {1}", order.Id, error);
                _logger.InsertLog(LogLevel.Error, logError, logError);
            }
            return result.Errors;
        }

        /// <summary>
        /// Gets a value indicating whether order can be marked as refunded
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>A value indicating whether order can be marked as refunded</returns>
        public virtual bool CanRefundOffline(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderTotal == decimal.Zero)
                return false;

            //uncomment the lines below in order to allow this operation for cancelled orders
            //if (order.OrderStatus == OrderStatus.Cancelled)
            //     return false;

            if (order.PaymentStatus == PaymentStatus.Paid)
                return true;

            return false;
        }

        /// <summary>
        /// Refunds an order (offline)
        /// </summary>
        /// <param name="order">Order</param>
        public virtual void RefundOffline(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (!CanRefundOffline(order))
                throw new NopException("You can't refund this order");

            //amout to refund
            decimal amountToRefund = order.OrderTotal;

            //total amount refunded
            decimal totalAmountRefunded = order.RefundedAmount + amountToRefund;

            //update order info
            order.RefundedAmount = totalAmountRefunded;
            order.PaymentStatus = PaymentStatus.Refunded;
            _orderService.UpdateOrder(order);

            //add a note
            order.OrderNotes.Add(new OrderNote()
            {
                Note = string.Format("Order has been marked as refunded. Amount = {0}", _priceFormatter.FormatPrice(amountToRefund, true, false)),
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);

            //check order status
            CheckOrderStatus(order);
        }

        /// <summary>
        /// Gets a value indicating whether partial refund from admin panel is allowed
        /// </summary>
        /// <param name="order">Order</param>
        /// <param name="amountToRefund">Amount to refund</param>
        /// <returns>A value indicating whether refund from admin panel is allowed</returns>
        public virtual bool CanPartiallyRefund(Order order, decimal amountToRefund)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderTotal == decimal.Zero)
                return false;

            //uncomment the lines below in order to allow this operation for cancelled orders
            //if (order.OrderStatus == OrderStatus.Cancelled)
            //    return false;

            decimal canBeRefunded = order.OrderTotal - order.RefundedAmount;
            if (canBeRefunded <= decimal.Zero)
                return false;

            if (amountToRefund > canBeRefunded)
                return false;

            if ((order.PaymentStatus == PaymentStatus.Paid ||
                order.PaymentStatus == PaymentStatus.PartiallyRefunded) &&
                _paymentService.SupportPartiallyRefund(order.PaymentMethodSystemName))
                return true;

            return false;
        }

        /// <summary>
        /// Partially refunds an order (from admin panel)
        /// </summary>
        /// <param name="order">Order</param>
        /// <param name="amountToRefund">Amount to refund</param>
        /// <returns>A list of errors; empty list if no errors</returns>
        public virtual IList<string> PartiallyRefund(Order order, decimal amountToRefund)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (!CanPartiallyRefund(order, amountToRefund))
                throw new NopException("Cannot do partial refund for order.");

            var request = new RefundPaymentRequest();
            RefundPaymentResult result = null;
            try
            {
                request.Order = order;
                request.AmountToRefund = amountToRefund;
                request.IsPartialRefund = true;

                result = _paymentService.Refund(request);

                if (result.Success)
                {
                    //total amount refunded
                    decimal totalAmountRefunded = order.RefundedAmount + amountToRefund;

                    //update order info
                    order.RefundedAmount = totalAmountRefunded;
                    order.PaymentStatus = result.NewPaymentStatus;
                    _orderService.UpdateOrder(order);


                    //add a note
                    order.OrderNotes.Add(new OrderNote()
                    {
                        Note = string.Format("Order has been partially refunded. Amount = {0}", _priceFormatter.FormatPrice(amountToRefund, true, false)),
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow
                    });
                    _orderService.UpdateOrder(order);

                    //check order status
                    CheckOrderStatus(order);
                }
            }
            catch (Exception exc)
            {
                if (result == null)
                    result = new RefundPaymentResult();
                result.AddError(string.Format("Error: {0}. Full exception: {1}", exc.Message, exc.ToString()));
            }

            //process errors
            string error = "";
            for (int i = 0; i < result.Errors.Count; i++)
            {
                error += string.Format("Error {0}: {1}", i, result.Errors[i]);
                if (i != result.Errors.Count - 1)
                    error += ". ";
            }
            if (!String.IsNullOrEmpty(error))
            {
                //add a note
                order.OrderNotes.Add(new OrderNote()
                {
                    Note = string.Format("Unable to partially refund order. {0}", error),
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });
                _orderService.UpdateOrder(order);

                //log it
                string logError = string.Format("Error refunding order #{0}. Error: {1}", order.Id, error);
                _logger.InsertLog(LogLevel.Error, logError, logError);
            }
            return result.Errors;
        }

        /// <summary>
        /// Gets a value indicating whether order can be marked as partially refunded
        /// </summary>
        /// <param name="order">Order</param>
        /// <param name="amountToRefund">Amount to refund</param>
        /// <returns>A value indicating whether order can be marked as partially refunded</returns>
        public virtual bool CanPartiallyRefundOffline(Order order, decimal amountToRefund)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderTotal == decimal.Zero)
                return false;

            //uncomment the lines below in order to allow this operation for cancelled orders
            //if (order.OrderStatus == OrderStatus.Cancelled)
            //    return false;

            decimal canBeRefunded = order.OrderTotal - order.RefundedAmount;
            if (canBeRefunded <= decimal.Zero)
                return false;

            if (amountToRefund > canBeRefunded)
                return false;

            if (order.PaymentStatus == PaymentStatus.Paid ||
                order.PaymentStatus == PaymentStatus.PartiallyRefunded)
                return true;

            return false;
        }

        /// <summary>
        /// Partially refunds an order (offline)
        /// </summary>
        /// <param name="order">Order</param>
        /// <param name="amountToRefund">Amount to refund</param>
        public virtual void PartiallyRefundOffline(Order order, decimal amountToRefund)
        {
            if (order == null)
                throw new ArgumentNullException("order");
            
            if (!CanPartiallyRefundOffline(order, amountToRefund))
                throw new NopException("You can't partially refund (offline) this order");

            //total amount refunded
            decimal totalAmountRefunded = order.RefundedAmount + amountToRefund;

            //update order info
            order.RefundedAmount = totalAmountRefunded;
            //if (order.OrderTotal == totalAmountRefunded), then set order.PaymentStatus = PaymentStatus.Refunded;
            order.PaymentStatus = PaymentStatus.PartiallyRefunded;
            _orderService.UpdateOrder(order);

            //add a note
            order.OrderNotes.Add(new OrderNote()
            {
                Note = string.Format("Order has been marked as partially refunded. Amount = {0}", _priceFormatter.FormatPrice(amountToRefund, true, false)),
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);

            //check order status
            CheckOrderStatus(order);
        }



        /// <summary>
        /// Gets a value indicating whether void from admin panel is allowed
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>A value indicating whether void from admin panel is allowed</returns>
        public virtual bool CanVoid(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderTotal == decimal.Zero)
                return false;

            //uncomment the lines below in order to allow this operation for cancelled orders
            //if (order.OrderStatus == OrderStatus.Cancelled)
            //    return false;

            if (order.PaymentStatus == PaymentStatus.Authorized &&
                _paymentService.SupportVoid(order.PaymentMethodSystemName))
                return true;

            return false;
        }

        /// <summary>
        /// Voids order (from admin panel)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Voided order</returns>
        public virtual IList<string> Void(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (!CanVoid(order))
                throw new NopException("Cannot do void for order.");

            var request = new VoidPaymentRequest();
            VoidPaymentResult result = null;
            try
            {
                request.Order = order;
                result = _paymentService.Void(request);

                if (result.Success)
                {
                    //update order info
                    order.PaymentStatus = result.NewPaymentStatus;
                    _orderService.UpdateOrder(order);

                    //add a note
                    order.OrderNotes.Add(new OrderNote()
                    {
                        Note = "Order has been voided",
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow
                    });
                    _orderService.UpdateOrder(order);

                    //check order status
                    CheckOrderStatus(order);
                }
            }
            catch (Exception exc)
            {
                if (result == null)
                    result = new VoidPaymentResult();
                result.AddError(string.Format("Error: {0}. Full exception: {1}", exc.Message, exc.ToString()));
            }

            //process errors
            string error = "";
            for (int i = 0; i < result.Errors.Count; i++)
            {
                error += string.Format("Error {0}: {1}", i, result.Errors[i]);
                if (i != result.Errors.Count - 1)
                    error += ". ";
            }
            if (!String.IsNullOrEmpty(error))
            {
                //add a note
                order.OrderNotes.Add(new OrderNote()
                {
                    Note = string.Format("Unable to voiding order. {0}", error),
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });
                _orderService.UpdateOrder(order);

                //log it
                string logError = string.Format("Error voiding order #{0}. Error: {1}", order.Id, error);
                _logger.InsertLog(LogLevel.Error, logError, logError);
            }
            return result.Errors;
        }

        /// <summary>
        /// Gets a value indicating whether order can be marked as voided
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>A value indicating whether order can be marked as voided</returns>
        public virtual bool CanVoidOffline(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.OrderTotal == decimal.Zero)
                return false;

            //uncomment the lines below in order to allow this operation for cancelled orders
            //if (order.OrderStatus == OrderStatus.Cancelled)
            //    return false;

            if (order.PaymentStatus == PaymentStatus.Authorized)
                return true;

            return false;
        }

        /// <summary>
        /// Voids order (offline)
        /// </summary>
        /// <param name="order">Order</param>
        public virtual void VoidOffline(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (!CanVoidOffline(order))
                throw new NopException("You can't void this order");

            order.PaymentStatusId = (int)PaymentStatus.Voided;
            _orderService.UpdateOrder(order);

            //add a note
            order.OrderNotes.Add(new OrderNote()
            {
                Note = "Order has been marked as voided",
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);

            //check orer status
            CheckOrderStatus(order);
        }



        /// <summary>
        /// Place order items in current user shopping cart.
        /// </summary>
        /// <param name="order">The order</param>
        public virtual void ReOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            foreach (var orderItem in order.OrderItems)
            {
                var addresses = new List<ShoppingCartItemAddress>();
                //copy the Addresses of customized products with the items to shopping cart
                if (orderItem.Product.IsCustomizable)
                {                    
                    foreach (var orderItemAddress in orderItem.OrderItemShippingAddresses)
                    {
                        var shoppingCartItemAddress = new ShoppingCartItemAddress()
                        {
                            Address = orderItemAddress.Address,
                            Address_Id = orderItemAddress.AddressId,
                            Quantity = orderItemAddress.Quantity,
                        };
                        addresses.Add(shoppingCartItemAddress);
                    }
                }

                _shoppingCartService.AddToCart(orderItem.Order.Customer, orderItem.Product,
                    ShoppingCartType.ShoppingCart, orderItem.Order.StoreId, orderItem.AttributesXml,
                    orderItem.UnitPriceExclTax, orderItem.Quantity, false, addresses, orderItem.SendToManyAddressSelected);
            }
        }
        
        /// <summary>
        /// Check whether return request is allowed
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Result</returns>
        public virtual bool IsReturnRequestAllowed(Order order)
        {
            if (!_orderSettings.ReturnRequestsEnabled)
                return false;

            if (order == null || order.Deleted)
                return false;

            if (order.OrderStatus != OrderStatus.Complete)
                return false;

            bool numberOfDaysReturnRequestAvailableValid = false;
            if (_orderSettings.NumberOfDaysReturnRequestAvailable == 0)
            {
                numberOfDaysReturnRequestAvailableValid = true;
            }
            else
            {
                var daysPassed = (DateTime.UtcNow - order.CreatedOnUtc).TotalDays;
                numberOfDaysReturnRequestAvailableValid = (daysPassed - _orderSettings.NumberOfDaysReturnRequestAvailable) < 0;
            }
            return numberOfDaysReturnRequestAvailableValid;
        }
        


        /// <summary>
        /// Valdiate minimum order sub-total amount
        /// </summary>
        /// <param name="cart">Shopping cart</param>
        /// <returns>true - OK; false - minimum order sub-total amount is not reached</returns>
        public virtual bool ValidateMinOrderSubtotalAmount(IList<ShoppingCartItem> cart)
        {
            if (cart == null)
                throw new ArgumentNullException("cart");

            //min order amount sub-total validation
            if (cart.Count > 0 && _orderSettings.MinOrderSubtotalAmount > decimal.Zero)
            {
                //subtotal
                decimal orderSubTotalDiscountAmountBase = decimal.Zero;
                Discount orderSubTotalAppliedDiscount = null;
                decimal subTotalWithoutDiscountBase = decimal.Zero;
                decimal subTotalWithDiscountBase = decimal.Zero;
                _orderTotalCalculationService.GetShoppingCartSubTotal(cart, false,
                    out orderSubTotalDiscountAmountBase, out orderSubTotalAppliedDiscount,
                    out subTotalWithoutDiscountBase, out subTotalWithDiscountBase);

                if (subTotalWithoutDiscountBase < _orderSettings.MinOrderSubtotalAmount)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Valdiate minimum order total amount
        /// </summary>
        /// <param name="cart">Shopping cart</param>
        /// <returns>true - OK; false - minimum order total amount is not reached</returns>
        public virtual bool ValidateMinOrderTotalAmount(IList<ShoppingCartItem> cart)
        {
            if (cart == null)
                throw new ArgumentNullException("cart");

            if (cart.Count > 0 && _orderSettings.MinOrderTotalAmount > decimal.Zero)
            {
                decimal? shoppingCartTotalBase = _orderTotalCalculationService.GetShoppingCartTotal(cart);
                if (shoppingCartTotalBase.HasValue && shoppingCartTotalBase.Value < _orderSettings.MinOrderTotalAmount)
                    return false;
            }

            return true;
        }

        #endregion
    }
}
